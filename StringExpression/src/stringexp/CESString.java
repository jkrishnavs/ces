package stringexp;


import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import myvisitors.*;
import syntaxtree.Goal;

public class CESString {
	
	
	
	public static String getLeastSignificantArrayIndex(String s) {
		if(s.isEmpty())
			return null;
		Goal g = StringTest.parseString(s);
		GetLeastSignificantArrayIndex idx = new GetLeastSignificantArrayIndex();
		g.accept(idx);
		return idx.getArrayIndex();
	}
	
	
	public static boolean isSingleIdentifier(String s) {
//		System.err.println("CESSTRING singleidentifier "+ s);
		if(s.isEmpty())
			return false;
		Goal g = StringTest.parseString(s);
	
		LiteralCount l = new LiteralCount();
		g.accept(l);
		if(l.getVarCount() == 1)
			return true;
		else
			return false;
	}
	
	public static boolean hasArrayRead(String s){
		if(s.isEmpty())
			return false;
		Goal g = StringTest.parseString(s);
	
		HasArrayRead l = new HasArrayRead();
		g.accept(l);
		return l.hasArrayRead();
	} 
	
	
	
	
	
	public static String getTruthProbabilityEvaluated(String string, ArrayList<String> acceptedVars, String max) {
		if(string.isEmpty())
			return "0";
		Goal g = StringTest.parseString(string);
		
	
		
		
		EvaluateProbability prob = new EvaluateProbability();
		prob.setacceptedVariables(acceptedVars);
		prob.setDefaultConstant(max);
		String cval = g.accept(prob, "");
		
		if(cval ==  null || cval.equals(max) || cval.isEmpty())
			cval = EvaluateProbability.unknownProb;
		
		return cval;
	}
	
	
	
	public static int getDegreeofTerm(String s){
		if(s.isEmpty())
			return 0;
		Goal g = StringTest.parseString(s);
		DegreeofTerm dg = new DegreeofTerm();
		g.accept(dg);
		return dg.degree();
	}
	
	public static boolean findVariable(String s,String var){
		if(s.isEmpty())
			return false;
		Goal g = StringTest.parseString(s);
		FindVariable fv = new FindVariable();
		g.accept(fv, var);
		return fv.varaiableFound();
	}
	
	/*
	public static String mostSignificantTerm(String s){
		if(s.isEmpty())
			return new String();
		Goal g = StringTest.parseString(s);
		MostSignificantTerm mst = new MostSignificantTerm();
		g.accept(mst,"");
		return mst.getMostSignificantTerm();
	}
	
	
	public static String mostSignificantTermWIthVariable(String s, String var){
		if(s.isEmpty())
			return new String();
		Goal g = StringTest.parseString(s);
		LargestTermwithVariable ltv = new LargestTermwithVariable();
		ltv.setSearchVariable(var);
		g.accept(ltv,"");
		return ltv.getMostSignificatTermwithVariable();
	}
	
	
	
	/*
	public static String normalize(String s){
		if(s.isEmpty())
			return s;
		Goal g = StringTest.parseString(s);
		NormalizeVariables normalizer = new NormalizeVariables();
		g.accept(normalizer,"");
		return normalizer.getNormalizedStr();
	}
	
	*/
	
	public static String getCoefficient(String s, String var) {
		if(s.length() == 0)
			return "0";
		else{
			Goal g = StringTest.parseString(s);
			Reducer rd = new Reducer();
			ArrayList<Minterms> finalList = g.accept(rd, new ArrayList<Minterms>());
			Collections.reverse(finalList);
			for(Minterms f :finalList){
				String str = f.findCoefficent(var);
				if(!str.isEmpty())
					return str;
			}
		}
		return "0";
	}

	
	public static ArrayList<String> getVariableList(String s){
		if(s.length() == 0)
			return new ArrayList<String>();
		else{
			Goal g = StringTest.parseString(s);
			GetVariableList glt = new GetVariableList();
			g.accept(glt);
			return glt.variableList();
		}
		
		
	}
	
	
	public static ArrayList<String> getasTokens(String s){
		if(s.isEmpty())
			return new ArrayList<String>();
		Goal g = StringTest.parseString(s);
		GetasTokens glt = new GetasTokens();
		g.accept(glt);
		return glt.tokens();
		
	}
	
	/*
	public static ArrayList<String> getTokensofOuterExpression(String s){
		if(s.isEmpty())
			return new ArrayList<String>();
		Goal g = StringTest.parseString(s);
		GetOuterTokens glt = new GetOuterTokens();
		g.accept(glt);
		return glt.tokens();
		
	}
	
	
	/*
	public static ArrayList<String> getvarlistinTerm(String term){
		if(term.isEmpty())
			return new ArrayList<String>();
		Goal g = StringTest.parseString(term);
		GetLiteralTokens glt = new GetLiteralTokens();
		g.accept(glt);
		return glt.getVarTokens();
	}
	
	
	*/
	
	public static boolean isNegative(String term){
		if(term.isEmpty())
			return false;
		boolean retVal = false;
		for (int i = 0; i < term.length(); i++) {
			if(term.charAt(i) == '-')
				retVal ^= true;
		}
		return retVal;
	}
	
	
	public static ArrayList<Minterms> Reduce(String var) {
		errWriter   = new PrintWriter(System.err);
		ArrayList<Minterms> argu = new ArrayList<Minterms>();
		Goal g = StringTest.parseString(var);
		Reducer rd = new Reducer();
		ArrayList<Minterms> finalList = g.accept(rd, argu);
		if(finalList != null) {
			return finalList;
		}
		else 
			return argu;
	}
	
	public static PrintWriter errWriter = null;
	
	public static void addErrorMessage(String s){
		DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
		Date date = new Date();
		errWriter.println("ERROR "+  dateFormat.format(date) + ": "+ s);
		errWriter.flush();
	}
	

	

}
