package stringexp;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import syntaxtree.Goal;



public class StringTest {
	
	
	//static StringExpression sp = new StringExpression(null);
	// hello 1 2 3 4 
	public static void reductionTest() {
//		String s1 = "(   4.0 * n3  )  + (  7.0 * n1  )  + (  4.0 * n2  )  + (  3.0 * N_THREADS  )  + (  1.0 * m3  )  + (  -4.0 * n2 * n1  )  + (  1.0 * m3 * m1  )  + (  1.0 * m3 * m1 * m2  )  + (n3 - 1  ) - (1 ) * ( ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + N_THREADS + (n3 - 1  ) - (1 ) * ( (n3 - 1  ) - (1 ) * ( ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + N_THREADS";
//		String s1 = "(   4.0 * n3  )  + (  7.0 * n1  )  + (  4.0 * n2  )  + (  3.0 * N_THREADS  )  + (  1.0 * m3  )  + (  -4.0 * n2 * n1  )  + (  1.0 * m3 * m1  )  + (  1.0 * m3 * m1 * m2  )  + (n3 - 1  ) - (1 ) * ( ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + N_THREADS + (n3 - 1  ) - (1 ) * ( (n3 - 1  ) - (1 ) * ( ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + ( n2 - 1  - 1  )  * ( ( n1 - 1  - 1  )  * ( 2.0 ) + 1.0 ) + 1.0 ) + N_THREADS";
//		String s1 = "fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 )";
//		String s1 = "(dims [ 2 ] [ 0 ]  ) - (0 )"; 
		String s1 = "( this -> num_nodes () - 0 )";
		String s2 = "j_nom_24 < sz ";
//		String s2 = "15 * sizeof ( int ) - 4 * sizeof ( void * ) - sizeof ( size_t )" ;
		
		
		String s7 = "(  3650.0  +   26.0 * d[2]  +  +   264.0 * m  +   90.0 * n  +   6.0 * d[1]  +   6.0 * fftblock  +   26.0 * d[2] * fftblock  +  +   144.0 * m * li  +   90.0 * n * fftblock  +   6.0 * d[1] * fftblock  +   6.0 * fftblock * d[0]  +  +   72.0 * m * li * lk  +   72.0 * m * li * lk * ny ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) +  32  * ( 1.0 ) + 5.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) +  32  * ( 1.0 ) + 5.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) +  32  * ( 1.0 ) + 5.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) +  32  * ( 1.0 ) + 5.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + ( (d [ 0 ] - fftblock  + 1)  / fftblock  )  * ( ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + ( d [ 1 ]  )  * ( fftblock  * ( 1.0 ) + 1.0 ) + 9.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) + 4.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) + 4.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) + 4.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + 3  * (  32  * ( 1.0 ) +  32  * ( 1.0 ) + 4.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 ) + ( (d [ 1 ] - fftblock  + 1)  / fftblock  )  * ( fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + ( (m  + 1 - 1 )  / 2  )  * ( li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + li  * ( lk  * ( ny  * ( 1.0 ) + 1.0 ) + 2.0 ) + 11.0 ) + n  * ( fftblock  * ( 1.0 ) + 1.0 ) + fftblock  * ( ( d [ 0 ]  )  * ( 1.0 ) + 1.0 ) + 5.0 )";
		
		
///		String sk = "i < j";
//		String sk = "! a (c,c,d)";
//		String sk = "sk != bk && dk != bk ";
//		String sk = " G_member [ j ] != num "; 
		
		ArrayList<String> accepted = new ArrayList<String>();
		accepted.add("i");
		ArrayList<Minterms> terms =  CESString.Reduce(s7);
		String evaluated = CESString.getTruthProbabilityEvaluated(s2, accepted, "G.num_nodes()");
		System.out.println(evaluated);
		String s = ""; 
		ArrayList<Minterms> minterms =  CESString.Reduce(s1);
		boolean plus = false;
		
		for (Minterms m : minterms) {
			String minterString =  m.getString();
			if(minterString.isEmpty() )
				continue;
			if(plus) {
				
				s += " + ("+  m.getString() + " ) ";
			} else {
				if(minterms.size() == 1) 
					s = m.getString();
				else
					s = " ( " + m.getString() + " ) ";
				plus = true;		 
			}
		}
			 
		System.err.println("The reduced term is "+ s);
	}
	
	
	
	public static void fullTest(){
		String s1 = "lastrow - firstrow + 1";
		if(CESString.isSingleIdentifier(s1)){
			System.err.println("Single Identifier failed");
		}
		String s2 = "NA + 1 ";
		if(! CESString.isSingleIdentifier(s2)){
			System.err.println("Single Identifier failed");
		}
		String s3  = "x<x<x<x";
		
		if(CESString.getDegreeofTerm(s3) != 4){
			System.err.println("Degree failed");
		}
		
		String s4  = "5 * x + 7* x - 3*x";
		 String s = ""; 
		ArrayList<Minterms> minterms =  CESString.Reduce(s4);
		 for (Minterms m : minterms) {
				s += m.getString();
		 }
		 
		 
		 
		 
		System.err.println("The reduced term is "+ s + " value " );

		if(CESString.findVariable(s4, "y")){
			System.err.println("Error in finding variable");
		}
		
		if(CESString.findVariable(s4, "x")){
			System.err.println("Error in finding variable");
		}
		
		 String s5 = " (3 * x ) - (2 * x) + 1 ";
		 minterms =  CESString.Reduce(s5);
		 s = "";
		 boolean plus = false;
		  
		 for (Minterms m : minterms) {
			 if(plus) {
				s += " + ("+  m.getString() + " ) ";
			 } else {
				 if(minterms.size() == 1) 
					 s = m.getString();
				 else
					 s = " ( " + m.getString() + " ) ";
				 plus = true;		 
			 }
		 }
		 
		System.err.println("The reduced term is "+ s + " value " );
		
		
		 String s6 = " (grid_points [ 0 ] - 1  ) - (1 )";
		 minterms =  CESString.Reduce(s6);
		 s = "";
		 plus = false;
		  
		 for (Minterms m : minterms) {
			 if(plus) {
				s += " + ("+  m.getString() + " ) ";
			 } else {
				 if(minterms.size() == 1) 
					 s = m.getString();
				 else
					 s = " ( " + m.getString() + " ) ";
				 plus = true;		 
			 }
		 }
		 
		System.err.println("The reduced term is "+ s + " value " );
	
		
		return;
	}
	
	public static void reduceTest() {
		String s = "NTHREADS";
		ArrayList<Minterms> min = CESString.Reduce(s);
		
	}
	
	public static Goal parseString(String s){
		InputStream is = new ByteArrayInputStream( s.getBytes() );
		//StringExpression.ReInit(is);
		StringExpression sp = new StringExpression(is);
		Goal exp = null;
		try {
			exp = sp.Goal();
		} catch (Exception e) {
			return null;
		}
		return exp;
	}
	public static void main(String [] args) {
		CESString.errWriter = new PrintWriter(System.err);
		// fullTest();
		reductionTest();
	}
	
}
