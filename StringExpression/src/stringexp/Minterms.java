package stringexp;

import java.util.ArrayList;

public class Minterms {
	double constant;
	
	ArrayList<String> varList;
	
	/*
	 * All variable in this varlist has degree -1.
	 * 
	 */
	ArrayList<String> invVarList;
	
	
	public Minterms() {
		constant = 0;
		varList = new ArrayList<String>();
		invVarList = new ArrayList<String>();
	}
	
	public Minterms(double s) {
		constant = s;
		varList = new ArrayList<String>();
		invVarList = new ArrayList<String>();
	}
	
	public Minterms(String s) {
		constant = 1;
		varList = new ArrayList<String>();
		invVarList = new ArrayList<String>();
		varList.add(s);
	}
	
	public void addVarList(ArrayList<String> v) {
		varList.addAll(v);
	}
	

	public void addInvVarList(ArrayList<String> v) {
		invVarList.addAll(v);
	}
	
	public ArrayList<String> getVarList() {
		return varList;
	}
	
	public ArrayList<String> getInvVarList() {
		return invVarList;
	}
	
	public int getDegree() {
		return varList.size();
	}
	
	public boolean isConstant() {
		return varList.isEmpty();
	}
	
	public double getConstant() {
		return constant;
	}
	
	public void addConstant(double d) {
		constant +=d;
	}
	
	public void multiply(double d){
		constant = constant * d;
	}
	
	public void multiply(String v){
		int i=0;
		for (String var : varList) {
			if(var.compareTo(v) > 0 )
				break;
		}
		varList.add(i, v);
	}
	
	
	public boolean equivalentMinterms(Minterms v) {
		if(v.varList.size() != varList.size())
			return false;
		for (int i = 0; i < varList.size(); i++) {
			if(!(v.varList.get(i).equals(varList.get(i))))
				return false;
		}
		
		if(v.invVarList.size() != invVarList.size())
			return false;
		
		for (int i = 0; i < invVarList.size(); i++) {
			if(!(v.invVarList.get(i).equals(invVarList.get(i))))
				return false;
		}
		return true;
	}
	
	public boolean equivalentMinterms(String s) {
		if(varList.size() == 1 && varList.get(0).equals(s) &&  invVarList.isEmpty())
			return true;
		return false;
	}
	
	public void addtoInvVarList(String e) {
		invVarList.add(e);
	}
	
	
	public boolean findandRemoveVar(String s){
		for (int i = 0; i < varList.size(); i++) {
			if((varList.get(i).equals(s))){
				varList.remove(i);
				return true;
			}
		}
		return false;
	}
	
	public String findCoefficent(String s) {
		boolean found = false;
		boolean started = false;
		String str = String.valueOf(constant);
		for (int i = 0; i < varList.size(); i++) {
			if(!found) {
				if((varList.get(i).equals(s))) {
					found = true;
					continue;
				}
			}
			if(!started) {
				str = varList.get(i);
				started = true;
			} else {
				str += " * " + varList.get(i);
			}
		}
		if(!found)
			return "";
		else
			return str ;
	}
	
	public String getString(){
		String retVal="";
		if(!invVarList.isEmpty()) {
			reduceinVarListIfPossible();
		}
		
		
		boolean flag = false;
		if(this.constant != 0){
			if(this.constant != 1 || varList.isEmpty()) {
				retVal =  "  " + String.valueOf(constant);
				flag = true;
			}
			for (String var : varList) {
				if(flag)
					retVal += " * " + var ;
				else {
					retVal += var;
					flag = true;
				}
			}
			
			flag = false;
			
			if(invVarList.size() != 0 ){
				retVal += "/(";
				for (String var : invVarList) {
					if(flag)
						retVal += " * " + var ;
					else {
						retVal += var;
						flag = true;
					}
					
				}
				retVal +=")";
			} 
			retVal += " ";
		}
		return retVal;
	}

	private void reduceinVarListIfPossible() {
		boolean found;
		for (int i = 0; i < invVarList.size() ; i++) {
			found = findandRemoveVar(invVarList.get(i));
			if(found) {
				invVarList.remove(i);
				i--;
			}
		}
		
	}
}
