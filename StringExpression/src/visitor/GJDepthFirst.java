//
// Generated by JTB 1.3.2
//

package visitor;
import syntaxtree.*;
import java.util.*;

/**
 * Provides default methods which visit each node in the tree in depth-first
 * order.  Your visitors may extend this class.
 */
public class GJDepthFirst<R,A> implements GJVisitor<R,A> {
   //
   // Auto class visitors--probably don't need to be overridden.
   //
   public R visit(NodeList n, A argu) {
      R _ret=null;
      int _count=0;
      for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
         e.nextElement().accept(this,argu);
         _count++;
      }
      return _ret;
   }

   public R visit(NodeListOptional n, A argu) {
      if ( n.present() ) {
         R _ret=null;
         int _count=0;
         for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
            e.nextElement().accept(this,argu);
            _count++;
         }
         return _ret;
      }
      else
         return null;
   }

   public R visit(NodeOptional n, A argu) {
      if ( n.present() )
         return n.node.accept(this,argu);
      else
         return null;
   }

   public R visit(NodeSequence n, A argu) {
      R _ret=null;
      int _count=0;
      for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
         e.nextElement().accept(this,argu);
         _count++;
      }
      return _ret;
   }

   public R visit(NodeToken n, A argu) { return null; }

   //
   // User-generated visitor methods below
   //

   /**
    * f0 -> Expression()
    */
   public R visit(Goal n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> LogicalNotExpression()
    *       | PreIncrementExpression()
    *       | PreDecrementExpression()
    *       | BitwiseNOTExpression()
    *       | PrimaryExpressionORBracketExpression() ( BinaryExpressionRightPart() )?
    */
   public R visit(Expression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> LeftShiftAssignmentExpression()
    *       | RightShiftAssignmentExpression()
    *       | BitwiseANDAssignmentExpression()
    *       | BitwiseXORAssignmentExpression()
    *       | BitwiseORAssignmentExpression()
    *       | LogicalANDExpression()
    *       | LogicalORExpression()
    *       | NotEqualsExpression()
    *       | LeftShiftExpression()
    *       | RightShiftExpression()
    *       | PostIncrementExpression()
    *       | PostDecrementExpression()
    *       | EqualsExpression()
    *       | GreaterorEqualExpression()
    *       | LesserorEqualExpression()
    *       | AddAssignmentExpression()
    *       | SubtractAssignmentExpression()
    *       | MultiplyAssignmentExpression()
    *       | DivideAssignmentExpression()
    *       | ModulusAssignmentExpression()
    *       | GreaterExpression()
    *       | LesserExpression()
    *       | AssignmentExpression()
    *       | MultiplicationExpression()
    *       | PlusExpression()
    *       | MinusExpression()
    *       | DivisionExpression()
    *       | RaisedtoExpression()
    *       | BitwiseANDExpression()
    *       | BitwiseORExpression()
    *       | BitwiseXORExpression()
    *       | ModulusExpression()
    */
   public R visit(BinaryExpressionRightPart n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LOGICALNOT>
    * f1 -> Expression()
    */
   public R visit(LogicalNotExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <INCREMENT>
    * f1 -> Expression()
    */
   public R visit(PreIncrementExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <DECREMENT>
    * f1 -> Expression()
    */
   public R visit(PreDecrementExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LEFTSHIFTASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(LeftShiftAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <RIGHTSHIFTASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(RightShiftAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEANDASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(BitwiseANDAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEXORASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(BitwiseXORAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEORASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(BitwiseORAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LOGICALAND>
    * f1 -> Expression()
    */
   public R visit(LogicalANDExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LOGICALOR>
    * f1 -> Expression()
    */
   public R visit(LogicalORExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <NOTEQUALS>
    * f1 -> Expression()
    */
   public R visit(NotEqualsExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LEFTSHIFT>
    * f1 -> Expression()
    */
   public R visit(LeftShiftExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <RIGHTSHIFT>
    * f1 -> Expression()
    */
   public R visit(RightShiftExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <INCREMENT>
    */
   public R visit(PostIncrementExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <DECREMENT>
    */
   public R visit(PostDecrementExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <EQUALS>
    * f1 -> Expression()
    */
   public R visit(EqualsExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <GREATEROREQUAL>
    * f1 -> Expression()
    */
   public R visit(GreaterorEqualExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LESSEROREQUAL>
    * f1 -> Expression()
    */
   public R visit(LesserorEqualExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <ADDASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(AddAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <SUBTRACTASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(SubtractAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <MULTIPLYASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(MultiplyAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <DIVIDEASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(DivideAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <MODULUSASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(ModulusAssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <GREATER>
    * f1 -> Expression()
    */
   public R visit(GreaterExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <LESSER>
    * f1 -> Expression()
    */
   public R visit(LesserExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <ASSIGNMENT>
    * f1 -> Expression()
    */
   public R visit(AssignmentExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <MODULUS>
    * f1 -> Expression()
    */
   public R visit(ModulusExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <STAR>
    * f1 -> Expression()
    */
   public R visit(MultiplicationExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <PLUS>
    * f1 -> Expression()
    */
   public R visit(PlusExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <MINUS>
    * f1 -> Expression()
    */
   public R visit(MinusExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <DIVIDE>
    * f1 -> Expression()
    */
   public R visit(DivisionExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEOR>
    * f1 -> Expression()
    */
   public R visit(BitwiseORExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEAND>
    * f1 -> Expression()
    */
   public R visit(BitwiseANDExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <BITWISEXOR>
    * f1 -> Expression()
    */
   public R visit(BitwiseXORExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <RAISEDTO>
    * f1 -> Expression()
    */
   public R visit(RaisedtoExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> PrimaryExpression()
    *       | BracketExpression()
    */
   public R visit(PrimaryExpressionORBracketExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> "("
    * f1 -> Expression()
    * f2 -> ")"
    */
   public R visit(BracketExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      n.f2.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> ( <IDENTIFIER> | BracketExpression() )
    */
   public R visit(ChildExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> "~"
    * f1 -> Expression()
    */
   public R visit(BitwiseNOTExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> UnaryMinusExpression()
    *       | StarExpression()
    *       | SquareBracketExpression()
    *       | PointerExpression()
    *       | DotExpression()
    *       | FunctionExpression()
    *       | <IDENTIFIER>
    *       | <INTEGER_LITERAL>
    *       | <DOUBLE_LITERAL>
    */
   public R visit(PrimaryExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <STAR>
    * f1 -> PrimaryExpressionORBracketExpression()
    */
   public R visit(StarExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <MINUS>
    * f1 -> PrimaryExpressionORBracketExpression()
    */
   public R visit(UnaryMinusExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> PrimaryExpressionORBracketExpression()
    * f1 -> ( "," PrimaryExpressionORBracketExpression() )*
    */
   public R visit(VariableList n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> <IDENTIFIER>
    * f1 -> <LPAREN>
    * f2 -> ( VariableList() )?
    * f3 -> <RPAREN>
    * f4 -> ( <DOT> ChildExpression() | <ARROW> ChildExpression() | <LSQBRACK> Expression() <RSQBRACK> | <LPAREN> ( VariableList() )? <RPAREN> )*
    */
   public R visit(FunctionExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      n.f2.accept(this, argu);
      n.f3.accept(this, argu);
      n.f4.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> ChildExpression()
    * f1 -> <LSQBRACK>
    * f2 -> Expression()
    * f3 -> <RSQBRACK>
    * f4 -> ( <DOT> ChildExpression() | <ARROW> ChildExpression() | <LSQBRACK> Expression() <RSQBRACK> | <LPAREN> ( VariableList() )? <RPAREN> )*
    */
   public R visit(SquareBracketExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      n.f2.accept(this, argu);
      n.f3.accept(this, argu);
      n.f4.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> ChildExpression()
    * f1 -> <DOT>
    * f2 -> ChildExpression()
    * f3 -> ( <DOT> ChildExpression() | <ARROW> ChildExpression() | <LSQBRACK> Expression() <RSQBRACK> | <LPAREN> ( VariableList() )? <RPAREN> )*
    */
   public R visit(DotExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      n.f2.accept(this, argu);
      n.f3.accept(this, argu);
      return _ret;
   }

   /**
    * f0 -> ChildExpression()
    * f1 -> <ARROW>
    * f2 -> ChildExpression()
    * f3 -> ( <DOT> ChildExpression() | <ARROW> ChildExpression() | <LSQBRACK> Expression() <RSQBRACK> | <LPAREN> ( VariableList() )? <RPAREN> )*
    */
   public R visit(PointerExpression n, A argu) {
      R _ret=null;
      n.f0.accept(this, argu);
      n.f1.accept(this, argu);
      n.f2.accept(this, argu);
      n.f3.accept(this, argu);
      return _ret;
   }

}
