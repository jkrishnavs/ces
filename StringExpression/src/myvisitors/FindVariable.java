//
// Generated by JTB 1.3.2
//

package myvisitors;
import syntaxtree.*;
import visitor.GJVoidDepthFirst;

/**
 * Provides default methods which visit each node in the tree in depth-first
 * order.  Your visitors may extend this class.
 */
public class FindVariable extends GJVoidDepthFirst<String> {

	public boolean varaiableFound(){
		return variablefound;
	}
	boolean variablefound  = false;


	public void visit(PrimaryExpression n, String argu) {
	   
	   if(n.f0.choice.getClass() !=  UnaryMinusExpression.class) { 
		   if(n.getString().equals(argu))
			   variablefound = true; 
	   } else {
		   UnaryMinusExpression uns = (UnaryMinusExpression) n.f0.choice;
		   if(uns.f1.getString().equals(argu))
			   variablefound = true; 
	   }
	}

}
