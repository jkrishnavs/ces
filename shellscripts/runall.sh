#!/bin/bash
SRCDIR=../../../shellscripts/npbtest
binhome=../ces-project/bin/ces
exe=imop.MainforCESCompilation
cd $binhome
pwd
cp="-cp ./:../../StringExpression.jar:../../imop.jar"
ERR=totalerr.txt
LOG=totallog.txt
FACTORS=factors.txt
OUT=CESCompiledFiles
rm -r $OUT
mkdir $OUT
#rm $ERR || true
#rm $LOG || true
#rm $FACTORS || true
for file in  $SRCDIR/*
do
    #echo "Inside"
    if [[ ${file: -2} == ".c" ]]
    then
	echo "############ NEXT BENCHMARK#########" >> $LOG
	fname=`basename $file`
	outfile=$OUT/${fname}
	echo $outfile
	echo ${fname} >> $LOG 
	echo ${file} >> $LOG
	echo "/* ${fname} */"  > $outfile
	java -ea $cp $exe -project classifier < $file 1>>$outfile 2>>$LOG	
    fi
done
#grep -r "ERROR\|BENCHMARK\|testNPBFinal" $LOG >>$ERR
#grep -A 7 -r " FACTORS ***\|BENCHMARK " $LOG >>$FACTORS
cd $OUT
mkdir cesnpb
mv *_pre.c cesnpb

