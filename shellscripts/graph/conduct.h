/*****************************************************
 * The original source code can be found 
 * at https://github.com/jkrishnavs/OpenMPGraphAlgorithms
 * This transformed code has some functions inlined.
 *
 *****************************************************/
float __conduct = 0;

int32_t *G_member;

void outputConduct(graph *G) {
  printf("sum C = %lf\n", __conduct);
}

void conduct(graph *G) {
  inittracking("conduct.csv");
  __conduct = 0;
  int i;
  for (i= 0; i < 4; i++) {  
    float m = 0.0 ;
    int32_t __S2 = 0 ;
    int32_t __S3 = 0 ;
    int32_t __S4 = 0 ;
    
#pragma omp parallel
    {
      int32_t __S2_prv = 0 ;
      __S2_prv = 0 ;

	node_t u;
#pragma omp for schedule(dynamic,1024)
      for (u = 0; u < G->numNodes; u ++) 
	if ((G_member[u] == i))
	  {
	    __S2_prv = __S2_prv + (G->begin[u+1] - G->begin[u]) ;
	  }
      
#pragma omp atomic
      __S2 += __S2_prv;
    }

#pragma omp parallel
    {
      int32_t __S3_prv = 0 ;
      __S3_prv = 0 ;
      node_t u0;
#pragma omp for schedule(dynamic,1024)
      for (u0 = 0; u0 < G->numNodes; u0 ++) 
	if ((G_member[u0] != i))
	  {
	    __S3_prv = __S3_prv + (G->begin[u0+1] - G->begin[u0]) ;
	  }

#pragma omp atomic
      __S3 += __S3_prv;
    }
    

    
#pragma omp parallel
    {
      int32_t __S4_prv = 0 ;
      __S4_prv = 0 ;
      node_t u1;
#pragma omp for schedule(static)
      for (u1 = 0; u1 < G->numNodes; u1 ++) 
	if ((G_member[u1] == i))
	  {
	   edge_t j_idx;
	    for (j_idx = G->begin[u1];j_idx < G->begin[u1+1] ; j_idx ++) 
	      {
		node_t j = G->node_idx [j_idx];
	      if ((G_member[j] != i))
		{
		  __S4_prv = __S4_prv + 1 ;
		}
	      }
	  }
#pragma omp atomic
      __S4 += __S4_prv;
    }
    
    m = (float)((__S2 < __S3)?__S2:__S3) ;

    if(m == 0) {
      __conduct += __S4;
    } else {
      __conduct += __S4/m;
    }
  
  }
  endtracking();
  
}
