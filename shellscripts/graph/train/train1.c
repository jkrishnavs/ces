void train(graph *G, int id) {

  printf("The update Element Array %d \n", id);
  char title[50];
  node_t * G_member = (int*)malloc (G->numNodes * sizeof(int));
  srand(0);
  int i;
  for(i = 0; i< G->numNodes; i++) {
    G_member[i] = rand() % G->numNodes; 
  }

  sprintf(title, "element_%d.csv",id);
  gettimeofday(&start, NULL);
  inittracking(title);

#pragma omp parallel
  {
    int threadid = omp_get_thread_num();
    iters[threadid] = 0;
    int t = 0;
    node_t u1;
#pragma omp for schedule(dynamic, 1024)
    for (u1 = 0; u1 < G->numNodes; u1 ++) {
     edge_t j_idx;
      for (j_idx = G->begin[u1];j_idx < G->begin[u1+1] ; j_idx ++) {
	iters[threadid]++;
	node_t j = G->node_idx [j_idx];
	node_t k = G_member[j];
	if(k > G->numNodes/2) {
	  t++;
	}	
      }
    }
    printf("dummy %d \n",t);
    printf("The num iters thread id %d =  %lld \n", threadid, iters[threadid]);
  }
  endtracking();
  gettimeofday(&end, NULL);
  printTiming(ALGO_KERNEL,((end.tv_sec - start.tv_sec)*1000 + ((double)(end.tv_usec - start.tv_usec))/1000));
  free(G_member);
}


