/*****************************************************
 * The original source code can be found 
 * at https://github.com/jkrishnavs/OpenMPGraphAlgorithms
 * This transformed code has some functions inlined.
 *
 *****************************************************/
#include "nodeIntMap.h"

node_t* comm = NULL;

int maxItrs;

typedef struct nodeIntMapElement {
  node_t key;
  int value;
}nodeIntMapElement;




void outputCommunities(graph *G) {
  // print output.
  node_t commList[10];
  int commCount[10];
  int i;
  for(i=0;i<10; i++) {
    commList[i] = NIL_NODE;
    commCount[i] = 0;
  }
  int found;
  int curIndex = 0;
  int t;
  for (t = 0; t < G->numNodes; t++) {
    found  = -1;
    for(i = 0; i<10;i++) {
      if(comm[t] == commList[i]) {
	found = i;
	break;
      }
    }
    if(found != -1) {
      commCount[found]++;
    } else if(curIndex < 10) {
      commCount[curIndex] = 1;
      commList[curIndex] = comm[t];
      curIndex++;
    }    
  }
  printf("Community\t#Nodes\t\t(Showing max 10 entries)\n");
  for (i=0; i<10; i++) {
    if(commList[i] != NIL_NODE)
      printf("%d\t\t%d\n", commList[i], commCount[i]);
  }
  free(comm);
  comm = NULL;

}


void initCommunities(graph *G) {
  
  comm = (node_t*) malloc (G->numNodes * sizeof(node_t));
  assert(comm != NULL);

}


void communities(graph* G) {
  inittracking("communities.csv");
  bool finished = false ;
  
  finished = true ;
  

#pragma omp parallel
  {
    node_t x;
#pragma omp  for schedule(dynamic,1024)
    for (x = 0; x < G->numNodes; x ++) 
      comm[x] = x ;
  }


  int itrs = 0;
  
  do
    {
      finished = true ;


#pragma omp parallel
      {
	nodeIntMap *map;
	map = NULL;
	map = initNodeIntMap(map, 32, 0);
	node_t x0;

#pragma omp  for schedule(dynamic,1024)
	for (x0 = 0; x0 < G->numNodes; x0 ++) {
	    if(mapSize > map->maxSize) {
	      map->maxSize = mapSize;
	      map->list = (nodeIntMapElement*) realloc (map->list, mapSize * sizeof (nodeIntMapElement));    
	    }
	    map->size = 0;
	    int i;
	    for(i=0; i< map->maxSize; i++) {
	      map->list[i].value = 0;
	    }
	    edge_t y_idx;
	    for (y_idx = G->begin[x0];y_idx < G->begin[x0+1] ; y_idx ++) {
	      node_t y = G->node_idx [y_idx];
	      node_t source;
#pragma omp atomic read
	      source = comm[y];
	        int i;
		for(i=0;i<map->size;i++) {
		  if(source == map->list[i].key) {
		    map->list[i].value += inc;
		    break;
		  }  
		}
		if(i == map->size ) {
		  map->list[i].key = key;
		  map->list[i].value = inc;
		  map->size++;
		}
	    }
	    node_t maxVal = NIL_NODE;
	    for(i=0;i<map->size;i++) {
	      if(max < map->list[i].value) {
		maxVal = map->list[i].key;
		max = map->list[i].value;
	      }  
	    }
	    if ( comm[x0] != maxVal && maxVal != NIL_NODE) {
#pragma omp atomic write
	      comm[x0] = maxVal;
	      finished = false ;
	    }
	}
	closeNodeIntMap(map);
      }
      itrs++;
    } while ( !finished && maxItrs > itrs);
  
  
  endtracking();
}
