exe=imop.MainforCESCompilation
#npb=~/Dropbox/newFrameSrc/npb
#armissue=~/Dropbox/newFrameSrc/armissue
arm=~/Dropbox/newFrameSrc/armpreprocessednpb 
binhome=~/Dropbox/imopworkspace/imopforces/bin
cp="-cp ./:/home/jkrishnavs/Dropbox/imopworkspace/imopforces/StringExpression.jar"
#echo "in NPB"
declare -a arr
# cd $npb
# pwd
# for file in *; do
#     name=$npb/$file
#     arr=("${arr[@]}" $name)
# #    echo ${arr[@]}
# done
# cd $armissue

# for file in *; do
#     name=$armissue/$file
#     arr=("${arr[@]}" $name)
#  #   echo ${arr[@]}
# done
cd $arm
for file in *; do
    name=$arm/$file
    arr=("${arr[@]}" $name)
  #  echo ${arr[@]}
done

cd $binhome
touch current.t
rm *.txt
for file in ${arr[@]};
do
    echo $file
    java $cp $exe < $file 1>out.txt 2>errandlog.txt
    grep -r -n "Exception" errandlog.txt >> err.txt
    grep -r -n "ERROR" errandlog.txt >>err.txt
    if [ -s err.txt ]
    then
	echo "error in parsing file " $file >> current.t
	filename=$(basename "$file")
#	echo "file name " $filename
	mv errandlog.txt $filename-errandlog.txt
	mv err.txt $filename-errandlog.txt
    fi
done 
echo "*****************Newly introduced errors***************"
comm -2 -3 <(sort current.t) <(sort previous.t)
echo "*****************Previous Errors***********************"
comm -1 -2 <(sort current.t) <(sort previous.t)

echo "*****************Fixed Errors**************************"
comm -1 -3 <(sort current.t) <(sort previous.t)

mv current.t previous.t

