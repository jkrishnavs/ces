#!/bin/bash
#SRCDIR=~/odroiddata/testNPBFinal/*
SRCDIR=~/cipdps/*
PROPDIR=~/cipdps/prop/*
PREPROP=~/cipdps/preprop/*
binhome=~/Dropbox/imopworkspace/imopforces/bin
exe=imop.MainforCESCompilation
cd $binhome
cp="-cp ./:/home/jkrishnavs/Dropbox/imopworkspace/imopforces/StringExpression.jar"
ERR=~/totalerr.txt
LOG=~/totallog.txt
FACTORS=~/factors.txt
OUT=~/CESCompiledFiles
PRE=preprocessed
rm $ERR || true
rm $LOG || true
rm $FACTORS || true


rm -r $OUT/*
mkdir $OUT/preprocessed

for graph in $PROPDIR
do
    echo "############ GRAPH #######################" $graph
    gname=`basename $graph`
    graphname="${gname%.*}"
    echo $graphname
    mkdir  $OUT/$graphname
    for file in  $SRCDIR
    do
	if [[ ${file: -2} == ".h"  ]]
	then
	    echo "############ NEXT BENCHMARK#########" >> $LOG
	    fname=`basename $file`
	    filename="${fname%.*}"
	    echo $filename
	    outfile=$OUT/$graphname/${filename}'.h'
	    echo $outfile
	    echo ${fname} >> $LOG 
	    echo ${file} >> $LOG
	    java -ea $cp $exe -project graph -costfunction edp -graphPropFile $graph -mainFunction $filename < $file 1>$outfile 2>>$LOG	
	fi
    done
done



for graph in $PREPROP
do
    echo "############ GRAPH #######################" $graph
    gname=`basename $graph`
    graphname="${gname%.*}"
    echo $graphname
    mkdir  $OUT/$PRE/$graphname
    for file in  $SRCDIR
    do
	if [[ ${file: -2} == ".h"  ]]
	then
	    echo "############ NEXT BENCHMARK#########" >> $LOG
	    fname=`basename $file`
	    filename="${fname%.*}"
	    echo $filename
	    outfile=$OUT/$PRE/$graphname/${filename}'.h'
	    echo $outfile
	    echo ${fname} >> $LOG 
	    echo ${file} >> $LOG
	    java -ea $cp $exe -project graph -costfunction edp -graphPropFile $graph -mainFunction $filename < $file 1>$outfile 2>>$LOG	
	fi
    done
done

cd $OUT
grep -r -n "optimalConfig" . &> selected.txt
grep -r -n "oprimalChunk" . &> selectedchunks.txt

cp selected.txt ~/odroiddata/OpenMPGraphAlgorithms/emsoft/testing
cd -

# grep -r "ERROR\|BENCHMARK\|testNPBFinal\|_cconvert" $LOG >>$ERR
# grep -A 7 -r " FACTORS ***\|BENCHMARK " $LOG >>$FACTORS
# cd $OUT
# cp *.cc ~/converted/
# scp -r ~/converted/ odroid@10.21.227.55:~/Desktop/
