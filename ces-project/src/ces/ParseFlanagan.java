/*
 * Copyright (c) 2018 Aman Nougrahiya, V Krishna Nandivada, IIT Madras.
 * This file is a part of the project IMOP, licensed under the MIT license.
 * See LICENSE.md for the full text of the license.
 *
 * The above notice shall be included in all copies or substantial 
 * portions of this file.
 */
package ces;

import flanagan.analysis.Regression;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ParseFlanagan {
	
	
	static void main() {
		
		String[] listofFiles = {};
		
		  // x1 data array
        double[] xArray = {0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5};
        // x2 data array
        double[] zArray = {0.0,0.13,0.26,0.39,0.52,0.65,0.78,0.91,1.04,1.17,1.3,1.43,1.56,1.69,1.82,1.95};
        // observed y data array
        double[] yArray = {12.1379,10.1886,8.9674,7.7824,7.9645,7.3671,6.8216,7.2602,7.1105,7.2873,7.9745,8.5188,9.1492,9.3891,10.4923,11.8675};
        // estimates of the standard deviations of y
        double[] sdArray = {0.25,0.22,0.27,0.23,0.21,0.26,0.28,0.24,0.25,0.23,0.27,0.22,0.23,0.26,0.28,0.24};

        Regression reg = new Regression(xArray, yArray);
        
        reg.polynomial(2);
        reg.plotXY();
        reg.getBestEstimates();
       // reg.set
        //reg.print();
        
        for(int i=0;i<listofFiles.length;i++) {
        	String f = listofFiles[i];
        	
        	BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";

            try {

                br = new BufferedReader(new FileReader(f));
            //    int arrS
                while ((line = br.readLine()) != null) {

                    // use comma as separator
                    String[] country = line.split(cvsSplitBy);


                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        	
        }
        
	}

}
