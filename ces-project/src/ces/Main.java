/*
 * Copyright (c) 2018 Aman Nougrahiya, V Krishna Nandivada, IIT Madras.
 * This file is a part of the project IMOP, licensed under the MIT license.
 * See LICENSE.md for the full text of the license.
 *
 * The above notice shall be included in all copies or substantial 
 * portions of this file.
 */
package ces;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imop.ast.info.*;
//import java.util.*;
import imop.ast.node.*;
import imop.parser.CParser;
import imop.lib.util.*;
import ces.SystemConfigFile;
import ces.data.GraphData.ParseGraphProperties;
import ces.data.base.GraphModelData;
import ces.data.base.GraphPropertiesData;
import ces.data.classifier.ClassifyGraphWorkload;
import ces.data.classifier.ClassifyWorkload;
import ces.data.constructData.*;
import ces.data.workloadGetter.OMPGraphWorkloadGetter;
import ces.data.workloadGetter.OMPWorkloadGetter;
import ces.enums.CostFunction;
import ces.enums.JKPROJECT;
import ces.enums.OptimizationLevel;
import ces.enums.RegressionFunctionChoice;
import ces.getter.*;
import ces.preprocess.*;
import ces.util.*;


public class Main {
	
	public static PrintWriter errWriter = null;
	public static PrintWriter outputWriter = null;
	public static PrintWriter logWriter = null;
	
	public static void InitWriter(){
		Node.setcesCompilation(true);
		try {
			logWriter   = new PrintWriter("log.txt", "UTF-8");
			errWriter   = new PrintWriter("err.txt", "UTF-8");
			outputWriter= new PrintWriter("out.c", "UTF-8");
			imop.Main.logWriter = logWriter;
			imop.Main.errWriter = errWriter;
			imop.Main.outputWriter = outputWriter;
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public static void printTreeHeirarchy() {
		HierarchyPrinter hp = new HierarchyPrinter();
		Main.root.accept(hp,0);
		System.exit(0);
	} 
	
	
	public static void printForLoopdata() {
		HierarchyASTNodeGetter han = new HierarchyASTNodeGetter();
		Main.root.accept(han, ForStatement.class.getName());
		ArrayList<Node> fS = han.getASTNodes();
		for(Node child : fS) {
			System.out.println("The size of the loop is " + CesSpecificMisc.getForIterations((ForStatement)child) + ".");
		}
		System.exit(0);
	}
	
	public static void printWhileLoopdata() {
		HierarchyASTNodeGetter han = new HierarchyASTNodeGetter();
		Main.root.accept(han, WhileStatement.class.getName());
		ArrayList<Node> fS = han.getASTNodes();
		for(Node child : fS) {
			System.out.println("The size of the loop is " + CesSpecificMisc.getWhileIterations((WhileStatement)child) + ".");
		}
		System.exit(0);
		
	}
	
	
	// Done and saved no need to do again
	
//	public static void preprocessing(){
	//	int done = 0;
	//	while(done != 1){
//			Preprocessing preprocess =  new Preprocessing(Main.root);
//			preprocess.execute();
//			   try {
//				   	System.err.println("Done ???????????(1)");
//			    	  InputStreamReader input = new InputStreamReader(System.in);
//			    	  BufferedReader buf = new BufferedReader(input);
//			    	  done = Integer.parseInt(buf.readLine());
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//		   try {
//			outputWriter= new PrintWriter("out1.c", "UTF-8");
//		} catch (FileNotFoundException | UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		   printCode(Main.root);
//		   FileInputStream fis = null;
//		   InitWriter();
//		   try {
//			fis = new FileInputStream(file);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		   Main.root = null;
//		   Main.root = (TranslationUnit) CParser.createRefinedASTNode(fis, TranslationUnit.class);
//		}
//	}
	
	
   public static void preprocess() {
	   /**
	    * Pre-processing parallel regions.
	    * 
	    * 
	    */
	   
	   root.accept(new EnsureCompondStatement());
	  // Main.addlog(root.getInfo().getString());
	   root.accept(new ConvertSwitchStatement());
	   new SeparateParallelSections(root).execute();
	   SeparateParallelFor separator = new SeparateParallelFor(root);
	   separator.execute();
	   
	   /*
	    * We expect compound statements attaches to for's 
	    * ifs else's etc.
	    * 
	    */
	   
	   
	   if(SystemConfigFile.project == JKPROJECT.GRAPH)
		CesSpecificMisc.setBaseFunction(Main.root);
	
   }	
	
	
	
	
	public static void execute(){
	  	
	  OMPWorkloadGetter wlgetter = new OMPWorkloadGetter();
	  root.accept(wlgetter,null);
	  OMPFunctionWorkload mainLoad = wlgetter.getFinalData(SystemConfigFile.baseFunction);
	  ClassifyWorkload cW = new ClassifyWorkload(mainLoad, Main.root);
	  cW.execute();	  	
	  	
		boolean sucess  = true;
		if(!sucess){
			Main.addErrorMessage("failed in data Anaylsis. compilation aborted\n");
			return;
		}	
	}
	
	
	public static void addErrorMessage(String s){
		DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
		Date date = new Date();
		errWriter.println("ERROR "+  dateFormat.format(date) + ": "+ s);
		Main.errWriter.flush();
	}
	
	
	public static void myTests() {
		HierarchyASTNodeGetter astGetter = new HierarchyASTNodeGetter();
		astGetter.reset();
		root.accept(astGetter, WhileStatement.class.getName());
		
		String S = CesSpecificMisc.getWhileIterations((WhileStatement)astGetter.getASTNodes().get(0)).toString();
		Main.addlog(S);
	}
	
	
	public static void addlog(String s){
		DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
		Date date = new Date();
		logWriter.println("LOG "+ dateFormat.format(date) + ": " + s);
		logWriter.flush();
	}
	
	public static void printCode(Node n){
	/*	if(SystemConfigFile.project != JKPROJECT.GRAPH) { */
			PrintCodeVisitor printer = new PrintCodeVisitor();
			n.accept(printer,0);
			Main.outputWriter.flush();
/*		} else { */
		
//			PrintCodeVisitor printerV = new PrintCodeVisitor();
//			n.accept(printerV,0);
//			Main.outputWriter.flush();
//						
			/*
			PrintCPPCodeVisitor printer = new PrintCPPCodeVisitor();
			n.accept(printer, 0);
			Main.outputWriter.flush();
			
			
		
			
		} */
	}
	
	
	
	
	static String costfunctionOptions = "(time|energy|edp|pdp)";
	static String optimizationOptions = "(hardware|dynamic|custom)";
	static String regressionOptions = "(linear|quadratic|gaussian|best)";
	static String projectOptions = "(ces|classifier|graph|task)";
	static String trueFalse = "(true|false)";
	static String costfunctionStr = "-costfunction";
	static String optimizeStr = "-optimize";
	static String regressonStr = "-regression";
	static String alufactorStr = "-alufactor";
	static String dynamicStr = "-dynamicChunk";
	static String singleMaster = "-singleMaster";
	static String irregularGraphStr = "-irregularGraph";
	static String mainFunctionStr = "-mainFunction";
	static String graphPropFile = "-graphPropFile";
	static String helpStr = "-help";
	static String projectStr = "-project";
	private static String optimizationStr;

	public static void printhelpMenu() {
		String s = "java -ea -cp ./:<IMOP_HOME>/imopforces/StringExpression.jar imop.MainforCESCompilation <options> < "
				+ "<TEST FILE> 1> <OUTPUTFILE> 2><LOG_AND_ERROR_FILE>\n"
				+ "options: \n"
				+ projectStr      + " " + projectOptions + "\n"
				+ costfunctionStr + " " + costfunctionOptions + "\n"
				+ optimizationStr + " " + optimizationOptions + "\n"
				+ regressonStr    + " " + regressionOptions + "\n"
				+ alufactorStr    + " " + trueFalse + "\n"
				+ dynamicStr      + " " + trueFalse + "\n"
				+ singleMaster    + " " + trueFalse + "\n"
				+ irregularGraphStr + " " + trueFalse + "\n";
		Main.addlog(s);
	}


	
	public static void setparameters(String[] args) {
		int i=0;
	
		while(i< args.length) {
			
			if(args[i].equals(helpStr)) {
				printhelpMenu();
			}
			if(args[i].equals(projectStr)) {
				i++;
				if(args[i].equals("ces")) {
					SystemConfigFile.project = JKPROJECT.CES;
				} else if (args[i].equals("classifier")) {
					SystemConfigFile.project = JKPROJECT.CLASSIFIER;
				} else if (args[i].equals("graph")) {
					SystemConfigFile.project = JKPROJECT.GRAPH;
				} else if (args[i].equals("task")) {
					SystemConfigFile.project = JKPROJECT.TASK;
				} else if(args[i].equals("siam")) {
					SystemConfigFile.project = JKPROJECT.SIAM;	
				} else {
					Main.addlog("Unknown option for " + projectStr + " choose one of" + projectOptions);
					printhelpMenu();
				}
				
				Main.addlog(projectStr + " "+args[i]);
			} else if(args[i].equals(costfunctionStr)) {
				i++;
				if(args[i].equals("time")) {
					SystemConfigFile.selectedCostFunction = CostFunction.EXECUTIONTIME;
				} else if (args[i].equals("energy")) {
					SystemConfigFile.selectedCostFunction = CostFunction.ENERGY;
				} else if (args[i].equals("edp")) {
					SystemConfigFile.selectedCostFunction = CostFunction.EDP;
				} else if (args[i].equals("pdp")) {
					SystemConfigFile.selectedCostFunction = CostFunction.PDP;
				} else {
					Main.addlog("Unknown option for " + costfunctionStr + " choose one of" + costfunctionOptions);
					printhelpMenu();
				}
				Main.addlog(costfunctionStr + " " + args[i]);
			} else if (args[i].equals(optimizeStr)) {
				i++;
				if(args[i].equals("hardware")) {
					SystemConfigFile.optimizationLevel = OptimizationLevel.HardwareConfigOnly;
				} else if (args[i].equals("dynamic")) {
					SystemConfigFile.optimizationLevel = OptimizationLevel.DynamicScheduling;
				} else if (args[i].equals("custom")) {
					SystemConfigFile.optimizationLevel = OptimizationLevel.CustomScheduling;
				} else  {
					Main.addlog("Unknown option for " + optimizeStr + " choose one of " + optimizationOptions);
					printhelpMenu();
				}
				Main.addlog(optimizationStr + " " + args[i]);
			} else if (args[i].equals(regressonStr)) {
				i++;
				if(args[i].equals("linear")) {
					SystemConfigFile.regressionFunctionChoice = RegressionFunctionChoice.linear;
				} else if (args[i].equals("quadratic")) {
					SystemConfigFile.regressionFunctionChoice =  RegressionFunctionChoice.quad;
				} else if (args[i].equals("gaussian")) {
					SystemConfigFile.regressionFunctionChoice = RegressionFunctionChoice.gaussian;
				} else  if (args.equals("best")) {
					SystemConfigFile.regressionFunctionChoice = RegressionFunctionChoice.bestFit;		
				} else {
					Main.addlog("Unknown option for " + regressonStr + " choose one of " + regressionOptions);
					printhelpMenu();
				}
				Main.addlog(regressonStr + " " + args[i]);
			} else if (args[i].equals(alufactorStr)) {
				i++;
				if(args[i].equals("true")) {
					SystemConfigFile.ALUFACTOR = true;
				} else if (args[i].equals("false")) {
					SystemConfigFile.ALUFACTOR = false;
				} else {
					Main.addlog("Unknown option for " + alufactorStr + " choose one of " + trueFalse);
					printhelpMenu();
				}
				Main.addlog(alufactorStr + args[i]);
			} else if (args[i].equals(dynamicStr)){
				i++;
				
				if(SystemConfigFile.optimizationLevel != OptimizationLevel.DynamicScheduling) {
					Main.addlog( dynamicStr + " option has nno effect unless the dynamic optimization level is taken ");
				}
				
				if(args[i].equals("true")) {
					SystemConfigFile.dynamicChunkSize = true;
				} else if (args[i].equals("false")) {
					SystemConfigFile.dynamicChunkSize = false;
				} else {
					Main.addlog("Unknown option for " + dynamicStr + " choose one of " + trueFalse);
					printhelpMenu();
				}
				Main.addlog(dynamicStr + args[i]);
			} else if (args[i].equals(singleMaster)){
				i++;
				
				if(args[i].equals("true")) {
					SystemConfigFile.optimizeSinglAndMasterForCes = true;
				} else if (args[i].equals("false")) {
					SystemConfigFile.optimizeSinglAndMasterForCes = false;
				} else {
					Main.addlog("Unknown option for " + singleMaster + " choose one of " + trueFalse);
					printhelpMenu();
				}
				Main.addlog(singleMaster + args[i]);
			} else if (args[i].equals(irregularGraphStr)){
				i++;
				
				if(SystemConfigFile.project != JKPROJECT.GRAPH) {
					Main.addlog( dynamicStr + " option has no effect unless the project is graph ");
				}
				if(args[i].equals("true")) {
					SystemConfigFile.irregularGraph = true;
				} else if (args[i].equals("false")) {
					SystemConfigFile.irregularGraph = false;
				} else {
					Main.addlog("Unknown option for " + irregularGraphStr + " choose one of " + trueFalse);
					printhelpMenu();
				}
				Main.addlog(irregularGraphStr + args[i]);
			} else if (args[i].equals(graphPropFile)){
				i++;
				if(SystemConfigFile.project != JKPROJECT.GRAPH) {
					Main.addlog( graphPropFile + " option has no effect unless the project is graph ");
				}
				SystemConfigFile.graphPropertyFile = args[i];
				Main.addlog(graphPropFile + args[i]);
			} else if (args[i].equals(mainFunctionStr)){
				i++;
				if(SystemConfigFile.project != JKPROJECT.GRAPH) {
					Main.addlog( dynamicStr + "Currently, the option has no effect unless the project is graph ");
				}
				SystemConfigFile.baseFunction = args[i];
				Main.addlog(mainFunctionStr + args[i]);
			}
			i++;
		}
	}
	
	
	
	
	public static void initCES() {
		Node.setcesCompilation(true);
		Node.cesImopBridge = new CesImopBridgeImp();
	}
	

	
	
	public static TranslationUnit root = null;
	
	public static void debugexecute(){
		Node main = ((RootInfo)root.getInfo()).getMainFunction();
		boolean value  =  Misc.hasFloatType(main);
		if(value)
			Main.addlog("Has Float");
	}
	
	public static void main(String [] args) throws FileNotFoundException {
		
		
	//	File file = new File("armpreprocessednpb/sp_pre.c");
	//	File file = new File("/home/jkrishnavs/benchmarks/macpreprocessed/npb/bt.i");
	//	File file = new File("/home/jkrishnavs/a1.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/sp.i");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/is.i");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/btCopy.i");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/ft.i");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/cg.i");
		//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/cgcut.i");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/sections/parsec.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue2.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue7.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue7.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue10.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue8.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue11.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armissue/armissue13.c");
	//  File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/bt_pre.c");	
	 // 	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/cg_pre.c");	
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/ft_pre.c");	
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/is_pre.c");	
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/lu_pre.c");	
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/mg_pre.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/ep_pre.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/armpreprocessednpb/sp_pre.c");
		//	File file = new File("/home/jkrishnavs/Dropbox/imopworkspace/imopforces/out1.c");
	//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/btCopy.i");
	
	
		
		//File file = new File("/home/jkrishnavs/inter/adamicAdar.cc");	
		
		//File file = new File("/home/jkrishnavs/odroiddata/testthis.c");
		// File file = new File("/home/jkrishnavs/odroiddata/adamicAdar_cconvert.c");
		// File file = new File("/home/jkrishnavs/Desktop/SSSP/sssp_main.c");
		
		
		
	//	File file = new File("/home/jkrishnavs/cconvert/pagerank_cconvert.c");
	//	File file = new File("/home/jkrishnavs/cconvert/sssp_path_cconvert.c");	
	//	File file = new File("/home/jkrishnavs/cconvert/triangle_counting_cconvert.c");
	//	File file = new File("/home/jkrishnavs/cipdps/conduct_cconvert.c");
	//	File file = new File("/home/jkrishnavs/cconvert/potential_friends_cconvert.c");
	//	File file = new File("/home/jkrishnavs/cipdps/communities_cconvert.c");
		
//				
//		File file = new File("/home/jkrishnavs/cconvert/adamicAdar_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/kosaraju_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/pagerank_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/potential_friends_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/sssp_path_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/triangle_counting_cconvert.c");
//		File file = new File("/home/jkrishnavs/cconvert/v_cover_cconvert.c");
		
		   File file = new File ("/home/jkrishnavs/cipdps/conduct.h");
		// File file = new File ("/home/jkrishnavs/cipdps/communities.h");
		// File file = new File ("/home/jkrishnavs/cipdps/pageRank.h");
		// File file = new File ("/home/jkrishnavs/cipdps/sssp.h");
		// File file = new File ("/home/jkrishnavs/cipdps/triangleCounting.h");
	
		// File file = new File ("/home/jkrishnavs/cipdps/train/train5.c");
		 // File file = new File ("/home/jkrishnavs/cipdps/train/train1.c");
		//  File file = new File ("/home/jkrishnavs/cipdps/train/train2.c");
		// File file = new File ("/home/jkrishnavs/cipdps/train/train3.c");
		// File file = new File ("/home/jkrishnavs/cipdps/train/train4.c");
		
		
							
		// Main test case
		// File file = new File("/home/jkrishnavs/odroiddata/test.c");	
		// File file = new File("/home/jkrishnavs/odroiddata/while.c"); 
		
		 // File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/bt_pre.c");	
		//	File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/cg_pre.c");	
		//    File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/dc_pre.c");
		//	File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/ep_pre.c");
		//	File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/ft_pre.c");	
		// File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/is_pre.c");	
		//	File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/lu_pre.c");	
		//	File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/mg_pre.c");	
		//  File file = new File("/home/jkrishnavs/odroiddata/testNPBFinal/sp_pre.c");
		//File file = new File("/home/jkrishnavs/odroiddata/ces/predata");

		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/bt_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/cg_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/ep_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/ft_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/is_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/lu_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/mg_pre.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/sp_pre.c");
		 InitWriter();
		 initCES();
		 if(args.length > 0) {
				setparameters(args);
		 }
		 
		// File file = new File("/home/jkrishnavs/odroiddata/ces/predata/ft_pre.c");
		//  File file = new File("/home/jkrishnavs/odroiddata/ces/predata/is_pre.c");	 
		//  File file = new File("/home/jkrishnavs/odroiddata/ces/predata/mg_pre.c"); 
		
		// File file = new File("/home/jkrishnavs/odroiddata/test/test.c");
		// File file = new File("/home/jkrishnavs/odroiddata/test/test.c");
		//   File file = new File("/home/jkrishnavs/odroiddata/ces/parser/try.c");
		// File file = new File("/home/jkrishnavs/odroiddata/ces/parser/test5.c");
		//		File file = new File("/home/jkrishnavs/odroiddata/test/test4.c");
			
		//	File file = new File("/home/jkrishnavs/Dropbox/imopworkspace/imopforces/bin/for.c");
		// 	File file = new File("/home/jkrishnavs/Dropbox/imopworkspace/imopforces/bin/while.c");
		//	File file = new File("/home/jkrishnavs/Dropbox/newFrameSrc/npb/cg.i");
		FileInputStream fis = null;
		try {
				fis = new FileInputStream(file);
				root = (TranslationUnit) CParser.createRefinedASTNode(fis, TranslationUnit.class);
				
			//	RegressionFunction.tryFlanagan();
			//			myTests();
				preprocess();
				
				if(SystemConfigFile.project == JKPROJECT.SIAM) {
					executeGraph();
				} else {
					execute();
				}
			//	debugexecute();
				printCode(root);
		
			}catch (Exception e) {
				if(true)
					throw e;
				Main.addErrorMessage(e.getMessage());
				Thread.dumpStack();
				Main.addErrorMessage(e.getStackTrace().toString());
			}finally{
				try{
					if(fis != null)
						fis.close();
					Main.errWriter.close();
				}catch(IOException e){
					Main.addErrorMessage(e.getMessage());

					Main.errWriter.println(e.getStackTrace());
					e.printStackTrace();
				}			
			}
	}
	
	
	
	public static void executeTask() {
		
		
	}



	public static void executeGraph() {
		/**
		 * TODO change to parse for each dynamic chunks.
		 * 
		 * We currently assume that we have only one irregular workload
		 * (Single loop)
		 */
		ParseGraphProperties graphProp = new ParseGraphProperties();
		
		GraphPropertiesData properties = new GraphPropertiesData();

		properties = graphProp.parseGraphProp(properties);

		
		OMPGraphWorkloadGetter gwl = new OMPGraphWorkloadGetter();
		
		/**
		 * TODO Hack
		 */
		
		
		Node wlRoot = root;
	
		if((!SystemConfigFile.baseFunction.equals("main")) || (!SystemConfigFile.baseFunction.isEmpty()) ) {
			wlRoot = CesSpecificMisc.getFunctionDefinitionofName(root, SystemConfigFile.baseFunction);
		}
		assert(wlRoot != null);
		
		
		
		
		gwl.init(root);
		wlRoot.accept(gwl, null);
		GraphModelData data = gwl.getGraphModel();
		data.accumulateData();
		data.addUpMemOps();
		data.dumpData();
		data.upateBaseVal(properties);
		properties.updateAlgoData(data);
		ClassifyGraphWorkload cgw = new ClassifyGraphWorkload();
		cgw.execute(properties, wlRoot);
		
		
		
	}
} 



