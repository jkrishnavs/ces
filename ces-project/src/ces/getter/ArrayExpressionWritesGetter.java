/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.ArrayList;
import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.GJVoidDepthFirst;
import ces.data.node.ArrayExpression;


/***
 * We are implementing only write mode for 
 * we need this for 
 * 
 * @author jkrishnavs
 *
 */
public class ArrayExpressionWritesGetter extends GJVoidDepthFirst<Integer> {
	
	
		private static int flagUnknown = 0;
		private static int flagNonConditionalExpression = 1;
		private static int flagConditionalExpression = 2;
		private static int flagPlusPlus = 4;
		private static int flagMinusMinus = 5;
		
		
	
		ArrayList<ArrayExpression> allArrayExpression = new ArrayList<ArrayExpression>();
		
		int updateOperation;
		
		public ArrayList<ArrayExpression> getAllArrayExpressions() {
			return allArrayExpression;
		}
		
		ArrayList<BracketExpression> curExprList = new ArrayList<BracketExpression>();
		
		
		private void addtoList(Node base,String basestr, Integer a) {
			if(curExprList.size() > 0) {
				ArrayExpression newExpr = new ArrayExpression(base, basestr,  curExprList, true);
				allArrayExpression.add(newExpr);
			}
			curExprList.clear();
		}
	
	
	   /**
	    * f0 -> "["
	    * f1 -> Expression()
	    * f2 -> "]"
	    */
	   public void visit(BracketExpression n,Integer a) {
		   if(a == flagConditionalExpression || a == flagNonConditionalExpression) 
			   curExprList.add(n);
		   else 
			   Main.addErrorMessage("Unkown flag for Writa arrays " + a );
	   }
	   
	   
	   
	   /**
	    * f0 -> UnaryExpression()
	    * f1 -> AssignmentOperator()
	    * f2 -> AssignmentExpression()
	    */
	   public void visit(NonConditionalExpression n, Integer argu) {
		   UnaryExpression uF0 = n.f0;
		   String base = null;
		   if(uF0.f0.choice.getClass() == PostfixExpression.class) {
			  PostfixExpression exp =  (PostfixExpression) uF0.f0.choice;
			  base = exp.f0.getInfo().getString();
		   }
	      n.f0.accept(this, flagNonConditionalExpression);	      
	      if(base != null && !base.isEmpty()) {
	    	  if(!curExprList.isEmpty()) {
	    		  addtoList(uF0, base, flagNonConditionalExpression);
	    	  }
	      }
	    }
	   
	   public void visit(ConditionalExpression n, Integer argu) {
		   updateOperation = flagUnknown;	
		   n.f0.accept(this, flagConditionalExpression);
		  
		   
		     // n.f1.accept(this, argu);
	   }
	   
	   public void visit(UnaryExpression n, Integer argu) {
		   String base = null;
		   if(argu == flagConditionalExpression && curExprList.isEmpty()) {
			   updateOperation = flagUnknown;
			   if(n.f0.choice.getClass() == PostfixExpression.class) {
					  PostfixExpression exp =  (PostfixExpression) n.f0.choice;
					  base = exp.f0.getInfo().getString();
				}
		   }
		   n.f0.accept(this, argu);
		   if(base != null && !base.isEmpty()) {
			   if(updateOperation == flagPlusPlus ||
					   updateOperation == flagMinusMinus) {
				   addtoList(n, base, flagConditionalExpression);
			   }
		   }
	   }
	   
		/**
		 * f0 -> Declarator()
		 * f1 -> ( "=" Initializer() )?
		 */
		public void visit(InitDeclarator n, Integer argu) {
			Main.addErrorMessage("Not handled for ArrayExpressiongetter  for " + n.toString());
		}

		/**
		 * f0 -> <IDENTIFIER>
		 * f1 -> "="
		 * f2 -> Expression()
		 */
		public void visit(OmpForInitExpression n, Integer argu) {
			Main.addErrorMessage("Not handled for ArrayExpressiongetter  for " + n.toString());
		}


		public void visit(PlusPlus n,  Integer argu) {
			updateOperation = flagPlusPlus;
		}

		public void visit(MinusMinus n, Integer argu) {
			updateOperation = flagMinusMinus;
			//	Main.addErrorMessage("Not handled MinusMinus");
		}

	   
}
