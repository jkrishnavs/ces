/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import ces.data.node.ArrayExpression;
import imop.lib.getter.LvalueAccessGetter;


/***
 * We are implementing only write mode for 
 * we need this for {@link LvalueAccessGetter}
 * 
 * @author jkrishnavs
 *
 */
public class WriteArrayExpressionGetter extends DepthFirstVisitor {

		ArrayList<ArrayExpression> allArrayExpression = new ArrayList<ArrayExpression>();
		
		public ArrayList<ArrayExpression> getAllArrayExprssions() {
			return allArrayExpression;
		}
		
		
		class BracketList {
			
			
		}
		
		ArrayList<BracketExpression> curExprList = new ArrayList<BracketExpression>();
		
		
		private void addtoList(Node base) {
			
			if(curExprList.size() > 0) {
				ArrayExpression newExpr = new ArrayExpression(base,"", curExprList, true);
				allArrayExpression.add(newExpr);
			} 
			curExprList.clear();
		}
		
		
		public WriteArrayExpressionGetter() {
			Main.addErrorMessage("Write Array Expressions not added");
		}
	
		/**
		 * f0 -> "["
		 * f1 -> Expression()
		 * f2 -> "]"
		 */
		public void visit(BracketExpression n,Integer a) {
			curExprList.add(n);
		}	

		public void visit(OmpForAdditive n) {} 
		public void visit(OmpForMultiplicative n) {}
		public void visit(OmpForSubtractive n) {}
		public void visit(ShortAssignPlus n) {}
		public void visit(PreIncrementId n) {}
		public void visit(PreDecrementId n) {}
		public void visit(ShortAssignMinus n) {}
		public void visit(UnaryCastExpression n) {}
		public void visit(ShiftExpression n) {}
		public void visit(RelationalExpression n) {}
		public void visit(AdditiveExpression n) {}
		public void visit(MultiplicativeExpression n) {}
		public void visit(CastExpressionTyped n) {}
		public void visit(ANDExpression n) {}
		public void visit(EqualityExpression n) {}
		public void visit(ConditionalExpression n) {}
		public void visit(ConstantExpression n) {}
		public void visit(ExclusiveORExpression n) {}
		
		/**
		 * f0 -> PrimaryExpression()
		 * f1 -> PostfixOperationsList()
		 */
		public void visit(PostfixExpression n) {
			n.f0.accept(this);
			curExprList.clear();
			NodeListOptional nodeList = n.f1.f0;
			
			if(! nodeList.nodes.isEmpty()) {
				int size = nodeList.nodes.size();
				int i = 0;
				while(i < size) {
					Node opNode = ((APostfixOperation) nodeList.nodes.get(i)).f0.choice;
					if (opNode instanceof PlusPlus) {
						addtoList(n.f0);
					} else if (opNode instanceof MinusMinus) {
						addtoList(n.f0);
					} else if (opNode instanceof BracketExpression) {
						curExprList.add((BracketExpression) opNode);
					}
					i++;
				}
			}
		}
		
		
		
		 /**
		    * f0 -> UnaryExpressionPreIncrement()
		    *       | UnaryExpressionPreDecrement()
		    *       | UnarySizeofExpression()
		    *       | UnaryCastExpression()
		    *       | PostfixExpression()
		    */
		   public void visit(UnaryExpression n) {
		//	  Main.addlog("The unary Expression is "+ n.getInfo().getString());
		      n.f0.accept(this);
		   }
		
		/**
		 * f0 -> "--"
		 * f1 -> UnaryExpression()
		 */
		public void visit(UnaryExpressionPreDecrement n) {
			curExprList.clear();
			n.f1.accept(this);
			addtoList(n.f1);
		}
		
		
		/**
		 * f0 -> "++"
		 * f1 -> UnaryExpression()
		 */
		public void visit(UnaryExpressionPreIncrement n) {
			curExprList.clear();
			n.f1.accept(this);
			addtoList(n.f1);
		}
		
		
		/**
		 * f0 -> UnaryExpression()
		 * f1 -> AssignmentOperator()
		 * f2 -> AssignmentExpression()
		 */
		public void  visit(NonConditionalExpression n) {
			curExprList.clear();
			n.f0.accept(this);
			addtoList(n.f0);
		}
		
		/**
		    * f0 -> NonConditionalExpression()
		    *       | ConditionalExpression()
		    */
		   public void visit(AssignmentExpression n) {
			//   Main.addlog("The assignment Expression is "+ n.getInfo().getString());
		      n.f0.accept(this);
		   }
}
