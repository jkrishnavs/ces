/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.ArrayList;
import java.util.Stack;

import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.GJVoidDepthFirst;
import ces.data.node.ArrayExpression;


/***
 * We are implementing only write mode for 
 * we need this for 
 * 
 * @author jkrishnavs
 *
 */
public class ArrayExpressionReadsGetter extends GJVoidDepthFirst<Integer> {
	
	
		private static int flagConditionalExpression = 2;
		private static int flagPlusPlus = 4;
		private static int flagMinusMinus = 5;
		
		
	
		ArrayList<ArrayExpression> allArrayExpression = new ArrayList<ArrayExpression>();
		
		int updateOperation;
		
		public ArrayList<ArrayExpression> getAllArrayExpressions() {
			return allArrayExpression;
		}
		
		ArrayList<BracketExpression> curExprList = new ArrayList<BracketExpression>();
		
		
		Stack< ArrayList<BracketExpression> > myStack = new Stack< ArrayList<BracketExpression> >();
		
		
		private void addtoList(Node base,String basestr, Integer a) {
			if(curExprList.size() > 0) {
				ArrayExpression newExpr = new ArrayExpression(base, basestr,  curExprList, false);
				allArrayExpression.add(newExpr);
			}
			curExprList.clear();
		}
	
	
	   /**
	    * f0 -> "["
	    * f1 -> Expression()
	    * f2 -> "]"
	    */
	   public void visit(BracketExpression n,Integer a) {
		   curExprList.add(n);
		   myStack.push(curExprList);
		   curExprList = new ArrayList<BracketExpression>();
		   n.f1.accept(this, a);
		   curExprList = myStack.pop();
	   }
	   
	   
	   
	   /**
	    * f0 -> UnaryExpression()
	    * f1 -> AssignmentOperator()
	    * f2 -> AssignmentExpression()
	    */
	   public void visit(NonConditionalExpression n, Integer argu) {
		  n.f2.accept(this, argu);
	    }
	   
	  
	   
	   public void visit(UnaryExpression n, Integer argu) {
		   String base = null;
		   if(n.f0.choice.getClass() == PostfixExpression.class) {
			   PostfixExpression exp =  (PostfixExpression) n.f0.choice;
			   base = exp.f0.getInfo().getString();
		   }
		   n.f0.accept(this, argu);
		   if(base != null && !base.isEmpty() && ! curExprList.isEmpty()) {
			   addtoList(n, base, flagConditionalExpression);
		   }
	   }
	   
		/**
		 * f0 -> Declarator()
		 * f1 -> ( "=" Initializer() )?
		 */
		public void visit(InitDeclarator n, Integer argu) {
			n.f1.accept(this, argu);
		}

		/**
		 * f0 -> <IDENTIFIER>
		 * f1 -> "="
		 * f2 -> Expression()
		 */
		public void visit(OmpForInitExpression n, Integer argu) {
			n.f2.accept(this,argu);
		}


		public void visit(PlusPlus n,  Integer argu) {
			updateOperation = flagPlusPlus;
		}

		public void visit(MinusMinus n, Integer argu) {
			updateOperation = flagMinusMinus;
		//	Main.addErrorMessage("Not handled MinusMinus");
		}

	   
}
