/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import imop.baseVisitor.GJVoidDepthFirst;
import ces.data.node.ArrayExpression;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.util.Misc;


/***
 * We are implementing only write mode for 
 * we need this for 
 * 
 * @author jkrishnavs
 *
 */
public class ArrayExpressionGetter extends GJVoidDepthFirst<Integer> {
	
	
//	
//		final static int readmode = 1;
//		final static int writemode = 4;
//	
//		ArrayList<ArrayExpression> allArrayExpression = new ArrayList<ArrayExpression>();
//		
//		public ArrayList<ArrayExpression> getAllArrayExprssions() {
//			return allArrayExpression;
//		}
//		
//		ArrayList<BracketExpression> curExprList = new ArrayList<BracketExpression>();
//		
//		
//		private void addtoList(Node base, Integer a) {
//			if(curExprList.size() > 0) {
//				if(a%writemode == 1) {
//					ArrayExpression newExpr = new ArrayExpression(base, curExprList, false);
//					allArrayExpression.add(newExpr);
//				}
//				
//				if(a/writemode == 1) {
//					ArrayExpression newExpr = new ArrayExpression(base, curExprList, true);
//					allArrayExpression.add(newExpr);
//				}
//			}
//			curExprList.clear();
//		}
//	
//	
//	   /**
//	    * f0 -> "["
//	    * f1 -> Expression()
//	    * f2 -> "]"
//	    */
//	   public void visit(BracketExpression n,Integer a) {
//		   curExprList.add(n);
//	   }
//	   
//		/**
//		 * f0 -> Declarator()
//		 * f1 -> ( "=" Initializer() )?
//		 */
//		public void visit(InitDeclarator n) {
//			Main.addErrorMessage("Not handled for ArrayExpressiongetter  for " + n.toString());
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "="
//		 * f2 -> Expression()
//		 */
//		public void visit(OmpForInitExpression n) {
//
//			Main.addErrorMessage("Not handled for ArrayExpressiongetter  for " + n.toString());
//		}
//
//
//
//	
//
//	
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "+="
//		 * f2 -> Expression()
//		 */
//		public void visit(ShortAssignPlus n, Integer a) {
//			
//			addReads(n.f2.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "-="
//		 * f2 -> Expression()
//		 */
//		public Vector<Symbol> visit(ShortAssignMinus n) {
//			Symbol sym = Misc.getSymbolEntry(n.f0.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::ShortAssignMinus");
//				System.exit(0);
//			}
//			lvalueReadList.add(sym);
//			lvalueWriteList.add(sym);
//			addReads(n.f2.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "="
//		 * f2 -> <IDENTIFIER>
//		 * f3 -> "+"
//		 * f4 -> AdditiveExpression()
//		 */
//		public Vector<Symbol> visit(OmpForAdditive n) {
//			Symbol sym = Misc.getSymbolEntry(n.f0.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForAdditive (1)");
//				System.exit(0);
//			}
//			lvalueWriteList.add(sym);
//			sym = Misc.getSymbolEntry(n.f2.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForAdditive (2)");
//				System.exit(0);
//			}
//			lvalueReadList.add(sym);
//			addReads(n.f4.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "="
//		 * f2 -> <IDENTIFIER>
//		 * f3 -> "-"
//		 * f4 -> AdditiveExpression()
//		 */
//		public Vector<Symbol> visit(OmpForSubtractive n) {
//			Symbol sym = Misc.getSymbolEntry(n.f0.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForSubtractive (1)");
//				System.exit(0);
//			}
//			lvalueWriteList.add(sym);
//			sym = Misc.getSymbolEntry(n.f2.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForSubtractive (2)");
//				System.exit(0);
//			}
//			lvalueReadList.add(sym);
//			addReads(n.f4.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 * f1 -> "="
//		 * f2 -> MultiplicativeExpression()
//		 * f3 -> "+"
//		 * f4 -> <IDENTIFIER>
//		 */
//		public Vector<Symbol> visit(OmpForMultiplicative n) {
//			Symbol sym = Misc.getSymbolEntry(n.f0.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForMultiplicative (1)");
//				System.exit(0);
//			}
//			lvalueWriteList.add(sym);
//			sym = Misc.getSymbolEntry(n.f4.tokenImage, n);
//			if(sym == null) {
//				Main.addErrorMessage("Some problem with symbol table updates! Exiting from LvalueAccessGetter::OmpForMultiplicative (2)");
//				System.exit(0);
//			}
//			lvalueReadList.add(sym);
//			addReads(n.f2.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> AssignmentExpression()
//		 * f1 -> ( "," AssignmentExpression() )*
//		 */
//		public Vector<Symbol> visit(Expression n) {
//			if(n.f1.nodes.isEmpty()) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				for(Node seq: n.f1.nodes) {
//					assert seq instanceof NodeSequence;
//					AssignmentExpression aE = (AssignmentExpression) ((NodeSequence) seq).nodes.get(1);
//					addReads(aE.accept(this));
//				}
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> NonConditionalExpression()
//		 *       | ConditionalExpression()
//		 */
//		public Vector<Symbol> visit(AssignmentExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> UnaryExpression()
//		 * f1 -> AssignmentOperator()
//		 * f2 -> AssignmentExpression()
//		 */
//		public Vector<Symbol> visit(NonConditionalExpression n) {
//			// UnaryExpression may either be an lvalue, or not,
//			// but in this case, it must be an lvalue since it is on LHS of assignment
//			String operator = ((NodeToken) n.f1.f0.choice).tokenImage;
//			if (operator.equals("=")) {
//				// UnaryExpression is only written to
//				addWrites(n.f0.accept(this));
//				//addWrites(symList);
//			}
//			else {
//				// UnaryExpression is both read and written
//				Vector<Symbol> sym = (n.f0.accept(this));
//				addReads(sym);
//				addWrites(sym);
//			}
//			addReads(n.f2.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> LogicalORExpression()
//		 * f1 -> ( "?" Expression() ":" ConditionalExpression() )?
//		 */
//		public Vector<Symbol> visit(ConditionalExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				NodeSequence seq = (NodeSequence) n.f1.node;
//				Expression exp = (Expression) seq.nodes.get(1);
//				ConditionalExpression condExp = (ConditionalExpression) seq.nodes.get(3);
//
//				addReads(n.f0.accept(this));
//				addReads(exp.accept(this));
//				addReads(condExp.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> ConditionalExpression()
//		 */
//		public Vector<Symbol> visit(ConstantExpression n) {
//			addReads(n.f0.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> LogicalANDExpression()
//		 * f1 -> ( "||" LogicalORExpression() )?
//		 */
//		public Vector<Symbol> visit(LogicalORExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(((NodeSequence)n.f1.node).nodes.get(1).accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> InclusiveORExpression()
//		 * f1 -> ( "&&" LogicalANDExpression() )?
//		 */
//		public Vector<Symbol> visit(LogicalANDExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(((NodeSequence)n.f1.node).nodes.get(1).accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> ExclusiveORExpression()
//		 * f1 -> ( "|" InclusiveORExpression() )?
//		 */
//		public Vector<Symbol> visit(InclusiveORExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(((NodeSequence)n.f1.node).nodes.get(1).accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> ANDExpression()
//		 * f1 -> ( "^" ExclusiveORExpression() )?
//		 */
//		public Vector<Symbol> visit(ExclusiveORExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(((NodeSequence)n.f1.node).nodes.get(1).accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> EqualityExpression()
//		 * f1 -> ( "&" ANDExpression() )?
//		 */
//		public Vector<Symbol> visit(ANDExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(((NodeSequence)n.f1.node).nodes.get(1).accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> RelationalExpression()
//		 * f1 -> ( EqualOptionalExpression() )?
//		 */
//		public Vector<Symbol> visit(EqualityExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(n.f1.node.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> EqualExpression()
//		 *       | NonEqualExpression()
//		 */
//		public Vector<Symbol> visit(EqualOptionalExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "=="
//		 * f1 -> EqualityExpression()
//		 */
//		public Vector<Symbol> visit(EqualExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "!="
//		 * f1 -> EqualityExpression()
//		 */
//		public Vector<Symbol> visit(NonEqualExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> ShiftExpression()
//		 * f1 -> ( RelationalOptionalExpression() )?
//		 */
//		public Vector<Symbol> visit(RelationalExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(n.f1.node.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> RelationalLTExpression()
//		 *       | RelationalGTExpression()
//		 *       | RelationalLEExpression()
//		 *       | RelationalGEExpression()
//		 */
//		public Vector<Symbol> visit(RelationalOptionalExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "<"
//		 * f1 -> RelationalExpression()
//		 */
//		public Vector<Symbol> visit(RelationalLTExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> ">"
//		 * f1 -> RelationalExpression()
//		 */
//		public Vector<Symbol> visit(RelationalGTExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "<="
//		 * f1 -> RelationalExpression()
//		 */
//		public Vector<Symbol> visit(RelationalLEExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> ">="
//		 * f1 -> RelationalExpression()
//		 */
//		public Vector<Symbol> visit(RelationalGEExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> AdditiveExpression()
//		 * f1 -> ( ShiftOptionalExpression() )?
//		 */
//		public Vector<Symbol> visit(ShiftExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(n.f1.node.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> ShiftLeftExpression()
//		 *       | ShiftRightExpression()
//		 */
//		public Vector<Symbol> visit(ShiftOptionalExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> ">>"
//		 * f1 -> ShiftExpression()
//		 */
//		public Vector<Symbol> visit(ShiftLeftExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "<<"
//		 * f1 -> ShiftExpression()
//		 */
//		public Vector<Symbol> visit(ShiftRightExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> MultiplicativeExpression()
//		 * f1 -> ( AdditiveOptionalExpression() )?
//		 */
//		public Vector<Symbol> visit(AdditiveExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(n.f1.node.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> AdditivePlusExpression()
//		 *       | AdditiveMinusExpression()
//		 */
//		public Vector<Symbol> visit(AdditiveOptionalExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "+"
//		 * f1 -> AdditiveExpression()
//		 */
//		public Vector<Symbol> visit(AdditivePlusExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "-"
//		 * f1 -> AdditiveExpression()
//		 */
//		public Vector<Symbol> visit(AdditiveMinusExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> CastExpression()
//		 * f1 -> ( MultiplicativeOptionalExpression() )?
//		 */
//		public Vector<Symbol> visit(MultiplicativeExpression n) {
//			if(n.f1.node == null) {
//				return n.f0.accept(this);
//			}
//			else {
//				addReads(n.f0.accept(this));
//				addReads(n.f1.node.accept(this));
//				return null;
//			}
//		}
//
//		/**
//		 * f0 -> MultiplicativeMultiExpression()
//		 *       | MultiplicativeDivExpression()
//		 *       | MultiplicativeModExpression()
//		 */
//		public Vector<Symbol> visit(MultiplicativeOptionalExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "*"
//		 * f1 -> MultiplicativeExpression()
//		 */
//		public Vector<Symbol> visit(MultiplicativeMultiExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "/"
//		 * f1 -> MultiplicativeExpression()
//		 */
//		public Vector<Symbol> visit(MultiplicativeDivExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> "%"
//		 * f1 -> MultiplicativeExpression()
//		 */
//		public Vector<Symbol> visit(MultiplicativeModExpression n) {
//			return n.f1.accept(this);
//		}
//
//		/**
//		 * f0 -> CastExpressionTyped()
//		 *       | UnaryExpression()
//		 */
//		public Vector<Symbol> visit(CastExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "("
//		 * f1 -> TypeName()
//		 * f2 -> ")"
//		 * f3 -> CastExpression()
//		 */
//		public Vector<Symbol> visit(CastExpressionTyped n) {
//			addReads(n.f3.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> UnaryExpressionPreIncrement()
//		 *       | UnaryExpressionPreDecrement()
//		 *       | UnarySizeofExpression()
//		 *       | UnaryCastExpression()
//		 *       | PostfixExpression()
//		 */
//		public Vector<Symbol> visit(UnaryExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> "++"
//		 * f1 -> UnaryExpression()
//		 */
//		public Vector<Symbol> visit(UnaryExpressionPreIncrement n) {
//			Vector<Symbol> symList = n.f1.accept(this);
//			addReads(symList);
//			addWrites(symList);
//			return null;
//		}
//
//		/**
//		 * f0 -> "--"
//		 * f1 -> UnaryExpression()
//		 */
//		public Vector<Symbol> visit(UnaryExpressionPreDecrement n) {
//			Vector<Symbol> symList = n.f1.accept(this);
//			addReads(symList);
//			addWrites(symList);
//			return null;
//		}
//
//		/**
//		 * f0 -> UnaryOperator()
//		 * f1 -> CastExpression()
//		 */
//		public Vector<Symbol> visit(UnaryCastExpression n) {
//			// Obtain the list of symbols referred by castExpression as a whole
//			Vector<Symbol> symList = n.f1.accept(this);
//
//			String operator = ((NodeToken) n.f0.f0.choice).tokenImage;
//			switch (operator) {
//			case "&":
//				// &x doesn't read or write to the value of x, so this is not an access to x.
//				return null;
//			case "*":
//				// *x reads/writes to all those symbols which are pointed to by x,
//				// and it also reads from x.
//				addReads(symList);
//				Vector<Symbol> sym = new Vector<Symbol>(symList);
//				return sym;
//			case "+":
//			case "-":
//			case "~":
//			case "!":
//				addReads(symList);
//				return null;
//			default:
//				return null;	
//			}
//		}
//
//		/**
//		 * f0 -> SizeofTypeName()
//		 *       | SizeofUnaryExpression()
//		 */
//		public Vector<Symbol> visit(UnarySizeofExpression n) {
//			return n.f0.accept(this);
//		}
//
//		/**
//		 * f0 -> <SIZEOF>
//		 * f1 -> UnaryExpression()
//		 */
//		public Vector<Symbol> visit(SizeofUnaryExpression n) {
//			// n.f1.accept(this);
//			// Expression is not evaluated when present as the argument to sizeof expression
//			return null;
//		}
//
//		/**
//		 * f0 -> <SIZEOF>
//		 * f1 -> "("
//		 * f2 -> TypeName()
//		 * f3 -> ")"
//		 */
//		public Vector<Symbol> visit(SizeofTypeName n) {
//			return null;
//		}
//
//		/**
//		 * f0 -> PrimaryExpression()
//		 * f1 -> PostfixOperationsList()
//		 */
//		public Vector<Symbol> visit(PostfixExpression n) {
//			Vector<Symbol> symList = n.f0.accept(this);
//			NodeListOptional nodeList = n.f1.f0;
//			if(nodeList.nodes.isEmpty()) {
//				return symList;
//			}
//			else {
//				int size = nodeList.nodes.size();
//				int i = 0;
//				while(i < size) {
//					Node opNode = ((APostfixOperation) nodeList.nodes.get(i)).f0.choice;
//
//					if(opNode instanceof BracketExpression) {
//						((BracketExpression) opNode).accept(this);
//					}
//					else if (opNode instanceof ArgumentList) {
//						((ArgumentList) opNode).accept(this);
//						symList = new Vector<Symbol>();
//					}
//					else if (opNode instanceof DotId) {
//						// ||p|.id| = |p|
//					}
//					else if (opNode instanceof ArrowId) {
//						addReads(symList);
//						symList = new Vector<Symbol>(getPointsToSet(symList));
//					}
//					else if (opNode instanceof PlusPlus) {
//						addReads(symList);
//						addWrites(symList);
//						symList = new Vector<Symbol>();
//					}
//					else if (opNode instanceof MinusMinus) {
//						addReads(symList);
//						addWrites(symList);
//						symList = new Vector<Symbol>();
//					}
//					else {
//						Main.addErrorMessage("Some error in LvalueAccessGetter::PostfixExpression");
//						System.exit(0);
//					}
//					i++;
//				}
//				return symList;
//			}
//		}
//
//		/**
//		 * f0 -> "["
//		 * f1 -> Expression()
//		 * f2 -> "]"
//		 */
//		public Vector<Symbol> visit(BracketExpression n) {
//			addReads(n.f1.accept(this));
//			return null;
//		}
//
//		/**
//		 * f0 -> "("
//		 * f1 -> ( ExpressionList() )?
//		 * f2 -> ")"
//		 */
//		public Vector<Symbol> visit(ArgumentList n) {
//			n.f1.accept(this);
//			return null;
//		}
//
//		/**
//		 * f0 -> <IDENTIFIER>
//		 *       | Constant()
//		 *       | ExpressionClosed()
//		 */
//		public Vector<Symbol> visit(PrimaryExpression n) {
//			if(n.f0.choice instanceof NodeToken) {
//				Vector<Symbol> _ret = new Vector<Symbol>(); 
//				Symbol sym = Misc.getSymbolEntry(((NodeToken)n.f0.choice).tokenImage, n);
//				if(sym != null) 
//					_ret.add(sym);
//				return _ret;
//			}
//			else if (n.f0.choice instanceof ExpressionClosed) {
//				return n.f0.choice.accept(this);
//			}
//			else if (n.f0.choice instanceof Constant) {
//				return null;
//			}
//			else
//				return null;
//		}
//
//		/**
//		 * f0 -> AssignmentExpression()
//		 * f1 -> ( "," AssignmentExpression() )*
//		 */
//		public Vector<Symbol> visit(ExpressionList n) {
//			// Put the closure of expressions in read and write lists.
//			// Pure arguments, and non-array non-pointer arguments should
//			// not be taken a closure of, but assuming that points-to
//			// information will take care of that, we take closure of
//			// all the arguments.
//			Vector<Symbol> symList = n.f0.accept(this);
//			addReads(symList);
//			HashSet<Symbol> symSet= getPointsToClosure(symList);
//			addReads(new Vector<Symbol>(symSet));
//			addWrites(new Vector<Symbol>(symSet));
//
//			for(Node seq: n.f1.nodes) {
//				symList = ((NodeSequence)seq).nodes.get(1).accept(this);
//				addReads(symList);
//				symSet= getPointsToClosure(symList);
//				addReads(new Vector<Symbol>(symSet));
//				addWrites(new Vector<Symbol>(symSet));
//			}
//			return null;
//		}
//
	   
}
