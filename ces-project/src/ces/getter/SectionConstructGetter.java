/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.Vector;

import imop.Main;
import imop.ast.node.ParallelSectionsConstruct;
import imop.ast.node.SectionsConstruct;
import imop.baseVisitor.DepthFirstVisitor;
import ces.SystemConfigFile;

/***
 * This will return only the first level of Section construct getter.
 * 
 * We need to analyse and transform only the outermost sections
 * as no new team of threads are formed if 
 * {@link SystemConfigFile}.allowMultilevelThreading is false;
 *
 * @author jkrishna
 *
 */


public class SectionConstructGetter extends DepthFirstVisitor {
	private Vector<SectionsConstruct> sectionConstructList = new Vector<SectionsConstruct>();


	int currentlevel = 0;
	
	public Vector<SectionsConstruct> getList(){
		return sectionConstructList;
	}
	
	

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTIONS>
	 * f2 -> NowaitDataClauseList()
	 * f3 -> OmpEol()
	 * f4 -> SectionsScope()
	 */
	public void visit(SectionsConstruct n) {
		sectionConstructList.add(n);	
	}
	
	
	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <SECTIONS>
	 * f3 -> UniqueParallelOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> SectionsScope()
	 */
	public void visit(ParallelSectionsConstruct n) {
		Main.addErrorMessage("Found unseparated parallel"
				+ " section list. This could lead to eroneous evauation. please"
				+ " use separator to seperate parallel constructs and "
				+ " sections constructs.");
		System.exit(0);
	}
	
}
