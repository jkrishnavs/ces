/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import imop.ast.node.Node;
import imop.baseVisitor.DepthFirstProcess;

/*
 * This sets the boolean value isParent to true if
 * the Node in childNcde is a child of 
 * the current Tree. 
 * 
 * */

public class IsParentNode extends DepthFirstProcess{
	private boolean _isParent  = false;
	private Node childNode; 
	public IsParentNode(Node child) {
		childNode = child;
	}
	
	public boolean isParent(){
		return _isParent;
	}
	
	public void initProcess(Node n) {   
		if(n == childNode)
			_isParent = true;
	}
}
