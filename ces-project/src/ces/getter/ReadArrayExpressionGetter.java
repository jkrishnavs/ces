/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.ArrayList;
import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import ces.data.node.ArrayExpression;


/***
 * We are implementing only write mode for 
 * we need this for 
 * 
 * @author jkrishnavs
 *
 */
public class ReadArrayExpressionGetter extends DepthFirstVisitor {
	
	ArrayList<ArrayExpression> allArrayExpression = new ArrayList<ArrayExpression>();
	
	public ArrayList<ArrayExpression> getAllArrayExprssions() {
		return allArrayExpression;
	}
	
	ArrayList<BracketExpression> curExprList = new ArrayList<BracketExpression>();
	
	
	private void addtoList(Node base) {
		if(curExprList.size() > 0) {
			ArrayExpression newExpr = new ArrayExpression(base," ", curExprList, false);
			allArrayExpression.add(newExpr);
		}
		curExprList.clear();
	}
	

	public void visit(OmpForAdditive n) {} 
	public void visit(OmpForMultiplicative n) {}
	public void visit(OmpForSubtractive n) {}
	public void visit(ShortAssignPlus n) {}
	public void visit(PreIncrementId n) {}
	public void visit(PreDecrementId n) {}
	public void visit(UnaryCastExpression n) {}
	public void visit(ShiftExpression n) {}
	public void visit(RelationalExpression n) {}
	public void visit(CastExpressionTyped n) {}
	public void visit(ANDExpression n) {}
	public void visit(EqualityExpression n) {}
	public void visit(ConditionalExpression n) {}
	public void visit(ConstantExpression n) {}
	public void visit(ExclusiveORExpression n) {}
	
	
	
	/**
	 * f0 -> PrimaryExpression()
	 * f1 -> PostfixOperationsList()
	 */
	public void visit(PostfixExpression n) {
		n.f0.accept(this);
		curExprList.clear();
		NodeListOptional nodeList = n.f1.f0;
		
		if(! nodeList.nodes.isEmpty()) {
			int size = nodeList.nodes.size();
			int i = 0;
			while(i < size) {
				Node opNode = ((APostfixOperation) nodeList.nodes.get(i)).f0.choice;
				if (opNode instanceof PlusPlus) {
					addtoList(n.f0);
				} else if (opNode instanceof MinusMinus) {
					addtoList(n.f0);
				} else if (opNode instanceof BracketExpression) {
					curExprList.add((BracketExpression) opNode);
				}
				i++;
			}
		}
	}
	
	
	/**
	 * f0 -> "--"
	 * f1 -> UnaryExpression()
	 */
	public void visit(UnaryExpressionPreDecrement n) {
		curExprList.clear();
		n.f1.accept(this);
		addtoList(n.f1);
	}
	
	
	/**
	 * f0 -> "++"
	 * f1 -> UnaryExpression()
	 */
	public void visit(UnaryExpressionPreIncrement n) {
		curExprList.clear();
		n.f1.accept(this);
		addtoList(n.f1);
	}
	
	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "="
	 * f2 -> Expression()
	 */
	public void visit(OmpForInitExpression n) {
		curExprList.clear();
		n.f2.accept(this);
		addtoList(n.f2);
	}
	
	
	
		
		

	

	

		/**
		 * f0 -> <IDENTIFIER>
		 * f1 -> "+="
		 * f2 -> Expression()
		 */
		public void visit(ShortAssignPlus n, Integer a) {
			curExprList.clear();
			n.f2.accept(this);
			addtoList(n.f2);
		}

		/**
		 * f0 -> <IDENTIFIER>
		 * f1 -> "-="
		 * f2 -> Expression()
		 */
		public void  visit(ShortAssignMinus n) {
			curExprList.clear();
			n.f2.accept(this);
			addtoList(n.f2);
		}

	


		/**
		 * f0 -> AssignmentExpression()
		 * f1 -> ( "," AssignmentExpression() )*
		 */
		public void visit(Expression n) {
			curExprList.clear();
			n.f0.accept(this);
			addtoList(n.f0);
		}

		
	



		/**
		 * f0 -> LogicalANDExpression()
		 * f1 -> ( "||" LogicalORExpression() )?
		 */
		public void visit(LogicalORExpression n) {
			if(n.f1.node == null) {
				return;
			}
			else {
				curExprList.clear();
				n.f0.accept(this);
				addtoList(n.f0);
			}
		}

		/**
		 * f0 -> InclusiveORExpression()
		 * f1 -> ( "&&" LogicalANDExpression() )?
		 */
		public void visit(LogicalANDExpression n) {
			if(n.f1.node == null) {
				return ;
			}
			else {
				curExprList.clear();
				n.f0.accept(this);
				addtoList(n.f0);
			}
		}

		/**
		 * f0 -> ExclusiveORExpression()
		 * f1 -> ( "|" InclusiveORExpression() )?
		 */
		public void visit(InclusiveORExpression n) {
			if(n.f1.node == null) {
				return;
			}
			else {
				curExprList.clear();
				n.f0.accept(this);
				addtoList(n.f0);
			}
		}

	


		

	
	

		/**
		 * f0 -> MultiplicativeExpression()
		 * f1 -> ( AdditiveOptionalExpression() )?
		 */
		public void visit(AdditiveExpression n) {
			if(n.f1.node == null) {
				return;
			}
			else {
				curExprList.clear();
				n.f0.accept(this);
				addtoList(n.f0);
				curExprList.clear();
				n.f1.accept(this);
				addtoList(n.f1);
			}
		}

		


		
		


		/**
		 * f0 -> AssignmentExpression()
		 * f1 -> ( "," AssignmentExpression() )*
		 */
		public void visit(ExpressionList n) {
			// Put the closure of expressions in read and write lists.
			// Pure arguments, and non-array non-pointer arguments should
			// not be taken a closure of, but assuming that points-to
			// information will take care of that, we take closure of
			// all the arguments.
			
			curExprList.clear();
			n.f0.accept(this);
			addtoList(n.f0);
			
			for(Node seq: n.f1.nodes) {
				curExprList.clear();
				((NodeSequence)seq).nodes.get(1).accept(this);
				addtoList(seq);
			}
			return;
		}

	
	   
}
