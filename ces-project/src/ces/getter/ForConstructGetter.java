/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.getter;

import java.util.Vector;

import imop.Main;
import imop.ast.node.ForConstruct;
import imop.ast.node.ParallelForConstruct;
import imop.baseVisitor.DepthFirstVisitor;
import ces.SystemConfigFile;

/**
  * This will return only the first level of For construct getter.
 * 
 * We need to analyse and transform only the outermost Fors
 * as no new team of threads are formed if 
 * {@link SystemConfigFile}.allowMultilevelThreading is false;
 * 
 */

public class ForConstructGetter extends DepthFirstVisitor {
	private Vector<ForConstruct> forConstructList = new Vector<ForConstruct>();


	public Vector<ForConstruct> getList(){
		return forConstructList;
	}
	
	
	
	/**
	 * f0 -> OmpPragma()
	 * f1 -> ForDirective()
	 * f2 -> OmpForHeader()
	 * f3 -> Statement()
	 */
	public void visit(ForConstruct n) {
		forConstructList.add(n);
	}


	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <FOR>
	 * f3 -> UniqueParallelOrUniqueForOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> OmpForHeader()
	 * f6 -> Statement()
	 */
	public void visit(ParallelForConstruct n) {
		Main.addErrorMessage("Found unseparated parallel"
				+ " for list. This could lead to erroneous evauation. Please"
				+ " use separator to seperate parallel constructs and "
				+ " for constructs.");
		System.exit(0);
	}
	

}
