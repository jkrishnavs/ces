/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */

package ces.getter;

import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import java.util.Vector;

/**
 * This class adds to the list of parallel for construct all the parallel constructs,
 * including the nested ones.
 */
public class InfiParallelForConstructGetter extends DepthFirstVisitor {

	private Vector<ParallelForConstruct> parallelConstructList = new Vector<ParallelForConstruct>();
	
	public Vector<ParallelForConstruct> getList(){
		return parallelConstructList;
	}
	
	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <FOR>
	 * f3 -> UniqueParallelOrUniqueForOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> OmpForHeader()
	 * f6 -> Statement()
	 */
	
	public void visit(ParallelForConstruct n) {
		parallelConstructList.add(n);
		n.f6.accept(this);
	}


}
