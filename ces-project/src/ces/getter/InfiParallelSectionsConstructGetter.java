/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */

package ces.getter;

import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import java.util.Vector;

/**
 * This class adds to the list of parallel sections construct all the parallel constructs,
 * including the nested ones.
 */
public class InfiParallelSectionsConstructGetter extends DepthFirstVisitor {

	private Vector<ParallelSectionsConstruct> parallelConstructList = new Vector<ParallelSectionsConstruct>();

	public Vector<ParallelSectionsConstruct> getList(){
		return parallelConstructList;
	}
	
	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <SECTIONS>
	 * f3 -> UniqueParallelOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> SectionsScope()
	 */
	
	public void visit(ParallelSectionsConstruct n) {
		parallelConstructList.add(n);
		n.f5.accept(this);
	}


}
