/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
//
// Generated by JTB 1.3.2
//

package ces.getter;
import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.DepthFirstVisitor;
import stringexp.CESString;
import stringexp.Minterms;

import java.util.*;

/**
 * Provides default methods which visit each node in the tree in depth-first
 * order.  Your imop.baseVisitors may extend this class.
 */
public class GetMaxNumber extends DepthFirstVisitor {

	int maxSize = 0;
	
	public int getMaxSize() {
		return maxSize;
	}

	
	
	private void tryandGetData(double val) {
		if(maxSize < val) {
			maxSize = (int) val;
		}
		
	}
	
	
	  /**
	    * f0 -> <INTEGER_LITERAL>
	    *       | <FLOATING_POINT_LITERAL>
	    *       | <CHARACTER_LITERAL>
	    *       | ( <STRING_LITERAL> )+
	    */
	   public void visit(Constant n) {
		   if(n.f0.choice instanceof NodeToken) {
			   NodeToken choice = (NodeToken)n.f0.choice;
			   String data = choice.tokenImage;
			   try {
				   double value = Double.parseDouble(data);
				   tryandGetData(value);
				   int i  = Integer.parseInt(data);
				   tryandGetData(i);
			   } catch (Exception e) {
			   
			   }
		   } else {
		//	   Main.addlog("Non Node token");
		   }
		   
	   }
 

}
