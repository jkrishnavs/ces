/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.CPPdata;

import java.util.ArrayList;
import java.util.HashMap;

import imop.Main;
import imop.ast.node.*;
import ces.ast.info.CesFunctionDefinitionInfo;
import ces.util.*;
import imop.lib.cg.CallSite;

public class CtoCPPFunctionMapping {
	
	public class CPPFunction {
		String className;
		String functionName;
		String cFunctionName;
		ArrayList<String> argList;
		ArrayList<String> cArgList;
	
		public CPPFunction (String classname, String functionname,  ArrayList<String> arglist,
				String cfunctionname, ArrayList<String> carglist) {
			className = classname;
			functionName = functionname;
			argList = arglist;
			cFunctionName = cfunctionname;
			cArgList = carglist;
		}
		
		public boolean containsObjectAsArgument() {
			return (cArgList.size() > argList.size());
		}
		
		public String returnCPPFunctionName(String cfunctionname, ArrayList<String> cargs) {
			assert(cfunctionname.equals(cFunctionName));
			String returnString="";
			/**
			 * TODO : assuming the args are free of CPP functionCalls
			 * 
			 */
			int i = 0;
			if(containsObjectAsArgument()) {
				returnString = cargs.get(0) + "." + functionName + "(";
				i = 1;
			} else {
				returnString = functionName + "( ";
			}
			
			for(;i< cargs.size() ; i++) {
				returnString += cargs.get(i) ;
				if(i < cargs.size()-1)
					returnString += ", ";
			}
			returnString += ")";
			
			
			assert(!returnString.isEmpty());
			
			return returnString;
		}
		
	}
	
	
	HashMap<String, CPPFunction> functionMap;
	
	public String removeSemicolon(String s) {
		return s.replaceAll(";", "");
	}
	
	
	
	
	public CtoCPPFunctionMapping() {
		functionMap = new HashMap<String, CPPFunction>();
	}
	
	public void addCPPMapping(FunctionDefinition def, String classdetails, String functionDetails) {
		assert(classdetails.startsWith("skip fnDef"));
		assert(functionDetails.startsWith("Fnsignature"));
		
		
		String className= null;
		String functionName = null;
		String cfunctionName = null;
		
		
		ArrayList<String> argList = new ArrayList<>();
		ArrayList<String> cargList = new ArrayList<>();
		
		
		if( classdetails.contains("class") ) {
				int startPoint = classdetails.indexOf("class") + 5; // size of "class"
				int endPoint = classdetails.indexOf(" function ");
				className = classdetails.substring(startPoint, endPoint);
				startPoint = classdetails.indexOf(" function ") + 10; // size of " function "
				functionName = classdetails.substring(startPoint);
				functionName = functionName.replace("\n", "");
		} else if(classdetails.contains("fnDef")){
			Main.addlog("Function outside class. added without name change "+  classdetails);
		} else {
				Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
		}
		
		
		if(className != null) {
			if (functionDetails.startsWith("Fnsignature")) {
				String funDefString = functionDetails.substring(11); // size of "Fnsignature"
				funDefString = removeSemicolon(funDefString);
				funDefString = funDefString.substring(funDefString.indexOf("(") + 1 );
				funDefString = funDefString.substring(0,funDefString.indexOf(")")  );
				Main.addlog(funDefString);
			
				while(funDefString.indexOf(",") != -1) {
					String s = funDefString.substring(0, funDefString.indexOf(","));
					s= s.trim();
					assert(!s.isEmpty());
					argList.add(s);
					funDefString = funDefString.substring(funDefString.indexOf(",") + 1 );
				}
			
				funDefString = funDefString.trim();
				if(!funDefString.isEmpty()) {
					argList.add(funDefString);
				}
			}
			 
			ArrayList<Node> paramList = CesSpecificMisc.collectAllChildrensofClass(def, ParameterDeclaration.class.getName());
			
			for(Node n : paramList) {
				cargList.add(n.getInfo().getString());
			}
			
			cfunctionName = ((CesFunctionDefinitionInfo)def.getInfo()).getFunctionName();
			
			assert(functionName != null);
			assert(cfunctionName != null);
			CPPFunction newFunction =  new CPPFunction(className, functionName, argList, cfunctionName, cargList);
			functionMap.put(cfunctionName, newFunction);
			
		}
		
	}
	
	
	

	public String getCPPFunctionCall(CallSite cFunctionCall) {
		//TODO
		return null;
	}
	
	public String getCPPFunctionCall(String cFunctionCall) {
		
	
		String functionCall = CesSpecificMisc.getFunctionName(cFunctionCall);
		functionCall = functionCall.trim();
		ArrayList<String> args = CesSpecificMisc.getArgLists(cFunctionCall);
		if(functionCall!= null) {
			CPPFunction function = functionMap.get(functionCall);
			if(function != null) {
				return function.returnCPPFunctionName(functionCall, args);
			}
		}
		return null;
	}

}
