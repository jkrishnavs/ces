/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.*;
import ces.data.classifier.ClassificationDetails;
import ces.data.node.ArrayExpression;
import ces.enums.DataStatus;
import ces.getter.HierarchyASTNodeGetter;
import ces.util.CesSpecificMisc;

public class ConditionalWorkload extends OMPWorkload {
	
	public enum curLoad {
		enumIfLoad,
		enumElseLoad,
		enumNone,
	};
	
	private curLoad load;
	
	private curLoad maxLoad;
	
	BlockWorkload ifLoad;
	BlockWorkload elseLoad;
	
	ExprWorkload condExprWorkload;
	
	
	public BlockWorkload getIfLoad() {
		return ifLoad;
	}
	

	public void setLiveLoad(BlockWorkload bW) {
		assert(bW.getParent() == this);
		switch (load) {
		case enumElseLoad:
			elseLoad = bW;
			break;
		case enumIfLoad:
			ifLoad = bW;
			break;
		default:
			Main.addErrorMessage("We should not reach here");
		}	
		
	}
	
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		if(load == curLoad.enumNone) {
			condExprWorkload.addArrayAccess(list);
		} else {
			liveLoad().addArrayAccess(list);
		}
	}
	
	@Override
	public void addReadArrayList(ArrayList<ArrayExpression> list) {
		if(load == curLoad.enumNone) {
			condExprWorkload.addReadArrayList(list);
		} else {
			liveLoad().addReadArrayList(list);
		}
		
	}
	
	public OMPWorkload liveLoad(){
		switch (load) {
		case enumElseLoad:
			return elseLoad;
		case enumIfLoad:
			return ifLoad;
		case enumNone:
			return this;
		default:
			Main.addErrorMessage("We should not reach here");
		}		
		return null;
	}
	
	@Override
	public void add(OMPWorkload s) {
		if(liveLoad() != null)
			assert(liveLoad().isLive());
		
		if(liveLoad() != this ) { 
			if(liveLoad() != null) {
				liveLoad().add(s);
			} else 	if(s.getClass() ==  ExprWorkload.class) {
				setLiveLoad(new OMPBlockWorkload(s.getNode(), this));
				liveLoad().add(s);		
			} else if(s.getClass() == BlockWorkload.class ||
					s.getClass() == OMPBlockWorkload.class) {
				setLiveLoad((BlockWorkload) s); 
			} else if(s.getClass() == ContinueWorkload.class ||
					s.getClass() == BreakWorkload.class ||
					s.getClass() == ReturnWorkload.class ||
					s.getClass() == ConditionalWorkload.class) { 
				setLiveLoad(new BlockWorkload(s.getNode(), this));
				liveLoad().add(s);
			} else if(s.getClass() == OMPFlushWorkload.class) { 
				setLiveLoad(new OMPBlockWorkload(s.getNode(), this));
				liveLoad().add(s);
			} else {
					super.add(s);
			}
		} else {
			super.add(s);
		}
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(load != curLoad.enumNone) {
			BlockWorkload liveLoad = (BlockWorkload) liveLoad();
			if(liveLoad == null) {
				liveLoad = new OMPBlockWorkload(n, this);
				setLiveLoad(liveLoad);
			}
			return liveLoad.getExprWorkload(n);
		}
		return condExprWorkload;
	}
	

	
	public BlockWorkload getElseLoad() {
		return elseLoad;
	}
	
	
	
	public void setIfLoad(BlockWorkload b) {
		ifLoad = b;
	}
	
	public void setElseLoad(BlockWorkload b) {
		elseLoad = b;
	}

	public ConditionalWorkload(Node n, OMPWorkload p) {
		super(n, p);
		condExprWorkload = new ExprWorkload(n, this);
		setLoad(curLoad.enumNone);
		maxLoad = curLoad.enumNone;
	}

	public curLoad getLoad() {
		return load;
	}

	public void setLoad(curLoad load) {
		this.load = load;
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(liveLoad() == null) {
			if(parallel)
				return new OMPBlockWorkload(n, this);
			else
				return new BlockWorkload(n, this);
		}
		
		return liveLoad().getNewBlockWorkload(n, parallel);
	}
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(liveLoad() == null) {
			return this;
		} else {
			return liveLoad().getCurrentBlockWorkload(n);
		}
	}
	
	AccumulatedData ifData;
	AccumulatedData elseData;
	
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		setMaxLoad();
		ifData = new AccumulatedData();
		elseData = new AccumulatedData();
		accdata = new AccumulatedData();
		if(ifLoad != null)
			ifLoad.accumulateBranches(ifData);
		if(elseLoad != null)
			elseLoad.accumulateBranches(elseData);
		if(maxLoad == curLoad.enumIfLoad) {
			accdata.branchOperations.add(ifData.branchOperations);
		} else if( maxLoad == curLoad.enumElseLoad) {
			accdata.branchOperations.add(elseData.branchOperations);
		} else {
			Main.addErrorMessage(" Max Load is not properly set");
		}
		accdata.branchOperations.addconstPart(1);
		cur.branchOperations.add(accdata.branchOperations);
		
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(collect) {
			condExprWorkload.accumulateCritical(accdata, collect);
		}
		
		
		if(ifLoad != null) {
			for( OMPWorkload child : ifLoad.childWorkloads) {
				child.accumulateCritical(ifData, collect);
			}
		}
		if(elseLoad != null) {
			for( OMPWorkload child : elseLoad.childWorkloads) {
				child.accumulateCritical(elseData, collect);
			}
		}
		if(maxLoad == curLoad.enumIfLoad) {
			accdata.criticalOperations.add(ifData.criticalOperations);
		} else if( maxLoad == curLoad.enumElseLoad) {
			accdata.criticalOperations.add(elseData.criticalOperations);
		} else {
			Main.addErrorMessage(" Max Load is not properly set");
		}
		
		cur.criticalOperations.add(accdata.criticalOperations);
		
		return cur;
	}
	

	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.MasterCollected) {
			if(collect) {
				condExprWorkload.accumulateMaster(accdata, collect);
			}

			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulateMaster(ifData, collect);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulateMaster(elseData, collect);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.masterOperations.add(ifData.masterOperations);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.masterOperations.add(elseData.masterOperations);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
		}
		cur.masterOperations.add(accdata.masterOperations);
		setStatus(DataStatus.MasterCollected);
		
		return cur;
	}
	
	
	
	
	
	
	private void setMaxLoad() {
		if(elseLoad == null || 
		   CesSpecificMisc.sizeofAGreaterthanB(ifLoad.getNode(), elseLoad.getNode()))
			maxLoad = curLoad.enumIfLoad;
		else 
			maxLoad = curLoad.enumElseLoad;
	}
	
	public BlockWorkload getCondMaxWorkload() {
		if(maxLoad == curLoad.enumIfLoad) {
			return ifLoad;
		} else
			return elseLoad;	
	}
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		if(getStatus() != DataStatus.ReductionsCollected) {
			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulateReductions(ifData);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulateReductions(elseData);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.reductions.add(ifData.reductions);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.reductions.add(elseData.reductions);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
			setStatus(DataStatus.ReductionsCollected);
		}
		cur.reductions.add(accdata.reductions);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
			condExprWorkload.accumulatedata(accdata);
			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulatedata(ifData);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulatedata(elseData);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.totalOps.add(ifData.totalOps);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.totalOps.add(elseData.totalOps);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
			setStatus(DataStatus.DataCollected);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
		
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		if(getStatus() != DataStatus.FalseSharingCollected) {
			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulateFalseSharing(ifData, displacement);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulateFalseSharing(elseData, displacement);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.falseSharingOperations.addSharingFalseSharing(ifData.falseSharingOperations);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.falseSharingOperations.addSharingFalseSharing(elseData.falseSharingOperations);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
			condExprWorkload.accumulateFalseSharing(accdata, displacement);
			setStatus(DataStatus.FalseSharingCollected);
		}
		cur.falseSharingOperations.addSharingFalseSharing(accdata.falseSharingOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.SingleCollected) {
			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulateSingle(ifData, collect);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulateSingle(elseData, collect);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.singleOperations.add(ifData.singleOperations);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.singleOperations.add(elseData.singleOperations);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
			condExprWorkload.accumulateSingle(accdata, collect);
			setStatus(DataStatus.SingleCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		if(getStatus() != DataStatus.unEvenDataCollected) {
			if( collect)
				condExprWorkload.accumulateUnevenWorkloads(accdata, collect, itrDetails);
			if(ifLoad != null) {
				for( OMPWorkload child : ifLoad.childWorkloads) {
					child.accumulateUnevenWorkloads(ifData, collect, itrDetails);
				}
			}
			if(elseLoad != null) {
				for( OMPWorkload child : elseLoad.childWorkloads) {
					child.accumulateUnevenWorkloads(elseData, collect, itrDetails);
				}
			}
			if(maxLoad == curLoad.enumIfLoad) {
				accdata.unevenWorkload.add(ifData.unevenWorkload);
			} else if( maxLoad == curLoad.enumElseLoad) {
				accdata.unevenWorkload.add(elseData.unevenWorkload);
			} else {
				Main.addErrorMessage(" Max Load is not properly set");
			}
			setStatus(DataStatus.unEvenDataCollected);
		}
		cur.unevenWorkload.add(accdata.unevenWorkload);
		return cur;
	}
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		ifLoad.accumulateReentrantNodes(current, itrValue);
		if(elseLoad != null)
			elseLoad.accumulateReentrantNodes(current, itrValue);
	}
	
//	@Override
//	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds,
//			ArrayList<BaseSymbolToVariableMap> baseList) {
//		InternalAsymmetricScheduling ifBlock = new InternalAsymmetricScheduling(node, ControlTypes.IFBLOCK, null, null, baseList);
//		ArrayList<InternalAsymmetricScheduling> inl = new ArrayList<InternalAsymmetricScheduling>();
//		ifLoad.collectInnerLoopDetails(inl, baseList);
//		ifBlock.setInnerLoopSceduling(inl);
//		internalLoopBounds.add(ifBlock);
//		
//		if(elseLoad != null) {
//			elseLoad.collectInnerLoopDetails(internalLoopBounds, null);
//			InternalAsymmetricScheduling elseBlock = new InternalAsymmetricScheduling(node, ControlTypes.ELSEBLOCK, null, null, baseList);
//			inl = new ArrayList<InternalAsymmetricScheduling>();
//			elseLoad.collectInnerLoopDetails(inl, baseList);
//			elseBlock.setInnerLoopSceduling(inl);
//			internalLoopBounds.add(elseBlock);
//		}
//		
//		
//	}
	

	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		IfStatement ifNode =  (IfStatement) node;
		Expression condExpr = ifNode.f2;
		HierarchyASTNodeGetter varGetter = new HierarchyASTNodeGetter();
		varGetter.reset();
		condExpr.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}
		
		
		ifLoad.getVariablesofInterest(varList);
		if(elseLoad != null)
			elseLoad.getVariablesofInterest(varList);
	}
	
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		for (OMPWorkload c :ifLoad.childWorkloads) {
			c.transformCode(cd);
		}
		
		if(elseLoad != null) {
			for (OMPWorkload c :elseLoad.childWorkloads) {
				c.transformCode(cd);
			}
		}
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		for (OMPWorkload c :ifLoad.childWorkloads) {
			c.transformIrregularCode(cd);
		}
		
		if(elseLoad != null) {
			for (OMPWorkload c :elseLoad.childWorkloads) {
				c.transformIrregularCode(cd);
			}
		}
	}
}
