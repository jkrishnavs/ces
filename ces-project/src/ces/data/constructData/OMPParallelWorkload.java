/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.Stack;

import imop.Main;
import imop.ast.node.*;
import ces.data.classifier.ClassificationDetails;
import ces.enums.IrregularityType;
import imop.parser.CParser;

public class OMPParallelWorkload extends OMPWorkload{
	
	
	ArrayList<PrivatizationList> plist;

	private OMPParallelRegionWorkload parregion; // the parallel region ended by the barrier
	
	
	boolean embeddedRegion;
	EmbeddedParallelRegions embedDetails;
	

	
	private int occuranceFreqency;
	
	
	public AccumulatedData getAccumulatedData() {
		return parregion.accdata;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		parregion.getVariablesofInterest(varList);
	}
	
	
	
	
	public ClassificationDetails getClassificationDetails() {
		assert(parregion.sd != null);
		return parregion.sd;
	}
	
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n,boolean parallel) {
		if(parregion == null)
			return new OMPParallelRegionWorkload(n, this);
		return parregion.getNewBlockWorkload(n, parallel);
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	

	ArrayList<PrivatizationList> privateVars;
	private int numTreads;
	private boolean incomplete;
	
	public boolean isParFor() {
		if(node != null && node.getClass() == ParallelForConstruct.class)
			return true;
		else return false;
	}
	
	
	public boolean isParSection() {
		if(node != null && node.getClass() == ParallelSectionsConstruct.class)
			return true;
		else return false;
	}
	
	public OMPParallelWorkload(Node n, OMPWorkload p) {
		super(n, p);
		incomplete = true;
		parregion = null; //new OMPParallelRegionWorkload(n);
		plist = new ArrayList<PrivatizationList>();
		setOccuranceFreqency(0);
		embeddedRegion = false;
	}
	
	
	public void setEmbeddedRegion(EmbeddedParallelRegions em) {
		embeddedRegion = true;
		embedDetails = em;
	}
	
	public boolean isEmbeddedRegion() {
		return embeddedRegion;
	}
	
	public boolean isIncomplete() {
		return incomplete;
	}
	
	public void setComplete() {
		assert(incomplete == true);
		incomplete = false;
		closeWorkload();
	}
	
	
	
	@Override
	public void closeWorkload() {
		if(parregion == null) {
		//	Main.addlog("Parregion is empty");
		}
		super.closeWorkload();
	}
	
	public void setIncomplete() {
		incomplete = true;
	}
	
	static final int[] possibleThreads = {1,2,3,4,5,6,7,8};
	
	
	public int[] possibleNumberofThreads(){
		return possibleThreads;
	}
	
	public void setParallelregion(OMPParallelRegionWorkload w) {
		assert(parregion == null);
		if(w.getParent() == null)
			w.setParent(this);
		assert(w.getParent()  == this);
		parregion = w;
		if(node == null)
			setNode(parregion.node);
	}
	
	public OMPParallelRegionWorkload getParallelregionWorkload(){
		return parregion;
	}
	
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		return this;
	}
	
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(parregion != null)
			return parregion.getCurrentBlockWorkload(n);
		else return this;
	}
	
	@Override
	public void add(OMPWorkload s) {
		if(parregion == null) {
			if(s.getClass() == OMPParallelRegionWorkload.class) {
				setParallelregion((OMPParallelRegionWorkload) s);
			} else {
				setParallelregion(new OMPParallelRegionWorkload(s.node, this));
				s.setParent(parregion);
				this.parregion.add(s);
			}
			
		} else {
			if(s.getClass() == OMPParallelWorkload.class) {
				if(!incomplete)	{
					Stack<OMPWorkload> stackW = ((OMPParallelWorkload)s).parregion.childWorkloads; 
					for (int i = 0; i < stackW.size(); i++) {
						parregion.add(stackW.get(i));
					}
					
				} else {
					super.add(s);
				}
			} else if(s.getClass() == OMPParallelRegionWorkload.class) {
				super.add(s);
			} else {
				parregion.add(s);
			}
		}
	}

	public int getNumTreads() {
		return numTreads;
	}
	
	@Override
	public String dumpdata() {
		if(parregion != null)	
			return parregion.dumpdata();
		else
			return "";
	}

	public void setNumTreads(int numTreads) {
		this.numTreads = numTreads;
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(parregion == null) {
			setParallelregion(new OMPParallelRegionWorkload(n, this));
		}
		return parregion.getExprWorkload(n);
	}
	
	@Override
	public void addPrivatizationList(PrivatizationList p) {
		plist.add(p);
	}
	
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		if(this.parregion == null) {
			Main.addErrorMessage("Found a null parallel region");
		}
		
		return parregion.accumulateReductions(null);
	}
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		return parregion.accumulatedata(null);
	}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		return parregion.accumulateBranches(null);
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		return parregion.accumulateCritical(null, false);
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		return parregion.accumulateFalseSharing(null, null);
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		return parregion.accumulateMaster(null, false);
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		return parregion.accumulateSingle(null, false);
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) {
		return parregion.accumulateUnevenWorkloads(null, false, null);
	}
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		parregion.accumulateReentrantNodes(current, itrValue);
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		 parregion.transformCode(parregion.sd);
		 String parregionStr = parregion.sd.getParallelRegionCode();
		 if(! parregionStr.isEmpty()) {
			 assert(parregion.getNode().getClass() ==  CompoundStatement.class);
			 CompoundStatement cs = (CompoundStatement) parregion.getNode();
			 CompoundStatementElement cse = (CompoundStatementElement) CParser.createCrudeASTNode(parregionStr, 
					 CompoundStatementElement.class);
			 cs.f1.nodes.add(0, cse);
		 }
		 
	}
	
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		 parregion.transformIrregularCode(cd);
		 String parregionStr = parregion.sd.getParallelRegionCode();
		 if(! parregionStr.isEmpty()) {
			 assert(parregion.getNode().getClass() ==  CompoundStatement.class);
			 CompoundStatement cs = (CompoundStatement) parregion.getNode();
			 CompoundStatementElement cse = (CompoundStatementElement) CParser.createCrudeASTNode(parregionStr, 
					 CompoundStatementElement.class);
			 cs.f1.nodes.add(0, cse);
		 }
	}
	
	@Override
	public void verify(int flag) {
		//parregion.verify(flag);
	}

	public int getOccuranceFreqency() {
		return occuranceFreqency;
	}

	public void setOccuranceFreqency(int occuranceFreqency) {
		this.occuranceFreqency = occuranceFreqency;
	}
	
	public void incrementOccuranceFrequency(){
		occuranceFreqency++;
	}

	//private int clusterblock = -1;
	/*
	private boolean isStartPoint = false;
	private boolean isEndPoint = false;

	public boolean isStartPoint() {
		return isStartPoint;
	}
	
	public boolean isEndPoint() {
		return isEndPoint;
	}
	
	public void setasStartBlock() {
		Main.addlog("Start point set");
		isStartPoint = true;
	}
	
	public void setasEndPoint() {
		Main.addlog("EndPoint Set");
		isEndPoint = true;
	}
	*/
}
