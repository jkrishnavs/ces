/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import javax.xml.crypto.Data;

import imop.Main;
import imop.ast.node.Node;
import ces.data.base.TotalIterations;
import ces.enums.DataStatus;
import ces.util.CesStringMisc;
import ces.util.SystemStringConstants;
import imop.lib.analysis.dataflow.Symbol;

public abstract class IterationWorkload extends OMPWorkload {
	
	
	String exitcondtion;
	
	boolean continuousIterations;
	boolean equalWorkloadIterations;
	
	private ArrayList<BlockWorkload> incrementalWorkloads; 
	
	private BlockWorkload itrWorkload;
	
	private Symbol iterator;
	private TotalIterations iterations;
	
	public BlockWorkload getItrWorkload() {
		Main.addlog(node.getInfo().getString());
		assert(itrWorkload != null);
		return itrWorkload;
	}
	
	public boolean hasEvenWorkloads() {
		if(incrementalWorkloads.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setIterator(Symbol s) {
		iterator = s;
	}
	
	public Symbol getIterator() {
		if(iterator == null) 
			Main.addErrorMessage("The iterator is not set for " + this);
		return iterator;
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(itrWorkload != null) {
			return itrWorkload.getNewBlockWorkload(n, parallel);
		} else {
			if(parallel)
				return new OMPBlockWorkload(n, this);
			else
				return new BlockWorkload(n, this);
		}
	}
	
	
	@Override
	public void add(OMPWorkload s) {
		if(itrWorkload == null) {
			if(s.getClass() == BlockWorkload.class ||
			   s.getClass() == OMPBlockWorkload.class) {
				setItrWorkload((BlockWorkload) s);
			} else if(s.getClass() == ForWorkload.class ||
					s.getClass() == ConditionalWorkload.class ||
					s.getClass() == WhileWorkload.class) {
				setItrWorkload(new OMPBlockWorkload(s.getNode(),this));
				itrWorkload.add(s);
			} else{
				super.add(s);
			}
		} else {
			itrWorkload.add(s);
		}
	}
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(itrWorkload != null && itrWorkload.isLive())	
			return itrWorkload.getCurrentBlockWorkload(n);
		else
			return this;
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(itrWorkload == null) {
			setItrWorkload(new OMPBlockWorkload(n, this));
		}
		return itrWorkload.getExprWorkload(n);
	}
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		return itrWorkload.getBarrierWorkload(n);
	}
	
	public void setItrWorkload(BlockWorkload block) {
		assert(block.getParent() == this);
		itrWorkload = block;
	}
	
	public ArrayList<Node> exitPoints() {
		
		ArrayList<Node> exitPoints = new ArrayList<Node>();
		for (int i = 0; i < itrWorkload.childWorkloads.size(); i++) {
			if(itrWorkload.childWorkloads.get(i).getClass() == ReturnWorkload.class ||
					itrWorkload.childWorkloads.get(i).getClass() == BreakWorkload.class)
				exitPoints.add(itrWorkload.childWorkloads.get(i).getNode());
		}
		
		return null;
	}
	
	public boolean continuousIterations() {
		return continuousIterations;
	}
	
	public boolean  equalWorkloadIterations() {
		return equalWorkloadIterations;
	}
	
	public void continuousIterations(boolean b) {
		continuousIterations = b;
	}
	
	public void equalWorkloadIterations(boolean b) {
		equalWorkloadIterations = b;	
	}
	
	
	
	public IterationWorkload(Node n, OMPWorkload p) {
		super(n, p);
		continuousIterations = false;
		equalWorkloadIterations = false;
		incrementalWorkloads = new ArrayList<BlockWorkload>();
		setIterations(null);
	}

	public TotalIterations getIterations() {
		if(iterations.isEmpty()) {
			Main.addErrorMessage("Iterations not properly set");
			iterations = new TotalIterations(SystemStringConstants.cesTotalIterationPlaceHolder);
		}
		return iterations;
	}

	public void setIterations(TotalIterations iterations) {
		this.iterations = iterations;
	}
	
	
	
	
	
	
	


	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		if(getStatus() != DataStatus.ReductionsCollected) {
		
		if(accdata == null)
			accdata =  new AccumulatedData();
		
		itrWorkload.accumulateReductions(accdata);
		accdata.reductions.multiply(getIterations().getString());
		setStatus(DataStatus.ReductionsCollected);
		}
		cur.reductions.add(accdata.reductions);
		return cur;
	}
	
	
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		
		/*
		 * TODO we are not handling While inside master
		 * 
		 */
		if(getStatus() != DataStatus.MasterCollected) {
		assert(collect == false);
		itrWorkload.accumulateMaster(accdata, collect);
		accdata.masterOperations.multiply(getIterations().getString());
		setStatus(DataStatus.MasterCollected);
		}
		cur.masterOperations.add(accdata.masterOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		/*
		 * TODO  we are not handling While  inside single
		 * 
		 */
		if(getStatus() != DataStatus.SingleCollected) {
		assert(collect == false);
		itrWorkload.accumulateSingle(accdata, collect);

		accdata.singleOperations.multiply(getIterations().getString());
		setStatus(DataStatus.SingleCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		/*
		 * TODO We are not handling While inside critical
		 * 
		 */
		if(getStatus() != DataStatus.CriticalCollected) {
		assert(collect == false);
		itrWorkload.accumulateCritical(accdata, collect);
	
		accdata.criticalOperations.multiply(getIterations().getString());
		setStatus(DataStatus.CriticalCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	

	
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		/*
		 *  NO writes to array in conditions so we are probably safe here 
		 * with false sharing
		 */
		if(iterator != null)
			assert(!iterator.getName().contains("["));
		if(getStatus() != DataStatus.FalseSharingCollected) {
			itrWorkload.accumulateFalseSharing(accdata, displacement);
			setStatus(DataStatus.FalseSharingCollected);
		}
		cur.falseSharingOperations.addSharingFalseSharing(accdata.falseSharingOperations);
		return cur;
	}
	
	public void accumulateChildData() {
		itrWorkload.accumulatedata(accdata);
	}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(getStatus() != DataStatus.BranchesCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
		
		itrWorkload.accumulateBranches(accdata);
		this.accdata.branchOperations.addconstPart(1);
		this.accdata.branchOperations.multiply(getIterations().getString());
		setStatus(DataStatus.BranchesCollected);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
	
		/**
		 * TODO rather than multiplying itrValue for all 
		 * iteration we should only multiply actual number if possible.
		 * 
		 */
		getItrWorkload().accumulateReentrantNodes(current*itrValue, itrValue);
	}


}
