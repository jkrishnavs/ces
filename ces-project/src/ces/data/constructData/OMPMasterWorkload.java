/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.CompoundStatement;
import imop.ast.node.MasterConstruct;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import imop.ast.node.Statement;
import ces.SystemConfigFile;
import ces.data.classifier.ClassificationDetails;
import ces.enums.DataStatus;
import ces.enums.EigenMetric;
import ces.enums.JKPROJECT;
import ces.enums.Metric;
import ces.enums.OptimizationLevel;
import ces.enums.ThreadScheduling;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
import imop.parser.CParser;

public class OMPMasterWorkload extends OMPWorkload {
	
	BlockWorkload block;
	
	
	public OMPMasterWorkload(Node n, OMPWorkload p) {
		super(n, p);
		block  = null;
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	
	private void setMasterWorkload(BlockWorkload bW) {
		assert(bW.getParent() == this);
		block = bW;
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(block == null) {
			setMasterWorkload(new BlockWorkload(n, this));
		}
		return block.getExprWorkload(n);
	}
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if (block == null) {
			return this;
		}
		else
			return block.getCurrentBlockWorkload(n);
	}
	
	@Override
	public void add(OMPWorkload s) {
		if(block == null) {
			if(s.getClass() == BlockWorkload.class) {
				setMasterWorkload((BlockWorkload) s);
			} else if(s.getClass() == ExprWorkload.class) {
				setMasterWorkload( new BlockWorkload(s.node, this));
			} else {
				super.add(s);
			}
			
		} else {
			block.add(s);
		}
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(block == null) 
			return new BlockWorkload(n, this);
		else
			return block.getNewBlockWorkload(n, false);
	}
	
	
	
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(getStatus() != DataStatus.BranchesCollected) {
		if(accdata  == null)
			accdata = new AccumulatedData();
		
		for (OMPWorkload bW : block.childWorkloads) {
			bW.accumulateBranches(accdata);
		}
		accdata.branchOperations.addvarPart("N_THREADS");
		setStatus(DataStatus.BranchesCollected);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {return cur;}
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) { return cur; }
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) { 
		if(getStatus() != DataStatus.MasterCollected) {
		assert(collect == false);
		for(OMPWorkload child : block.childWorkloads) {
			child.accumulateMaster(accdata, true);
		}
		accdata.masterOperations.addbranch(accdata.branchOperations);
		setStatus(DataStatus.MasterCollected);
		}
		cur.masterOperations.add(accdata.masterOperations);
		return cur;
	}
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		accdata.totalOps.add(accdata.masterOperations);
		accdata.totalOps.addVarPart(SystemStringConstants.numThreadsString, Metric.branches);
		accdata.totalOps.addMasterCalls(1);
		setStatus(DataStatus.DataCollected);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) { assert(collect == false); return cur;}
	
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		block.accumulateReentrantNodes(current, itrValue);
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			Main.addErrorMessage("Unhandled transformation ");
			return;
		}
		
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		
		
		if(getStatus() != DataStatus.Transformed) {
			if(cd.getSchedulingDetails() == ThreadScheduling.ThreadtoCoreFix && 
					 shouldtransform(cd)) {
				assert(node.getClass() == MasterConstruct.class);
				MasterConstruct masterNode = (MasterConstruct) node;
				Statement enclosingStatement = (Statement) CesSpecificMisc.getAncestorOfType(Statement.class.getName(), node);
				assert(enclosingStatement != null);
				assert(cd != null);
				int prefferedCore = cd.scheduleMaster(this);
				String converted = " { if( omp_get_thread_num() == " + prefferedCore + " )  " 
						+ masterNode.f3.getInfo().getString() + " } "; 
				
				Main.addlog( "The master region has been converted to " + converted);
				CompoundStatement cs = 	(CompoundStatement) CParser.createCrudeASTNode(converted, CompoundStatement.class);
				assert(cs != null);
				enclosingStatement.f0.updateChoice(cs, 2);
			}
			setStatus(DataStatus.Transformed);
		} 
	}

	private boolean shouldtransform(ClassificationDetails cd) {
		if(SystemConfigFile.optimizeSinglAndMasterForCes == false)
			return false;
		return true;
	}
	
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		/**
		 * we don't need it.
		 */
	}

	
	/*
	public CoreType getPrefferedCore() {
		return prefferedCore;
	}


	public void setPrefferedCore(CoreType prefferedCore) {
		this.prefferedCore = prefferedCore;
	}
	
	*/
}
