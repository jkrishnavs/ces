/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.*;
import ces.enums.ControlTypes;

public class ContinueWorkload extends OMPWorkload {

	public ContinueWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}
	
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	
	
//	@Override
//	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds,
//			ArrayList<BaseSymbolToVariableMap> baseList) {
//		// do nothing?
//	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}

}
