/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.*;
import ces.data.base.Counter;
import ces.enums.Metric;

public class BreakWorkload extends OMPWorkload {

	public BreakWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}
	
	@Override
	public boolean isCompoundWorkload() {
	
		return false;
	}
	
	
	@Override
	public boolean isLive() {
		return false;
	}
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		cur.branchOperations.addconstPart(1);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) { 
		cur.totalOps.addConstPart(1, Metric.branches);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		assert(collect == false);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		assert(collect == false);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		//assert(collect ==false);
		cur.singleOperations.addbranch(new Counter(1, ""));
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		assert(collect == false);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		return cur;
	}

	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}
	
}
