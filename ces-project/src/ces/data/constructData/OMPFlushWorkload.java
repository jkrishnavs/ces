/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.awt.BufferCapabilities.FlipContents;
import java.util.ArrayList;

import imop.ast.node.*;
import ces.getter.HierarchyASTNodeGetter;

public class OMPFlushWorkload extends OMPWorkload{
	
	ArrayList<String> flushVariables;
	
	boolean conditionalExecution;

	public OMPFlushWorkload(Node n, OMPWorkload p) {
		super(n, p);
		flushVariables = new ArrayList<String>();
		conditionalExecution = false;
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	
	@Override
	public boolean isLive() {
		return false;
	}
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	
	public boolean isConditionallyExecuted(){
		return conditionalExecution;
	}
	
	public void addFlushVariables(String s) {
		flushVariables.add(s);
	}
	
	public void addFlushVariables(ArrayList<String> s) {
		flushVariables = s;
	}
	
	public ArrayList<String> getAllFlushVariables() {
		return flushVariables;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {  assert(collect == false); return cur; }
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) { return cur; }
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) { return cur;}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) { 
		// assert (displacement == null || displacement.isEmpty())
		return cur;}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) { 
		assert(collect ==false);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) { 
		cur.totalOps.addFlushCalls(flushVariables.size());
		return cur;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		FlushDirective flushNode =  (FlushDirective) node;
		NodeOptional nl = flushNode.f2;
		HierarchyASTNodeGetter varGetter = new HierarchyASTNodeGetter();
		varGetter.reset();
		nl.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}		
	}

}
