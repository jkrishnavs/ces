/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import ces.data.base.Counter;
import ces.data.classifier.ClassificationDetails;
import ces.data.node.ArrayExpression;
import ces.enums.DataStatus;

public class OMPCriticalWorkload extends OMPWorkload {

	public Counter sharedMemUpdates;
	
	public BlockWorkload criticalLoad;
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(criticalLoad == null)
			return this;
		else
			return criticalLoad.getCurrentBlockWorkload(n);
	}
	
	private void setCriticalLoad(BlockWorkload bW) {
		assert(bW.getParent() == this);
		criticalLoad = bW;
		
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(criticalLoad == null) {
			setCriticalLoad(new BlockWorkload(n, this));
		}
		return criticalLoad.getExprWorkload(n);
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(criticalLoad == null) {
			return new BlockWorkload(n, this);
		} else {
			return criticalLoad.getNewBlockWorkload(n, false);
		}
	}
	
	@Override
	public void add(OMPWorkload s) {
		if(criticalLoad == null) {
			if(s.getClass() == BlockWorkload.class ) {
				criticalLoad = (BlockWorkload) s;
			} else {
				Main.addErrorMessage("The critical load should not be null");
				setCriticalLoad(new BlockWorkload(s.getNode(), this));
				criticalLoad.add(s);
			}
		} else {
			criticalLoad.add(s);
		}
	}
	
	
	public OMPCriticalWorkload(Node n, OMPWorkload p) {
		super(n, p);
		criticalLoad = null;
	}
	
	public Counter getSharedMemoryUpdates(){
		return sharedMemUpdates;
	}
	
	public void setSharedMemoryUpdates(Counter c){
		sharedMemUpdates = c;
	}
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		criticalLoad.addArrayAccess(list);
	}
	
	public void addSharedMemoryUpdates(Object c){
		if(sharedMemUpdates ==  null)
			sharedMemUpdates =  new Counter();
		if(c instanceof Counter)
			sharedMemUpdates.add((Counter) c);
		else 
			sharedMemUpdates.addconstPart((double) c);
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.CriticalCollected) {
		assert(collect == false);
		for(OMPWorkload child : criticalLoad.childWorkloads) {
			child.accumulateCritical(accdata, true);
		}
		accdata.criticalOperations.addbranch(accdata.branchOperations);
		setStatus(DataStatus.CriticalCollected); 
		}
		cur.criticalOperations.add(accdata.criticalOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(getStatus() != DataStatus.BranchesCollected) {
		if(accdata == null)
			accdata =  new AccumulatedData();
		
		for(OMPWorkload child : criticalLoad.childWorkloads) {
			child.accumulateBranches(accdata);
		}
		setStatus(DataStatus.BranchesCollected);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		//assert(displacement == null || displacement.isEmpty());
		for(OMPWorkload c: criticalLoad.childWorkloads) {
			c.accumulateFalseSharing(cur, displacement);
		}
		return cur;
	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		accdata.totalOps.add(accdata.criticalOperations);
		accdata.totalOps.addCriticalCalls(1);
		cur.totalOps.add(accdata.totalOps);
		setStatus(DataStatus.DataCollected);
		}
		return cur;
	}
	
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		// have reductions also go for a flag?
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		if(collect == true) {
			accdata.unevenWorkload.add(accdata.criticalOperations);
		}
		cur.unevenWorkload.add(accdata.criticalOperations);
		return cur;
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		// DO nothing ?
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		// Do nothing
	}
	
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		criticalLoad.accumulateReentrantNodes(current, itrValue);
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		criticalLoad.getVariablesofInterest(varList);
	}
}
