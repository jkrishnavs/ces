/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

public class PrivatizationList {
	ArrayList<String> privateVars;
	
	public PrivatizationList(ArrayList<String> list){
		privateVars = list;
	}
	
	public String getAllPrivateStrings(){
		String retVal = privateVars.get(0);
		for (int i = 1; i < privateVars.size(); i++) {
			retVal += privateVars.get(i);
		}
		return retVal;
	}
}
