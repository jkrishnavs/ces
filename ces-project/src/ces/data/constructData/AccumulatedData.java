/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import imop.Main;
import ces.SystemConfigFile;
import ces.data.base.*;
import ces.data.memoryOps.FalseSharing;
import ces.enums.EigenMetric;

public class AccumulatedData {
	
	Counter reductions;
	BlockCounter singleOperations;
	BlockCounter masterOperations;
	BlockCounter criticalOperations;
	UnevenWorkCounter unevenWorkload;
	Counter branchOperations;
	FalseSharing falseSharingOperations;
	
	boolean evaluated;

	OmpParallelDataCounter totalOps;
	
	public OmpParallelDataCounter totalOps() {
		return totalOps;
	}
	
	public Counter reductions() {
		return reductions;
	}
	
	public BlockCounter singleOperations() {
		return singleOperations;
	}
	
	public BlockCounter masterOperations() {
		return masterOperations;
	}
	
	public BlockCounter criticalOperations() {
		return criticalOperations;
	}
	
	public UnevenWorkCounter unevenWorkload() {
		return unevenWorkload;
	}
	
	public Counter branchOperations() {
		return branchOperations;
	}
	
	public FalseSharing falseSharingOperations() {
		return falseSharingOperations;
	}
	
	public AccumulatedData() {
		reductions = new Counter();
		singleOperations = new BlockCounter();
		masterOperations = new BlockCounter();
		criticalOperations = new BlockCounter();
		unevenWorkload = new UnevenWorkCounter();
		branchOperations = new Counter();
		falseSharingOperations = new FalseSharing();
		totalOps = new OmpParallelDataCounter();
		evaluated = false;
	}
	
	double reductionData;
	double singleData;
	double masterData;
	double criticalData;
	double unEvenData;
	double branchData;
	double falseData;
	double totData;
	
	public void evaluate() {
		reductionData = reductions.evaluate();
		singleData = singleOperations.evaluate();
		masterData = masterOperations.evaluate();
		criticalData = criticalOperations.evaluate();
		unEvenData = unevenWorkload.evaluate();
		branchData = branchOperations.evaluate();
		falseData = falseSharingOperations.evaluate();
		totData = totalOps.evaluate();
		if(SystemConfigFile.ALUFACTOR)
			totData = totalOps.aluData;
		evaluated = true;
	}
	

	public double getALUEigenFactorValue(EigenMetric m) {
		assert(evaluated);
		switch(m) {
		case ompReductions:
			return reductionData/totData;
		case branches:
		case MemoryOps:
		case ompBarriers:
		case ompCritical:
		case ompFlush:
		case total:
		case ALUOps:
			return totalOps.getALUFactor(m);
		default:
			Main.addErrorMessage("Unexpected rach point "+ m);
		
		}
		
		return 1;	
	}
	
	
	public double getEigenFactorValue(EigenMetric m) {
		assert(evaluated);
		switch(m) {
		case ompReductions:
			return reductionData/totData;
		case branches:
		case MemoryOps:
		case ompBarriers:
		case ompCritical:
		case ompFlush:
		case total:
		case ALUOps:
			return totalOps.getFactor(m);
		default:
			Main.addErrorMessage("Unexpected rach point "+ m);
		
		}
		
		return 1;
	}

	public boolean isRegularWorkload() {
		
		if(unevenWorkload.isEmpty())
			return true;
		return false;
	}

}
