/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.DoStatement;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import imop.ast.node.WhileStatement;
import ces.data.classifier.ClassificationDetails;
import ces.enums.DataStatus;
import ces.getter.HierarchyASTNodeGetter;

public class WhileWorkload extends IterationWorkload {
	
	boolean isDoWhile;
	
	private ExprWorkload condWorkload;

	public WhileWorkload(Node n, boolean doWhile, OMPWorkload p) {
		super(n, p);
		isDoWhile = doWhile;
	}

	public ExprWorkload getCondWorkload() {
		return condWorkload;
	}

	public void setCondWorkload(ExprWorkload condWorkload) {
		this.condWorkload = condWorkload;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		Node condExpr = null;
		if(node.getClass() == WhileStatement.class)
			condExpr = ((WhileStatement)node).f2;
		else if (node.getClass() == DoStatement.class) {
			condExpr = ((DoStatement)node).f4;
		}
		HierarchyASTNodeGetter varGetter = new HierarchyASTNodeGetter();
		varGetter.reset();
		condExpr.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}
	}
	
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
	//	assert(collect == false);
		if(getStatus() != DataStatus.SingleCollected) {
		for(OMPWorkload child : getItrWorkload().childWorkloads) {
			child.accumulateSingle(accdata, collect);
		}
		condWorkload.accumulateSingle(cur, collect);
		accdata.singleOperations.multiply(getIterations().getString());
		setStatus(DataStatus.SingleCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	
	
	
	/*
	@Override
	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds,
			ArrayList<BaseSymbolToVariableMap> baseList) {
		
		TotalIterations loopBound;
		if(isDoWhile)
		 loopBound = CesSpecificMisc.getWhileIterations((DoStatement) node);
		else
		 loopBound =  CesSpecificMisc.getWhileIterations((WhileStatement) node);
		
		assert(!loopBound.isEmpty());
		
		ControlTypes lt = ControlTypes.WHILE;
		if(isDoWhile)
			lt = ControlTypes.DOWHILE;
		
		
		
		InternalAsymmetricScheduling is = new InternalAsymmetricScheduling(node, lt, getIterations(), getIterator(), baseList);
		
		ArrayList<BaseSymbolToVariableMap> childList = new ArrayList<BaseSymbolToVariableMap>();
		childList.addAll(baseList);
		BaseSymbolToVariableMap newVarMap = new BaseSymbolToVariableMap(getIterator());
		childList.add(newVarMap);
		
		
		ArrayList<InternalAsymmetricScheduling> inl = new ArrayList<InternalAsymmetricScheduling>();
		for (OMPWorkload c : getItrWorkload().childWorkloads) {
			c.collectInnerLoopDetails(inl, childList);
		}
		
		
		is.setInnerLoopSceduling(inl);
		internalLoopBounds.add(is);
		
	}
	
	*/
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		accumulateChildData();
		condWorkload.accumulatedata(accdata);
		accdata.totalOps.multiply(getIterations().getString());
		setStatus(DataStatus.DataCollected);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		assert(collect == false);
		return super.accumulateCritical(cur, collect);
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		if(getStatus() !=  DataStatus.unEvenDataCollected) {
		boolean found  = false;
		assert(collect == false);
		if(itrDetails != null) {
			for( String s : itrDetails ) {
				if(s.equals(getIterations())) {
					Main.addlog("Found the uneven workload");
					found = true;
				}
			}
		}
		ArrayList<String> newItrDetails = new ArrayList<String>();
		if( itrDetails != null) {
			newItrDetails.addAll(itrDetails);
		}
		if(getIterator() != null)
			newItrDetails.add(getIterator().getName());
		
		for(OMPWorkload child : getItrWorkload().childWorkloads) {
			child.accumulateUnevenWorkloads(accdata, found, newItrDetails);
		}
		setStatus(DataStatus.unEvenDataCollected);
		}
		cur.unevenWorkload.add(accdata.unevenWorkload);
		return cur;
		
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		for(OMPWorkload c : getItrWorkload().childWorkloads) {
			c.transformIrregularCode(cd);
		}
	}
	
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		for(OMPWorkload c : getItrWorkload().childWorkloads) {
			c.transformCode(cd);
		}
	}

}
