/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.data.base.Counter;
import ces.data.classifier.ClassificationDetails;
import ces.enums.DataStatus;
import ces.enums.JKPROJECT;
import ces.enums.ThreadScheduling;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
import imop.parser.CParser;

public class OMPSingleWorkload extends OMPWorkload {
	
	
	BlockWorkload singleBlock;
	
	OMPBarrierWorkload bW;
	boolean hasNoWait;
	boolean barrierAdded;
	
	//private CoreType prefferedCore;
	
	@Override
	public void closeWorkload() {
		super.closeWorkload();
	}
	
	private void setSingleLoad(BlockWorkload bW){
		assert(bW.getParent() == this);
		singleBlock = bW;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		singleBlock.getVariablesofInterest(varList);
	}
	

	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(singleBlock == null) {
			return new BlockWorkload(n, this);			
		}
		else
			return singleBlock.getNewBlockWorkload(n, false);
	}
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(singleBlock !=  null) {
			return singleBlock.getCurrentBlockWorkload(n);
		}
		return this;
	}
	
	@Override
	public void add(OMPWorkload s) {
		if(singleBlock == null) {
			if(s.getClass() == BlockWorkload.class) {
				setSingleLoad((BlockWorkload) s);
			} else if(s.getClass() == ExprWorkload.class ||
					s.getClass() == ForWorkload.class) {
				setSingleLoad(new BlockWorkload(s.node, this));
				singleBlock.add(s);
			} else  
				super.add(s);
		} else if (s.getClass() == OMPBarrierWorkload.class) { 
			bW = (OMPBarrierWorkload) s;
		} else {
			singleBlock.add(s);
		}
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(singleBlock == null)
			setSingleLoad(new BlockWorkload(n, this));
		return singleBlock.getExprWorkload(n);
	}
	
	public void hasNoWait(boolean b) {
		hasNoWait = b;
		
	}
	
	public boolean hasNoWait() {
		return hasNoWait;
	}

	public OMPSingleWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(getStatus() != DataStatus.BranchesCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
		for (OMPWorkload bW : singleBlock.childWorkloads) {
			bW.accumulateBranches(accdata);
		}
		accdata.branchOperations.addvarPart(SystemStringConstants.numThreadsString);
		setStatus(DataStatus.BranchesCollected);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	
	
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		assert(collect == false);
		return cur;
	}
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		for(OMPWorkload c: singleBlock.childWorkloads) {
			c.accumulatedata(accdata);
		}
		accdata.totalOps.addbranch(new Counter(SystemStringConstants.numThreadsString));
		if(bW != null)	
			bW.accumulatedata(accdata);
		setStatus(DataStatus.DataCollected);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) { return cur; }
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) { return cur;}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) { assert(collect == false); return cur;}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(getStatus() !=  DataStatus.SingleCollected) {
		assert(collect == false);
		Main.addlog(this.node.getInfo().getString());
		for(OMPWorkload c: singleBlock.childWorkloads) {
			Main.addlog(c.node.getInfo().getString());
			c.accumulateSingle(accdata, true);
		}
		accdata.singleOperations.addbranch(accdata.branchOperations);
		setStatus(DataStatus.SingleCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}

	
	/*
	public CoreType getPrefferedCore() {
		return prefferedCore;
	}

	public void setPrefferedCore(CoreType prefferedCore) {
		if(this.prefferedCore != CoreType.none)
			assert(this.prefferedCore == prefferedCore);
		this.prefferedCore = prefferedCore;
	}
	
	*/
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			Main.addErrorMessage("Unhandled transformation ");
			return;
		}
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		
		if(getStatus() != DataStatus.Transformed) {
			if(cd.getSchedulingDetails() == ThreadScheduling.ThreadtoCoreFix && shouldtransform(cd) ) {
				assert(node.getClass() == SingleConstruct.class);
				SingleConstruct singleNode = (SingleConstruct) node;
				Statement enclosingStatement = (Statement) CesSpecificMisc.getAncestorOfType(Statement.class.getName(), node);
				assert(enclosingStatement != null);
				assert(cd != null);
				String converted = " { if( omp_get_thread_num() == " + cd.scheduleSingle(this) + " )  " + singleNode.f4.getInfo().getString();
				if(!hasNoWait()) {
					converted += SystemStringConstants.barrierString;
				}
				converted += " } "; 
				
				Main.addlog( "The Single region has been converted to " + converted);
				CompoundStatement cs = 	(CompoundStatement) CParser.createCrudeASTNode(converted, CompoundStatement.class);
				assert(cs != null);
				enclosingStatement.f0.updateChoice(cs , 2);
				
				
			}
			setStatus(DataStatus.Transformed);
		} 
		
	}
	
	
	private boolean shouldtransform(ClassificationDetails cd) {
		
		if(SystemConfigFile.optimizeSinglAndMasterForCes == false)
			return false;
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		singleBlock.accumulateReentrantNodes(current, itrValue);
	}
}
