/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.ast.info.CesForConstructinfo;
import ces.data.base.*;
import ces.data.base.IrregularityData.workloadcalculation;
import ces.data.ces.*;
import ces.data.classifier.ClassificationDetails;
import ces.enums.*;
import ces.getter.*		;
import ces.util.*;
import ces.util.CesSpecificMisc.copyType;
import imop.lib.util.Misc;
import imop.parser.CParser;
import stringexp.CESString;
import stringexp.Minterms;


public class OMPForWorkload extends OMPWorkload {

	OMPForIterationWorkload forIterationWorkload;
	
	OMPBarrierWorkload bW;
	
	TotalIterations totalIterations;
	String iterator;
	
	int dynamicBlockSize;
	
	
	
	
	boolean isPeriodicallyIncreasing;
	boolean isPeriodicallyDecreasing;
	
	
	
	ArrayList<OMPReductionWorkload> reductions;
	ArrayList<PrivatizationList> plist;
	
	CesForConstructinfo getForInfo() {
		return (CesForConstructinfo) node.getInfo();
	}
	
	
	public TotalIterations getTotalIterations() {
		return totalIterations;
	}
	
	@Override
	public void addPrivatizationList(PrivatizationList p) {
		plist.add(p);
	}
	
	@Override
	public boolean isParallel() {
		return true;
	}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(accdata == null) {
			accdata = new AccumulatedData();
			forIterationWorkload.accdata = new AccumulatedData();
		}
		
		for (OMPWorkload child: forIterationWorkload.childWorkloads) {
			child.accumulateBranches(this.accdata);
			child.accumulateBranches(forIterationWorkload.accdata);
		}
		this.accdata.branchOperations.addconstPart(1);

	//	this.accdata.branchOperations.multiply(totalIterations);
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}

	
	public void addReductionWorkload(OMPReductionWorkload r) {
		reductions.add(r);
	}
	
	public void addReductionsStatement(Node statement,String s) {
		for (OMPReductionWorkload r : reductions) {
			if(r.reductionVariable == s ) {
				r.addReductionStatments(statement);
				return;
			}
		}
	}
	
	
	

	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(forIterationWorkload != null) {
			return forIterationWorkload.getCurrentBlockWorkload(n);
		} else {
			return this;
		}
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(forIterationWorkload == null) {
			return new OMPForIterationWorkload(n, this);
		} else {
			return forIterationWorkload.getNewBlockWorkload(n, true);
		}
	}
	
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		Main.addlog(n.getInfo().getString());
		assert(forIterationWorkload!= null);
		return forIterationWorkload.getExprWorkload(n);
	}
	
	
	@Override
	public void add(OMPWorkload s) {
		
		if(s.getClass() == OMPForIterationWorkload.class) {
			assert(forIterationWorkload == null);
			forIterationWorkload = (OMPForIterationWorkload) s;
		} else if(s.getClass() == OMPReductionWorkload.class) {
			addReductionWorkload((OMPReductionWorkload) s);
		} else if(s.getClass() == OMPBarrierWorkload.class) {
			bW = (OMPBarrierWorkload) s;
		} else {
			assert(forIterationWorkload != null);
			forIterationWorkload.add(s);
		}
	}
	
	public ExprWorkload getInitialExprWorkload() {
		return initialExprWorkload;
	}
	public void setInitialExprWorkload(ExprWorkload initialExprWorkload) {
		this.initialExprWorkload = initialExprWorkload;
		initialExprWorkload.setParent(this);
	}

	public ExprWorkload getIncrementalOMPWorkload() {
		return incrementalOMPWorkload;
	}
	public void setIncrementalOMPWorkload(ExprWorkload incrementalOMPWorkload) {
		this.incrementalOMPWorkload = incrementalOMPWorkload;
	}

	public ExprWorkload getConditionalWorkload() {
		return conditionalWorkload;
	}
	public void setConditionalWorkload(ExprWorkload conditionalWorkload) {
		this.conditionalWorkload = conditionalWorkload;
	}
	
	
	
/*
	public void setIterations(String s) {
		totalIterations = s;
	}
	*/
	
	
	
	public void setIterator(String s) {
		iterator = s;
	}


	public boolean isPeriodicallyIncreasing() {
		return isPeriodicallyIncreasing;
	}
	
	public void isPeriodicallyIncreasing(boolean b) {
		isPeriodicallyIncreasing = b;
	}
	
	public void isPeriodicallyDecresing(boolean b) {
		isPeriodicallyDecreasing  = b;
	}	
	
	
	public boolean isPeriodicallyDecreasing() {
		return isPeriodicallyDecreasing;
	}
	
	
	/*
	public void setIncrementalWorkloads(OMPForIterationWorkload f) {
		incrementalWorkloads.add(f);
	}
	
	
	public ArrayList<OMPForIterationWorkload> getIncrementalWorkloads() {
		return incrementalWorkloads;
	} 
	
	*/
	
	public boolean isItrVauleKnownAtCompileTime() {
		return itrCompileTime;
	}
			
	
	public OMPForWorkload(Node n, OMPWorkload p) {
		super(n, p);
		itrCompileTime = false;
		forIterationWorkload =  null;
		reductions  = new ArrayList<OMPReductionWorkload>();
		plist = new ArrayList<>();
		decrementalIteration = false;
	}
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		if(getStatus() != DataStatus.ReductionsCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
		
		for (OMPReductionWorkload red : reductions) {
			assert(node.getClass() == ForConstruct.class ||
					node.getClass() == ParallelForConstruct.class);
			ReductionGetter rG = new ReductionGetter(red.reductionVariable);
			node.accept(rG);
			if(rG.getWriteList().size() == 0) {
				Main.addErrorMessage("The reduction list is null");
			}
			
			accdata.reductions.addconstPart(rG.getWriteList().size());
			accdata.reductions.multiply(totalIterations.getString());
		}
		setStatus(DataStatus.ReductionsCollected);
		}
		cur.reductions.add(accdata.reductions);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		if(getStatus() != DataStatus.FalseSharingCollected) {

			if(accdata == null)
				accdata = new AccumulatedData();	
			//		if(displacement == null) {
			//			Main.addErrorMessage("all displacements are  null");
			//		} else {
			//			Main.addErrorMessage(" We have " + displacement);
			//		} 

			assert(displacement == null);
			displacement = iterator;
			assert(displacement != null);
			for(OMPWorkload child: forIterationWorkload.childWorkloads) {
				child.accumulateFalseSharing(accdata, displacement);
				child.accumulateFalseSharing(forIterationWorkload.accdata, displacement);
				
			}

			//		for(OMPWorkload child : f.childWorkloads ) {
			//			if(child.getClass() == OMPForWorkload.class) {
			//				if(displacement.isEmpty()) {
			//					Main.addErrorMessage("displacement not proper");
			//				}
			//				OMPForWorkload childFor = (OMPForWorkload) child;
			//				childFor.accumulateFalseSharing(accdata, iterator);
			//			} else {
			//				
			//			}
			//		}
			
			/***
			 * TODO : we are not considering the total iterations while considering false sharing 
			 * 
			 */
			setStatus(DataStatus.FalseSharingCollected);
		}
		cur.falseSharingOperations.addSharingFalseSharing(accdata.falseSharingOperations);
		return cur;
	}

	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(getStatus() !=  DataStatus.CriticalCollected){
		assert(collect == false);
		for (OMPWorkload child : forIterationWorkload.childWorkloads) {
			child.accumulateCritical(accdata, collect);
			child.accumulateCritical(forIterationWorkload.accdata, collect);
		}
		accdata.criticalOperations.multiply(totalIterations.getString());
		setStatus(DataStatus.CriticalCollected);
		}
		cur.criticalOperations.add(accdata.criticalOperations);
		return cur;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		
		OmpForCondition condExpression = CesSpecificMisc.getForConditionExpression(node);
		HierarchyASTNodeGetter varGetter = new HierarchyASTNodeGetter();
		varGetter.reset();
		condExpression.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}
		/**
		 * We need to get into the OMP constructs only for Irregular workloads as 
		 * a result We have added the filter of the project.
		 * TODO remove the filer
		 */
		
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			for(OMPWorkload wl : forIterationWorkload.childWorkloads) {
				wl.getVariablesofInterest(varList);
			}
		}		
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		assert(collect == false);
		return cur;
	}

	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		assert(collect == false);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) {
		if(getStatus() != DataStatus.FalseSharingCollected) {
		ArrayList<String> newItrDetails = null;
		assert(collect == false);
		if(itrDetails == null) {
			newItrDetails = new ArrayList<String>();
		} else {
			newItrDetails = itrDetails;
		}
		
		if(collect) {
			// TODO check if the collected data is also there assume the assert to be ok now
		}
		// TODO : add displacement to list
		
		for(OMPWorkload child : forIterationWorkload.childWorkloads) {
			child.accumulateUnevenWorkloads(accdata, collect, newItrDetails);
			child.accumulateUnevenWorkloads(forIterationWorkload.accdata, collect, newItrDetails);
		}
		setStatus(DataStatus.FalseSharingCollected);
		}
		cur.unevenWorkload.add(accdata.unevenWorkload);
		return cur;
	}
	

	
	private ExprWorkload initialExprWorkload;
	private ExprWorkload incrementalOMPWorkload;
	private ExprWorkload conditionalWorkload;
	
	private OmpParallelDataCounter childdata;
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		for(OMPWorkload  child: forIterationWorkload.childWorkloads) {
			child.accumulatedata(accdata);
			child.accumulatedata(forIterationWorkload.accdata);
		}
		
		childdata = new OmpParallelDataCounter();
		childdata.add(accdata.totalOps);
		conditionalWorkload.accumulatedata(accdata);
		
		accdata.totalOps.multiply(totalIterations.getString());	
		
		initialExprWorkload.accumulatedata(accdata);
		incrementalOMPWorkload.accumulatedata(accdata);
	
		conditionalWorkload.accumulatedata(accdata);
		if(bW != null)
			bW.accumulatedata(accdata);
		
		setStatus(DataStatus.DataCollected);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	

	
	
	/*
	public void setPrefferedRation(double ratio){
		prefferedRatio = ratio;
	}
	private double prefferedRatio; // the big littleRatio.
	*/
	boolean hasdynamicSchedling; 
	boolean decrementalIteration; // set if iteration starts from N to 0;
	
	String updateFunctionString;
	
	
	
   void setdecrementalIteration() {
		decrementalIteration = true;
	}
	
	void hasdynamicScheduling() {
		hasdynamicSchedling = true;
	}
	

	
	
	public void updateString(String update) {
		updateFunctionString = update;
	}
	
	
	

	
	
	public double getIterationValue() {
		return itrValue;
	}
	
	
	
	
	long itrValue;
	boolean itrCompileTime;
	
	String startPoint="";
	
	private void setIterationValue(long v) {
		itrValue = v;
		itrCompileTime = true;
	}
	
	private void setTotalIterations(TotalIterations s) {
		totalIterations = s;
		Counter val = new Counter(totalIterations.getString());
		val.reduce();
		if(val.isconstant()) {
			setIterationValue(Math.round(val.getConstPart()));
		}
	}
	
	public void setIterationDetails(OmpForHeader header) {
		setIterator(header.f2.f0.tokenImage);
		OmpForReinitExpression update = header.f6;
		startPoint = header.f2.f2.getInfo().getString();
		if(update.f0.choice.getClass() == PostIncrementId.class ||
		   update.f0.choice.getClass() == PreIncrementId.class	) {
			updateFunctionString = "++";
			
		} else if(update.f0.choice.getClass() == PostDecrementId.class ||
				   update.f0.choice.getClass() == PreDecrementId.class ) {
			setdecrementalIteration();
			updateFunctionString = "--";
			
		} else if(update.f0.choice.getClass() ==  ShortAssignPlus.class ||
				update.f0.choice.getClass() ==  ShortAssignMinus.class  ||
				update.f0.choice.getClass() == OmpForAdditive.class     ||
				update.f0.choice.getClass() == OmpForSubtractive.class  ||
				update.f0.choice.getClass() == OmpForMultiplicative.class) {
			Main.addErrorMessage("The update mechanism not handled");
		}
		setTotalIterations(CesSpecificMisc.getNoofParallelIterationsinOmpFor(header));
		
	}
	
	
	public void setClauseDetails(NodeListOptional clauselist) {
	//	Main.addlog(clauselist.getInfo().getString());
		HierarchyASTNodeGetter ufcs = new HierarchyASTNodeGetter();
		clauselist.accept(ufcs, UniqueForClauseSchedule.class.getName());
		if(! ufcs.getASTNodes().isEmpty()) {
			UniqueForClauseSchedule sched =  (UniqueForClauseSchedule) ufcs.getASTNodes().get(0);
			String schedule = ((NodeToken)sched.f2.f0.choice).tokenImage;
			
			String scheduleData = sched.f3.getInfo().getString();
			if(!scheduleData.isEmpty()) {
				scheduleData = scheduleData.replace(",", "");
				scheduleData = scheduleData.trim();
				try {
					dynamicBlockSize = Integer.parseInt(scheduleData);
				} catch (Exception e) {
					dynamicBlockSize = 0;
				}
			}
			
		//	Main.addlog("Dynamic schedule size " + dynamicBlockSize);
			
			if(schedule.equals("dynamic") || schedule.equals("guided")) {
				hasdynamicScheduling();
			}
				
		}
	}
	
	
//	ArrayList<InternalAsymmetricScheduling> collectInnerLoopDetails() {
//		
//		ArrayList<InternalAsymmetricScheduling> internalLoopBounds = new ArrayList<InternalAsymmetricScheduling>();
//		CesForConstructinfo info =  (CesForConstructinfo) node.getInfo();
//		assert(info != null);
//		HashSet<Symbol> symbolList = info.getPrivateReads();
//		Symbol itrSymbol = null;
//		
//		for(Symbol s: symbolList) {
//			if(s.getName().equals(iterator)) {
//				itrSymbol = s;
//				break;
//			}
//		}
//		
//		assert(itrSymbol != null);
//	
//		BaseSymbolToVariableMap baseSymbol = new BaseSymbolToVariableMap(itrSymbol);	
//		ArrayList<BaseSymbolToVariableMap> baseSymbols = new ArrayList<BaseSymbolToVariableMap>();
//		baseSymbols.add(baseSymbol);
//		for(OMPWorkload child : forIterationWorkload.childWorkloads) {
//			child.collectInnerLoopDetails(internalLoopBounds, baseSymbols);
//		}
//		return internalLoopBounds;
//	}
	
	
	 private String combine(ArrayList<String> updateString , boolean reduce) {
			String combined  = "";
			boolean start = false;
			for(String s :updateString) {
				if(!containsStatement(s)) {
					if(start) {
						combined =  combined + "*" +s;
					}  else  if(!start) {
						combined = s;
						start = true;
					}
				} else {
					combined += combined + ";" + s;
					start = false;
				}
			}
			if(reduce)
				combined = CesStringMisc.getReducedString(combined);
			return combined;
	 }
	
	 @Override
	 public void transformIrregularCode(ClassificationDetails cd) {

		 
		 IrregularityData data = new IrregularityData(node);
		 data.addIterator(iterator);		 
		 data.addLoopBound(totalIterations.getString());

		 IrregularWorkloadGetter asymmetric = new IrregularWorkloadGetter(node, data);
		 
		 
		 
		 node.accept(asymmetric, data);
		 data.postProcessing();
		 
		 
		 
		 
		 
		 if(data.hasNoAsymmetricElement()) {
			 transformIterationsForCPPTemplate();
			 transformCode(cd);
			 return;
		 }
		 
		 assert(data.getMaxData() != null);

		 IrregularityType irType = data.getIrType();

		 if(irType  != IrregularityType.Edge && 
				 irType  != IrregularityType.REdge)
			 return;


		 IrregularityData.hasMultipleStatements = false;
		 IrregularityData.hasencountedIrregularity = false;
		 ArrayList<String> updateString = data.getIrregularityWorkloadString(workloadcalculation.OVERALL);
		 
		 
		 IrregularityData.hasMultipleStatements = false;
		 IrregularityData.hasencountedIrregularity = false;
		 ArrayList<String> chunkMeasure = data.getIrregularityWorkloadString(workloadcalculation.CHUNK);
		 IrregularityData.hasMultipleStatements = false;
		 IrregularityData.hasencountedIrregularity = false;
		 ArrayList<String> individualMeasure = data.getIrregularityWorkloadString(workloadcalculation.ITERATION);
		
	//	 updateString = getNormalization(updateString);
		 assert(!updateString.isEmpty());
		 if(updateString.size() == 1) {
			 double  scale  = CesStringMisc.getscale(updateString.get(0));
			 String updateStringFinal = CesStringMisc.scale(scale, updateString.get(0));
			 double scalechunk  = CesStringMisc.getscale(chunkMeasure.get(0));
			 assert(scalechunk == scale);
			 String chunkMeasureFinal = CesStringMisc.scale(scale, chunkMeasure.get(0));
			 double scaleind  = CesStringMisc.getscale(individualMeasure.get(0));
			 assert(scaleind == scale);
			 String individualMeasureFinal = CesStringMisc.scale(scale, individualMeasure.get(0));

			 String irregularityMeasureCalculation = SystemStringConstants.mainSumString + "  =  " + chunkMeasureFinal + ";\n" ;
			 cd.setIrregularityDetails(irType, updateStringFinal, individualMeasureFinal, 
					 totalIterations.getString(), irregularityMeasureCalculation);
		 } else {
			 String combinedWorkload = null;
			 String irregularityString = null;
			 String irregularityMeasureCalculation = null;
			 if(IrregularityData.hasMultipleStatements == false) {
			   combinedWorkload =  combine(updateString, true);
			   irregularityString = combine(individualMeasure, true);
			   String chunkString = combine(chunkMeasure, true);
			   
			     double  scale  = CesStringMisc.getscale(combinedWorkload);
				 combinedWorkload = CesStringMisc.scale(scale, combinedWorkload);
				 double scalechunk  = CesStringMisc.getscale(irregularityString);
				 assert(scalechunk == scale);
				 irregularityString = CesStringMisc.scale(scale, irregularityString);
				 double scaleind  = CesStringMisc.getscale(chunkString);
				 assert(scaleind == scale);
				 chunkString = CesStringMisc.scale(scale, chunkString);
			   
			   
			   irregularityMeasureCalculation = SystemStringConstants.mainSumString + "  =  " + chunkString + ";\n" ;
			 } else {
				 /**
				  * TODO
				  * How to differentiate between the irregularity measure of a single 
				  * 
				  */
				 
				 if(IrregularityData.hasencountedIrregularity == true) {
					 /**
					  * We cannot apply chuncking as the degree of irregularity which depends on the 
					  * base iterator is higher.
					  * So we should use individual measure to obtain chunk workload.
					  * 
					  * For example in TC we get
					  * 
					  * N(v) * N(v) where v is the vertex id.
					  * Sigma (N(v)*NV) not equals N(Sigma(v)) * N(Sigma(v))
					  */
					 combinedWorkload = combine(updateString, true);
					 irregularityString = combine(individualMeasure, false);
					 Main.addlog(irregularityString);
					 /**
					  * 
					  */
					 irregularityMeasureCalculation = SystemStringConstants.mainSumString + " = 0; \nfor (;"+ SystemStringConstants.mainIteratorString + "< "
							 +SystemStringConstants.mainEndString+"; "+SystemStringConstants.mainIteratorString+"++) {";
					irregularityMeasureCalculation += SystemStringConstants.mainSumString + "+=" + irregularityString + ";";		 
					 irregularityMeasureCalculation += "}";
				 } else {
					 
					 irregularityString = combine(chunkMeasure, true);
				 }
			 }
			 assert(irregularityMeasureCalculation !=null);

			 
			 
			 
			 cd.setIrregularityDetails(irType, irregularityString, combinedWorkload, totalIterations.getString(), 
					 irregularityMeasureCalculation);
		 }

		 Statement enclosingStatement = (Statement) CesSpecificMisc.getAncestorOfType(Statement.class.getName(), node);

		 if(dynamicBlockSize > 0)
			 ClassificationDetails.setIrrChunkSize(dynamicBlockSize); 

		 cd.setBigLittleRatio(data);


		 /**
		  * TODO Node irregularity not handled.
		  * 
		  */


		 if(cd.isSMP(cd.getSelected()))
			 return;

		 assert(node.getClass() == ForConstruct.class);
		 ForConstruct forConstruct = (ForConstruct) node;
		 childdata.evaluate();


		 accdata.unevenWorkload.evaluate();
		 Main.addlog(accdata.unevenWorkload.dumpdata());

		 String cs = "{\n";
		 String itr = iterator;

		 if(irType == IrregularityType.Edge || irType == IrregularityType.REdge) {
			 cs += "int tid = omp_get_thread_num();\n"
			 		+ " if(tid > 3) tid++;"
					 + "int __ces_jj = "+SystemStringConstants.startSched+"[tid];\n" 
					 + "int __ces_kk = "+SystemStringConstants.startSched+"[tid+1];\n"
					 + "for (;__ces_jj < __ces_kk; __ces_jj++) {\n"
					 + "node_t "+itr+" = "+SystemStringConstants.schedBlock+"[__ces_jj].start;\n"
					 + "for (;"+itr+"<"+SystemStringConstants.schedBlock+"[__ces_jj].end;"+itr+"++) \n"
					 + forConstruct.f3.getInfo().getString()
					 + "\n"
					 + "}\n";
			 
			 // Now do buffer 
			 String itr1 = "__i";
			 cs  += "int "+ itr1+";\n node_t "+itr+";\n" + "#pragma omp for schedule(dynamic,1)\n"
			 		+ "for ("+ itr1 + "=0; "+ itr1 +" < "
			 		+ SystemStringConstants.bufferSize + "; "+ itr1  + " ++ ) {"
			 		+ itr + " = " + SystemStringConstants.bufferBlock + "["+ itr1 +"] . start;"
			 		+ "for ( ;" + itr + " < " + SystemStringConstants.bufferBlock + "["+itr1+"].end;"
			 		+ itr + "++)\n"
			 		+ forConstruct.f3.getInfo().getString()
			 		+ "\n"
			 		+ "}\n";
			 		
	 
			 
			 // remove all #pragma associated with this parallel for.

			 CompoundStatementElement cseElement = (CompoundStatementElement) 
					 CesSpecificMisc.getAncestorOfType(CompoundStatementElement.class.getName(), enclosingStatement);

			 CompoundStatement compS = 
					 (CompoundStatement) CesSpecificMisc.getAncestorOfType(CompoundStatement.class.getName(), cseElement);

			 Vector<Node> nl = compS.f1.nodes;

			 int constPos = nl.indexOf(cseElement);
			 if(constPos > 0) {
				 int startPos = constPos;
				 CompoundStatementElement cse = (CompoundStatementElement) nl.get(startPos -1);
				 while((startPos != 0) && 
						 (! CesSpecificMisc.collectAllChildrensofClass(cse, UnknownPragma.class.getName()).isEmpty())) {
					 startPos --;
					 if(startPos != 0)
						 cse = (CompoundStatementElement) nl.get(startPos -1);
				 }

				 // Some pragmas to be removed.
				 /**
				  * TODO : These unknown pragma nodes need to be gracefully removed.
				  */
				 if(startPos != constPos) {
					 while(startPos != constPos) {
						 Node unKnownPragma = nl.remove(startPos);
						 Main.addlog(unKnownPragma.getInfo().getString());
						 assert(!CesSpecificMisc.collectAllChildrensofClass(unKnownPragma, UnknownPragma.class.getName()).isEmpty());
						 constPos --;
					 }	
				 }

			 }

		 } else if (irType == IrregularityType.Node) {
			 // TODO
		 } 
		 cs += "}\n";
		 Main.addlog(cs);
		 CompoundStatement csNode = 	(CompoundStatement) CParser.createCrudeASTNode(cs, CompoundStatement.class);
		 enclosingStatement.f0.updateChoice(csNode, 2); 
	 }


	
	
	
//	private ArrayList<String> getNormalization(ArrayList<String> boundString) {
//		int degree = 0;
//		for(String s : boundString) {
//			
//			if(s.equals(SystemStringConstants.edgesString)) {
//				degree ++;
//			}
//		}
//		
//		if(degree >= 2) {
//			for(String s : boundString) {
//				
//				if(s.equals(SystemStringConstants.edgesString)) {
//					boundString.set(boundString.indexOf(SystemStringConstants.edgesString), 
//							SystemStringConstants.edgesString + "/" + SystemStringConstants.nodeString);
//				}
//			}
//			
//			 boundString.add(SystemStringConstants.nodeString);
//				 /**
//				  * TODO : This is hard coded. Please remove it ASAP.
//				  */
//				 boundString.add(0,"1.2");
//		}
//		
//		return boundString;
//	}


	private boolean containsStatement(String s) {		
		if(s.contains(";"))
			return true;
		return false;
	}


	private void transformIterationsForCPPTemplate() {
		/**
		 * TODO: This should be done in Print COde. We should not do it here
		 * We should generate appropriate pragmas here.
		 */
		
		
		ForConstruct forConstruct = (ForConstruct) node;
		String initVal = CesSpecificMisc.getInitialValueforForConstructIterator(forConstruct);
		String finalVal = CesSpecificMisc.getFinalValueforForConstructIterator(forConstruct);
		CompoundStatementElement parentNode = 
				(CompoundStatementElement) CesSpecificMisc.getAncestorOfType(CompoundStatementElement.class.getName(), forConstruct);
		
		CompoundStatement compS = 
				(CompoundStatement) CesSpecificMisc.getAncestorOfType(CompoundStatement.class.getName(), parentNode);
		
		Vector<Node> nl = compS.f1.nodes;
		
		int constPos = nl.indexOf(parentNode);
		assert(constPos !=  -1);
		int startPos = constPos;
		
		if(startPos == 0)
			return;
		CompoundStatementElement cse = (CompoundStatementElement) nl.get(startPos -1);
		
		while(! CesSpecificMisc.collectAllChildrensofClass(cse, UnknownPragma.class.getName()).isEmpty() && startPos != 0 ) {
			startPos --;
			if(startPos != 0)
				cse = (CompoundStatementElement) nl.get(startPos -1);
		}
		
		if(startPos != constPos) {	
			
			while(startPos != constPos) {
				Node node = nl.remove(startPos);
				UnknownPragma upragma = 
						(UnknownPragma) CesSpecificMisc.collectAllChildrensofClass(node, UnknownPragma.class.getName()).get(0);
				String data =  upragma.f2.getInfo().getString();
				data = data.trim();
				if(data.startsWith("replace fnCall for_init")) {
					data = data.replace("replace fnCall for_init", "");
					data = data.replace(";", "");
					data = data.substring(data.indexOf("=") +1);
					
					totalIterations = new TotalIterations(totalIterations.getString().replace(initVal, data));
					
					data = data.trim();
					
				} else if(data.startsWith("replace fnCall fortest")) {
					data = data.replace("replace fnCall fortest", "");
					data = data.replace(";", "");
					data = data.substring(data.indexOf("<") +1);
					
					totalIterations =  new TotalIterations(totalIterations.getString().replace(finalVal, data));
					
					
					data = data.trim();
					
				} else if(data.startsWith("replace fnCall  increment")){
					Main.addErrorMessage("Not handled ");
				}else {
					Main.addErrorMessage("big Error");
				}				
				constPos --;
			}	
		}
		
	}


	@Override
	public void transformCode(ClassificationDetails cd) {
		assert(node.getClass() == ForConstruct.class ||
				node.getClass() == ParallelForConstruct.class);
		if(getStatus() != DataStatus.Transformed) {
			setStatus(DataStatus.Transformed);
			if(cd.getSchedulingDetails() == ThreadScheduling.Freeexecution) {
				if(hasdynamicSchedling || cd.isSMP(cd.getSelected()))
					return;
				else {
					assert(node.getClass() == ForConstruct.class);
					ForConstruct forC = (ForConstruct) node;
					NodeListOptional clauselist = forC.f1.f1.f0;
					HierarchyASTNodeGetter ufcs = new HierarchyASTNodeGetter();
					
					String clausesStr = "";
					
					for(Node n : clauselist.nodes) {
						ufcs.reset();
						clauselist.accept(ufcs, UniqueForClauseSchedule.class.getName());
						if(ufcs.getASTNodes().isEmpty())
							clausesStr += n.getInfo().getString() + " ";
					}
					
					
					
					ArrayList<String> chunkSize = cd.getDynamicChunkSize(this);
					String schedule = " schedule (dynamic" +  chunkSize.get(0) + ") ";
					clausesStr +=schedule + " ";
					
					String newForString = forC.f0.getInfo().getString() + " for " + clausesStr 
							+ forC.f1.f2.getInfo().getString() + " " + forC.f2.getInfo().getString() + " "
							+ forC.f3.getInfo().getString();
					ForConstruct newForC =  (ForConstruct) CParser.createCrudeASTNode(newForString, ForConstruct.class);
					//String schedule = " schedule(static,1) ";
					NodeChoice parent  = (NodeChoice) forC.parent;
					parent.updateChoice(newForC, parent.which);
					/*if(node.getClass() == ForConstruct.class) {
						list = ((ForConstruct)node).f1.f1.f0;
						AUniqueForOrDataOrNowaitClause c = (AUniqueForOrDataOrNowaitClause) 
								CParser.createCrudeASTNode(schedule, AUniqueForOrDataOrNowaitClause.class);
						list.addNode(c);
					}
					if(node.getClass() == ParallelForConstruct.class) {
						list = ((ParallelForConstruct)node).f3.f0;
						AUniqueParallelOrUniqueForOrDataClause c = (AUniqueParallelOrUniqueForOrDataClause) 
								CParser.createCrudeASTNode(schedule, AUniqueParallelOrUniqueForOrDataClause.class);
						list.addNode(c);	
					}*/
					Main.addlog(parent.getInfo().getString());
				}
				
			} else if(cd.getSchedulingDetails() == ThreadScheduling.ThreadtoCoreFix) {
				boolean canChange = preprocessForConstruct();
				if(canChange)
					cesTransformation(cd);
			} 
		}
	}
	
	String afterFor = "";

	protected void cesTransformation(ClassificationDetails cd) {
		if(cd.isSMP(cd.getSelected()))
			return;
		assert(node.getClass() == ForConstruct.class);
		ForConstruct forConstruct = (ForConstruct) node;
		childdata.evaluate();	
		boolean shouldSteal = false;
		boolean shouldUpdate = false;
		if(childdata.aluData > SystemConfigFile.BigLoopSize) {
			SystemStringFunctions.shouldSteal(true);
			shouldSteal = true;
		} if(getOccurances() > SystemConfigFile.UpdateOccuranceLimit) {
			SystemStringFunctions.shouldUpdate(true);
			shouldUpdate = false;
		} 
		
		Statement enclosingStatement = (Statement) CesSpecificMisc.getAncestorOfType(Statement.class.getName(), node);
		assert(enclosingStatement != null);
		Node itrNode = forConstruct.f2.f2.f0;
		FunctionDefinition enclosingFunction  =  (FunctionDefinition) Misc.getEnclosingFunction(forConstruct);
			
		String replacementVar = CesSpecificMisc.findandReplaceVariable(forConstruct.f3, itrNode, enclosingFunction, 
				copyType.noCopy, false);
		String typeString = CesSpecificMisc.getTypeStringWithoutAnySpecifiers(itrNode.toString(), itrNode);
		String initStr = typeString + " " + replacementVar + ";"; 
		
		
		String statementString = ((ForConstruct) node).f3.getInfo().getString();
		
		String forStr = "{ \n \t int " + SystemStringConstants.threadid +" = "+ 
				SystemStringConstants.getThreadIDfunction+ ";";
		/**
		 * TODO if decremental we 
		 * do not add stealing code
		 * should revisit later
		 */
		if(! shouldSteal || decrementalIteration ) {
		//	if(cd.isSMP(cd.getSelected()))
		//		return;
			
			//			String statementString = "";
			
			/*
			if(node.getClass() == ParallelForConstruct.class) {
				forStr += SystemStringConstants.parallelString + "\n {";
			} 
			*/
			String[] retVal = setIterationString(cd, false);
			forStr += retVal[1];
			
			
			assert(updateFunctionString != null);
			// change iterator to a private variable
		//	Node itrNode = forConstruct.f2.f2.f0;
		//	FunctionDefinition enclosingFunction  =  (FunctionDefinition) Misc.getEnclosingFunction(forConstruct);
		//	String replacementVar = CesSpecificMisc.findandReplaceVariable(forConstruct.f3, itrNode, enclosingFunction, 
		//			copyType.noCopy, false);
			
			/*
			if(node.getClass() == ParallelForConstruct.class)
				statementString = ((ParallelForConstruct) node).f6.getInfo().getString();
			else
			*/
			
			
			forStr += initStr;
			forStr += "for( " + replacementVar + "= startPoint; "+ replacementVar +" != endPoint; " + replacementVar 
					+ updateFunctionString + ") \n";
			forStr += statementString;
			if(node.getClass() == ParallelForConstruct.class) 
				forStr += "\n}";
			
			forStr += afterFor;
			
			if(!CesSpecificMisc.hasNowaitClause(forConstruct)) {
				forStr += SystemStringConstants.barrierString;
				
			}
			
			forStr += "\n}";
			CompoundStatement cs = 	(CompoundStatement) CParser.createCrudeASTNode(forStr, CompoundStatement.class);
			assert(cs != null);
			/**
			 * f0 -> ( LabeledStatement() 
			 *        | ExpressionStatement() 
			 *        | CompoundStatement() .... 
			 */
			enclosingStatement.f0.updateChoice(cs, 2);
		} else {
			/*
			 * Initialising the worklist boundaries.
			 * */
			// varName should save the ratio
			
		//	String forStr = "{ \n \t int " + SystemStringConstants.threadid +" = "+ 
		//			SystemStringConstants.getThreadIDfunction+ ";";
			
			String retVal[] = setIterationString(cd, shouldSteal);
			String boundaries = retVal[0];
			forStr += retVal[1];
			if(shouldUpdate) {
				forStr += SystemStringConstants.updateVariable + "["+SystemStringConstants.threadid+"] = 0;\n";
			//	forStr += "int __ces_sum = 0;";
			}
			CesForConstructinfo info = (CesForConstructinfo) node.getInfo();
			//String iteration  = forConstruct.f3.getInfo().getString();
			String whileString = typeString + " " + replacementVar + " = " +SystemStringConstants.doIterFunctionName+"("+SystemStringConstants.threadid +");\n";
			whileString  +=  "\n\n   while("+replacementVar+" != -1) { \n";
					//+ info.getIterationVariable() + "= __iter;\n" ;
			/**
			 * TODO we have switched off shouldupdate now.
			 * Should revisit later.
			 */
			if(shouldUpdate)
				whileString += SystemStringConstants.updateVariable + "["+SystemStringConstants.threadid +"]++;\n";

			whileString +=  statementString + "\n"
					+ replacementVar +"  = "+SystemStringConstants.doIterFunctionName+"("+SystemStringConstants.threadid +");\n"
					+ "\t /*** end of while ***/\n}\n ";
			
			forStr += whileString;	
		//	if(shouldUpdate) {
				// forStr += SystemStringConstants.atomicConstruct;
				//forStr += "__ces_sum += "+SystemStringConstants.updateVariable  + "["+SystemStringConstants.threadid+"];";
			//}
			
			forStr += afterFor;
			
			if(! CesSpecificMisc.hasNowaitClause(forConstruct)) {
				forStr += SystemStringConstants.barrierString;
			}
			
			String s = "";	
			assert(boundaries != null);
			if(shouldUpdate) {
				if(decrementalIteration) {
					s = " \n\n  if("+SystemStringConstants.threadid+ " != 0) \n" +
							/*SystemStringConstants.atomicConstruct + */
							SystemStringConstants.criticalStr +
							boundaries+"["+SystemStringConstants.threadid +"] = "+ 
							boundaries+"["+ SystemStringConstants.threadid +" +1]" + 
							" + (double)" + SystemStringConstants.updateVariable + "[" + 
							SystemStringConstants.threadid +"]/"+ totalIterations +"; \n ";   
				} else {
					s = "\n\n   if("+SystemStringConstants.threadid+ " != "+SystemStringConstants.numThreadsString+") \n" +
//							SystemStringConstants.atomicConstruct + 
							SystemStringConstants.criticalStr +
							boundaries+"["+SystemStringConstants.threadid +" + 1 ] = "+ 
							boundaries+"["+ SystemStringConstants.threadid +"]" + 
							" + (double)" + SystemStringConstants.updateVariable + "[" + 
							SystemStringConstants.threadid +"]/ "+ totalIterations +"; \n "; 
				}
				if(shouldSteal)
					s += " "+SystemStringConstants.locksVar+"["+SystemStringConstants.threadid+"] = INITIAL;\n";
				
			//	s += SystemStringConstants.barrierString;
			}
			forStr += s;
			forStr += "}";
			Main.addlog("\n\n\n\n\n" + forStr);
			CompoundStatement cs = 	(CompoundStatement) CParser.createCrudeASTNode(forStr, CompoundStatement.class);
			assert(cs != null);
			/**
			 * f0 -> ( LabeledStatement() 
			 *        | ExpressionStatement() 
			 *        | CompoundStatement() .... 
			 */
			enclosingStatement.f0.updateChoice(cs, 2);
		
		}

	}

	private String[] setIterationString(ClassificationDetails cd, boolean steal) {
		String boundaries ="";
		String forStr = "";
		String startVariable = "";
		String endVariable = "";
		
		
		
		
		if(steal) {
			startVariable = SystemStringConstants.iterationVar + "[ " + SystemStringConstants.threadid + "] ";
			endVariable = SystemStringConstants.endIterationVar + "[ " + SystemStringConstants.threadid + "] ";
		} else {
			startVariable = "int startPoint ";
			endVariable = "int endPoint ";
		}
		if(itrCompileTime) {
			boundaries = cd.setLoopBoundaries(this,  itrValue, decrementalIteration);
			assert(boundaries != null);
			if(decrementalIteration) {
				forStr += "     "+ endVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " ] ;\n" ;
				forStr += "     "+startVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " + 1 ] ;\n" ;	
			} else {
				forStr += "      "+startVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " ] ;\n" ;
				forStr += "       "+endVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " + 1 ] ;\n" ;
			}
		} else {
		//	Main.addlog(node.getInfo().getString());
			boundaries = cd.setLoopBoundaries(this,  totalIterations.getString(), decrementalIteration);
			String reducedItr;
			if(decrementalIteration) {
				// Iterations are stored in nregative val (end - start)
				reducedItr = CesStringMisc.getReducedString("(" + totalIterations.getString() + ")" );
				
				forStr += "      "+endVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " ] * "
						+ reducedItr + "  + " + startPoint +";\n" ;
				
				forStr += "       "+startVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " + 1 ] * "
						+ reducedItr +" + " + startPoint + ";\n" ;
				
			} else {
				reducedItr = CesStringMisc.getReducedString(totalIterations.getString());
				forStr += "      "+startVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " ] * "
					+ reducedItr + "  + " + startPoint+";\n" ;
				forStr += "      "+endVariable+" =  "+ boundaries + "[ " + SystemStringConstants.threadid + " + 1 ] * "
					+ reducedItr +" + " + startPoint + ";\n" ;
			}
		}
		String[] retVal = {boundaries,forStr};
		return retVal;
	}

	

	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		
		/**
		 *  TODO rather than multiplying itrValue for all 
		 * iteration we should only multiply actual number if possible.
		 * 
		 */
		for(OMPWorkload  child: forIterationWorkload.childWorkloads) {
			child.accumulateReentrantNodes(current*itrValue , itrValue);	
		}
	}

	
	private boolean preprocessForConstruct() {
		assert(node.getClass() == ForConstruct.class);

		ForConstruct forConstruct = (ForConstruct) node;
		
		
		if(itrCompileTime && itrValue < SystemConfigFile.forScheduleMinimumSize)
			return false;

		if(forConstruct.f1.f1.f0.size() == 0)
			return true;
		/*
		 * go through entire list and do necessary
		 * First find if the thensformation is possible after going through all the 
		 * clauses. Once trnsformation starts there is no turning back.
		 * */
		NodeListOptional clauselist = forConstruct.f1.f1.f0;
		int clauselistsize = clauselist.size();
		boolean canchange = true;
		ArrayList< ArrayList<Node> > dataclauseset = new ArrayList<ArrayList<Node>>();
		ArrayList<ReductionData> reductionlist = new ArrayList<ReductionData>();
		for (int j = 0; j <DataClauses.values().length; j++) {
			dataclauseset.add(new  ArrayList<Node>());
		}
		for (int i = 0; i < clauselistsize; i++) {
			AUniqueForOrDataOrNowaitClause clause = (AUniqueForOrDataOrNowaitClause) clauselist.elementAt(i);
			if(clause.f0.choice instanceof DataClause) {
				// add data clauses to list
				// we should not start the changes before we can assure that we can allow there changes.
				DataClause dataclause = (DataClause) clause.f0.choice;
				if(dataclause.f0.choice instanceof OmpPrivateClause){
					OmpPrivateClause privateclause = (OmpPrivateClause)dataclause.f0.choice;
					VariableList varList = privateclause.f2;
					Node curVar = varList.f0;
					(dataclauseset.get(DataClauses.OMPPRIVATECLAUSE.getValue())).add(curVar);	
					Vector<Node> remainingVars = varList.f1.nodes;
					for (int j = 0; j < remainingVars.size(); j++) {
						curVar = ((NodeSequence)remainingVars.get(j)).nodes.get(1);
						(dataclauseset.get(DataClauses.OMPPRIVATECLAUSE.getValue())).add(curVar);	
					}
				}else if(dataclause.f0.choice instanceof OmpFirstPrivateClause){
					OmpFirstPrivateClause privateclause = (OmpFirstPrivateClause)dataclause.f0.choice;
					VariableList varList = privateclause.f2;
					Node curVar = varList.f0;
					(dataclauseset.get(DataClauses.OMPFIRSTPRIVATECLAUSE.getValue())).add(curVar);	
					Vector<Node> remainingVars = varList.f1.nodes;
					for (int j = 0; j < remainingVars.size(); j++) {
						curVar = ((NodeSequence)remainingVars.get(j)).nodes.get(1);
						(dataclauseset.get(DataClauses.OMPFIRSTPRIVATECLAUSE.getValue())).add(curVar);	
					}
				}else if(dataclause.f0.choice instanceof OmpLastPrivateClause){
					OmpLastPrivateClause privateclause = (OmpLastPrivateClause)dataclause.f0.choice;
					VariableList varList = privateclause.f2;
					Node curVar = varList.f0;
					(dataclauseset.get(DataClauses.OMPLASTPRIVATECLAUSE.getValue())).add(curVar);	
					Vector<Node> remainingVars = varList.f1.nodes;
					for (int j = 0; j < remainingVars.size(); j++) {
						curVar = ((NodeSequence)remainingVars.get(j)).nodes.get(1);
						(dataclauseset.get(DataClauses.OMPLASTPRIVATECLAUSE.getValue())).add(curVar);	
					}
				}else if(dataclause.f0.choice instanceof OmpCopyinClause){
					OmpCopyinClause privateclause = (OmpCopyinClause)dataclause.f0.choice;
					VariableList varList = privateclause.f2;
					Node curVar = varList.f0;
					(dataclauseset.get(DataClauses.OMPCOPYINCLAUSE.getValue())).add(curVar);	
					Vector<Node> remainingVars = varList.f1.nodes;
					for (int j = 0; j < remainingVars.size(); j++) {
						curVar = ((NodeSequence)remainingVars.get(j)).nodes.get(1);
						(dataclauseset.get(DataClauses.OMPCOPYINCLAUSE.getValue())).add(curVar);	
					}
				}else if(dataclause.f0.choice instanceof OmpDfltSharedClause){
					/* 
					 * do nothing ???? 
					 * */
				}else if(dataclause.f0.choice instanceof OmpDfltNoneClause){
					/*
					 * Risky affair Try not to change this Parallel for
					 * construct unless and until we absolutely require it.
					 * else if compile it before changing if no error
					 * just go ahead. 
					 * */
				}else if(dataclause.f0.choice instanceof OmpReductionClause){
					/**
					 * Approach : 
					 * rename the occurrences t create a local variable with is initialised to
					 * the following table 
					 * Operator	Initial value
					 * +	0
					 * *	1
					 * -	0
					 * &	~0
					 * |	0
					 * ^	0
					 * &&	1
					 * ||	0
					 * 
					 * Then add reduction clause at the end of the loop with NO_OF_THREADS. 
					 * Make sure the for loops with size NO_OF_THREADS is not take as a candidate
					 * for transformation.
					 * 
					 */
					OmpReductionClause redclause = (OmpReductionClause) dataclause.f0.choice;
					String reductionop = redclause.f2.getInfo().getString();
					VariableList varlist = redclause.f4;
					String varName = varlist.f0.tokenImage;
					ReductionData newdata = new ReductionData(reductionop, varName, varlist.f0);
					newdata.setEnclosingNode(forConstruct);
					reductionlist.add(newdata);
					Vector<Node> list = varlist.f1.nodes;
					for (int j = 0; j < list.size(); j++) {
						String variableName = list.get(i).getInfo().getString();
						variableName = variableName.replaceAll("\\s", "");
						variableName = variableName.replaceAll(",","");
						Node curVar = ((NodeSequence)list.get(j)).nodes.get(1);
						newdata = new ReductionData(reductionop, variableName, curVar);
						newdata.setEnclosingNode(forConstruct);
						reductionlist.add(newdata);
					}	

				}else{
					Main.addErrorMessage("Unknown/Unexpected directive encountered"
							+ dataclause.f0.choice.toString() + ", Compilation might be faulty");
				}
			}else if(clause.f0.choice instanceof UniqueForClause){
				UniqueForClause forclause  = (UniqueForClause) clause.f0.choice;
				if(forclause.f0.choice.toString().equals("ordered")){
					canchange = false;
					break;
				}else if(forclause.f0.choice instanceof UniqueForClauseSchedule){
					// ignore scheduling 
					Main.addlog("Ignoring the Scheduling clause " + forclause.f0.choice.getInfo().getString());
				}else if(forclause.f0.choice instanceof UniqueForCollapse){
					Main.addlog("Ignoring the Collapsing clause " + forclause.f0.choice.getInfo().getString());
				}

			}else if(clause.f0.choice instanceof NowaitClause){
				Main.addlog("Ignoring the NoWait clause ");
			}else
				Main.addErrorMessage("Unexpected for clause " + clause.f0.choice.getInfo().getString());
			
			
			
		}

		if(canchange){
			FunctionDefinition enclosingFunction  =  (FunctionDefinition) Misc.getEnclosingFunction(forConstruct);
			
			/*update data clauses changes*/
			for(DataClauses cls : DataClauses.values()){
				ArrayList<Node> varList = dataclauseset.get(cls.getValue());
				CesSpecificMisc.copyType ctype = copyType.INVALID;
				if(cls == DataClauses.OMPPRIVATECLAUSE){
					ctype = copyType.noCopy;
				}else if(cls == DataClauses.OMPFIRSTPRIVATECLAUSE || cls == DataClauses.OMPCOPYINCLAUSE){
					ctype = copyType.copyin;
				}else if(cls == DataClauses.OMPLASTPRIVATECLAUSE){
					ctype = copyType.copyout;
				}
				for (int i = 0; i < varList.size(); i++) {
					CesSpecificMisc.findandReplaceVariable(forConstruct.f3, varList.get(i), enclosingFunction
							,ctype, true);
				}
			}
			CompoundStatement enclosingcs = (CompoundStatement) Misc.getEnclosingBlock(node);
			NodeListOptional nl = enclosingcs.f1;
			Vector<Node> elems = nl.nodes;
			int forPos = (elems).indexOf(CesSpecificMisc.getEnclosingNodeFromNodeList(nl, node));
		
			
			/*updates for reduction data*/
			for (int i = 0; i < reductionlist.size(); i++) {
				
				/*replace with local variables */
				ReductionData data = reductionlist.get(i);	
				String replacementName = CesSpecificMisc.findandReplaceVariable(forConstruct.f3, data.getCurrentVar(), enclosingFunction, 
						copyType.noCopy, false);
				/* add init statement */
				Node  dec  = CParser.createCrudeASTNode(data.getinitilizationStatement(replacementName), 
						Declaration.class);
				nl.addNodeatPosition(forPos,dec);
				/* final copy back*/
				forPos = (elems).indexOf(CesSpecificMisc.getEnclosingNodeFromNodeList(nl, node));
				//		Main.addlog(data.getReductionStatement());
				afterFor += "\n" + data.getReductionStatement() + "\n";
			} 
		}
		return canchange;
	}

	public int getFalseSharingMaximumStride() {
		return accdata.falseSharingOperations.getMaximumStride();
	}
}
