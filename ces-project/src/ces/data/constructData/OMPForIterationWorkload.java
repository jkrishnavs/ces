/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.Node;

public class OMPForIterationWorkload extends BlockWorkload {
	
	ArrayList<OMPFlushWorkload> flushList;
	ArrayList<OMPCriticalWorkload> criticalList;
	

	@Override
	public boolean isParallel() {
		return true;
	}
	
	
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(childWorkloads.isEmpty() || !childWorkloads.peek().isLive()) {
			return new  OMPBlockWorkload(n, this);
		} else {
			return childWorkloads.peek().getNewBlockWorkload(n, parallel);
		}
	}
	
	
	public OMPForIterationWorkload(Node n, OMPWorkload p) {
		super(n, p);
		flushList = new ArrayList<OMPFlushWorkload>();
		criticalList = new ArrayList<>();
	}
	
	@Override
	public void add(OMPWorkload s) {
		if (!childWorkloads.isEmpty() && childWorkloads.peek().isLive()
				&& childWorkloads.peek().isCompoundWorkload()) {
			childWorkloads.peek().add(s);
		} else {
			if(s.getClass() == OMPCriticalWorkload.class) {
				addCriticalWorkload((OMPCriticalWorkload) s);
				addChildWorkload(s);
			} else if(s.getClass() == OMPFlushWorkload.class) {
				addFlushWorkload((OMPFlushWorkload) s);
				addChildWorkload(s);
			} else
				super.add(s);
		}
	}
	public void addCriticalWorkload(OMPCriticalWorkload c) {
		criticalList.add(c);
	}
	
	public void addFlushWorkload(OMPFlushWorkload f) {
		flushList.add(f);
	}

	public ArrayList<OMPFlushWorkload> getFlushList() {
		return flushList;
	}
	
	public ArrayList<OMPCriticalWorkload> getCriticalList() {
		return criticalList;
	}
	

	
	
}
