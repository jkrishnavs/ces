/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.Stack;

import imop.Main;
import imop.ast.info.FunctionDefinitionInfo;
import imop.ast.node.*;
import ces.data.node.ArrayExpression;
import imop.lib.cg.CallSite;

public class OMPFunctionWorkload extends OMPWorkload {
	
	
	private ArrayList<OMPParallelWorkload> parRegions; // Individual Parallel regions.
	
	
	public String functionName;
	
	private FunctionDefinition function;
	private Stack<OMPWorkload> childWorkloads;
	
	
	private ArrayList<EmbeddedParallelRegions> embeddedParallel;
	

	
	
	
	public Stack<OMPWorkload> getChildWorkloads() {
		return childWorkloads;
	}
	
	
	private void addParallelRegion(OMPParallelWorkload w) {
		if (this.parRegions.contains(w)) {
			w.incrementOccuranceFrequency();
		} else {
			this.parRegions.add(w);
		}
	}
	
	
	public void addEmbeddedRegion(EmbeddedParallelRegions w) {
		if (this.embeddedParallel.contains(w)) {
			((OMPParallelWorkload) w.getParallelRegion()).incrementOccuranceFrequency();
		} else {
			this.embeddedParallel.add(w);
		}
	}
	
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		
	}
	
	
	
	private void addToChildWorkload(OMPWorkload w) {
		if(w.getParent() == null)
			w.setParent(this);
		assert(w.getParent() == this);
		childWorkloads.push(w);
		if(w.getClass() == OMPParallelWorkload.class) {
			addParallelRegion((OMPParallelWorkload) w);
		}
	}
	
	public void removeBarrierWorkload(OMPParallelWorkload bW) {
		// IMP TODO : We are not removing the child workload
		// as for some benchmarks, the stack of child workloads is
		// dynamically resized as it falls below certain size(Issue with java Arraylist) . 
		// As a result
		// the childWorkload stack address changes and we are left at
		// an inconsistent state. For more details see
		// the place of call of this function.
		// This means we will still have empty barrier workloads in 
		// child Workloads.
		// childWorkloads.remove(bW);
		
		
		// assumption embedded parallel regions are not empty.
		assert(bW.isEmbeddedRegion() != true);
		
		boolean found = parRegions.remove(bW);
		if(!found)
			Main.addErrorMessage("The barrier not found in parregions");
	}
	
	
	/*
	private void addToChildWorkload(int pos, OMPWorkload w) {
		w.setParent(this);
		childWorkloads.add(pos, w);
		if(w.getClass() == OMPParallelWorkload.class) {
			addParallelRegion((OMPParallelWorkload) w);
		}
	}
	*/
	/*
	private void removeChildWorkload(OMPWorkload child) {
		childWorkloads.remove(child);
	}
	*/
	private void addToChildWorkload(int pos, Stack<OMPWorkload> c) {
		for (OMPWorkload child : c) {
			child.setParent(this);
			childWorkloads.add(pos++, child);
			if(child.getClass() == OMPParallelWorkload.class) {
				addParallelRegion((OMPParallelWorkload) child);;
			}
		}	
	}
	
	private void addEmbeddedWorkload(ArrayList<EmbeddedParallelRegions> e) {
		for (EmbeddedParallelRegions child : e) {
			addEmbeddedRegion(child);
		}
	}
	
	
	@Override
	public OMPFunctionCallWorkload getNewFunctionCallWorkload(Node n, OMPWorkload p, CallSite callSite) {
		OMPFunctionCallWorkload fcW = new OMPFunctionCallWorkload(n, p, callSite);
		fcW.setParent(this);
		addToChildWorkload(fcW);
		return fcW;
	}
	

	
	@Override
	public void closeWorkload() {
		super.closeWorkload();
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(childWorkloads.isEmpty()) {
			return new OMPBlockWorkload(n, this);
		} else if (! childWorkloads.peek().isLive()) {
			return new BlockWorkload(n, this);
		} else {
			return childWorkloads.peek().getNewBlockWorkload(n, parallel);
		}
	}
	
	public OMPBlockWorkload getIndParRegionData() {
		if(childWorkloads.size() > 1)
			assert(childWorkloads.get(1).getClass() == OMPParallelWorkload.class);
		assert(childWorkloads.get(0).getClass()  ==  OMPBlockWorkload.class);
		return (OMPBlockWorkload) childWorkloads.get(0);
	}
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		
		if(!childWorkloads.empty() && childWorkloads.peek().isLive())
			childWorkloads.peek().addArrayAccess(list);
		else {
			super.addArrayAccess(list);
		}
	}
	
	@Override
	public void addReadArrayList(ArrayList<ArrayExpression> list) {

		if(!childWorkloads.empty() && childWorkloads.peek().isLive())
			childWorkloads.peek().addReadArrayList(list);
		else {
			super.addArrayAccess(list);
		}
	}
	
	
	/*
	public boolean hasParallelRegions() {
		if(parRegions.size() > 0)
			return true;
		return false;
	}
	
	*/
	
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(childWorkloads.isEmpty() || !childWorkloads.peek().isLive() 
				|| childWorkloads.peek().getClass() == ExprWorkload.class)	
			return this;
		else 
			return childWorkloads.peek().getCurrentBlockWorkload(n);
	}
	
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(!childWorkloads.peek().isLive()) { // extression WOrkload is always live
			addToChildWorkload(new ExprWorkload(n, this));
		}
		return childWorkloads.peek().getExprWorkload(n);
	}
	
	public void closeBarrier(boolean embedded) {
		if(embedded == false && parRegions.size() != 0) {
			if(embeddedParallel.size() != 0)
				assert(((OMPParallelWorkload)embeddedParallel.get(
					embeddedParallel.size()-1).getParallelRegion()).isIncomplete() == false);
			parRegions.get(parRegions.size()-1).setComplete();
		} else if (embedded && embeddedParallel.size() != 0){
			if(parRegions.size() != 0)
				assert(parRegions.get(parRegions.size()-1).isIncomplete() == false);
			((OMPParallelWorkload)embeddedParallel.get(
					embeddedParallel.size()-1).getParallelRegion()).setComplete();
		} else {
			Main.addErrorMessage("Some error in parallel region collection ");
		}
	}
	
	/*
	public void normalizeWorkloads() {
		for (int i=0;i<childWorkloads.size();i++) {
			if(childWorkloads.get(i).getClass() ==  OMPBarrierWorkload.class ) {
				OMPBarrierWorkload bW = (OMPBarrierWorkload) childWorkloads.get(i);
				if(bW.isIncomplete()) {
					boolean completed = false;
					while((!completed) &&  i !=  childWorkloads.size() -1) {
						OMPBarrierWorkload bWNext = (OMPBarrierWorkload) childWorkloads.get(i+1);
						bW.add(bWNext);
						childWorkloads.remove(bWNext);
						parRegions.remove(bWNext);
						if(bWNext.isIncomplete() == false) {
							completed = true;
							bW.setComplete();
						}
					}
				}
			}
		}
	}
	*/
	
	public boolean resolveFunctionCall(OMPWorkload caller, OMPFunctionWorkload callee) {

		 
		 if (caller.getClass() == OMPFunctionCallWorkload.class) {
			if(caller.isInsideParallelRegion()) {
				BlockWorkload cpW = (BlockWorkload) caller.getParent();
				int callerPos = cpW.childWorkloads.indexOf(caller) +1;
				assert(callerPos != -1);
				for(OMPWorkload cW : callee.childWorkloads) {
					assert(cW.getClass() != OMPParallelWorkload.class); // nested parallelism
					assert(cW.hasEmbeddedParallelRegion() == false);
					cW.setParent(cpW);
					cpW.addChildWorkload(callerPos++, cW);
				}
			} else if(caller.getParent() == this) {
				//  +1 will ensure that the data is added after the 
				// FuntionCallWorkload
				int callerPos = childWorkloads.indexOf(caller) +1;
				addToChildWorkload(callerPos, callee.childWorkloads);
				
			} else {
				BlockWorkload cpW = (BlockWorkload) caller.getParent();
				
				int callerPos = cpW.childWorkloads.indexOf(caller) +1;
				assert(callerPos != -1);

				for(OMPWorkload cW : callee.childWorkloads) {
				
					if(cW.getClass() == OMPParallelRegionWorkload.class ) {
						Main.addErrorMessage("parallel region inside blocks NOt handled right Now");	
					}
					/**
					 * Update the embedded parallel region list of the callee 
					 * function.
					 * TODO: The update should be done on the outermost block in the current function call.
					 */
					if(cW.hasEmbeddedParallelRegion() == true) {
						cpW.appendParallelRegions(cW.embeddedParallelRegion);
						for(EmbeddedParallelRegions childPR : cW.embeddedParallelRegion) {
							childPR.containers.add(0, cpW);
						}
					}
					cW.setParent(cpW);
					cpW.addChildWorkload(callerPos++, cW);
				}
			}	
			addEmbeddedWorkload(callee.embeddedParallel);
		} else {
			Main.addErrorMessage("Unknow Caller " + caller);
		}
		return true;
	}
	
	public boolean isExternal() {
		return true;
	}
	

	public OMPFunctionWorkload(Node n, OMPWorkload p) {
		super(n, p);
		parRegions = new ArrayList<OMPParallelWorkload>();
		FunctionDefinition funcDef = (FunctionDefinition) n;
		function(funcDef);
		childWorkloads = new Stack<OMPWorkload>();
		functionName = ((FunctionDefinitionInfo)n.getInfo()).getFunctionName();
		embeddedParallel = new ArrayList<EmbeddedParallelRegions>();
		
	}
	
	
	
	
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		if(parRegions.size() >0) {
			return parRegions.get(parRegions.size() -1);
		}
		return null;
	
	}
	
	

	@Override
	public void add(OMPWorkload n) {
		if(n instanceof BeginWorkload) {
			childWorkloads.add(n);
		//	Main.addErrorMessage("StartWorkload added");
		} else if (n instanceof EndWorkload) {
			childWorkloads.add(n);
		}else if(n instanceof OMPParallelWorkload) {
			OMPWorkload curW = getCurrentBlockWorkload(n.node);
			if(curW == this || curW.isOuterMostBlock()) {
				
			} else {
				if(curW.isInsideParallelRegion()) {
					
				}
			}

			OMPParallelWorkload curBW = getBarrierWorkload(null);
			if(parRegions.isEmpty()) {
				/**
				 * Removing assertion as it no longer  holds
				 * The  startNode will also break the initial
				 * nodes. 
				 */
		//		assert(childWorkloads.size() == 1);
				childWorkloads.peek().closeWorkload();
			}
			if(curBW != null) {
			//	Main.addlog("Hello");
				if(curBW.isLive())
					curBW.closeWorkload();
				
			}
			addToChildWorkload(n);
		} else if(n instanceof OMPFunctionWorkload) {
			super.add(n);
		} else if(n.getClass() == OMPBlockWorkload.class  && childWorkloads.isEmpty()) {
			addToChildWorkload(n);
		} else if(n.getClass() == BlockWorkload.class  && ! childWorkloads.isEmpty()) {
			addToChildWorkload(n);
		} else if(n.getClass() == ConditionalWorkload.class ||
				n.getClass() == ForWorkload.class) {
			if(childWorkloads.isEmpty() || !childWorkloads.peek().isLive()
					|| !childWorkloads.peek().isCompoundWorkload()){
				addToChildWorkload(n);
			} else {
				childWorkloads.peek().add(n);
			}
			
		} else {
		//	Main.addlog("Tring to add " + n + " to function "+ this.functionName + " From code " + n.node.getInfo().getString());
			if(childWorkloads.isEmpty() || !childWorkloads.peek().isLive()
					|| !childWorkloads.peek().isCompoundWorkload()){
				addToChildWorkload(n);
			} else {
				childWorkloads.peek().add(n);
			}
		}
		
	}

	@Override
	public String dumpdata() {
		String retVal = "The number of parallel regions in "+ 
				((FunctionDefinitionInfo)function().getInfo()).getFunctionName() + 
				" is " + parRegions.size() + "\n";
		for (OMPParallelWorkload bW : parRegions) {
			retVal += "******* PARALLEL REGION ********************";
	
			retVal += bW.dumpdata();	
		}	
		return retVal;
	}


	public FunctionDefinition function() {
		return function;
	}


	public void function(FunctionDefinition function) {
		this.function = function;
	}


}
