/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import imop.Main;
import imop.ast.node.*;
import ces.data.classifier.ClassificationDetails;
import ces.enums.DataStatus;
import ces.getter.HierarchyASTNodeGetter;

public class ForWorkload extends IterationWorkload {
	
	private ExprWorkload initialExprWorkload;
	private ExprWorkload incrementalExprWorkload;
	private ExprWorkload conditionalWorkload;
	
	
	public ForWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}

	public ExprWorkload getInitialExprWorkload() {
		return initialExprWorkload;
	}
	public void setInitialExprWorkload(ExprWorkload initialExprWorkload) {
		this.initialExprWorkload = initialExprWorkload;
	}

	public ExprWorkload getIncrementalOMPWorkload() {
		return incrementalExprWorkload;
	}
	public void setIncrementalOMPWorkload(ExprWorkload incrementalOMPWorkload) {
		this.incrementalExprWorkload = incrementalOMPWorkload;
	}

	public ExprWorkload getConditionalWorkload() {
		return conditionalWorkload;
	}
	public void setConditionalWorkload(ExprWorkload conditionalWorkload) {
		this.conditionalWorkload = conditionalWorkload;
	}
	
	
	
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
			accumulateChildData();
			incrementalExprWorkload.accumulatedata(accdata);
			conditionalWorkload.accumulatedata(accdata);
			accdata.totalOps.multiply(getIterations().getString());
			initialExprWorkload.accumulatedata(accdata);
			setStatus(DataStatus.DataCollected);
		}
	//	accdata.totalOps.addbranch(accdata.branchOperations);
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.CriticalCollected) {
			getItrWorkload().accumulateCritical(accdata, collect);	
			
			if(collect) {
				incrementalExprWorkload.accumulateCritical(accdata, collect);
				conditionalWorkload.accumulateCritical(accdata, collect);
			}
		
			accdata.criticalOperations.multiply(getIterations().getString());
		
			if(collect) {
				initialExprWorkload.accumulateCritical(accdata, collect);
			}
			setStatus(DataStatus.CriticalCollected);
		}
		cur.criticalOperations.add(accdata.criticalOperations);
		
		return cur;
	}


	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.SingleCollected) {
			getItrWorkload().accumulateSingle(accdata, collect);
			if(collect) {
				incrementalExprWorkload.accumulateSingle(accdata, collect);
				conditionalWorkload.accumulateSingle(accdata, collect);
			}
			accdata.singleOperations.multiply(getIterations().getString());
		
			if(collect)
				initialExprWorkload.accumulateSingle(accdata, collect);
			setStatus(DataStatus.SingleCollected);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
	//	assert(collect == false);
		if(getStatus() != DataStatus.MasterCollected) {
			getItrWorkload().accumulateMaster(accdata, collect);
			if(collect) {
				incrementalExprWorkload.accumulateMaster(accdata, collect);
				conditionalWorkload.accumulateMaster(accdata, collect);
			}
			accdata.masterOperations.multiply(getIterations().getString());
			if(collect)
				initialExprWorkload.accumulateMaster(accdata, collect);
			setStatus(DataStatus.MasterCollected);
		}
		cur.masterOperations.add(accdata.masterOperations);
		return cur;
	}
	
	
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		if(getStatus() != DataStatus.unEvenDataCollected) {
			boolean found  = false;
			assert(collect == false);
			if(itrDetails != null) {
				for( String s : itrDetails ) {
					if(s.equals(getIterations())) {
						Main.addlog("Found the uneven workload");
						found = true;
					}
				}
			}
			ArrayList<String> newItrDetails = new ArrayList<String>();
			if( itrDetails != null) {
				newItrDetails.addAll(itrDetails);
			}
			newItrDetails.add(getIterator().getName());
		
			getItrWorkload().accumulateUnevenWorkloads(accdata, collect, newItrDetails);
			setStatus(DataStatus.unEvenDataCollected);
		}
		cur.unevenWorkload.add(accdata.unevenWorkload);
		return cur;
		
	}
	
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		for(OMPWorkload c : getItrWorkload().childWorkloads) {
			c.transformCode(cd);
		}
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		for(OMPWorkload c : getItrWorkload().childWorkloads) {
			c.transformIrregularCode(cd);
		}
	}
	
	/*
	
	@Override
	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds,
		ArrayList<BaseSymbolToVariableMap> baseList) {
		
		ForStatement fs = (ForStatement) node;
		
		String loopBound = CesSpecificMisc.getForIterations(fs).toString();
		HashSet<Symbol> symTable =  null;
		if(fs.f8.f0.choice.getClass() == CompoundStatement.class) {		
			CompoundStatementInfo csinfo = (CompoundStatementInfo) ((ForStatement) node).f8.f0.choice.getInfo();
			symTable = csinfo.getSymbolTable();
		}
		
		
		
		InternalAsymmetricScheduling is = new InternalAsymmetricScheduling(node, ControlTypes.FORLOOP, getIterations(), getIterator(), baseList);
		
		ArrayList<InternalAsymmetricScheduling> inl = new ArrayList<InternalAsymmetricScheduling>();
		
		
		ArrayList<BaseSymbolToVariableMap> childList = new ArrayList<BaseSymbolToVariableMap>();
		childList.addAll(baseList);
		BaseSymbolToVariableMap newVarMap = new BaseSymbolToVariableMap(getIterator());
		childList.add(newVarMap);
		
		
		if(symTable != null) {
			for(Symbol s : symTable) {
				BaseSymbolToVariableMap newMap =  new BaseSymbolToVariableMap(s);
				childList.add(newMap);
			}
		}
		
		
		for (OMPWorkload c : getItrWorkload().childWorkloads) {
			c.collectInnerLoopDetails(inl, childList);
			
		}
		is.setInnerLoopSceduling(inl);
		
		internalLoopBounds.add(is);
	}
	
	*/
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		Node conditionalNode = ((ForStatement)node).f4;
		
		HierarchyASTNodeGetter varGetter = new HierarchyASTNodeGetter();
		varGetter.reset();
		conditionalNode.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}
		getItrWorkload().getVariablesofInterest(varList);
		
		
		Node startNode = ((ForStatement)node).f2;
		
		varGetter.reset();
		startNode.accept(varGetter, PostfixExpression.class.getName());
		for( Node n : varGetter.getASTNodes()) {
			varList.add((PostfixExpression) n);
		}
		getItrWorkload().getVariablesofInterest(varList);
	}
	
	
}
