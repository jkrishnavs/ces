/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.Node;

public class OMPReductionWorkload extends OMPWorkload {
	
	String reductionVariable;
	ArrayList<Node> reductionStatments;
	String reductionOperation;

	
	public OMPReductionWorkload(Node n, String rop, OMPWorkload p) {
		super(n, p);
		reductionStatments =  new ArrayList<Node>();
		reductionVariable = n.toString();
		reductionOperation = rop;
	}
	
	
	public String getReductionOperation() {
		return reductionOperation;
	}
	
	public String getVaribale(){
		return reductionVariable;
	}
	
	public void addReductionStatments(Node reductionstatement){
		reductionStatments.add(reductionstatement);
	}

	public ArrayList<Node> getallReductionStatements(){
		return reductionStatments;
	}
	
	public int numberofReductionStatments(){
		return reductionStatments.size();
	}
	

	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		return super.accumulateReductions(cur);
	}

}
