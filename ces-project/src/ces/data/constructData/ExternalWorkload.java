/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.*;
import ces.enums.DataStatus;

public class ExternalWorkload extends BlockWorkload {

	public ExternalWorkload(Node n, OMPWorkload p) {
		super(n, p);
		setStatus(DataStatus.ShouldNotCollect);
	}
	
	
	public boolean isExternal() {
		return true;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}
	
	public void addPrivatizationList(PrivatizationList p) {}
}
