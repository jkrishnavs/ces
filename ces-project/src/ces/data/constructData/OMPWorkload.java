/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import imop.Main;
import imop.ast.info.CesInfo;
import imop.ast.node.*;
import ces.data.classifier.ClassificationDetails;
import ces.data.node.ArrayExpression;
import ces.enums.DataStatus;
import ces.enums.Metric;
import imop.lib.analysis.type.Operator;
import imop.lib.cg.CallSite;
import imop.lib.util.OMPWorkloadInterface;

public abstract class OMPWorkload implements OMPWorkloadInterface {
	public Node node;
	boolean live = true;
	
	static int counter = 0;
	
	private int __id;
	
	AccumulatedData accdata;
	
	private OMPWorkload parent;
	
	DataStatus status;
	
	
	/**
	 * EmbeddedParallelRegions provides a list of 
	 * all parallel regions that are embedded deep inside
	 * the code and is not visible at the function level.
	 * When we are expanding a function call we need to 
	 * update the information of the caller node with
	 * the callee's information. 
	 */
	ArrayList<EmbeddedParallelRegions> embeddedParallelRegion;
	
	public ArrayList<EmbeddedParallelRegions> getEmbeddedDetails(){
		return embeddedParallelRegion;
	}
	
	public void appendParallelRegions(ArrayList<EmbeddedParallelRegions> r) {
		if(embeddedParallelRegion == null)
			embeddedParallelRegion = new ArrayList<>();
		this.embeddedParallelRegion.addAll(r);
	}

	public boolean hasEmbeddedParallelRegion(){
		return (embeddedParallelRegion != null && !embeddedParallelRegion.isEmpty());
	}

	public void hasEmbeddedParallelRegion(EmbeddedParallelRegions region) {
		if(embeddedParallelRegion == null)
			embeddedParallelRegion = new ArrayList<>();
		this.embeddedParallelRegion.add(region);
	}
	
	
	
	private boolean isSequentail;
	
	
	public Node getNode() {
		return node;
	}
	
	public AccumulatedData getAccumulatedData() {
		return accdata;
	}
	
	public boolean isExternal() {
		return false;
	}
	
	public boolean isInsideParallelRegion() {
		if(this.isParallel() ) {
			return true;
		} else if(this.getClass() == OMPFunctionWorkload.class){
			return false;
		} else {
			return getParent().isInsideParallelRegion();
		}
	}
	
	
	
	public void setParent(OMPWorkload p) {
		if(p == this) {
			Main.addErrorMessage("Some error in setting the parent");	
		}
		parent = p;
	}
	
	public boolean isOuterMostBlock() {
		if(this.getClass() != OMPFunctionWorkload.class){
			return (getParent().getClass() == OMPFunctionWorkload.class);
		}
		return true;
	}
	
	
	public OMPWorkload(Node n, OMPWorkload p) {
		setNode(n);
		setID();
		setSequentail(false);
		setParent(p);
		status =  DataStatus.Building;
		embeddedParallelRegion = null;
	}
	
	public DataStatus getStatus() {
		return status;
	}
	
	public void setStatus(DataStatus status) {
		this.status = status;
	}
	
	
	public OMPFunctionCallWorkload getNewFunctionCallWorkload(Node n, OMPWorkload p, CallSite callSite) {
		Main.addErrorMessage("getNewFunctionCallWorkload not defined for " + this);
		return null;
	}
	
	public void addOperations (HashMap<Operator, Integer> opCountMap ) {
		Main.addErrorMessage("addOperations not defined for " + this);
	}
	
	public boolean createAnewBarrieronFunctionCall() {
		return false;
	}
	
	
	/**
	 * Why we need  is if we are in Sequential region
	 * we need to create BlockWorkload object and if we are
	 * in Parallel region we need to create OMPBlockWorkload.
	 * 
	 */
	public BlockWorkload getNewBlockWorkload(Node n,boolean parallel) {
		Main.addErrorMessage("getNewBlockWorkload not defined for " + this + n.getInfo().getString());
		return null;
	}
	
	
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		Main.addErrorMessage("getCurrentBlockWorkload not defined for " + this);
		return null;
	}
	
	public boolean isCompoundWorkload() {
		return true;
	}
	
	public void resolve(){
		Main.addErrorMessage("Resolve not defined for  " + this);
	}
	
	
	public void setNode(Node n) {
		
		assert(n!=null);
		
		OMPWorkload nodeWorkload = (OMPWorkload) ((CesInfo)n.getInfo()).getOMPWorkload();
		if(node == null) {
			node  = n;
			if(nodeWorkload == null)
				((CesInfo)n.getInfo()).setOMPWorkload(this);
		}
		
		if(nodeWorkload == null) {
			checkIfAppropriateWorkLoadisAdded(n);
		}	
		
	}
	
	
	
	private void checkIfAppropriateWorkLoadisAdded(Node n) {
		try {
			if (n instanceof FunctionDefinition) {	
				// Since we are inlining all function calls
				// We use OMPFunctionCallWorkload for all Functions.
				assert(this instanceof OMPFunctionWorkload || this instanceof OMPFunctionCallWorkload);
			}  else if (n instanceof BreakStatement) {
				assert(this instanceof BreakWorkload);
			}  else if (n instanceof CriticalConstruct) {
				assert(this instanceof OMPCriticalWorkload);
			} else if (n instanceof FlushDirective) {
				assert(this instanceof EndWorkload);
			} else if (n instanceof ForConstruct) {
				assert(this instanceof OMPForWorkload );
			} else if (n instanceof MasterConstruct) {
				assert(this instanceof OMPMasterWorkload);
			} else if (n instanceof SingleConstruct) {
				assert(this instanceof OMPSingleWorkload);
			} else if (n instanceof ReturnStatement) {		
				assert(this instanceof ReturnWorkload);
			} else if (n instanceof WhileStatement || n instanceof DoStatement) {
				assert(this instanceof WhileWorkload);
			} else if (n instanceof ForStatement) {
				assert(this instanceof ForWorkload);
			} else if (n instanceof IfStatement) {
				assert(this instanceof ConditionalWorkload);
			} else if(n instanceof CompoundStatement) {
				assert(this instanceof OMPBlockWorkload || this instanceof BlockWorkload);
			} else if (n instanceof ExpressionStatement) {
				// The beginning and end of profiling is marked by begin workload
				//  and End Workload.
				// Also We always create OMPBlockWOrkload or BlockWorkload for 
				// CFG blocks created for branches.
				// For example  if(c) A; 
				// Here A is an Expression statement but since it represents 
				// a CFG block we create a block Workload.
				assert(this instanceof ExprWorkload 
					|| this instanceof BeginWorkload 
					|| this instanceof EndWorkload
					|| this instanceof OMPBlockWorkload
					|| this instanceof BlockWorkload);
			} else {
				Main.addlog("The Workload"+ this.getClass().getName() + " is created for "+ n.getClass().getName());
			}
		} catch(AssertionError e) {
			Main.addErrorMessage("Assertion failed");
		}
	}

	public boolean isLive() {
		return live;
	}
	
	public void closeWorkload(){
		live = false;
	}
	

	public void add(OMPWorkload s) {
		Main.addErrorMessage("Undefined for add() "  + this + " for operand " + s + "where code is" +  s.node.getInfo().getString());
	}

	
	
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		setNode(n);
		Main.addErrorMessage("Undefined for getBarrierWorkload() "  + this);
		return null;	
	}
	
	
	public void addPrivatizationList(PrivatizationList p) {
		Main.addErrorMessage("Undefined for addPrivatizationList() "  + this + " for operand " + p);
	}
	
	
	public void addlist(ArrayList<OMPWorkload> s) {
		Main.addErrorMessage("Undefined for addlist() "  + this + " for operand " + s);	
	}
	
	
	public void addReadArrayList(ArrayList<ArrayExpression> list) {
		Main.addErrorMessage("Undefined for addReadArrayList" + this);
	}

	
	public void addFunctionCall(CallSite f){
		Main.addErrorMessage("Undefined addFunctionCall for " + this);
	}
	
	
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		Main.addErrorMessage("Undefined for AddArrayAccess()"  + this );	
	}
	
	public void addFunctionCalls(HashSet<CallSite> c) {
		Main.addErrorMessage("Undefined function call "+ c);
	}
	
	public ArrayList<CallSite> getAllFunctionCalls() {
		Main.addErrorMessage("Undefined getAllFunction Calls for " + this);
		return null;
	}
	

	
	/*
	public boolean hasCalltoFunction(FunctionDefinition f) {
		for (int i = 0; i < calls.size(); i++) {
			if(f == calls.get(i).getCalleeDefinition()) {
				return true;
			}
		}
		return false;
		
	}
	
	*/

	public ExprWorkload getExprWorkload(Node n) {
 		Main.addErrorMessage("getExprWorkload is undefined for " + this + " " +n.getInfo().getString());
		return null;
	}

	public void addConstantMetric(Metric m, double val) {
		Main.addErrorMessage("addConstantMetric not defined for " + this);
	}


	public String dumpdata() {
		Main.addErrorMessage(" dumpdata not defined for " + this);	
		return null;
	}


	public boolean isSequentail() {
		return isSequentail;
	}


	public void setSequentail(boolean isSequentail) {
		this.isSequentail = isSequentail;
	}
	
	
	public boolean isParallel() {
		return false;
	}




	public int getID() {
		return __id;
	}
	
	

	
	
	
	
	


	public void setID() {
		this.__id = counter++;
//		if(this.__id == 1296)
//			Main.addlog("Created");
	}
	
	
	
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		Main.addErrorMessage("accumulatedata not defined for " + this);
		return cur;		
	}

	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) {
		Main.addErrorMessage("accumulateUnevenWorkloads not defined for " + this);
		return cur;
	}
	
	public AccumulatedData accumulateReductions(AccumulatedData cur){
		Main.addErrorMessage("accumulatereductions not defined for " + this);
		return cur;
	}
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect){
		Main.addErrorMessage("accumulateCritical not defined for " + this);
		return cur;
		
	}
	
	public AccumulatedData accumulateBarriers(AccumulatedData cur) {
		Main.addErrorMessage("Undefined getAllFunction Calls for " + this);
		return null;
	}
	
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement){
		Main.addErrorMessage("accumulateFalseSharing not defined for " + this);	
		return cur;
	}
	public AccumulatedData accumulateBranches(AccumulatedData cur){
		Main.addErrorMessage("accumulateBranches not defined for " + this);
		return cur;
	}
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect){
		Main.addErrorMessage("accumulateSingle not defined for " + this);
		return cur;
	}
	
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect){
		Main.addErrorMessage("accumulateMaster not defined for " + this);
		return cur;
	}
	
	
	public void setClassificationDetails(ClassificationDetails c, int occurances){
		Main.addErrorMessage("ClassificationDetailsNotDone");
	}
	
	public void transformCode(ClassificationDetails cd){
		Main.addErrorMessage("Transform Code Not Done" + this.getClass().getName());
	}
	
	public void transformIrregularCode(ClassificationDetails cd){
		Main.addErrorMessage("Transform Irregular Code Not Done" + this.getClass().getName());
	}
	
	
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		Main.addErrorMessage("VarList collection not done" + this.getClass().getName());
	}
	
	
	private long occurances;
	
	
	public long getOccurances() {
		return occurances;
	}
	
	
	public void accumulateReentrantNodes(long current, long itrValue) {
		occurances += current;
	}
	
	
	public void verify(int flag) {
		// We will use asserts. strict verification for each type of workloads.
	//	Main.addErrorMessage("verify not defined for " + this);		
	}

	public OMPWorkload getParent() {
		return parent;
	}

//	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds, ArrayList<BaseSymbolToVariableMap> baseList) {
//		Main.addErrorMessage("Not handled " + this.getClass().getName());
//	}

	
	
}
