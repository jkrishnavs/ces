/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import ces.data.base.Counter;
import ces.data.classifier.ClassificationDetails;
import ces.enums.DataStatus;
import imop.lib.cg.CallSite;

public class OMPFunctionCallWorkload extends OMPWorkload {

	private CallSite callSite;
	
	public OMPFunctionCallWorkload(Node n, OMPWorkload p, CallSite cs) {
		super(n, p);
		callSite = cs;
	}
	
	@Override
	public boolean isLive() {
		return false;
	}
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if( getStatus() != DataStatus.BranchesCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
		accdata.branchOperations.addconstPart(1);
		setStatus(DataStatus.BranchesCollected);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) { 
		if(collect) {
		//	accdata.criticalOperations.addbranch(accdata.branchOperations);
	//		cur.criticalOperations.add(accdata.criticalOperations);
		}
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		if(collect) {
		//.masterOperations.addbranch(accdata.branchOperations);
		//	cur.masterOperations.add(accdata.masterOperations);
		}
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(collect) {
			//accdata.singleOperations.addbranch(accdata.branchOperations);
		//	cur.singleOperations.add(accdata.singleOperations);
		}
		return cur;

	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) { return cur; }
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) { return cur;}

	@Override
	public void verify(int flag) {	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		accdata.totalOps.addbranch(accdata.branchOperations);
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		if(collect) {
			cur.unevenWorkload.addbranch(new Counter(1,""));
		}
		return cur;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}
	
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		// do nothing
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		// do nothing
	}
	
	
	/*
	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds, 
			ArrayList<BaseSymbolToVariableMap> baseList) {	
		Main.addlog(callSite.callerCFGNode().getInfo().getString());
		assert(callSite != null);
		//FunctionDefinition callee = callSite.getCalleeDefinition();
		
		PostfixExpression ps = callSite.callerExp;
		Main.addlog(ps.getInfo().getString());
		ArrayList<Node> esList = CesSpecificMisc.collectAllChildrensofClass(ps, ArgumentList.class.getName());
		assert(esList.size() == 1);
		ArrayList<Node> argList = CesSpecificMisc.collectAllChildrensofClass(esList.get(0), AssignmentExpression.class.getName());
		
		CesFunctionDefinitionInfo callee = (CesFunctionDefinitionInfo) callSite.getCalleeDefinition().getInfo();
		
		Vector<Symbol> parameterList = callee.getparameterlist();
		
		/*
		 * If parameter list size  of the callee function is 
		 * not equal to argList size
		 * we are unable to map these together.
		 * It means that the function call is not simplified.
		 * For example: 
		 * abs ( val - G_pg_rank [ t ] )
		 * The arglist will contain {val, G_pg_rank [ t ]}
		 * while the parameterList will contain just element.
		 
		if(!argList.isEmpty()  && (parameterList.size() == argList.size())) {
			for(Node var: argList) {
				int pos  = argList.indexOf(var);
				boolean alreadyAdded = false;
				String varName = var.getInfo().getString().trim();
				for(BaseSymbolToVariableMap symbol : baseList) {
					if(symbol.compareBaseName(varName)) {
						assert(!alreadyAdded);
						symbol.addVariableName(parameterList.get(pos).getName());
						alreadyAdded = true;
					}
				}
			}
		}
	}
	 */
}
