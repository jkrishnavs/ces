/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

public class EmbeddedParallelRegions {
	
	/**
	 * Original Parent of the embedded parallel region.
	 * 
	 */
	OMPWorkload parent;
	
	/**
	 * The parallel region.
	 */
	private OMPWorkload parallelRegion;
	
	/**
	 * This provides a list of all ancestral containers (expanding function call.)
	 * 
	 */
	ArrayList<OMPWorkload> containers;
	
	/*
	 * Original function container
	 * 
	 */
	OMPFunctionWorkload baseFunction;
	
	/**
	 * Set to true if inside a loop.
	 * 
	 */
	boolean isInsideLoop;
	
	public void setParentFunction(OMPFunctionWorkload f) {
		baseFunction = f;
	}
	
	public OMPFunctionWorkload getBaseFunctionWorkload() {
		return baseFunction;
	}
	
	
	public void setInsideLoop() {
		isInsideLoop = true;
	}
	
	public boolean isInsideLoop() {
		return isInsideLoop;
	}
	
	
	
	public EmbeddedParallelRegions(OMPWorkload parallel, OMPWorkload parent) {
		containers = new ArrayList<OMPWorkload>();
		setParallelRegion(parallel);
		this.parent = parent;
	}
	
	public void addPathList(ArrayList<OMPWorkload> l) {
		containers = l;
		
	//	for(OMPWorkload c : path) {
	//		((BlockWorkload)c).
	//	}
		
		
	}

	public OMPWorkload getParallelRegion() {
		return parallelRegion;
	}

	public void setParallelRegion(OMPWorkload parallelRegion) {
		this.parallelRegion = parallelRegion;
	}
	
}
