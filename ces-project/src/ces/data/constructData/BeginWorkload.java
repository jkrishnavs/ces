/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import imop.ast.node.Node;

public class BeginWorkload extends OMPWorkload {

	public BeginWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}

	@Override
	public boolean isLive() {
		return false;
	}
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	
}
