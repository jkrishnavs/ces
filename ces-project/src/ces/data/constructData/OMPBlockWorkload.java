/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.Node;
import ces.data.classifier.ClassificationDetails;
import ces.data.node.ArrayExpression;

public class OMPBlockWorkload extends BlockWorkload {
	
	ArrayList<OMPCriticalWorkload> criticals;
	ArrayList<OMPFlushWorkload> flushs;
	ArrayList<OMPMasterWorkload> masters;
	ArrayList<OMPSingleWorkload> singles;
	ArrayList<OMPForWorkload> forWorkloads;
	ArrayList<OMPReductionWorkload> reductions;
	ArrayList<OMPBarrierWorkload> barriers;
	
	
	
	
	public boolean  hasParallelData() {
		/*
		 * TODO  
		 * IMP
		 * We are not considering the data  that are embedded deep
		 * in another layer of OMPBlockWorkload.
		 * 
		 */
		if(criticals.size() > 0 ||
		   flushs.size() > 0	||
		   masters.size() > 0   ||
		   singles.size() > 0   ||
		   forWorkloads.size() > 0 ||
		   reductions.size() > 0 ) {
			return true;
		}
		
		else return false;
	}
	
	
	
	public OMPBlockWorkload(Node n, OMPWorkload p) {
		super(n, p);
		criticals = new ArrayList<OMPCriticalWorkload>();
		flushs = new ArrayList<OMPFlushWorkload>();
		masters = new ArrayList<OMPMasterWorkload>();
		singles = new ArrayList<OMPSingleWorkload>();
		forWorkloads = new ArrayList<OMPForWorkload>();
		reductions = new ArrayList<OMPReductionWorkload>();
		barriers = new ArrayList<OMPBarrierWorkload>();
	}
	
	
	
	
	
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		getExprWorkload(null).addArrayAccess(list);
	}
	
	@Override
	public void addReadArrayList(ArrayList<ArrayExpression> list) {
		getExprWorkload(null).addReadArrayList(list);
	}
	

	public OMPBlockWorkload(OMPBlockWorkload a, OMPWorkload p){
		super(a.getNode(), p); 
		
	}
	


	@Override
	public void add(OMPWorkload n) {
		if(n == null) {
			Main.addErrorMessage("Some Error " + node.getInfo().getString());
			
		}
		
		
		if (!childWorkloads.isEmpty() && childWorkloads.peek().isLive()
				&& childWorkloads.peek().isCompoundWorkload()) {
			childWorkloads.peek().add(n);
		} else {			
			if(n.getClass() == OMPReductionWorkload.class) {
				Main.addErrorMessage("NPB BECHMARKS DONT HAVE REductions outside for");
				reductions.add((OMPReductionWorkload) n);
				addChildWorkload(n);
			} else if(n.getClass() == OMPCriticalWorkload.class) {
				criticals.add((OMPCriticalWorkload) n);
				addChildWorkload(n);
			}else if(n.getClass() == OMPFlushWorkload.class) {
				flushs.add((OMPFlushWorkload) n);
				addChildWorkload(n);
			}else if(n.getClass() == OMPMasterWorkload.class) {
				masters.add((OMPMasterWorkload) n);
				addChildWorkload(n);
			}else if(n.getClass() == OMPSingleWorkload.class) {
				singles.add((OMPSingleWorkload) n);
				addChildWorkload(n);
			}else if(n.getClass() == OMPForWorkload.class) {
				forWorkloads.add((OMPForWorkload)n);
				addChildWorkload(n);
			} else if(n.getClass() == OMPBarrierWorkload.class) {
				this.barriers.add((OMPBarrierWorkload) n);
				addChildWorkload(n);
			} else if(n.getClass() == ExprWorkload.class) {
			// no special list list
				addChildWorkload(n);
			} else if(n.getClass() == OMPBlockWorkload.class) {
				this.addChildWorkload(n);
			} else if(n.getClass() == OMPParallelWorkload.class) {
				OMPParallelWorkload pw = (OMPParallelWorkload) n;
				assert(pw.embeddedRegion);
				assert(pw.embedDetails.baseFunction != null);
				this.addChildWorkload(pw);
			} else {
				super.add(n);
			}
		}
	}
	
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if((!childWorkloads.isEmpty()) && childWorkloads.peek().isLive()){
			return childWorkloads.peek().getExprWorkload(n);
		} else {
			addChildWorkload(new ExprWorkload(n, this));
			return childWorkloads.peek().getExprWorkload(n);
		}
	}
	
	@Override
	public String dumpdata() {
		return "\nNo of Critical region " + criticals.size() + "\nNo of Flush region " + flushs.size() 
			+ "\nNo of Master region " + masters.size() + "\nNo fo Single region " + singles.size()
			+  "\nNo of fordata " + forWorkloads.size()
			+ "\n Reductions is " + reductions.size();
	}
	
	@Override
	public void addPrivatizationList(PrivatizationList p) {
		Main.addErrorMessage("Skipping Privatization request " + p.getAllPrivateStrings());
		//super.addPrivatizationList(p);	
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		for (OMPWorkload c : childWorkloads) {
			c.transformCode(cd);
		}
	}
	
	/*
	@Override
	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds, ArrayList<BaseSymbolToVariableMap> baseList) {
		for (OMPWorkload c : childWorkloads) {
			c.collectInnerLoopDetails(internalLoopBounds, baseList);
		}
	}
	
	*/
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		for (OMPWorkload c : childWorkloads) {
			c.transformIrregularCode(cd);
		}
	}
	
	
	
}
