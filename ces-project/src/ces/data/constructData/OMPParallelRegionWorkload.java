/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.*;

import imop.Main;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.data.classifier.ClassificationDetails;
import ces.data.classifier.ClassifyWorkload;
import ces.enums.DataStatus;
import ces.enums.EigenMetric;
import ces.enums.HardwareConfig;

public class OMPParallelRegionWorkload extends OMPBlockWorkload {
	
	ClassificationDetails sd;
	
	
	public OMPParallelRegionWorkload(Node n, OMPWorkload p ) {
		super(n, p);
	}
	
	@Override
	public boolean isOuterMostBlock() {
		if(getParent().isOuterMostBlock()) {
			return true;
		} 
		return false;
	}
	
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {

		for(OMPWorkload child : childWorkloads ) {
			child.getVariablesofInterest(varList);
		}
	}

	
	@Override
	public void add(OMPWorkload n) {
		if (!childWorkloads.isEmpty() && childWorkloads.peek().isLive()
				&& childWorkloads.peek().isCompoundWorkload()) {
			childWorkloads.peek().add(n);
		} else {
			if(n.getClass() == OMPBlockWorkload.class) {
				Stack<OMPWorkload> blocklist = ((OMPBlockWorkload) n).childWorkloads;
				for (int i = 0; i < blocklist.size(); i++) {
					add(blocklist.get(i));
				} 
			} else if(n.getClass() ==  OMPParallelRegionWorkload.class) {
				Main.addErrorMessage("Should not be here " + n.getNode().getInfo().getString());
				addChildWorkload((OMPBlockWorkload) n);	
			} else {
				super.add(n);
			}
		}
	}
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		setNode(n);
		if(childWorkloads.isEmpty()) {
			return super.getBarrierWorkload(n);
		} else {
			return super.getBarrierWorkload(n);
		}
	}
	
	@Override
	public String dumpdata() {
		return super.dumpdata()  + "\nNo of Blocks " + childWorkloads.size() + 
				"\n *** TOTAL OPS ******\n" + accdata.totalOps.dumpdata() +
				"\n****branch data " + accdata.branchOperations.getdata() +
				"\n **** critical data" + accdata.criticalOperations.dumpdata() +
				"\n ***** false sharing" + accdata.falseSharingOperations.dumpData() +
				"\n ****** master data" + accdata.masterOperations.dumpdata() +
				"\n ****** redcutions data" + accdata.reductions.getdata() +
				"\n ****** single data" + accdata.singleOperations.dumpdata() +
				"\n ****** uneven data" + accdata.unevenWorkload.dumpdata();
	}
	
	
	
	
	
	
	
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		if(getStatus() != DataStatus.DataCollected) {
		for(OMPWorkload child: childWorkloads) {
			child.accumulatedata(accdata);
		}
		setStatus(DataStatus.DataCollected);
		}
		return accdata;
	}
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if(getStatus() != DataStatus.BranchesCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
		
		for(OMPWorkload child : childWorkloads ) {
			child.accumulateBranches(accdata);
		}
		setStatus(DataStatus.BranchesCollected);
		}
		return accdata;
	}

	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.CriticalCollected) {
		for (OMPWorkload child: childWorkloads) {
			child.accumulateCritical(accdata, false);
		}
		setStatus(DataStatus.CriticalCollected);
		}
		return accdata;
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		if(getStatus() != DataStatus.FalseSharingCollected) {
		for(OMPWorkload child : childWorkloads ) {
			child.accumulateFalseSharing(accdata, null);
		}
		setStatus(DataStatus.FalseSharingCollected);
		}
		return accdata;
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		if(getStatus() !=  DataStatus.MasterCollected) {
		for(OMPWorkload child : childWorkloads ) {
			if(child.getClass() == OMPMasterWorkload.class) {
				child.accumulateMaster(accdata, false);
			}
		}	
		setStatus(DataStatus.MasterCollected);
		}
		return accdata;
	}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		if(getStatus() != DataStatus.ReductionsCollected) {
		if(accdata == null)
			accdata = new AccumulatedData();
	
		for(OMPWorkload child : childWorkloads ) {
			child.accumulateReductions(accdata);
		}
		setStatus(DataStatus.ReductionsCollected);
		}
		return accdata;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(getStatus() != DataStatus.SingleCollected) {
		for (OMPWorkload child : childWorkloads) {
			if(child.getClass() == OMPSingleWorkload.class) {
				child.accumulateSingle(accdata, false);
			}
		}
		setStatus(DataStatus.SingleCollected);
		}
		return accdata;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) {
		if(getStatus() != DataStatus.unEvenDataCollected) {
		for (OMPWorkload child: childWorkloads) {
			child.accumulateUnevenWorkloads(accdata, false, null);
		}
		setStatus(DataStatus.unEvenDataCollected);
		}
		return accdata;
	}
	
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		for(OMPWorkload child: childWorkloads) {
			child.accumulateReentrantNodes(current, itrValue);
		}
	}
	
	@Override
	public void verify(int flag) {
		for (OMPWorkload child: childWorkloads) {
			child.verify(flag);
		}
	
	}
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		for (OMPWorkload child: childWorkloads) {
			child.transformCode(cd);
		}
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		for (OMPWorkload child: childWorkloads) {
			child.transformIrregularCode(cd);
		}
	}
	
	
	

	public void classify() {
		sd = new ClassificationDetails();
		accdata.evaluate();
		debugPrintData(accdata);
		
		sd.setPreffered(ClassifyWorkload.cpe.Classify(accdata));
	}
	@SuppressWarnings("unused")
	private void debugPrintData(AccumulatedData data) {
		double retVal = 0;
		double barVal = data.getEigenFactorValue(EigenMetric.ompBarriers);
		double memVal = data.getEigenFactorValue(EigenMetric.MemoryOps);
		double criticalVal = data.getEigenFactorValue(EigenMetric.ompCritical);
		double reductionVal = data.getEigenFactorValue(EigenMetric.ompReductions);
		double branchesVal = data.getEigenFactorValue(EigenMetric.branches);
		double flushVal = data.getEigenFactorValue(EigenMetric.ompFlush);
		if(SystemConfigFile.ALUFACTOR){
			barVal = data.getALUEigenFactorValue(EigenMetric.ompBarriers);
			memVal = data.getALUEigenFactorValue(EigenMetric.MemoryOps);
			criticalVal = data.getALUEigenFactorValue(EigenMetric.ompCritical);
			reductionVal = data.getALUEigenFactorValue( EigenMetric.ompReductions);
			branchesVal = data.getALUEigenFactorValue(EigenMetric.branches);
			flushVal  = data.getALUEigenFactorValue(EigenMetric.ompFlush);
		}
		
		String s = "\t\t ********************** FACTORS ***************************** \n "
				+ " \t BARRIER FACTOR  : " + barVal + " \n"
				+ " \t BRANCH FACTOR   : " + branchesVal + " \n"
				+ " \t CRITICAL FACTOR : " + criticalVal + " \n"
				+ " \t FLUSH FACTOR    : " + flushVal + " \n"
				+ " \t MEMORY FACTOR   : " + memVal + " \n"
				+ " \t REDUCTION FACTOR: " + reductionVal;
		
		Main.addlog(s);
		
	}

	public void ces() {
		sd = new ClassificationDetails();
		sd.setPreffered(SystemConfigFile.globalHardwareConfig);
		sd.setSelected(SystemConfigFile.globalHardwareConfig);
	}


}
