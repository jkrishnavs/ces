/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.Stack;

import imop.Main;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import ces.data.node.ArrayExpression;

public class SequentialWorkload extends OMPBlockWorkload {

	
	public SequentialWorkload(Node n, OMPWorkload p) {
		super(n, p);
		childWorkloads = new Stack<OMPWorkload>();
	}
	
	
	@Override
	public void add(OMPWorkload s) {
		if(! childWorkloads.isEmpty() && childWorkloads.peek().isLive() 
				&& childWorkloads.peek().isCompoundWorkload()) {
			childWorkloads.peek().add(s);
		} else {
			childWorkloads.add(s);
		}
	}
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n,boolean parallel) {
		if(!childWorkloads.isEmpty() && childWorkloads.peek().isLive())
			return childWorkloads.peek().getNewBlockWorkload(n,false);
		BlockWorkload wl = new BlockWorkload(n, this);
		return wl;
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if(!childWorkloads.isEmpty() && childWorkloads.peek().isLive())
			return childWorkloads.peek().getExprWorkload(n);
		childWorkloads.add(new ExprWorkload(n, this));
		return childWorkloads.peek().getExprWorkload(n);
	}
	
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		if(list.size() > 0)
			Main.addlog("Array Access called for sequential region by " + list.get(0).getBaseName());
	}
	
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}
	
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		return super.getBarrierWorkload(n);
	}

}
