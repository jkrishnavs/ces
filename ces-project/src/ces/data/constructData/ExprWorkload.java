/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import imop.Main;
import imop.ast.node.*;
import ces.data.aluoperations.ALUOperations;
import ces.data.base.Counter;
import ces.data.classifier.ClassificationDetails;
import ces.data.memoryOps.MemoryOperations;
import ces.data.node.ArrayExpression;
import ces.enums.Metric;
import imop.lib.analysis.type.Operator;
import imop.lib.cg.CallSite;

public class ExprWorkload extends OMPWorkload {
	

	
	
	
	@Override
	public void addOperations(HashMap<Operator, Integer> opCountMap) {
		aluoperations.addOperations(opCountMap);
	}
	
//	@Override
//	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds, ArrayList<BaseSymbolToVariableMap> baseList) {
//		//do nothing
//	}
//	
	
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		return cur;
	}
	
	
	@Override
	public void add(OMPWorkload s) {
		if(s.getClass() == ExprWorkload.class) {
			addArrayAccess(((ExprWorkload) s).falsesharingList);
			addReadArrayList(((ExprWorkload) s).readArrayList);
			memops.addMemoryOperations(((ExprWorkload) s).memops);
			aluoperations.add(((ExprWorkload) s).aluoperations);
		} else
			super.add(s);
	}
	

	public ExprWorkload(Node n, OMPWorkload p) {
		super(n, p);
		falsesharingList =  new ArrayList<ArrayExpression>();
		readArrayList = new ArrayList<ArrayExpression>();
		memops = new MemoryOperations();
		aluoperations = new ALUOperations();
	}
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		return this;
	}
	
	@Override
	public void closeWorkload() {
		// never close Expr Workload.
	}
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		falsesharingList.addAll(list);
		/**
		 * TODO:
		 *  alu should take care of appending the indexes to the list.
		 *  Not the right place to add these info but now 
		 *  we have no other choice
		 */
		for(ArrayExpression exp : list) {
			aluoperations.addConstantMetric(Metric.ops_arith, exp.matrixDimension());
		}
	}
	
	@Override
	public void addReadArrayList(ArrayList<ArrayExpression> list) {
		readArrayList.addAll(list);
		/**
		 * TODO:
		 *  alu should take care of appending the indexes to the list.
		 *  Not the right place to add these info but now 
		 *  we have no other choice
		 */
		for(ArrayExpression exp : list) {
			aluoperations.addConstantMetric(Metric.ops_arith, exp.matrixDimension());
		}

		
	}
	
	
	@Override
	public boolean isLive() {
		return true;
	}
	
	
	/**
	 * This function is written to get the most significant term containing the 
	 * @param iterator
	 * @return
	 */
//	
//	public String getMostSignificantTermwithIterator(String iterator){
//		int degree = 0;
//		String mst = ""; 
//		for(Metric metric: Metric.values()){
//			String cur = getVal(metric).getMostSignificantTermWithVariable(iterator);
//			if(CesStringMisc.getdegree(cur) > degree){
//				mst = cur;
//				degree = CesStringMisc.getdegree(cur);
//			}else if (CesStringMisc.getdegree(cur) < degree){
//				mst = cur;
//			}else
//				mst = CesStringMisc.addTwoTerms(mst,cur);
//		}
//		
//		return mst;
//	}
//	
//	
	/*
	
	public String getMostSignificantTerm(){
		int degree = 0;
		String mst = null; 
		for(Metric metric: Metric.values()){
			String cur = getVal(metric).getMostSignificantTerm();
			if(CesStringMisc.getdegree(cur) > degree){
				mst = cur;
				degree = CesStringMisc.getdegree(cur);
			}else if (CesStringMisc.getdegree(cur) == degree){
				mst = CesStringMisc.addTwoTerms(mst,cur);
			}
		}
		return mst;
	}
	
	*/

	@Override
	public void addConstantMetric(Metric m, double val) {
		switch(m) {
			case memread:
			case memwrite:
			case branches:
			case funcCalls:
			case sharedRead:
			case sharedWrite:
			case privateRead:
			case privateWrite:
				memops.addConstantMetric(m, val);	
				break;
			case ops_arith:
			case ops_mult:
			case ops_bitwise:
				aluoperations.addConstantMetric(m, val);
				break;
			default:
		
		}
	}
	
	
	/*

	public void addStringMetric(Metric m, String val) {
		switch(m){
		case memread:
		case memwrite:
		case branches:
		case funcCalls:
		case sharedRead:
		case sharedWrite:
			memops.addStringMetric(m, val);
			break;
		case ops_arith:
		case ops_mult:
		case ops_bitwise:
			aluoperations.addStringMetric(m, val);
			break;
		default:
			Main.addErrorMessage("Unknown option "+ m + " for " + this);
		
		}
	}
	
	*/
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}
	
	public Counter getVal(Metric m) {
		switch(m) {
		case memread:
		case memwrite:
		case branches:
		case funcCalls:
		case sharedRead:
		case sharedWrite:
		case privateRead:
		case privateWrite:	
			memops.getVal(m);
			break;
		case ops_arith:
		case ops_mult:
		case ops_bitwise:
			aluoperations.getVal(m);
			break;
		default:
			Main.addErrorMessage("Unknown option "+ m + " for " + this);
	
		}
		return new Counter();
	}
	
	
	/*
	public void setVal(Metric m, Counter c) {
		
		switch(m) {
		case memread:		
		case memwrite:
		case branches:
		case funcCalls:
		case sharedRead:
		case sharedWrite:
		case privateRead:
		case privateWrite:
			memops.setVal(m, c);
		case ops_arith:
		case ops_mult:
		case ops_bitwise:
		default:
			Main.addErrorMessage("Unhandled metric");
			break;
	
		}
	}
	*/
	
	@Override
	public void addFunctionCalls(HashSet<CallSite> c) {
		memops.addFunctionCalls(c);
	}
	
	@Override
	public void addFunctionCall(CallSite f) {
		memops.addFunctionCall(f);
	}
	

	public void addAnalysisData(OMPWorkload a) {
	
	}
	
	
	public void multiply(String S) {
		
	}
	
	public boolean isEmpty() {
		Main.addErrorMessage("Empty Undefined for this "  + this );
		return false;
	}


	public String dumpdata() {
		return "ERROR:The dump data is not defined for this ";
	}
	
	@Override
	public void verify(int flag) {}
	
	
	
	ArrayList<ArrayExpression> falsesharingList;
	ArrayList<ArrayExpression> readArrayList;
	ALUOperations aluoperations;
	MemoryOperations memops;
	
	
	
	
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		if(collect) {
			cur.criticalOperations.addALUOperations(aluoperations);
			cur.criticalOperations.addMemoryOps(memops);
			cur.criticalOperations.addArrayOperations(falsesharingList.size());
			cur.criticalOperations.addArrayOperations(readArrayList.size());
		}
		return cur;
	}
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		cur.totalOps.addALUOperations(aluoperations);
		cur.totalOps.addMemoryOps(memops);
		cur.totalOps.addArrayOperations(falsesharingList.size());
		cur.totalOps.addArrayOperations(readArrayList.size());
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		if(collect) {
			cur.masterOperations.addALUOperations(aluoperations);
			cur.masterOperations.addMemoryOps(memops);
			cur.masterOperations.addArrayOperations(falsesharingList.size());
			cur.masterOperations.addArrayOperations(readArrayList.size());
		}
		return cur;
	}
	

	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		if(collect) {
			cur.singleOperations.addALUOperations(aluoperations);
			cur.singleOperations.addMemoryOps(memops);
			cur.singleOperations.addArrayOperations(falsesharingList.size());
			cur.singleOperations.addArrayOperations(readArrayList.size());
		}
		return cur;
	}

	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect, ArrayList<String> itrDetails) {
		if(collect) {
			cur.unevenWorkload.addALUOperations(aluoperations);
			cur.unevenWorkload.addMemoryOps(memops);
			cur.unevenWorkload.addArrayOperations(falsesharingList.size());
			cur.unevenWorkload.addArrayOperations(readArrayList.size());
		}
		return cur;
	}

	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		cur.falseSharingOperations.addSharingList(falsesharingList, displacement);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		return cur;
	}
	
	
	@Override
	public void transformCode(ClassificationDetails cd) {
		// do nothing
	}
	
	@Override
	public void transformIrregularCode(ClassificationDetails cd) {
		
	}
	
}
