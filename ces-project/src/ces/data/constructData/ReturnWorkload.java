/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;

import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;

public class ReturnWorkload extends ExprWorkload {

	public ReturnWorkload(Node n, OMPWorkload p) {
		super(n, p);
	}
	
	@Override
	public boolean isCompoundWorkload() {
		return false;
	}
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
	}

}
