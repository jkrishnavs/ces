/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.constructData;

import java.util.ArrayList;
import java.util.Stack;

import imop.Main;
import imop.ast.node.Node;
import imop.ast.node.PostfixExpression;
import ces.data.node.ArrayExpression;
import imop.lib.cg.CallSite;

public class BlockWorkload extends OMPWorkload {


	ArrayList<BreakWorkload> breakW;
	ArrayList<ConditionalWorkload> condW;
	ArrayList<ContinueWorkload> contW;
	ArrayList<ForWorkload> forW;
	ArrayList<WhileWorkload> whileW;
	ArrayList<ReturnWorkload> returnW;
	ArrayList<OMPFunctionCallWorkload> fcCallsW;
	
	
	Stack<OMPWorkload> childWorkloads;
	
	
	public Stack<OMPWorkload> getChildWorkloads() {
		return childWorkloads;
	}
	
	
	
	
	@Override
	public AccumulatedData accumulateBranches(AccumulatedData cur) {
		if (accdata == null)
			accdata = new AccumulatedData();
		for(OMPWorkload child : childWorkloads) {
			child.accumulateBranches(accdata);
		}
		cur.branchOperations.add(accdata.branchOperations);
		return cur;
	}
	
	
	@Override
	public void addArrayAccess(ArrayList<ArrayExpression> list) {
		
		if(!childWorkloads.empty() && childWorkloads.peek().isLive())
			childWorkloads.peek().addArrayAccess(list);
		else {
			super.addArrayAccess(list);
		}
	}
	
	@Override
	public void addReadArrayList(ArrayList<ArrayExpression> list) {
		if(!childWorkloads.empty() && childWorkloads.peek().isLive())
			childWorkloads.peek().addReadArrayList(list);
		else {
			super.addReadArrayList(list);
		}
	}
	
	public void addChildWorkload(OMPWorkload child) {
		childWorkloads.push(child);
	}
	
	public void addChildWorkload(int pos, OMPWorkload child) {
		childWorkloads.add(pos, child);
	}
	
	
	@Override
	public OMPFunctionCallWorkload getNewFunctionCallWorkload(Node n, OMPWorkload p, CallSite callSite) {
		if(this.getClass() == BlockWorkload.class ||	
		    this.getClass() == OMPBlockWorkload.class ||
		    this.getClass() == OMPParallelRegionWorkload.class ||
		    this.getClass() == OMPForIterationWorkload.class) {
			OMPFunctionCallWorkload fcW = new OMPFunctionCallWorkload(n, p, callSite);
			addChildWorkload(fcW);
			fcCallsW.add(fcW);
			return fcW;
		} else {
			Main.addlog("The function call is " + p.getNode().getInfo().getString());
			return super.getNewFunctionCallWorkload(n, p, callSite);
		}
	}
	
	
	public BlockWorkload(Node n, OMPWorkload p) {
		super(n, p);
		breakW = new ArrayList<BreakWorkload>();
		condW = new ArrayList<ConditionalWorkload>();
		contW =  new ArrayList<ContinueWorkload>();
		forW = new ArrayList<ForWorkload>();
		whileW = new ArrayList<WhileWorkload>();
		childWorkloads = new Stack<OMPWorkload>();
		returnW = new ArrayList<ReturnWorkload>(); 
		fcCallsW = new ArrayList<OMPFunctionCallWorkload>();
		setSequentail(true);
	}
	
	@Override
	public OMPParallelWorkload getBarrierWorkload(Node n) {
		if(! childWorkloads.isEmpty() && childWorkloads.peek().isLive()) {
			if(childWorkloads.peek().getClass() == OMPParallelWorkload.class) {
				return (OMPParallelWorkload) childWorkloads.peek();
			} else if(! childWorkloads.peek().isCompoundWorkload()) {
				return null;
			} else {		
				return childWorkloads.peek().getBarrierWorkload(n);
			}
			
		} else if(childWorkloads.peek().getClass() == OMPForWorkload.class ||
				  childWorkloads.peek().getClass() == OMPSingleWorkload.class) {
			return null;
		} else {
			return super.getBarrierWorkload(n);
		}
	}
	
	
	
	@Override
	public ExprWorkload getExprWorkload(Node n) {
		if (! childWorkloads.isEmpty() && childWorkloads.peek().isLive()) {
			return childWorkloads.peek().getExprWorkload(n);
		} else {
			addChildWorkload(new ExprWorkload(n, this));
			return childWorkloads.peek().getExprWorkload(n);
		}
	}
	
	
	
	
	@Override
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(! childWorkloads.isEmpty() && childWorkloads.peek() == this){
			Main.addErrorMessage("Child and Parent are the same ");	
		}
		if(childWorkloads.isEmpty() || 
			!(childWorkloads.peek().isLive()) || 
			!childWorkloads.peek().isCompoundWorkload())
			return this;
		else if(childWorkloads.peek().isLive())
			return childWorkloads.peek().getCurrentBlockWorkload(n);
		else
			return super.getCurrentBlockWorkload(n);
	}
	
	
	
	@Override
	public BlockWorkload getNewBlockWorkload(Node n, boolean parallel) {
		if(!childWorkloads.isEmpty() && childWorkloads.peek().isLive() 
				&& (childWorkloads.peek().isCompoundWorkload()))
			return childWorkloads.peek().getNewBlockWorkload(n,parallel);
		else if(this.getClass() == OMPParallelRegionWorkload.class) {
			return new OMPBlockWorkload(n, this);
		} else {  
			return super.getNewBlockWorkload(n, parallel);
		}
	}
	
	
	
	@Override
	public void add(OMPWorkload s) {
		if (!childWorkloads.isEmpty() && childWorkloads.peek().isLive()
				&& childWorkloads.peek().isCompoundWorkload()) {
			childWorkloads.peek().add(s);
		} else {
			if(s instanceof BreakWorkload) {
			breakW.add((BreakWorkload) s);	
			} else if (s.getClass()  == ConditionalWorkload.class) {
				condW.add((ConditionalWorkload) s);
			} else if (s.getClass() ==  ContinueWorkload.class) {
				contW.add((ContinueWorkload) s);
			} else if(s.getClass() == ForWorkload.class) {
				forW.add((ForWorkload) s);
			} else if(s.getClass() == WhileWorkload.class) {
				whileW.add((WhileWorkload) s);
			} else if(s.getClass() == ReturnWorkload.class ) {
				returnW.add((ReturnWorkload) s);
			} else if(s.getClass() == OMPFunctionCallWorkload.class) {
				childWorkloads.add(s); 
			} else {
				Main.addErrorMessage("Block Workload asked to add "+ s);
				super.add(s);
				return;
			}
			addChildWorkload(s);
		}
	}
	
	
	
	
	@Override
	public AccumulatedData accumulatedata(AccumulatedData cur) {
		
		for(OMPWorkload child : childWorkloads) {
			child.accumulatedata(accdata);
		}
		cur.totalOps.add(accdata.totalOps);
		return cur;
	}
	
	
	@Override
	public AccumulatedData accumulateCritical(AccumulatedData cur, boolean collect) {
		for(OMPWorkload child : childWorkloads) {
			child.accumulateCritical(accdata, collect);
		}
		cur.criticalOperations.add(accdata.criticalOperations);
		return cur;
	}
	
	
//	@Override
//	public void collectInnerLoopDetails(ArrayList<InternalAsymmetricScheduling> internalLoopBounds, ArrayList<BaseSymbolToVariableMap> baseList) {
//		for (OMPWorkload c : childWorkloads) {
//			c.collectInnerLoopDetails(internalLoopBounds, baseList);
//		}
//	}
//	
	
	@Override
	public AccumulatedData accumulateMaster(AccumulatedData cur, boolean collect) {
		for(OMPWorkload child : childWorkloads) {
			child.accumulateMaster(accdata, collect);
		}
		cur.masterOperations.add(accdata.masterOperations);
		return cur;
	}
	
	@Override
	public void getVariablesofInterest(ArrayList<PostfixExpression> varList) {
		for(OMPWorkload child : childWorkloads) {
			child.getVariablesofInterest(varList);
		}
	}
	
	@Override
	public AccumulatedData accumulateReductions(AccumulatedData cur) {
		for(OMPWorkload child: childWorkloads) {
			child.accumulateReductions(accdata);
		}
		cur.reductions.add(accdata.reductions);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateSingle(AccumulatedData cur, boolean collect) {
		for(OMPWorkload child : childWorkloads) {
			child.accumulateSingle(accdata, collect);
		}
		cur.singleOperations.add(accdata.singleOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateFalseSharing(AccumulatedData cur, String displacement) {
		for(OMPWorkload child : childWorkloads) {
			child.accumulateFalseSharing(accdata, displacement);
		}
		cur.falseSharingOperations.addSharingFalseSharing(accdata.falseSharingOperations);
		return cur;
	}
	
	@Override
	public AccumulatedData accumulateUnevenWorkloads(AccumulatedData cur, boolean collect,
			ArrayList<String> itrDetails) {
		for(OMPWorkload child : childWorkloads) {
			child.accumulateUnevenWorkloads(accdata, collect, itrDetails);
		}
		cur.unevenWorkload.add(accdata.unevenWorkload);
		return cur;
	}
	
	
	@Override
	public void accumulateReentrantNodes(long current, long itrValue) {
		super.accumulateReentrantNodes(current, itrValue);
		for(OMPWorkload child : childWorkloads) {
			child.accumulateReentrantNodes(current, itrValue);
		}
	}
	
	@Override
	public void verify(int flag) {
		for(OMPWorkload child: childWorkloads) {
			child.verify(flag);
		}
	}

	

}
