/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import java.util.ArrayList;

import imop.Main;
import imop.ast.info.CesInfo;
import imop.ast.node.*;
import ces.data.classifier.ClassifyWorkload;
import ces.data.constructData.AccumulatedData;
import ces.data.constructData.ForWorkload;
import ces.data.constructData.OMPWorkload;
import ces.enums.IrregularityType;
import ces.util.CesStringMisc;
import ces.util.SystemStringConstants;
import ces.util.SystemStringFunctions;
import stringexp.CESString;

public class IrregularityData {


	// input
	ArrayList<String> iterators;
	ArrayList<String> loopBounds; 
	
	// irregularity data
	ArrayList<IrregularityData> data;	
	
	
	
	IrregularityType irType;
	IrregularityData maxData;
	Node node;
	
	
	public void addIrregularityData(IrregularityData d) {
		data.add(d);
	}

	
	
	
	public IrregularityData(Node n) {
	//odes = new ArrayList<Node>();
		iterators = new ArrayList<String>();
		loopBounds = new ArrayList<String>();
		irType = IrregularityType.None;
		data = new ArrayList<IrregularityData>();
		maxData = null;
		node  = n;
	}
	
	
	
	
	public void addIterator(String itr) {
		itr = itr.trim();
		iterators.add(itr);
	}
	
	
	
	
	public ArrayList<String> getLoopBoundsSet() {
		return loopBounds;
	}

	
	public void addIterators(ArrayList<String> itrs) {
		iterators.addAll(itrs);
	}
	
	public void addLoopBounds(ArrayList<String> bounds) {
		loopBounds.addAll(bounds);
	}
	
	public void addLoopBound(String l) {
		loopBounds.add(l);
	}
	
	
	
	public IrregularityType getIrType() {
		return irType;
	}

	

	
	public ArrayList<String> getIteratorsSet() {
		return iterators;
	}

	public boolean hasNoAsymmetricElement() {
		if(node instanceof IterationStatement )
			return false;
		
		for(IrregularityData i : data) {
			if(i.hasNoAsymmetricElement() == false)
				return false;
		}
		return true;
	}




	public int getItrationDegree() {
		int degree  = 0;
		int temp;
		
		for(IrregularityData i: data) {
			temp = i.getItrationDegree();
			if(temp < degree)
				degree = temp;
		}
		if(node instanceof IterationStatement)
			degree ++;
		return degree;
	}

	
	private IrregularityData getInnermostLoop() {
		IrregularityData innermost = null;
		if(maxData != null)
			innermost = maxData.getInnermostLoop();
		
		if(innermost != null) {
			return innermost;
		} else if(node instanceof ForStatement) {
			/**
			 * TODO for Statement or Iteration Statement. 
			 */
			return this;
		}
		return innermost;
	}
	
	public void updateMaxData() {
		IrregularityData max = null;
		if(!data.isEmpty()) {
			max = data.get(0);
			for( IrregularityData d : data) {
				if(d.getItrationDegree() > max.getItrationDegree())
					max = d;
			}
		
			maxData = max;
		
			maxData.updateMaxData();
		}
	}

	private static final String numnodes = "num_nodes";
	private static final String nnodes = "n_nodes";
	private static final String rbeginStr = "r_begin";
	private static final String beginStr  = "begin";

	private IrregularityType updateIRType() {
		
		if (node instanceof ForStatement) {
				if(( loopBoundsStringContains(numnodes) || loopBoundsStringContains(nnodes)) 
								&& loopBoundsStringContains(rbeginStr)) {
					irType = IrregularityType.REdge;
					return irType;
				} else if(( loopBoundsStringContains(numnodes) || loopBoundsStringContains(nnodes))
								&& loopBoundsStringContains(beginStr)) {
					irType = IrregularityType.Edge;
					return irType;
				} else if (loopBoundsStringContains("num_edges") && (loopBoundsStringContains("get_next") 
								|| loopBoundsStringContains(SystemStringConstants.INFINITY)) ) {
					irType = IrregularityType.Node;
					return irType;
				} else {
					irType =  IrregularityType.None;
					return irType;
				}
		} else if(maxData != null) {
			irType = maxData.updateIRType();
			return irType;
		}
		
		return 	irType;
	}
	
	private boolean loopBoundsStringContains(String string) {
		for(String s :loopBounds) {
			if(s.contains(string))
				return true;
		}
		return false;
	}




	private String getWorkloadString(String s) {
				/*
				 * 
				 *  TODO 
				 *  
				 *  add code for multiple loop code.
				 */
		Main.addErrorMessage("Error with multiple statements");
			hasMultipleStatements = true;
				return "1";
	}
	
	
   




	private String symbolContainsIterator(ArrayList<String> stList) {
		
		for(String i : iterators) {
			for(String s: stList ) {
				if(s.equals(i))
					return i;
			}
		}
		return null;
	}

	/**
	 * we will set the has multiple statement to true when we 
	 * have to set multiple statments to true.
	 * 
	 */
	public static boolean hasMultipleStatements;
	
	
	public static boolean hasencountedIrregularity;

	public enum workloadcalculation{
		OVERALL,
		CHUNK,
		ITERATION
	}

    public ArrayList<String> getIrregularityWorkloadString(workloadcalculation choice) {
	assert(irType != IrregularityType.UnKnown);
	String workloadString = "";
	ArrayList<String> subWorkloads = null;    
	if(maxData != null) {
	    subWorkloads = maxData.getIrregularityWorkloadString(choice);
	} else {
	    subWorkloads = new ArrayList<String>();
	}
	String myLoopBound = loopBounds.get(loopBounds.size()-1);
	String baseIterator = iterators.get(0);
	String baseBounds = loopBounds.get(0);
	    
	if(node instanceof IterationStatement) {
	    if(loopBoundsStringContains(numnodes) ||  loopBoundsStringContains(nnodes)) {
	    	if(!myLoopBound.equals(SystemStringConstants.INFINITY)) {
	    		if(myLoopBound.contains(rbeginStr) || myLoopBound.contains(beginStr)) {
	    			ArrayList<String> stList = CESString.getasTokens(myLoopBound); 
	    			if(stList.contains(baseIterator)) { 
	    				if(hasencountedIrregularity)
	    					hasMultipleStatements = true;

	    				if(choice == workloadcalculation.CHUNK)
	    					workloadString = SystemStringFunctions.getWorkloadString(SystemStringConstants.mainEndString);
	    				else if(choice == workloadcalculation.OVERALL && ! hasencountedIrregularity)
	    					workloadString = SystemStringConstants.edgesString;
	    				else if(choice == workloadcalculation.OVERALL &&  hasencountedIrregularity)
	    					workloadString = "(" + SystemStringConstants.edgesString +"/"+  SystemStringConstants.nodeString +")";
	    				else
	    					workloadString = SystemStringFunctions.getWorkloadString(SystemStringConstants.mainIteratorString);
				
	    				hasencountedIrregularity = true;		
	    			} else if(symbolContainsIterator(stList) != null) {
	    				String s = symbolContainsIterator(stList);
	    				if(choice != workloadcalculation.OVERALL) {
	    					workloadString =  getWorkloadString(s);
	    				} else
	    					workloadString = "(" + SystemStringConstants.edgesString +"/"+  SystemStringConstants.nodeString +")";
	    			} else {
	    				assert(false);
	    			}
	    		} else {
	    			workloadString = myLoopBound;
	    		}
	    	} else {
	    		
	    		/**
	    		 * TODO Ideally we should use the substitute for infinity. But here 
	    		 * we are going for a smaller number such the we do not see an overflow 
	    		 */
	    		// workloadString  = "( " + SystemStringConstants.INTSIZE + ")";
	    		workloadString  = "1";
	    	}
	    } else if(baseBounds.contains("num_edges") ) {
	    	//TODO	num edges once everything else id fixed.			

	    } else if(irType == IrregularityType.UnKnown) {
	    		
	    	}
		} else {
			if(node instanceof IfStatement) {
				IfStatement stmt = (IfStatement) node;
				Expression exp = stmt.f2;
				/**
				 * TODO add code to identify j as a node.
				 * and remove max hardcoding 
				 * 
				 */
				String maxNodes = ClassifyWorkload.ccppmapping.getCPPFunctionCall("gm_graph_num_nodes(G)");
				ArrayList<String> accepted = new ArrayList<>();
				String stringToEvaluate = exp.getInfo().getString();
				if(choice != workloadcalculation.OVERALL) {
					accepted.add(SystemStringConstants.mainIteratorString);
					ArrayList<String> tokens = CESString.getasTokens(stringToEvaluate);
					int i=0;
					boolean found = false;
					for(String s: tokens) {
						if(s.equals(baseIterator)) {
							tokens.set(i, SystemStringConstants.mainIteratorString);
							found  = true;
						}
						i++;
					}
					if(found) {
						stringToEvaluate = "";
								for(String s: tokens) {
									stringToEvaluate += s + " ";
								}
						Main.addlog(stringToEvaluate);
					}
				}
				
				String probability   = CesStringMisc.getTruthProbabilityEvaluated(stringToEvaluate, accepted, maxNodes);
				/**
				 * IMPORTANT Since most of the Strings associated are integers. We need to multiply the 
				 * the probability by 1.0 to prevent it to take value 0.
				 * 
				 */
				
				if(! probability.equals("0.5")) {
					probability = CesStringMisc.getReducedString(probability);
				}
				workloadString = probability;
	    }
		
	}	
	Main.addlog("The final workload string is " + workloadString);	
	if(workloadString.isEmpty())
	    subWorkloads.add(0, "1");
	else
	    subWorkloads.add(0, workloadString);
	assert(subWorkloads.size() > 0);
	return subWorkloads;		
    }
	
	
	

	public void postProcessing() {
		if(data.size() == 0)
			return;
		updateMaxData();
		updateIRType();
	}




	public IrregularityData getMaxData() {
		return maxData;
	}
	
	
	public AccumulatedData collectAllData(AccumulatedData data, OMPWorkload wl){
	//	wl.accumulateBranches(data);
	//	wl.accumulateReductions(data);
	//	wl.accumulateFalseSharing(data, "");
	//	wl.accumulateSingle(data, false);
	//	wl.accumulateCritical(data, false);
	//	wl.accumulateMaster(data, false);
	//	wl.accumulatedata(data);
		
		return data;
	}

	
	

	public AccumulatedData getAccumulatedData() {
		
		/**
		 * TODO We can either go for the max Node or innermost loop.
		 * Ideally we should go for the innermost Loop. As we are
		 * measuring the irregularity based on the innermost loop 
		 */
		
//		Node maxNode = maxData.node;	
//		OMPWorkload workload = ((CesInfo)maxNode.getInfo()).getOMPWorkload();
//		AccumulatedData totalData =  workload.getAccumulatedData();

		IrregularityData forNode  = getInnermostLoop();		
		ForStatement Fstatement = (ForStatement) forNode.node;
		ForWorkload fWorkload = (ForWorkload) ((CesInfo)Fstatement.getInfo()).getOMPWorkload();
		AccumulatedData totalData = fWorkload.getItrWorkload().getAccumulatedData();
		
		
		return totalData;
	}




	
}
