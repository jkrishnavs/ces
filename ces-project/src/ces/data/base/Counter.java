/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import imop.Main;
import imop.ast.node.Expression;
import ces.SystemConfigFile;
import ces.data.classifier.ClassificationDetails;
import ces.data.classifier.Classifier;
import ces.data.classifier.ClassifyWorkload;
import ces.enums.JKPROJECT;
import ces.util.CesStringMisc;
import imop.parser.CParser;
import stringexp.CounterObj;
import stringexp.Minterms;

import java.util.ArrayList;

import stringexp.CESString;


public class Counter {
	private double constPart;
	private String variablePart;
	
	
	ArrayList<String> unknownVariables;
	
	public static final int maxdegree = 6;
	
	public boolean isconstant() {
		if(variablePart.isEmpty())
			return true;
		return false;
	}
	
	
	
	public String getCoefficientString() {
		ArrayList<Minterms> minterms  = CesStringMisc.reduce(getString());
		String coeff = "[";
		
		double[] coeffdata = new double[3];
		boolean plus = false;
		for (Minterms m : minterms) {
			if(m.isConstant()) 
				coeffdata[2] += m.getConstant(); 
			else {
				if(plus) {
					String minStr = m.getString();
					if(!minStr.isEmpty()) {
						int degree = m.getDegree();
						if(degree <= 2)
							if( m.getConstant() >  0)
								coeffdata[2-degree] +=  m.getConstant();
					}
				} else {
					int degree = m.getDegree();
					if(degree <= 2)
						if( m.getConstant() <  0)
							coeffdata[2-degree] -=  m.getConstant();
				}
			}
		}
		
		for(int i=0;i<3;i++) {
			coeff += coeffdata[i];
			if(i<2)
				coeff += ", ";
		}
		
		coeff += "]";
		return coeff;
	}
	
	void init() {
	}
	
	
	public boolean isZero() {
		if(isconstant() && constPart == 0.0)
			return true;
		else
			return false;
	}
	
	private void setVariablePart(String s) {
		checkVariableError(s);
		this.variablePart = s;
		
	}
	

	

	public Counter() {
		init();
		constPart = 0;
		variablePart = new String();
	}
	
	public Counter(String s) {
		init();
		constPart = 0;
		setVariablePart(s);
	}
	
	
	
	public Counter(Counter c) {
		constPart = c.constPart;
		setVariablePart(c.variablePart);
	}
	
	
	
	public String getString() {
		
		if(variablePart.isEmpty())
			return String.valueOf(constPart);
		return (variablePart + " + " + constPart);
	}
	
	
	public Counter(double i,String s) {
		constPart = i;
		setVariablePart(s);
	}
	
	private void checkVariableError(String s) {
		if(s.contains("+  + ")) {
			Main.addErrorMessage("A  Major error Here");
		}
	}
	
	
	public void add(Counter c) {
		
		this.constPart += c.constPart;
		if(!c.variablePart.isEmpty()) {
			if(variablePart.isEmpty())
				setVariablePart(c.variablePart);
			else
				setVariablePart(variablePart + " + " +  c.variablePart);
		}
		
		checkandReduceVarpart();
	}
	
	private void checkandReduceVarpart() {
		if(variablePart.length() > SystemConfigFile.equationLimit) {
			Main.addlog(String.valueOf(System.identityHashCode(this)));
			this.reduce();
		}
		
	}

	public void setconstPart(double i) {
		constPart = i;
	}
	
	public void addconstPart(double i) {
		constPart += i;
	}
	
	
	public void addvarPart(String s) {
		if(!s.isEmpty()){
			Main.addlog("we are adding " +s);
			if(variablePart.isEmpty())
				setVariablePart(s);
			else
				setVariablePart(variablePart + "  + " + s);
		}
		checkandReduceVarpart();
	}
	
	public ArrayList<String> getUnknownVariableList() {
		return unknownVariables;
	}
	




	/**
	 * This is a copy of getReducedString function in {@link CesStringMisc}
	 * except for the slight difference of the constant minterms 
	 * are added directly added to the constPart. 
	 * @return
	 */
	public String reduce(){
		if(variablePart.isEmpty())
			return variablePart;

		ArrayList<Minterms> minterms  = CesStringMisc.reduce(variablePart);
		String s = ""; 
		boolean plus = false;
		for (Minterms m : minterms) {
			if(m.isConstant()) 
				constPart += m.getConstant();
			else {
				if(plus) {
					String minStr = m.getString();
					if(!minStr.isEmpty())
						s += " + ("+  m.getString() + " ) ";
				} else {
					if(minterms.size() == 1) 
						s = m.getString();
					else {
						String minStr = m.getString();
						if(!minStr.isEmpty()) {
							s = " ( " + m.getString() + " ) ";
							plus = true;
						}
					}
				}
			}
		}
		// Main.addlog("CESString String set for reduction "  + variablePart);
		setVariablePart(s);
		//	 Main.addlog("The variable part is (after reduction): "+ variablePart);
		return variablePart;
	}
	
	
	public String getReducedVariable() {
		reduce();
		/**
		 * TODO The second reduce was added just to remove the extra bracket 
		 * if the variable part has been set to a single variable.
		 * not ideal.
		 * 
		 */
		reduce();
		return variablePart;
	}

	
	public void dumpdata(){
		Main.addlog("Counter: "+ variablePart + " + "+ constPart);
	}
	
	
	public String getdata() {
		if(variablePart.isEmpty())
			return String.valueOf(constPart);
		return variablePart + " + "+ constPart;
	}
	
	public void multiply(String S) {
		if(!variablePart.isEmpty()) {
			if(constPart !=0 )
				setVariablePart(S + " * ( " + variablePart + " + " + constPart + " )");
			else	
				setVariablePart(S + " * ( " + variablePart + " )");
			

			constPart = 0;
		} else {
			if(constPart != 0) {
				if(CesStringMisc.isNumber(S)) {
					constPart = constPart * Double.parseDouble(S);
				} else { 
					setVariablePart(S + " * ( " + constPart + " )");
					constPart  = 0;
				}
		    }
	   }
		checkandReduceVarpart();
		/*
		evaluated = false;
		if(reduced)
			reduce();
			*/
    }
	
	public double getConstPart(){
		return constPart;
	}

	public String getVarPart() {
		return variablePart;
	}
	
	double evaluateddata;
	
	public double getEvaluatedData() {
		evaluate();
		return evaluateddata;
	}
	
	
	double siamscale = 1;
	
	public void setSiamScale(double s) {
		siamscale = s;
	}
	
	
	
	public double getHolderValueofVariable(String var, boolean val) {
		
		if(SystemConfigFile.project == JKPROJECT.SIAM) {
			/***
			 * TODO
			 * */
			
			if(val == true){
				// For negative value negative
				return 0;
			} else if(var.equals("map->maxSize")) {
				return 32.0;
			} else if (var.equals("map->size")){
				return 0.5* siamscale;
			} else {
				return siamscale;
			}
		} else {
			return Classifier.getHolderValueofVariable(var,val);
		}
	}
	
	
	
	public double getEvaluatedDataFromMinterms(ArrayList<Minterms> minterms) {
		double partialSum = 0;
		evaluateddata = 0;
		for (Minterms m : minterms) {
			partialSum = m.getConstant();
			for( String var : m.getVarList()) {
				if(partialSum < 0)
					partialSum = partialSum * getHolderValueofVariable(var,true);
				else
					partialSum = partialSum * getHolderValueofVariable(var,false);
			}
			evaluateddata += partialSum;
		}
		
		return evaluateddata;
	}

	public double evaluate() {
		/*
		if(evaluated)
			return data;
		else {
			 	*/
		
		if(variablePart.isEmpty()) {
			return constPart;
		} else {
//			if (variablePart.length() > SystemConfigFile.equationLimit) {
//				// DIVIDE AND CONQUER
//				data = divideAndConquer(maxSize);
//			} else {			
	//			Main.addlog("CESString String set for evaluation "  + variablePart);
		//		Expression e =  (Expression) CParser.createCrudeASTNode(variablePart, Expression.class);
		//		Main.addlog(e.getInfo().getBareMinimumString());
				ArrayList<Minterms> minterms = CesStringMisc.reduce(variablePart);
				evaluateddata = getEvaluatedDataFromMinterms(minterms);
//			}
		}
		if(constPart != 0.0)
			evaluateddata += constPart;
		
		return evaluateddata;
	}

	
	
	public String getMostSignificantTermWithVariable(String iterator) {
		if(variablePart.isEmpty())
			return "";
		ArrayList<Minterms> reduced = CesStringMisc.reduce(" ( " + variablePart + ") + " +constPart); 
		
		Minterms selected = new Minterms();
		
		for(Minterms m : reduced) {
			if(! m.findCoefficent(iterator).isEmpty()) {
				if(selected.getDegree() < m.getDegree()) {
					selected = m;
				} else if (selected.getDegree() == m.getDegree() && 
						selected.getConstant() < m.getConstant()) {
					selected = m;
				}
			}
		}
		return selected.getString();
	}

	public Counter multiplyConst(double branchconst) {
		Counter retVal = new Counter();
		retVal.setconstPart(constPart*branchconst);
		//Main.addlog("The variable Part is :" + variablePart + ".");
		if(variablePart != null && ! variablePart.isEmpty())
			retVal.setVariablePart(branchconst + " * ( "+variablePart + ")");
		return retVal;
	}

	public String getMostSignificantTerm() {
		if(variablePart.isEmpty())
			return String.valueOf(constPart);
		
		ArrayList<Minterms> reduced = CesStringMisc.reduce(" ( " + variablePart + ") + " +constPart); 
		return reduced.get(reduced.size() -1).getString();
	}
	

	
	
//	

//	private class OperandData{
//		int size;
//		int pos;
//		int operators;
//		Object operand;
//	}
//	
//	private OperandData getoperanddata(ArrayList<Object> operandQueue, int pos){
//		OperandData data = new OperandData();
//		data.size = 0;
//		int k = pos-1;
//		data.operators = 0;
//		Object operand = operandQueue.get(k);
//		if(!CesStringMisc.isOperator(operand)){
//			// single operand
//			data.operand = operand;
//			data.size = 1;
//			data.pos = k;
//			return data;
//		}
//		while(data.size < data.operators+1){
//			if(CesStringMisc.isOperator(operand))
//				data.operators++;
//			else
//				data.size++;
//			k--;
//			if(k==-1){
//				// End of Array
//				if(data.size < data.operators+1)
//					Main.addErrorMessage("Some error in determining the operands " + converttoString(operandQueue));
//				else
//					break;
//			}
//			data.operand = operand;
//			operand = operandQueue.get(k);
//		}
//		data.size = data.size + data.operators;
//		data.pos = k+1;
//		return data;
//	}
//	
//
//	
//	
//	private Varlist multiply(Varlist product,Object operand){
//		if(operand instanceof String){
//			/*either single var or double */
//			if(CesStringMisc.isNumber((String)operand)){
//				product.multiply(Double.parseDouble((String)operand));
//			}else{
//				product.multiply((String)operand);
//			}
//		}else if(operand instanceof Varlist){
//			Varlist op = (Varlist) operand;
//			product.multiply(op.data);
//			for (String var : op.varlist) {
//				product.multiply(var);
//			}
//		}else{
//			Main.addErrorMessage("Unhandled Error  !!!!" + operand.toString());
//		}
//		return product;
//	}
//	
//	
//	private void removeRange(ArrayList<Object> operandQueue,int start,int end){
//		
//		for (int i = 0; i < end - start; i++) {
//			operandQueue.remove(start);
//		}
//		
//	}
//	
//	
//	private int getType(Object obj){
//		int type;
//		if(obj instanceof Varlist){
//			type = 3;
//		}else if(CesStringMisc.isNumber((String) obj)){
//			type = 1;
//		}else
//			type = 2;
//		
//		return type;
//	}
//	
//	
//	private ArrayList<Object> copyList(ArrayList<Object> distributor){
//		ArrayList<Object> newList = new ArrayList<Object>();
//		for (int i = 0; i < distributor.size(); i++) {
//			if(distributor.get(i) instanceof Varlist){
//				Object copy = new Varlist((Varlist)distributor.get(i));
//				newList.add(copy);		
//			}else{
//				Object copy = new String((String)distributor.get(i));
//				newList.add(copy);
//			}
//		}
//		return newList;
//	}
//	
//	private ArrayList<Object> expandMultiplicationoverAddition(ArrayList<Object> expanded, ArrayList<Object> distributor){
//		ArrayList<Object> copy  = copyList(distributor);
//		OperandData op2 = getoperanddata(expanded, expanded.size()-1);
//		OperandData op1 = getoperanddata(expanded, op2.pos);
//		ArrayList<Object> result = new ArrayList<Object>();
//		result.addAll(expanded.subList(op1.pos,op1.pos+op1.size));
//		result.addAll(copy);
//		result.add("*");
//		result.addAll(expanded.subList(op2.pos,op2.pos+op2.size));
//		result.addAll(distributor);
//		result.add("*");
//		result.add(expanded.get(expanded.size()-1));
//		return result;
//	}
//	
//	private ArrayList<Object> expandMultiplicationoverDivision(ArrayList<Object> expanded, ArrayList<Object> distributor){
//		ArrayList<Object> result = new ArrayList<Object>();
//		OperandData op2 = getoperanddata(expanded, expanded.size()-1);
//		OperandData op1 = getoperanddata(expanded, op2.pos);
//		result.addAll(expanded.subList(op1.pos,op1.pos+op1.size));
//		result.addAll(distributor);
//		result.add("*");
//		result.addAll(expanded.subList(op2.pos,op2.pos+op2.size));
//		result.add(expanded.get(expanded.size()-1));
//		return result;
//	}
//	
//	
//	private ArrayList<Object> distributeMultiplicationOverOperation(ArrayList<Object> op1,ArrayList<Object> op2){
//	
//		if(op2.size() > 1){
//			ArrayList<Object> expanded = null;
//			int operandtype = getOperationPreference((String)op2.get(op2.size()-1));
//			if(operandtype < getOperationPreference("*")){
//				expanded = expandMultiplicationoverAddition(op2, op1);
//			}else{
//				expanded = expandMultiplicationoverDivision(op2, op1);
//			}
//			return expanded;
//		}
//		
//		
//		if(op1.size() > 1){
//			ArrayList<Object> expanded = null;
//			int operandtype = getOperationPreference((String)op1.get(op1.size()-1));
//			if(operandtype < getOperationPreference("*")){
//				expanded = expandMultiplicationoverAddition(op1, op2);
//			}else{
//				expanded = expandMultiplicationoverDivision(op1, op2);
//			}
//			return expanded;
//		}
//		return null;
//	}
//	
////	private ArrayList<Object> distributeAdditionoverAddition(ArrayList<Object> expanded,ArrayList<Object> distributed, Object operand1){		
////		OperandData op2 = getoperanddata(expanded, expanded.size()-1);
////		OperandData op1 = getoperanddata(expanded, op2.pos);
////		
////		ArrayList<Object> combination1 = new ArrayList<Object>();	
////		ArrayList<Object> combination2 = new ArrayList<Object>();
////		
////		/**
////		 * Make copy ?
////		 * a b c + +
////		 */
////		
///////		ArrayList<Object> a = new ArrayList<>()
////		
////		ArrayList<Object> A = new ArrayList<Object>(expanded.subList(op1.pos,op1.pos+op1.size));
////		ArrayList<Object> B = new ArrayList<Object>(expanded.subList(op2.pos,op2.pos+op2.size));
////		ArrayList<Object> C = new ArrayList<Object>(distributed);
////		
////		/**
////		 * Compare a & c
////		 * compare a & b
////		 */
////		boolean ac = true, ab = true;
////		if(A.size() == C.size()){
////			for (int i = 0; i < A.size(); i++) {
////				Object sum = add(A.get(i),C.get(i),"+");
////				if(sum == null){
////					ac = false;
////					break;
////				}
////			}
////		}else{
////			ac = false;
////		}
////		
////		
////		if(A.size() == B.size()){
////			for (int i = 0; i < A.size(); i++) {
////				Object sum = add(A.get(i),B.get(i),"+");
////				if(sum == null){
////					ab = false;
////					break;
////				}
////			}
////		}else{
////			ab = false;
////		}
////
////		if(ab==true){
////			
////			combination1.addAll(expanded.subList(op1.pos,op1.pos+op1.size));
////			combination1.addAll(expanded.subList(op2.pos,op2.pos+op2.size));
////			combination1.add(operand1);
////			combination1.addAll(distributed);
////			combination1.add(expanded.get(expanded.size()-1));
////			
////			return combination1;
////			
////		}else if(ac == true){
////			combination2.addAll(expanded.subList(op1.pos,op1.pos+op1.size));
////			combination2.addAll(distributed);
////			combination2.add(expanded.get(expanded.size()-1));
////			combination2.addAll(expanded.subList(op2.pos,op2.pos+op2.size));
////			combination2.add(operand1);
////			
////			return combination2;
////			
////		}
////		
////		return null;
////	}
//	
//	private ArrayList<Object> distributeAdditionoverDivision(ArrayList<Object> expanded,ArrayList<Object> distributed, Object operand){
//	return null;
//	}
//	
//	private ArrayList<Object> ditributeAdditionOverOperation(ArrayList<Object> op1,ArrayList<Object> op2, Object operand){
//		int operand2type  = 0;
//		int operand1type = 0;
//		if(op2.size() > 1)
//			operand2type = getOperationPreference((String)op2.get(op2.size()-1));
//		if(op1.size() > 1)
//			operand1type = getOperationPreference((String)op1.get(op1.size()-1));
//		if(op1.size() > 1 && op2.size() > 1 &&
//				operand2type > getOperationPreference("+")	&&
//				operand1type > getOperationPreference("+")){
//			return null;
//		}
//		
//		
//		if(op2.size() > 1){
//			
//			if(operand2type <= getOperationPreference("+")){
//			//	Removing because we are handling these additions more efficiently at the end.
//			// This will result in redundant work.
//			//	ArrayList<Object> result = distributeAdditionoverAddition(op2, op1,operand);
//				return null;
//			}else{
//			ArrayList<Object> result = distributeAdditionoverDivision(op2, op1, operand);
//				return result;
//			}
//		}
//		
//		if(op1.size() > 1){
//			if(operand1type <= getOperationPreference("+")){
//				//				Removing because we are handling these additions more efficiently at the end.
//				// This will result in redundant work.
//			//	ArrayList<Object> result = distributeAdditionoverAddition(op2, op1,operand);
//				return null;
//			}else{
//				ArrayList<Object> result = distributeAdditionoverDivision(op2, op1, operand);
//				return result;
//			}
//		}
//		
//	
//		return null;
//	}
//	
//	
//	private ArrayList<Object> ditributeDivisionOverOperation(ArrayList<Object> expanded,ArrayList<Object> distributed){
//		
//		int operandtype = getOperationPreference((String)expanded.get(expanded.size()-1));
//		OperandData op2 = getoperanddata(expanded, expanded.size()-1);
//		OperandData op1 = getoperanddata(expanded, op2.pos);
//		ArrayList<Object> result = new ArrayList<Object>();
//		if(operandtype == getOperationPreference("+")){
//			/**
//			 *  (a + b)/c  => ab+ c\
//			 *  a/c + b/c=> ac \  b c \ +
//			 * 
//			 */
//
//			
//			ArrayList<Object> distributedcopy =  copyList(distributed);
//			result.addAll(expanded.subList(op1.pos, op1.pos+op1.size));
//			result.addAll(distributed);
//			result.add("/");
//			result.addAll(expanded.subList(op2.pos, op2.pos+op2.size));
//			result.addAll(distributedcopy);
//			result.add("/");
//			result.add(expanded.get(expanded.size()-1));
//			return result;
//		}else if(operandtype == getOperationPreference("/")){
//			/**
//			 * a/b/c => abc //
//			 * ac/ b/
//			 */
//			result.addAll(expanded.subList(op1.pos, op1.pos+op1.size));
//			result.addAll(distributed);
//			expanded.get(expanded.size()-1);
//			result.addAll(expanded.subList(op2.pos, op2.pos+op2.size));
//			result.add("/");
//			return result;
//		}
//		
//		Main.addErrorMessage("The operand that needs to be expanded over division is neither / or + / -. but it is " + expanded.get(expanded.size()-1).toString());
//		return null;
//	}
//	
//	
//	private Object add(Object operand1,Object operand2,Object operand){
//		int type1 = getType(operand1);
//		int type2 = getType(operand2);
//		if(type1 != type2)
//			return null;
//		if(type1 == 1){
//			Double value;
//			if(operand.equals("+") )
//				value = Double.parseDouble((String) operand1) +  Double.parseDouble((String) operand2);
//			else
//				value = Double.parseDouble((String) operand1) -  Double.parseDouble((String) operand2);
//			
//			return String.valueOf(value);
//		}
//		if(type2 == 2){
//			if(operand1.equals(operand2)){
//				Varlist sum = new Varlist();
//				sum.multiply((String)operand1);
//				if(operand.equals("+"))
//					sum.data = 2;
//				else
//					sum.data = 0;
//				return sum;
//			}else
//				return null;
//		}
//		Varlist op1 = (Varlist) operand1;
//		Varlist op2 = (Varlist) operand2;
//		
//		if(op1.eqivalentdegree(op2)){
//			if(operand.equals("+"))
//				op1.data += op2.data;
//			else
//				op1.data -= op2.data;
//			return op1;
//		}else
//			return null;
//	}
//	
//	private boolean noVariablePart(Object operand){
//		if(operand instanceof Varlist){
//			return ((Varlist)operand).varlist.isEmpty();
//		}
//		try
//		{
//		  Double.parseDouble((String)operand);
//		}
//		catch(NumberFormatException e)
//		{
//		  return false;
//		}
//		return true;
//	}
//	
//	private Double getConstantValue(Object operand){
//		if(operand instanceof Varlist){
//			return ((Varlist)operand).data;
//		}
//		try
//		{
//		  return Double.parseDouble((String)operand);
//		}
//		catch(NumberFormatException e)
//		{
//		  Main.addErrorMessage("Error here in parsing double");
//		}
//		
//		return 0.0;
//	}
//	private ArrayList<Object> extractDoubleandReduceSum(ArrayList<Object> operandQueue){
//		// Reduce sum.
//		ArrayList<Object> result  = new ArrayList<Object>();
//		// Check we have only + or - else 
//		// we have a problem.
//		boolean onlyAddition =  true;
//		
//		if(operandQueue.size() > 1){
//			Main.addlog("Here");
//		}
//		
//		double data = 0.0;
//		for (int i = 0; i < operandQueue.size(); i++) {
//			if(CesStringMisc.isOperator(operandQueue.get(i))){
//				if(getOperationPreference((String)operandQueue.get(i))!= getOperationPreference("+")){
//					onlyAddition = false;
//				}
//			}
//		}
//		if(!onlyAddition){
//			Main.addlog("The remaining unresolved varPart operations other than addition or subtraction "+ converttoString(operandQueue));
//			return operandQueue;
//		}
//		else{
//			// all addition or subtraction
//			if(!noVariablePart(operandQueue.get(0))){
//				result.add(operandQueue.get(0));
//			}else
//				data = getConstantValue(operandQueue.get(0));
//			Object operand;
//			for (int i = 1; i < operandQueue.size(); i++) {
//				int operandcount  = 0; int operatorcount = 0;
//					if(!CesStringMisc.isOperator(operandQueue.get(i))){
//					// operand, now get its operator.
//						int k=i+1;
//						operand =  operandQueue.get(i);
//						while(!(operandcount==operatorcount && CesStringMisc.isOperator(operandQueue.get(k)))){ 
//							if(CesStringMisc.isOperator(operandQueue.get(k)))
//								operatorcount++;
//							else	
//								operandcount++;
//							k++;
//						}
//						Object operator = operandQueue.get(k);
//						boolean added = false;
//						for (int j = 0; j < result.size(); j++) {
//							Object sum = add(result.get(j), operand, operator);
//							if(sum != null){
//								added = true;
//								result.remove(j);
//								result.add(j,sum);
//								break;
//							}
//						}
//						if(!added){
//							if(noVariablePart(operand)){
//								if(operator.equals("+"))
//									data  += getConstantValue(operand);
//								else
//									data -= getConstantValue(operand);
//							}else{
//								result.add(operand);
//								result.add(operator);
//							}
//						}
//					}
//					
//				}
//			}
//		if(data != 0.0){
//			constPart += data;
//		}
//		return result;
//	}
//	                                                 
//	
//	
//	
//	private String converttoString(ArrayList<Object> operandQueue){
//		int i = 0;
//	//	Deque<Integer> opdata = new ArrayDeque<Integer>();
//		Deque<String>  data = new ArrayDeque<String>();
//		while(operandQueue.size() > 0) {
//			if(CesStringMisc.isOperator(operandQueue.get(i))){
//				String operator = ( String )operandQueue.remove(i);
//				String operand2 = data.pop();
//				String operand1 = data.pop();
//				String res = "( " +  operand1 + "  " + operator + "  " + operand2 + " ) " ;
//				data.push(res);
//			}else{
//				String s;
//				Object operand =  operandQueue.remove(i);
//				if (operand instanceof Varlist) {
//					s = ((Varlist) operand).getString();
//
//				}else{
//					s = (String) operand;
//				}
//				data.push(s);
//			}
//		}
//
//		if(data.size() > 1)
//			Main.addErrorMessage("Some error in processing postfix to infix stack size " + data.size());
//		if(data.isEmpty()){
//			Main.addlog("The String is empty after reduction");
//			return "";
//		}
//		return data.pop();
//	}
//	
//	private Object divide(Object operand1,Object operand2){
//		int type1 = getType(operand1);
//		int type2 = getType(operand2);		
//		
//		if(type1 == 1 && type2 == 1){
//			Double quotient = Double.parseDouble((String) operand1)/ Double.parseDouble((String) operand2);
//			return quotient;
//		}
//		if(type1 == 1 && type2 >1){
//			return null;
//		}
//		
//		if(type1 == 2 && type2 == 1){
//			Varlist quotient = new Varlist();
//			quotient.multiply((String)operand1);
//			quotient.data = 1.0/Double.parseDouble((String) operand2);
//			return quotient;
//		}
//		if(type1 == 2 && type2 == 2){
//			if(operand1.equals(operand2)){
//				return "1.0";
//			}else
//				return null;
//		}
//		if(type1 == 2 && type2 == 3){
//			Varlist op2 = (Varlist) operand2;
//			if(op2.varlist.size()==1 && op2.varlist.get(0).equals(operand1)){
//				return String.valueOf(1.0/op2.data);
//			}else
//				return null;
//		}if(type1 == 3 && type2 == 1){
//			
//			Varlist op1 = (Varlist) operand1;
//			op1.data  = op1.data / Double.parseDouble((String) operand2);
//			return op1;
//		}if(type1 == 3 && type2 == 2){
//			Varlist op1 = (Varlist) operand1;
//			boolean found  = op1.findandRemoveVar((String)operand2);
//			if(found)
//				return op1;
//			else
//				return false;
//		}if(type1 == 3 && type2 == 3){
//			Varlist quotient = new Varlist();
//			Varlist op1 = (Varlist) operand1;
//			Varlist op2 = (Varlist) operand2;
//			quotient.data  = op1.data / op2.data;
//			for (int i = 0; i < op1.varlist.size(); i++) {
//				quotient.multiply(op1.varlist.get(i));
//			}
//			boolean dividable = true;
//			for (int i = 0; i < op2.varlist.size(); i++) {
//				dividable = quotient.findandRemoveVar(op2.varlist.get(i));
//				if(!dividable)
//					return null;
//			}
//			return quotient;
//		}
//		
//		return null;
//	}
//	
//	private int updateResult(ArrayList<Object> operandQueue, int pos, Object result){	
//		operandQueue.remove(pos);
//		operandQueue.remove(pos-1);
//		operandQueue.remove(pos-2);
//		operandQueue.add(pos-2, result);
//		return pos-2;
//	}
//	
//	
//	public String reducePostFixExpression(ArrayList<Object> operandQueue){
//		/*evaluate post fix expression*/
//		for (int i = 0; i < operandQueue.size(); i++) {
//			Object val = operandQueue.get(i);
//			if(CesStringMisc.isOperator(val)){
//				
//				OperandData op2data =  getoperanddata(operandQueue, i);
//				OperandData op1data =  getoperanddata(operandQueue, op2data.pos);
//				if(val.equals("*")){
//					if(op1data.size == 1 && op2data.size == 1){
//						Varlist product = new Varlist();
//						product.data = 1.0;
//						product = multiply(product, op1data.operand);
//						product = multiply(product, op2data.operand);
//						i = updateResult(operandQueue, i, product);
//					}else{
//						ArrayList<Object> expanded = distributeMultiplicationOverOperation(
//								new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos + op1data.size)),
//								new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos + op2data.size)));
//						if(expanded != null){
//							removeRange(operandQueue, op1data.pos, op1data.pos+op1data.size + op2data.size+1);
//							operandQueue.addAll(op1data.pos, expanded);
//							i =  op1data.pos;
//						}else{
//							Main.addErrorMessage("Error in distributing multiplication over operands " +
//									converttoString(new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos + op1data.size))) +
//									" and "	+
//									converttoString(new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos + op2data.size))));
//						}
//					}
//				}else if(val.equals("+")){
//					if(op1data.size == 1 && op2data.size == 1){
//						Object sum = add(op1data.operand, op2data.operand,val);
//						if(sum != null){
//							i = updateResult(operandQueue, i, sum);
//						}
//					}else{
//						ArrayList<Object> expanded = ditributeAdditionOverOperation(
//								new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos+op1data.size)),
//								new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos+op2data.size)),
//								val);
//						if(expanded != null){
//							removeRange(operandQueue, op1data.pos, op1data.pos+op1data.size + op2data.size + 1);
//							operandQueue.addAll(op1data.pos, expanded);
//							i =  op1data.pos;
//						}else{
//							Main.addlog("Error in distributing addition over operands " +
//									converttoString(new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos+op1data.size))) +
//									" and "	+
//									converttoString(new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos+op2data.size))));
//						}
//					}
//				}else if(val.equals("-")){
//					if(op1data.size == 1 && op2data.size == 1){
//						Object sum = add(op1data.operand, op2data.operand,val);
//						if(sum != null){
//							i = updateResult(operandQueue, i, sum);
//						}
//					}else{
//						ArrayList<Object> expanded = ditributeAdditionOverOperation(
//								new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos+op1data.size)),
//								new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos+op2data.size)),
//								val);
//						if(expanded != null){
//							removeRange(operandQueue, op1data.pos, op1data.pos+ op1data.size + op2data.size + 1);
//							operandQueue.addAll(op1data.pos, expanded);
//							i =  op1data.pos;
//						}else{
//							Main.addlog("Error in distributing subtraction over operands " +
//									converttoString(new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos+op1data.size))) +
//									" and "	+
//									converttoString(new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos+op2data.size))));
//						}
//					}
//				}else if(val.equals("/")){
//					if(op1data.size == 1 && op2data.size ==1){
//						Object quotient = divide(op1data.operand,op2data.operand);
//						if(quotient != null){
//							i = updateResult(operandQueue, i, quotient);
//						}else{
//							Main.addErrorMessage("Error in dividing " + op1data.operand.toString() + " with " + op2data.operand.toString());
//						}
//					}else{
//						ArrayList<Object> expanded = ditributeDivisionOverOperation(
//								new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos + op1data.size)),
//								new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos + op2data.size)));
//						if(expanded != null){
//							removeRange(operandQueue, op1data.pos, op1data.pos + op1data.size + op2data.size + 1);
//							operandQueue.addAll(op1data.pos, expanded);
//							i =  op1data.pos;
//						}else{
//							Main.addlog("Error in distributing division over operands " +
//									converttoString(new ArrayList<Object>(operandQueue.subList(op1data.pos, op1data.pos + op1data.size))) +
//									" and "	+
//									converttoString(new ArrayList<Object>(operandQueue.subList(op2data.pos, op2data.pos + op2data.size))));
//						}
//					}
//
//				}
//			}
//		}
//		return converttoString(extractDoubleandReduceSum(operandQueue));
//	}
//	
//	
//	public  String reduceString(ArrayList<String> var){
//
//		/* make post fix expression. */
//		ArrayList<Object> operandQueue = new ArrayList<Object>();
//		Deque<String> operatorQueue = new ArrayDeque<String>();
//		for (int i = 0; i < var.size(); i++) {
//			String cur = var.get(i);
//			if(cur.equals("(")){
//				operatorQueue.push(cur);
//			}else if(cur.equals(")")){
//				String strVal = operatorQueue.pop();
//				while (!strVal.equals("(")) {
//					operandQueue.add(strVal);
//					strVal = operatorQueue.pop();
//				}
//			}else if(cur.equals("*") ||
//					cur.equals("+")  ||
//					cur.equals("/")  ||
//					cur.equals("-")  ){
//				while(operatorQueue.size() > 0 && (getOperationPreference(cur) < getOperationPreference(operatorQueue.peek()))){
//					operandQueue.add(operatorQueue.pop());
//				}
//				operatorQueue.push(cur);
//			}else if(CesStringMisc.isOperand(cur)){
//				operandQueue.add(cur);
//			}else if(CesStringMisc.isNumber(cur)){
//				operandQueue.add(cur);
//			}else{
//				Main.addErrorMessage("Unhandled operand/operation " + cur);
//			}
//		}
//		while (operatorQueue.size() > 0) {
//			operandQueue.add(operatorQueue.pop());		
//		}
//
//		return reducePostFixExpression(operandQueue);
//		
//	} 
//	
//	public  String reduceVarPart(String v){
//		Main.addlog("String before reduction is "+ v);
//		ArrayList<String> tokens = CESString.getTokensofOuterExpression(v);
//		variablePart = reduceString(tokens);
//		return variablePart;
//	} 
//	
//	
//	
//	
//	

//	
//	public Counter multiplyConst(double C){
//		Counter retVal = new Counter();
//		retVal.setconstPart(constPart*C);
//		//Main.addlog("The variable Part is :" + variablePart + ".");
//		if(variablePart != null && ! variablePart.isEmpty())
//			retVal.setvarPart(C + " * ( "+variablePart + ")");
//		return retVal;
//	}
//	
//	
//
//
//	


//	public void prconstEqn(){
//		Main.addlog("\n The integral part is "+ constPart +
//				           "\n The variable part is "+ variablePart);
//	}
//	
//	
//	
//
//	
//	public boolean isequal(Counter c){
//		if(constPart == c.getConstPart() 
//			&& variablePart.equals(c.getVarPart()) )
//			return true;
//		return false;
//	}
//
//	public Counter getRatio(Counter c){
//		Counter ratio = new Counter();
//		ratio.addconstPart(1);
//		Main.addlog("Asking for ratio mine and input");
//		dumpdata();
//		c.dumpdata();
//		ratio.setvarPart(CesStringMisc.divideSOPString(getdata(), c.getdata()));
//		return ratio;
//	}
//	
//	public String getIncrementalValue(String incrementer){
//		if(!isReduced())
//		reduce();
//		String retVal = CesStringMisc.divideSOPString(variablePart, incrementer);
//		return retVal;
//	}
//	
//	

//
//

//	public String getMostSignificantTermWithVariable(String var){
//		reduce();
//	//	ArrayList<String>  = CESString.getVariableList(variablePart);
//		
//		return CesStringMisc.getMostSignificantTermWithVariable(variablePart,var);
//	}
//	
//	
//
//
//

//
//	/*
//
//	public String getMostSignificantTerm() {
//		reduce();
//		if(variablePart.isEmpty())
//			return String.valueOf(constPart);
//		else{ 
//			Main.addlog("CESSTRING most significant Term "+ variablePart);
//			CESString.mostSignificantTerm(variablePart);
//		}
//		return null;
//	}
//	
//	*/
	
}