/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import imop.Main;
import ces.enums.EigenMetric;

public class OmpParallelDataCounter  extends BlockCounter {
	Counter singleCalls;
	Counter criticalCalls;
	Counter ompForCalls;
	//Counter reductionCalls;
	Counter masterCalls;
	
	
	public OmpParallelDataCounter(){
		super();
		singleCalls = new Counter();
		criticalCalls = new Counter();
		ompForCalls = new Counter();
		//reductionCalls = new Counter();
		masterCalls  =  new Counter();
	}
	
	public void addSingleCalls(double i) {
		singleCalls.addconstPart(i);
	}
	
	public void addCriticalCalls(double i) {
		criticalCalls.addconstPart(i);
	}
	
	public void addOmpForCalls(double i) {
		ompForCalls.addconstPart(i);
	}
	
	
	
	/*
	void addReductionCalls(double i) {
		reductionCalls.addconstPart(i);
	}
	*/
	
	
	public void addMasterCalls(double i) {
		masterCalls.addconstPart(i);
	}
	
	
	
	@Override
	public void multiply(String m) {
		if(m.isEmpty()) {
			Main.addErrorMessage("The string is not properly set");
		}
		super.multiply(m);
		singleCalls.multiply(m);
		criticalCalls.multiply(m);
		ompForCalls.multiply(m);
		masterCalls.multiply(m);
		
	}
	
	
	public void add(OmpParallelDataCounter b) {
		super.add(b);
		OmpParallelDataCounter bc = (OmpParallelDataCounter) b;
		singleCalls.add(bc.singleCalls);
		criticalCalls.add(bc.criticalCalls);
		ompForCalls.add(bc.criticalCalls);
		masterCalls.add(bc.flushCalls);
}
	
	
	@Override
	public String dumpdata() {
		return super.dumpdata() + 
				"\n Single Calls " + singleCalls.getdata() +
				"\n criticalCalls " + criticalCalls.getdata() +
				"\n OmpforCalls " + ompForCalls.getdata() +
				"\n masterCalls " + masterCalls.getdata();
	}
	
	
	double singleData;
	double criticalData;
	
	@Override
	public double evaluate() {
		super.evaluate();
		singleData = singleCalls.evaluate();
		criticalData = criticalCalls.evaluate();
		ompForCalls.evaluate();
		return data;
	}
	

	
	@Override
	public double getALUFactor(EigenMetric m) {
		switch (m) {
		case ompBarriers:
			return barrierData/aluData;
		case ompCritical:
			return criticalData/aluData;
		case ompFlush:
		case total:
		case MemoryOps:
		case branches:
		case ALUOps:
			return super.getALUFactor(m);
		default:
			Main.addErrorMessage("Unexpected reach point" + m);
		}
		return 1;
	}
	
	@Override
	public double getFactor(EigenMetric m) {
		
		switch (m) {
		case ompCritical:
			return criticalData/data;
		case ompFlush:
		case ompBarriers:
			case total:
		case MemoryOps:
		case branches:
		case ALUOps:
			return super.getFactor(m);
		default:
			Main.addErrorMessage("Unexpected reach point" + m);
		}
		return 1;
	}
}
