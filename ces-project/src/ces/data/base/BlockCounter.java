/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import imop.Main;
import ces.SystemConfigFile;
import ces.data.aluoperations.ALUOperations;
import ces.data.memoryOps.MemoryOperations;
import ces.enums.EigenMetric;
import ces.enums.Metric;

public class BlockCounter {
	Counter alu;
	Counter mem;
	Counter sharedMem;
	Counter arrayMem;
	Counter privateMem;
	Counter unknownFc;
	Counter branch;
	Counter flushCalls;
	private Counter barrierCalls;
	
	
	public boolean isEmpty() {
		return alu.isZero() && mem.isZero() && sharedMem.isZero() && arrayMem.isZero() 
		&&  privateMem.isZero() && unknownFc.isZero() && branch.isZero() && flushCalls.isZero() 
		&& barrierCalls.isZero();
	}
	
	
	

	public void addOMPBarrierCalls(double i) {
		barrierCalls.addconstPart(i);
	}
	
	boolean evaluated;
	
	public void addALUOperations(ALUOperations a) {
		alu.addconstPart(a.expectedARITHOperations());
		alu.addconstPart(a.expectedBitwiseOperations());
		alu.addconstPart(a.expectedMULTOperations());
	}
	
	
	public void addArrayOperations(double s) {
		arrayMem.addconstPart(s);
	}
	
	
	public void addVarPart(String s, Metric m) {
		switch(m) {
		case branches:
			branch.addvarPart(s);
			break;
		case funcCalls:
			unknownFc.addvarPart(s);
			break;
		case memread:
		case memwrite:
			mem.addvarPart(s);
			break;
		case ops_arith:
		case ops_bitwise:
		case ops_mult:
			alu.addvarPart(s);
			break;
		case privateRead:
		case privateWrite:
			privateMem.addvarPart(s);
			break;
		case sharedRead:
		case sharedWrite:
			sharedMem.addvarPart(s);
			break;
		case ompbarrier:
			barrierCalls.addvarPart(s);
			break;
		default:
			break;
		
		}
		
	}
	
	public void addConstPart(double s, Metric m) {
		switch(m) {
		case branches:
			branch.addconstPart(s);
			break;
		case funcCalls:
			unknownFc.addconstPart(s);
			break;
		case memread:
		case memwrite:
			mem.addconstPart(s);
			break;
		case ops_arith:
		case ops_bitwise:
		case ops_mult:
			alu.addconstPart(s);
			break;
		case privateRead:
		case privateWrite:
			privateMem.addconstPart(s);
			break;
		case sharedRead:
		case sharedWrite:
			sharedMem.addconstPart(s);
			break;
		case ompbarrier:
			barrierCalls.addconstPart(s);
			break;
		default:
			break;
		}
		
		
	}
	
	
	public void addMemoryOps(MemoryOperations m) {
		mem.addconstPart(m.getVal(Metric.memread));
		mem.addconstPart(m.getVal(Metric.memwrite));
		sharedMem.addconstPart(m.getVal(Metric.sharedRead));
		sharedMem.addconstPart(m.getVal(Metric.sharedWrite));
		privateMem.addconstPart(m.getVal(Metric.privateRead));
		privateMem.addconstPart(m.getVal(Metric.privateWrite));
		unknownFc.addconstPart(m.getVal(Metric.funcCalls));	
	}
	
	
	public void addFlushCalls(double i) {
		flushCalls.addconstPart(i);
	}
	
	

	public void addalu(Counter a) {
		alu.add(a);
	}
	
	public void addmem(Counter m) {
		mem.add(m);
	}
	
	public void addbranch(Counter b) {
		branch.add(b);
	}
	
	public void addSharedMem(Counter m) {
		sharedMem.add(m);
	}
	
	void addPrivateMem(Counter m) {
		privateMem.add(m);
	}
	
	void addunKnownFc(Counter f) {
		unknownFc.add(f);
	}
	
	void addArrayMem(Counter arr) {
		arrayMem.add(arr);
	}
	
	
	void addBarrier(Counter b) {
		barrierCalls.add(b);
	}
	
	/*
	void addNonArrayMem(Counter non) {
		nonArrayMem.add(non);
	}
	*/
	Counter getBranch() {
		return branch;
	}
	
	Counter getAlu() {
		return alu;
	}
	
	Counter getMem() {
		return mem;
	}
	
	Counter getSharedMem() {
		return sharedMem;
	}
	
	Counter getPrivateMem() {
		return privateMem;
	}
	
	Counter getunFnc() {
		return unknownFc;
	}
	
	Counter getArraCounter() {
		return arrayMem;
	}
	
	Counter getBarrierCounter() {
		return barrierCalls;
	}
	
	
	/*
	Counter getNonArrCounter() {
		return nonArrayMem;
	}
	*/
	
	public BlockCounter() {
		alu = new Counter();
		mem = new Counter();
		branch = new Counter();
		sharedMem = new Counter();
		privateMem = new Counter();
		unknownFc = new Counter();
		arrayMem = new Counter();
		flushCalls = new Counter();
		barrierCalls = new Counter();
		
		evaluated = false;
		
	}
	
	public void add(BlockCounter b) {
		alu.add(b.alu);
		branch.add(b.branch);
		mem.add(b.mem);
		sharedMem.add(b.sharedMem);
		privateMem.add(b.privateMem);
		unknownFc.add(b.unknownFc);
		arrayMem.add(b.arrayMem);
		flushCalls.add(b.flushCalls);
		barrierCalls.add(b.barrierCalls);
		
	}
	
	public void multiply(String s) {
		alu.multiply(s);
		branch.multiply(s);
		mem.multiply(s);
		sharedMem.multiply(s);
		privateMem.multiply(s);
		unknownFc.multiply(s);	
		flushCalls.multiply(s);
		
	//	nonArrayMem.multiply(s);
		arrayMem.multiply(s);	
		barrierCalls.multiply(s);
	}
	
	public String dumpdata(){
	 return  "\nAlu ops" + alu.getdata() +
		"\n Branchops " + branch.getdata() +
	   "\n Mem ops" + mem.getdata() +
	   "\n sharedMem " + sharedMem.getdata() +
	   "\n unKnown Fc " + unknownFc.getdata() +
	   "\n flushCalls " + flushCalls.getdata() +
	   "\n arrayMem " + arrayMem.getdata()+
	 	"\n barrierCalls " + barrierCalls.getdata();
		
	}


	public double data;
	public double aluData;
	public double memData;
	public double flushData;
	public double branchData;
	public double functionData;
	public double barrierData;
	
	
	
	
	double maxSize;
	
	public double evaluate() {
		data = 0;
		aluData = alu.evaluate();
		branchData = branch.evaluate();
		memData = mem.evaluate();
		flushData = flushCalls.evaluate();
		branchData = branch.evaluate();
		functionData = unknownFc.evaluate();
		barrierData = barrierCalls.evaluate();
		data += aluData + branchData + memData + flushData + functionData + barrierData; 
		

		//data.add(arrayMem.evaluate(maxSize));
		evaluated = true;
		this.maxSize = maxSize;
		return data;
	}
	
	
	public double getALUFactor(EigenMetric m) {
		assert(evaluated);
		switch (m){
		case ALUOps:
			return 1;
		case MemoryOps:
			return memData/aluData;
		case ompFlush:
			return flushData/aluData;
		case branches:
			return (branchData + functionData)/aluData;
		case total:
			return data/aluData;
		case ompBarriers:
			return barrierData/aluData;
		default:
		Main.addErrorMessage("Unexpected reaching point" + m);
		}
		
		return 1;
	}
	
	public double getBaseData() {
		if(SystemConfigFile.ALUFACTOR)
			return aluData;
		else
			return data;
	}
	
	public double getFactor(EigenMetric m) {
		assert(evaluated);
		switch (m){
		case ALUOps:
			return aluData /data;
		case MemoryOps:
			return memData/data;
		case ompFlush:
			return flushData /data;
		case branches:
			return (branchData + functionData)/data;
		case total:
			return 1;
		case ompBarriers:
			return barrierData/data;
		default:
			Main.addErrorMessage("Unexpected reaching point" + m);
		}
		
		return 1;
	}
}
