/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import java.util.ArrayList;

import imop.Main;

public class KnownVariableSizes {
	
	/*
	 * TO use this we need to have a alias analysis, constant propagation and pointer analysis.
	 *  TODO use this
	 *  TODO set this
	 */
	
	/**
	 * We will use this later to get
	 * 
	 * 
	 * @author jkrishnavs
	 *
	 */
	
	public class VarSize {
		String name;
		int size;
		
		public VarSize(String name, int size) {
			this.name = name;
			this.size = size;
		}
		
		public int getSize() {
			return size;
		}
		
		public void setSize(int s){
			this.size = s;
		}
		
		public boolean isVariable(String n) {
			return name.equals(n);
		}
	}
	
	public ArrayList<VarSize> list = new ArrayList<VarSize>();
	
	public void addtoList(String var, int size) {
		boolean found  = false;
		for(VarSize s : list) {
			if(s.isVariable(var)) {
				found  =true;
				if(size != s.getSize()) {
					Main.addErrorMessage("Error, smae varibale set twice " +var);
					s.setSize(-1);
				}
			}
		}
		if(!found) {
			list.add(new VarSize(var, size));
		}
	}
	
	
	public int getSize(String name) {
		for(VarSize s : list) {
			if(s.isVariable(name))
				return s.getSize();
			
		}
		
		return -1;
	}

}
