/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.Node;
import ces.data.aluoperations.ALUOperations;
import ces.util.SystemStringConstants;
import imop.lib.analysis.dataflow.Symbol;

public class GraphModelData {
	
	
	
	
	
	public void addmemOp(String s) {
		boolean found;
		found = false;
		for(String t: memops) {
			if(t.trim().equals(s.trim())) {
				found = true;
				break;
			}
		}
		if(found == false)
			memops.add(s);
	}
	
	
	public void addrandOps(String s) {
		boolean found;
		found = false;
		for(String t: randOps) {
			if(t.trim().equals(s.trim())) {
				found = true;
				break;
			}
		}
		if(found == false)
			randOps.add(s);
	}
	
	
	
	public void addmemOps(ArrayList<String> reads) {
		boolean found;
		for(String s: reads) {
			found = false;
			for(String t: memops) {
				if(t.trim().equals(s.trim()))
					found = true;
			}
			if(found == false)
				memops.add(s);
		}
	}
	

	
	public void addArrayOp(String s) {
		boolean found;
		found = false;
		for(String t: arrayops) {
			if(t.trim().equals(s.trim())) {
				found = true;
				break;
			}
		}
		if(found == false)
			arrayops.add(s);
	}
	
	public void addarrayops(ArrayList<String> reads) {
		boolean found;
		for(String s: reads) {
			found = false;
			for(String t: arrayops) {
				if(t.trim().equals(s.trim()))
					found = true;
			}
			if(found == false)
				arrayops.add(s);
		}
	}
	
	
	
	
	
	
	public void setIrregularity(Node n) {
		// assert(pConstruct != null);
		Main.addlog(n.getInfo().getString());
		assert(irr == null);
		irr = n;
	}
	
	public Node getIrregularityNode() {
		return irr;
	}
	
	
	public void setParallelConstruct(Node n) {
		pConstruct = n;
	}
	
	public Node getParallelConstructNode() {
		if(pConstruct != null)
			return pConstruct;
		else if (parent != null)
			return parent.getParallelConstructNode();
		return null;
	}
	
	public class GraphModelChilddata{
		String multiplier;
		GraphModelData data;
		public GraphModelChilddata(String m, GraphModelData d) {
			multiplier = m;
			data = d;
		}
	}
	
	GraphModelData parent;
	
	public void addChildData(String s, GraphModelData d) {
		childDatas.add(new GraphModelChilddata(s, d));
		d.parent = this;
		d.addIterators(this.getIterators());
	}
	
	public void addReductionList(ArrayList<String> l) {
		reductionList = l;
	}
	
	public int numberofReductions(ArrayList<String> r) {
		/**
		 * IMP add reductions with parent.
		 * 
		 */
		int reductions=0;
		if(reductionList.size() == 0 || r.size() == 0){
			return 0;
		}
		for(String s : r) {
			for(String t: reductionList) {
				if(s.trim().equals(t.trim()))
					reductions++;
			} 
		}
		return reductions;
	}
	
	
	ArrayList<GraphModelChilddata> childDatas;
	ArrayList<String> reductionList;
	boolean isIrregular;
	
    Node irr; /*Irregular loop Node*/
	Node pConstruct; /* The parallel construct containing the irregular Node*/ 
	ArrayList<String> memops; /* Total regular memory ops in this workload */
	ArrayList<String> arrayops; /* Total regular array operations in this workload */
	ArrayList<String> randOps; /* Total random operations 
	*/
	ArrayList<String> iterators; /* iterator */
	
	public ArrayList<String> getMemops() {
		return memops;
	}
	
	/** 
	 * @param var : The declaring Symbol of the iterator Variable
	 * @param itr : Iterator of the Declaring node of the iterator @param var 
	 * @param baseArray : The random Access.
	 */
	public void addRandVariable(Symbol var, String itr, String baseArray) {
		if(itr == null) {
			arrayops.add(baseArray);
		}else if(parent == null) {
			randOps.add(baseArray);
		}else if(itr.trim().equals(getMyIterator().trim())) {
			randOps.add(baseArray);
		} else {
			parent.addRandVariable(var, itr, baseArray);
		}
	}
	
	
	/*ALU operations as scale*/
	Counter scale;
	public void addALUOperations(double alu) {scale.addconstPart(alu);}
	public void multiplyALUOPerations(String m) {	scale.multiply(m);}
	public double getALUOperations() {		return scale.evaluate();}
	public double getScaleVal() {	return scale.evaluate(); }
	public String getStringScaleVal() { scale.reduce();  return scale.getdata(); }
	
	
	/*sequential memory access and individual memory access*/
	Counter seqVal;
	public void addSeqVal(double seq) {seqVal.addconstPart(seq);}
	public void multiplySeqVal(String m) {seqVal.multiply(m);}
	public double getSeqVal(){ return seqVal.getEvaluatedData();}
	public String getStringSeqVal() {seqVal.reduce();return seqVal.getdata();}
	
	public String trainData() {
				
		String train  = "[";
		train += seqVal.getCoefficientString() + ", " 
		       + randVal.getCoefficientString() + ", "
		       + atomics.getCoefficientString() + ", "
		       + scale.getCoefficientString();
		train += "]";
		
		return train;
	}
	
	/*Random accesses on individual locations */
	Counter randVal;
	// public void addRandVal(double r) {randVal.addconstPart(r);}
	public void multiplyRandVal(String s) {randVal.multiply(s);}
	public double getRandVal() {return randVal.getEvaluatedData();}
	public String getStringRandVal() {randVal.reduce(); return randVal.getdata();}
	/* atomic operations */
	Counter atomics;
	public void addAtomics(double a) { atomics.addconstPart(a);}
	public void multiplyAtomics(String s) { atomics.multiply(s);}
	public double getAtomics() {return atomics.getEvaluatedData(); }
	public String getStringAtomics() { atomics.reduce(); return atomics.getdata();}
	/*lock operations */
	Counter locks;
	public void addLocks(double d) { locks.addconstPart(d);}
	public  void multiplyLocks(String s) {locks.multiply(s);}
	private double getLocks() { return locks.evaluate();}
	
	
	
	String myIterator = null;
	
	public void addIterator(String s) {
		assert(myIterator == null);
		myIterator = s;
		iterators.add(s);
	}
	
	public void addIterators(ArrayList<String> strs) {
		iterators.addAll(strs);
	}
	
	public ArrayList<String> getIterators( ) {
		return iterators;
	}
	
	
	public static final int NOTITERATOR = 0;
	public static final int ITERATOR = 1;
	public static final int INNERMOSTITERATOR = 2;
	
	public int isIterator(String s) {
		boolean found = false;
		for(String itr: iterators) {
			if(itr.trim().equals(s.trim())) {
				found = true;
				break;
			}
		}
		if(found) {
			
			
			if(getMyIterator().trim().equals(s.trim()))
				return INNERMOSTITERATOR;
			return ITERATOR;
		} 
		
		return NOTITERATOR;
	}
	
	
	public String getMyIterator() {
		/* If myIterator == null
		 * Then we are inside a conditional statement
		 * Then we should adopt the immediately enclosing
		 * loop's iterator. 
		 * */
		if(myIterator == null)
			myIterator = parent.getMyIterator();
		return myIterator;
	}
	
	
	Node node = null;
	
	public GraphModelData(Node n) {
		node = n; /* For debuging*/
		isIrregular = false;
		seqVal = new Counter();
		randVal = new Counter();
		atomics = new Counter();
		locks = new Counter();
		scale = new Counter();
		irr = null;
		pConstruct = null;
		iterators = new ArrayList<String>();
		childDatas = new ArrayList<GraphModelChilddata>();
		reductionList = new ArrayList<String>();
		randOps = new ArrayList<String>();
		memops = new ArrayList<String>();
		arrayops = new ArrayList<String>();
		parent  = null;
	}
	
	public boolean isIrregular() {	return isIrregular; }
	public void setIrregular(boolean i) { isIrregular  = i; if(parent != null) parent.setIrregular(i);}
	
	


	public void accumulateData() {
	 	for(GraphModelChilddata c : childDatas) {
	 		c.data.accumulateData();
	 		if(c.multiplier.equals(SystemStringConstants.GRAPHSIZE)) {
	 			 clearChildData();
	 		} else {
	 			c.data.addUpArrayandRandOps();
	 			c.data.multiplySeqVal(c.multiplier);
	 			c.data.multiplyRandVal(c.multiplier);
	 			c.data.multiplyAtomics(c.multiplier);
	 			c.data.multiplyALUOPerations(c.multiplier);
	 		}
			c.data.dumpData();
			collectChildData(c);
		}
	}	



	public void dumpData() {
		Main.addlog(node.getInfo().getString());
		Main.addlog("seqVal  " + seqVal.getString());
		Main.addlog("randVal " + randVal.getString());
		Main.addlog("atomics " + this.atomics.getString());
		Main.addlog("scale " + scale.getString());
	}


	
	public void addUpMemOps(){
		seqVal.addconstPart(memops.size());
	}

	public void upateBaseVal(GraphPropertiesData properties) {
		// Main.addlog(node.getInfo().getString());
		//seqVal.addconstPart(arrayops.size());
		seqVal.setSiamScale(properties.getavgEdges());
		randVal.setSiamScale(properties.getavgEdges());
		atomics.setSiamScale(properties.getavgEdges());
		scale.setSiamScale(properties.getavgEdges());
		//seqVal.evaluate();
		// randVal.evaluate();
		// scale.evaluate();
		// atomics.evaluate();
		
	}

	private void addUpArrayandRandOps() {
		seqVal.addconstPart(arrayops.size());
		randVal.addconstPart(randOps.size());
	}

	private void collectChildData(GraphModelChilddata c) {
		
		seqVal.add(c.data.seqVal);
		randVal.add(c.data.randVal);
		atomics.add(c.data.atomics);
		scale.add(c.data.scale);
		memops.addAll(c.data.getMemops());
	}
	
	
	boolean neverCleared = true;
	
	private void clearChildData() {
		/**
		 * TODO handle multiple forconstructs.
		 * 
		 */
		assert(neverCleared);
		seqVal = new Counter();
		randVal = new Counter();
		atomics = new Counter();
		neverCleared = false;
	}



	public void addLockstoatomics(double lockScale) {
		
		addAtomics(getLocks() / lockScale);
	}

}
