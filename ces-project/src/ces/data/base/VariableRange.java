/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import imop.Main;
import ces.SystemConfigFile;

public class VariableRange {
	double maxValue;
	double rangeUpperLimit;
	double rangeLowerLimit;
	
	ArrayList<String> variablesInRange;
	
	
	/***
	 * Lists all the constants for which we will use the
	 * actual value rather than the max value.
	 */
	
	
	HashMap<String, Double> constants;
	
	public VariableRange(double maxValue) {
		this.maxValue = maxValue;
		
		if(this.maxValue != 0) {
			double lowerLimit = 1; 
			double upperLimit = SystemConfigFile.logBase;
			while(maxValue > upperLimit) {
				lowerLimit = upperLimit;
				upperLimit =  lowerLimit * SystemConfigFile.logBase;
			}
			this.rangeLowerLimit = lowerLimit;
			this.rangeUpperLimit = upperLimit;
		} else {
			this.rangeLowerLimit = 0;
			this.rangeUpperLimit = 0;
		}
		variablesInRange = new ArrayList<String>();
		constants = new HashMap<String, Double>();
	}
	
	public double upperLimit() {
		return rangeUpperLimit;
	}
	
	public void addVariable(String s) {
		variablesInRange.add(s);
	}
	
	public void addConstant(String s, Double value) {
		constants.put(s, value);
	}
	
	public double lowerLimit() {
		return rangeLowerLimit;
	}
	
	public boolean variableFound(String var) {
		for(String varString : variablesInRange) {
			if(varString.equals(var))
				return true;
			/**
			 * TODO We get back string from StringExpression 
			 * Project without any spaces.
			 * So for non primitive variables (like a[b]) we need
			 * to remove 
			 */
			String varStringWithoutSpaces = varString.replaceAll("\\s+","");
			if(varStringWithoutSpaces.equals(var))
				return true;
		}
		return false;
	}
	
	
	/**
	 * if sign is true This is going into an negative element so we need to get the min value.
	 * Currently setting it to N/ : we are asking for the minimum value.
	 * TODO : properly collect the min value and return.
	 * @param varName TODO
	 */
	public double getVariableValue(boolean sign, String varName) {
		if(constants.get(varName) != null)
			return constants.get(varName);
		
		if(sign)
			return maxValue/2;
		else
			return maxValue;
	}

	public boolean contains(double constant) {
		if(rangeLowerLimit <= constant && constant <= rangeUpperLimit)
			return true;
		return false;
	}
	
	public void removeVariableFromRange(String v) {
		for ( int i = 0;  i < variablesInRange.size(); i++) {
			String tempName = variablesInRange.get(i);
			if(tempName.equals(v) ) {
				variablesInRange.remove(i);
			}
		}
	}

	public void compareandSetValue(double value) {
		if(maxValue < value)
			maxValue = value;
	}
	
	public double getMaxValue() {
		return maxValue;
	}

	public void addlog() {
		
		String l = "FORVARVALUE  " + maxValue + " : " ;
		for(String s: variablesInRange) {
			l += ",  " + s;
		}
		Main.addlog(l);
		for (Map.Entry<String, Double> entry : constants.entrySet()) {
			Main.addlog("CONSTANTVALUES " + entry.getKey() +": " + entry.getValue());
		}
	}
}
