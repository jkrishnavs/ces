/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import imop.Main;

public class GraphPropertiesData {
	double density;
	double avgEdges;
	double aed;
	double cluster;
	
	double seqVal;
	double randVal;
	double atomics;
	
	long switchpoint;
	public void setSwitchPoint(long s) {	switchpoint = s;}
	public long getSwitchPoint() { return switchpoint; }
	
	
	
	public double getdensity() {		return density; }
	public void setdensity(double d) {		density = d; }
	public double getavgEdges() {		return avgEdges;}
	public void setAvgEdges(double d) {		avgEdges = d;	}
	public double getaed() {		return aed;}
	public void setaed(double d) {		aed = d;	}
	public double getCluster() {		return cluster;}
	public void setCluster(double d) {		cluster = d;}
	
	public double getSeqVal() {return seqVal; }
	public double getRandVal() {return randVal; }
	public double getAtomics() { return atomics; }
	
	public void setSeqVal(double s) {seqVal = s; }
	public void setRandVal(double s) { randVal = s; }
	public void setAtomics(double s) {atomics = s; }
	public void updateAlgoData(GraphModelData data) {
		
		Main.addlog("Seq Data: " + data.getStringSeqVal());
		Main.addlog("Rand Val: " + data.getStringRandVal());
		Main.addlog("Atomics: " + data.getStringAtomics());
		Main.addlog("Scale: " + data.getStringScaleVal());
		
		
		Main.addlog("Train data :" + data.trainData());
		
		seqVal = data.getSeqVal() / data.getScaleVal();
		randVal = data.getRandVal() /  data.getScaleVal();
		atomics = data.getAtomics() /  data.getScaleVal();
	}
}
