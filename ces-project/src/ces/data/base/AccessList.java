/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import imop.lib.analysis.dataflow.Symbol;

import java.util.Vector;


/**
 * Class created for handling the variable access list more carefully
 * The read list is supposed to contain the variables that are read.
 * The write list is supposed to contain the variables that are written
 * The access list will contain all the varibales that accessed.
 * 
 * 
 * The read and write lists will contain an entry for each access
 *  of the variable, while in accesslist we will purge duplicate entries
 *  using the util function
 * */
public class AccessList {
	private Vector<Symbol> readList;
	private Vector<Symbol> writeList;
	private Vector<Symbol> accessList;
	
	
	public void Init(){
		readList = new Vector<Symbol>();
		writeList= new Vector<Symbol>();
		accessList = new Vector<Symbol>();
		
	}
	
	public AccessList() {
		Init();
	}
	
	public AccessList(Vector<Symbol> l){
		Init();
		addtoaccessListall(l);
	}
	
	
	

	public Vector<Symbol> getReadList(){
		return readList;
	}
	
	
	public Vector<Symbol> getWriteList(){
		return writeList;
	}
	
	public void setReadList(Vector<Symbol> l){
		readList = l;
	}
	
	public void setWriteList(Vector<Symbol> l){
		writeList = l; 
	} 
	
	
	public void addtoreadListall(Vector<Symbol> l){
		readList.addAll(l);
	}
	
	public void addtoreadList(Symbol l){
		readList.add(l);
	}
	
	public void addtoWriteList(Symbol l){
		writeList.add(l);
	}
	
	public void addtoWriteListAll(Vector<Symbol> l){
		writeList.addAll(l);
	}
	
	
	public void addtoaccessListall(Vector<Symbol> l){
		accessList.addAll(l);	
	}
	
	public void addtoaccesslist(Symbol l){
		accessList.add(l);
	}
	
	public Vector<Symbol> getAccessList(){
		return accessList;
	}
}
