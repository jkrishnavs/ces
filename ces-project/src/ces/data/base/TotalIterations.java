/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.base;

import java.util.HashSet;

import imop.ast.node.Node;
import ces.util.SystemStringConstants;
import imop.lib.analysis.dataflow.Symbol;

public class TotalIterations {
	
	String iterationString;
	
	Symbol upperLimit;
	
	Node upperLimitNode; // when it is more than a single symbol
	
	Node lowerLimitNode; 
	
	Symbol lowerLimit;
	
	
	HashSet<Symbol> involvedSymbols;
	
	public boolean isSingleSymbolList() {
		if(upperLimit != null  && lowerLimit !=  null)
			return true;
		return false;
	}
	
	public Node getUpperLimitNode() {
		return upperLimitNode;
	}
	
	public Node getLowerLimitNode() {
		return lowerLimitNode;
	}
	
	public Symbol getupperLimit() {
		return upperLimit;
	}
	
	public Symbol  getLowerLimit() {
		return lowerLimit;
	} 
	
	
	public TotalIterations(String sysConstants) {
		involvedSymbols =  new HashSet<Symbol>();
		upperLimit = null;
		lowerLimit = null;
		upperLimitNode =  null;
		lowerLimitNode = null;
		iterationString = sysConstants;
	}
	
	
	public TotalIterations(Symbol ul, Symbol lL) {
		involvedSymbols =  new HashSet<Symbol>();
		upperLimit = ul;
		lowerLimit = lL;
		involvedSymbols.add(ul);
		involvedSymbols.add(lL);
		iterationString = "(" + upperLimit.getName() + " - " + lowerLimit.getName() + ")";
	}
	
	public TotalIterations(Node ul, Node lL, String str) {
		involvedSymbols =  new HashSet<Symbol>();
		upperLimitNode = ul;
		lowerLimitNode = lL;
		upperLimit = null;
		
		lowerLimit = null;
		String upperlimitString = ul.getInfo().getString();
		
		HashSet<Symbol> symbols = ul.getInfo().getSharedReads();
		if(symbols.size() ==  1)
			upperLimit = symbols.iterator().next();
		involvedSymbols.addAll(symbols);
		
		HashSet<Symbol>lowerSymbols = lL.getInfo().getSharedReads();
		if(lowerSymbols.size() ==  1)
			lowerLimit = lowerSymbols.iterator().next();
		involvedSymbols.addAll(lowerSymbols);
		
		if(str == null)
			iterationString = "(" + upperLimitNode.getInfo().getString() + " - " + lowerLimitNode.getInfo().getString() + ")";
		else
			iterationString = str;
		
	}
	
	@Override
	public String toString() {
		return iterationString;
	}
	
	public String getString() {
		return iterationString;
	}
	
	public String evaluate() {
		// TODO	
		return iterationString;
	}

	public boolean isEmpty() {
		return iterationString.isEmpty();
	}

	public boolean contains(String string) {
		// TODO Auto-generated method stub
		return iterationString.contains(string);
	}
	
	public String getValueSubstitutedString() {
		String retVal = iterationString;
		if(iterationString.contains(SystemStringConstants.INFINITY)) {
			retVal = retVal.replace(SystemStringConstants.INFINITY, " 1 "); 
		}
		
		return retVal;
	}
	
	

}
