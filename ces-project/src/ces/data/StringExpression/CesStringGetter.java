/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.StringExpression;

import imop.ast.node.CastExpressionTyped;
import imop.ast.node.NodeToken;
import ces.SystemConfigFile;
import ces.enums.JKPROJECT;
import imop.lib.getter.StringGetter;

public class CesStringGetter extends StringGetter {
	
	
	/**
	 * f0 -> "("
	 * f1 -> TypeName()
	 * f2 -> ")"
	 * f3 -> CastExpression()
	 */
	public void visit(CastExpressionTyped n) {
		/*
		 * n.f0.accept(this);
		 * n.f1.accept(this);
		 * n.f2.accept(this);
		 */
		n.f3.accept(this);
	}
	
	public void visit(NodeToken n) { 
		String res = n.toString();
		
		if(res.equals("\t")) {
			return;
		}else if(res.equals("NIL_NODE") && SystemConfigFile.project == JKPROJECT.GRAPH) {
			/**
			 * TODO;
			 * GreenMarl hack. remove this later. 
			 * 
			 */
			str += "gm_graph::" + res + " ";
		} else {
			str += res + " "; 
		}
		return; 
	}


}
