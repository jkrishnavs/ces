/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.GraphData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import ces.SystemConfigFile;
import ces.data.base.GraphModelData;
import ces.data.base.GraphPropertiesData;

public class ParseGraphProperties {

	public GraphPropertiesData parseGraphProp(GraphPropertiesData data) {
		BufferedReader newReader;
		
		try{
			newReader = new BufferedReader(new FileReader(SystemConfigFile.graphPropertyFile));
			String line  = newReader.readLine();
			
			while(line != null) {
				if(line.contains("Adjecency")) {
					String value = line.substring(line.indexOf('=')+1, line.length()); 
					value = value.trim();
					data.setAvgEdges(Double.parseDouble(value));
				} else if (line.contains("ClusterCoeff")) {
					String value = line.substring(line.indexOf('=')+1, line.length()); 
					value = value.trim();
					data.setCluster(Double.parseDouble(value));
				} else if (line.contains("Distance")) {
					String value = line.substring(line.indexOf('=')+1, line.length()); 
					value = value.trim();
					data.setaed(Double.parseDouble(value));
				} else if (line.contains("Sparsity")) {
					String value = line.substring(line.indexOf('=')+1, line.length()); 
					value = value.trim();
					data.setdensity(Double.parseDouble(value));
				}
				
				line = newReader.readLine(); 
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return data;
	}

}
