/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.workloadGetter;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Stack;
import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.GJDepthFirstProcess;
import ces.SystemConfigFile;
import ces.ast.info.CesParallelConstructInfo;
import ces.data.base.Counter;
import ces.data.base.GraphModelData;
import ces.data.base.TotalIterations;
import ces.data.classifier.GraphClassifier;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.cg.CallSite;
import imop.lib.util.Misc;
import stringexp.CESString;

public class OMPGraphWorkloadGetter extends GJDepthFirstProcess<GraphModelData, GraphModelData> {
	
	
	/***
	 * TODO Currently this is not inter procedural.
	 * 
	 */
	
	
	GraphModelData finaldata;
	
	
	ArrayList<GraphModelData> parallelWorkloadData;
	
	
	
	
	Stack<GraphModelData> fnList;
	
	GraphClassifier classifier;
	
	
	public String conditionalScale = "0.5";
	
	

	private String getConditionalScale(IfStatement n) {
		
		//if()
		
		/* TODO add more complex settings */
		return conditionalScale;
	}

	
	
	
	
	
	private void addToParallelWorkloadData(GraphModelData data) {
		parallelWorkloadData.add(data);
	}
	
	
	GraphModelData myData;
	
	
	private void updateMYSTATUS(int newStatus) {
		myStatus = newStatus;
	}
	
	
	
	public void init(Node root) {
		parallelWorkloadData = new ArrayList<GraphModelData>();
		finaldata = null;
		myData = null;
		classifier = new GraphClassifier(root);
	}
	
	
/*	
	enum curStatus {
		startofFunction,
		insideParallelRegion,
		insideWorkSharingConstruct,
		insideIrregularLoad,
		outSideIrregularLoad,
		outSideWorkSharingConstruct,
		outsideParallelRegion
	};
*/
	
	private static final int startofFunction = 0;
	private static final int insideParallelRegion = 1;
	private static final int insideWorkSharingConstruct = 2;
	private static final int insideIrregularLoad = 3;
	
	
	
	
	
	int myStatus = startofFunction;
	
	int countOperations;

	public GraphModelData getGraphModel() {
		if (finaldata != null) {
			return finaldata;
		} else {
			if( parallelWorkloadData.size() == 0)
				return null;
			if(parallelWorkloadData.size() == 1) {
				finaldata = parallelWorkloadData.get(0);
				return finaldata;
			}
				
			for( GraphModelData data : parallelWorkloadData) {
				if(data.isIrregular()) {
					/**
					 * TODO currently returning the first irregular data.
					 * return the largest irregular data
					 */
					finaldata = data;
					return finaldata;
				}
			}
		}
		
		return finaldata;
	}
	
	private void setFinalData(GraphModelData data) {
		/**
		 * IMP use workload based allocation
		 */
		if(data.isIrregular()) {
			finaldata = data; 
			
		}
	}

	
	
	
	
	public GraphModelData visit(NodeList n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
			_ret = e.nextElement().accept(this,argu);
		}
		return _ret;
	}

	public GraphModelData visit(NodeListOptional n, GraphModelData argu) {
		 GraphModelData _ret = null;
		initProcess(n, argu);
		if ( n.present() ) {
			for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
				_ret = e.nextElement().accept(this,argu);
			}
		}
		endProcess(n, argu);
		return _ret;
	}

	public GraphModelData visit(NodeOptional n, GraphModelData argu) {
		 GraphModelData _ret = null;
		initProcess(n, argu);
		if ( n.present() ) {
			_ret = n.node.accept(this,argu);
		}
		endProcess(n, argu);
		return _ret;
	}

	public GraphModelData visit(NodeSequence n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
			_ret = e.nextElement().accept(this,argu);
		}
		endProcess(n, argu);
		return _ret;
	}

	public GraphModelData visit(NodeChoice n, GraphModelData argu) {
		 GraphModelData _ret = null;
		initProcess(n, argu);
		_ret = n.choice.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	public GraphModelData visit(NodeToken n, GraphModelData argu) {
		initProcess(n, argu);
		endProcess(n, argu);
		return null;	
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> ParallelDirective()
	 * f2 -> Statement()
	 */
	public GraphModelData visit(ParallelConstruct n, GraphModelData argu) {
		Main.addlog(n.getInfo().getString());
		assert(myStatus < insideParallelRegion);
		myData = new GraphModelData(n);
		updateMYSTATUS(insideParallelRegion);
		myData.setParallelConstruct(n);
		addToParallelWorkloadData(myData);
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		updateMYSTATUS(insideParallelRegion-1);
		setFinalData(myData);
		return _ret;
	}
	
	
	
	/**
	 * f0 -> DeclarationSpecifiers()
	 * f1 -> ( InitDeclaratorList() )?
	 * f2 -> ";"
	 */
	public GraphModelData visit(Declaration n, GraphModelData argu) {
		GraphModelData _ret=null;
		//initProcess(n, argu);
		//n.f0.accept(this, argu);
		//n.f1.accept(this, argu);
		
		if(myData != null) {
			ArrayList<Node> declaratorlist = CesSpecificMisc.collectAllChildrensofClass(n, InitDeclarator.class.getName());
			for(Node c : declaratorlist) {
				InitDeclarator dec = ((InitDeclarator)c);
				if(!dec.f1.getInfo().getString().isEmpty())
					CollectoperandsandOperations(c, myData);
				else 
					myData.addALUOperations(1);
			}

		}
		//n.f2.accept(this, argu);
		//endProcess(n, argu);
		return _ret;
	}
	
	
	/**
	 * f0 -> <REDUCTION>
	 * f1 -> "("
	 * f2 -> ReductionOp()
	 * f3 -> ":"
	 * f4 -> VariableList()
	 * f5 -> ")"
	 */
	public GraphModelData visit(OmpReductionClause n, GraphModelData argu) {
		GraphModelData _ret=null;
		initProcess(n, argu);
		//		ArrayList<String> varList;
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		ArrayList<String> str = CesSpecificMisc.getVarList(n.f4);
		myData.addReductionList(str);
		endProcess(n, argu);
		return _ret;
	}

	





	/**
	 * f0 -> OmpPragma()
	 * f1 -> ForDirective()
	 * f2 -> OmpForHeader()
	 * f3 -> Statement()
	 */
	public GraphModelData visit(ForConstruct n, GraphModelData argu) {
		
		/***
		 * WE don't handle reductions right now.
		 * 
		 */
		assert(myStatus < insideWorkSharingConstruct);
		updateMYSTATUS(insideWorkSharingConstruct);
		Node pc = null;
		GraphModelData data =  new GraphModelData(n);
		if(myData != null) {
			pc = myData.getParallelConstructNode();
			myData.addChildData(SystemStringConstants.GRAPHSIZE, data);
		} else {
			// We have not seen a parallel construct in this region
			// so add it to parallel construct workload list.
			addToParallelWorkloadData(data);
		}
		GraphModelData backup = myData;
		myData  = data;
		if(pc != null) {
			myData.setParallelConstruct(pc);
		}
		
		Symbol itr  = CesSpecificMisc.getIterator(n);
		myData.addIterator(itr.getName());
		
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		updateMYSTATUS(insideWorkSharingConstruct -1);
		myData = backup;
		return myData;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTIONS>
	 * f2 -> NowaitDataClauseList()
	 * f3 -> OmpEol()
	 * f4 -> SectionsScope()
	 */
	public GraphModelData visit(SectionsConstruct n, GraphModelData argu) {
		Main.addErrorMessage("We are curently not handling this construct "+ n.getClass().getName());
		assert(false);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> "{"
	 * f1 -> ( Statement() )?
	 * f2 -> ( ASection() )*
	 * f3 -> "}"
	 */
	public GraphModelData visit(SectionsScope n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTION>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public GraphModelData visit(ASection n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SINGLE>
	 * f2 -> SingleClauseList()
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public GraphModelData visit(SingleConstruct n, GraphModelData argu) {
		Main.addErrorMessage("We are curently not handling this construct "+ n.getClass().getName());
		assert(myStatus < insideWorkSharingConstruct);
		updateMYSTATUS(insideWorkSharingConstruct);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		endProcess(n, argu);
		updateMYSTATUS(insideWorkSharingConstruct-1);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASK>
	 * f2 -> ( TaskClauseList() )*
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public GraphModelData visit(TaskConstruct n, GraphModelData argu) {
		Main.addErrorMessage("We are curently not handling this construct "+ n.getClass().getName());
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <FOR>
	 * f3 -> UniqueParallelOrUniqueForOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> OmpForHeader()
	 * f6 -> Statement()
	 */
	public GraphModelData visit(ParallelForConstruct n, GraphModelData argu) {
		assert(myStatus < insideParallelRegion);
		updateMYSTATUS(insideWorkSharingConstruct);
		myData = new GraphModelData(n);
		myData.setParallelConstruct(n);
		addToParallelWorkloadData(myData);
		myData.addIterator(CesSpecificMisc.getIterator(n).getName());
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
		endProcess(n, argu);
		updateMYSTATUS(insideParallelRegion);
		setFinalData(myData);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <SECTIONS>
	 * f3 -> UniqueParallelOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> SectionsScope()
	 */
	public GraphModelData visit(ParallelSectionsConstruct n, GraphModelData argu) {
		Main.addErrorMessage("We are curently not handling this construct "+ n.getClass().getName());
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <MASTER>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public GraphModelData visit(MasterConstruct n, GraphModelData argu) {
		Main.addErrorMessage("We are curently not handling this construct "+ n.getClass().getName());
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <CRITICAL>
	 * f2 -> ( RegionPhrase() )?
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public GraphModelData visit(CriticalConstruct n, GraphModelData argu) {
		
		
	
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		
		if(myStatus < insideWorkSharingConstruct) {
			countOperations = 0;
		} else if(n.f4.f0.choice instanceof CompoundStatement) {
			countOperations =  ((CompoundStatement)n.f4.f0.choice).f1.nodes.size();
		} else { 
			countOperations  = 1;
		}
		
		n.f4.accept(this, argu);
		
		myData.addAtomics(countOperations);
		
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ATOMIC>
	 * f2 -> ( AtomicClause() )?
	 * f3 -> OmpEol()
	 * f4 -> ExpressionStatement()
	 */
	public GraphModelData visit(AtomicConstruct n, GraphModelData argu) {
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		
		
		
		countOperations = 1;
		
		n.f4.accept(this, argu);
		
		if(myStatus >= insideWorkSharingConstruct)
			myData.addAtomics(countOperations);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <FLUSH>
	 * f2 -> ( FlushVars() )?
	 * f3 -> OmpEol()
	 */
	public GraphModelData visit(FlushDirective n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		
		
		

		if(myStatus >= insideWorkSharingConstruct)
			myData.addAtomics(1);
		
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> "("
	 * f1 -> VariableList()
	 * f2 -> ")"
	 */
	public GraphModelData visit(FlushVars n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ORDERED>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public GraphModelData visit(OrderedConstruct n, GraphModelData argu) {
		assert(1==0);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <BARRIER>
	 * f2 -> OmpEol()
	 */
	public GraphModelData visit(BarrierDirective n, GraphModelData argu) {
		GraphModelData _ret=null;
		if(myStatus < insideWorkSharingConstruct) {
			return myData;
		} else {
			initProcess(n, argu);
			n.f0.accept(this, argu);
			n.f1.accept(this, argu);
			n.f2.accept(this, argu);
			endProcess(n, argu);
		}
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKWAIT>
	 * f2 -> OmpEol()
	 */
	public GraphModelData visit(TaskwaitDirective n, GraphModelData argu) {
		assert(0 == 1);
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKYIELD>
	 * f2 -> OmpEol()
	 */
	public GraphModelData visit(TaskyieldDirective n, GraphModelData argu) {
		assert(0==1);
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <THREADPRIVATE>
	 * f2 -> "("
	 * f3 -> VariableList()
	 * f4 -> ")"
	 * f5 -> OmpEol()
	 */
	public GraphModelData visit(ThreadPrivateDirective n, GraphModelData argu) {
		assert(0==1);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <DECLARE>
	 * f2 -> <REDUCTION>
	 * f3 -> "("
	 * f4 -> ReductionOp()
	 * f5 -> ":"
	 * f6 -> ReductionTypeList()
	 * f7 -> ":"
	 * f8 -> Expression()
	 * f9 -> ")"
	 * f10 -> ( InitializerClause() )?
	 * f11 -> OmpEol()
	 */
	public GraphModelData visit(DeclareReductionDirective n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
		n.f7.accept(this, argu);
		n.f8.accept(this, argu);
		n.f9.accept(this, argu);
		n.f10.accept(this, argu);
		n.f11.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> ( TypeSpecifier() )*
	 */
	public GraphModelData visit(ReductionTypeList n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> AssignInitializerClause()
	 *       | ArgumentInitializerClause()
	 */
	public GraphModelData visit(InitializerClause n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "="
	 * f4 -> Initializer()
	 * f5 -> ")"
	 */
	public GraphModelData visit(AssignInitializerClause n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "("
	 * f4 -> ExpressionList()
	 * f5 -> ")"
	 * f6 -> ")"
	 */
	public GraphModelData visit(ArgumentInitializerClause n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <IDENTIFIER>
	 *       | "+"
	 *       | "*"
	 *       | "-"
	 *       | "&"
	 *       | "^"
	 *       | "|"
	 *       | "||"
	 *       | "&&"
	 */
	public GraphModelData visit(ReductionOp n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ( "," <IDENTIFIER> )*
	 */
	public GraphModelData visit(VariableList n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> SimpleLabeledStatement()
	 *       | CaseLabeledStatement()
	 *       | DefaultLabeledStatement()
	 */
	public GraphModelData visit(LabeledStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ":"
	 * f2 -> Statement()
	 */
	public GraphModelData visit(SimpleLabeledStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <CASE>
	 * f1 -> ConstantExpression()
	 * f2 -> ":"
	 * f3 -> Statement()
	 */
	public GraphModelData visit(CaseLabeledStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <DFLT>
	 * f1 -> ":"
	 * f2 -> Statement()
	 */
	public GraphModelData visit(DefaultLabeledStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> ( Expression() )?
	 * f1 -> ";"
	 */
	public GraphModelData visit(ExpressionStatement n, GraphModelData argu) {
		
		if(setlocking  == true) {
			
			assert(myData != null);
			myData.addLocks(1);
			if(n.getInfo().getString().contains("omp_unset_lock")) {
				double scale = 32;
				Vector<Symbol> reads = n.getInfo().getReads();
				for(Symbol s: reads) {
					double update = classifier.getVariableValueofVariable(s);
					if(update > scale)
						scale = update;
				}
				myData.addLockstoatomics(scale);
			} else {
				myData.addLocks(1);
			}
			return myData;
		}
		
		if(n.getInfo().getString().contains("omp_set_lock")) {
			assert(myData !=  null);
			setlocking = true;
			myData.addLocks(1);
			return myData;
		}
		
		if(myData != null) {
			HashSet<CallSite> callSites = n.getInfo().getCallSites();
			if(callSites.size() > 0) {
				for (CallSite cs : callSites) {
					/* TODO make inter procedural now just copying the function calls */
					//CesFunctionDefinitionInfo info = (CesFunctionDefinitionInfo)cs.getCalleeDefinition().getInfo();
					String calleeFunction = cs.callerExp.getInfo().getString();
					myData.addmemOps(CesSpecificMisc.getArgLists(calleeFunction));
				}
			} else {
				CollectoperandsandOperations(n.f0.node, myData);
			}
		}	
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}
	
	
	boolean setlocking = false;
	

	private void CollectoperandsandOperations(Node node, GraphModelData data) {
		
		
		
	
		
		String stringVal = node.getInfo().getString();
		/**
		 * TODO our String parsers cannot handle casting operations 
		 * remove the castings.
		 */
		if( stringVal.contains("( double )") ){
			stringVal = stringVal.replace("( double )", "");
		}
		
		ArrayList<String> varlist = CESString.getVariableList(stringVal);
		/*
		 * Operations will be equivalent to operand -1
		 * */
		if(varlist.size()> 1)
			data.addALUOperations(varlist.size()-1);
		else {
			data.addALUOperations(1);
		}
		
		for(String s : varlist) {
			
			String arrayIndex = CESString.getLeastSignificantArrayIndex(s);
			
			
			if(arrayIndex == null) {
				data.addmemOp(s);
			} else {
				/*Collect the variable part alone */
				Counter ctr = new Counter(arrayIndex);
				arrayIndex = ctr.getReducedVariable();
				int itrIndex = myData.isIterator(arrayIndex);
				if(itrIndex == GraphModelData.INNERMOSTITERATOR) {
					data.addArrayOp(s);
				} else if(itrIndex == GraphModelData.ITERATOR) {	
					data.addmemOp(s);
				} else {
					/*Get declaring Node and decide if it is indeed 
					 * Random*/
					Symbol sym = Misc.getSymbolEntry(arrayIndex.trim(), node);
					if(sym == null) {
						/* TO be on safe side if no information assume it to be 
						 * normal array access. */
						data.addRandVariable(sym, null, s);
					} else {
						Declaration dec = (Declaration) sym.getDeclaringNode();
						ArrayList<Node> declist = CesSpecificMisc.collectAllChildrensofClass(dec, 
								InitDeclarator.class.getName());
						/**
						 * We currently assume that all our declarations have a single
						 * variable declaration at a time. If this fails, 
						 * 	We need to change the implementation.
						 */
					
						assert(declist.size() == 1);
						InitDeclarator decinit =  (InitDeclarator) declist.get(0);
						Node opt = decinit.f1.node;
					
						if(opt == null) {
							/* TO be on safe side if no information assume it to be 
							 * normal array access. */
							data.addRandVariable(sym, null, s);
						} else {
							String declaration = opt.getInfo().getString().replace("=","").trim();
							String arrayArrayIndex = CESString.getLeastSignificantArrayIndex(declaration);
							if(arrayArrayIndex == null) {
								/* TO be on safe side if no information assume it to be 
								 * normal array access. */
								data.addRandVariable(sym, null, s);
							} else {
								data.addRandVariable(sym, arrayArrayIndex, s);
							} 
						
						}
					}
				}	
			}
			
		}
		
	}

	/**
	 * f0 -> "{"
	 * f1 -> ( CompoundStatementElement() )*
	 * f2 -> "}"
	 */
	public GraphModelData visit(CompoundStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> Declaration()
	 *       | Statement()
	 */
	public GraphModelData visit(CompoundStatementElement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> IfStatement()
	 *       | SwitchStatement()
	 */
	public GraphModelData visit(SelectionStatement n, GraphModelData argu) {
		
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <IF>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 * f5 -> ( <ELSE> Statement() )?
	 */
	public GraphModelData visit(IfStatement n, GraphModelData argu) {
		Main.addlog(n.getInfo().getString());
		GraphModelData newData = new GraphModelData(n);
		myData.addChildData(getConditionalScale(n), newData);
		GraphModelData data = myData;
		myData = newData;
		 GraphModelData _ret=null;
		initProcess(n, argu);
		CollectoperandsandOperations(n.f2, myData);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		endProcess(n, argu);
		myData = data;
		return _ret;
	}



	/**
	 * f0 -> <SWITCH>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public GraphModelData visit(SwitchStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> WhileStatement()
	 *       | DoStatement()
	 *       | ForStatement()
	 */
	public GraphModelData visit(IterationStatement n, GraphModelData argu) {
		
		
		
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <WHILE>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public GraphModelData visit(WhileStatement n, GraphModelData argu) {
		/**
		 * IMP  a while statement inside the work sharing can result in irregular workload 
		 * we are not handling it right now
		 */
		assert(myStatus < insideWorkSharingConstruct);
		GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <DO>
	 * f1 -> Statement()
	 * f2 -> <WHILE>
	 * f3 -> "("
	 * f4 -> Expression()
	 * f5 -> ")"
	 * f6 -> ";"
	 */
	public GraphModelData visit(DoStatement n, GraphModelData argu) {
		/**
		 * IMP  a do statement inside the worksharing can result in irregular workload 
		 * we are not handling it right now
		 */
		assert(myStatus < insideWorkSharingConstruct );
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <FOR>
	 * f1 -> "("
	 * f2 -> ( Expression() )?
	 * f3 -> ";"
	 * f4 -> ( Expression() )?
	 * f5 -> ";"
	 * f6 -> ( Expression() )?
	 * f7 -> ")"
	 * f8 -> Statement()
	 */
	public GraphModelData visit(ForStatement n, GraphModelData argu) {
    	GraphModelData data = myData;	
    	TotalIterations itrs = CesSpecificMisc.getForIterations(n);
    	Symbol s = CesSpecificMisc.getIterator(n);
		if(myStatus >= insideWorkSharingConstruct) {
			
			Main.addlog(n.getInfo().getString());
			/**
			 * If both start point and end points are both  read from an array 
			 * then it is irregular workload.
			 */
			Node upn = itrs.getUpperLimitNode();
			Node lwn = itrs.getLowerLimitNode();
			String upnStr = upn.getInfo().getString();
			String lwnStr = lwn.getInfo().getString();
			/**
			 * We verify irregularity with the end points. If the end points
			 * of the iterations are based on values read from some array 
			 * We assume that this is an irregular loop.
			 */
			if(CESString.hasArrayRead(upnStr) == true && CESString.hasArrayRead(lwnStr) == true) {
				Node pc = myData.getParallelConstructNode();
				if(pc != null) {
					CesParallelConstructInfo info = (CesParallelConstructInfo) pc.getInfo();
					assert(info != null);
					info.setIrregularParallelRegion(true);
				}
				if(myStatus == insideWorkSharingConstruct) {
					myData.setIrregularity(n);
					updateMYSTATUS(insideIrregularLoad);
					myData.setIrregular(true);
				}
			}
		}

		GraphModelData newData = new GraphModelData(n);
		if(myData != null) {
			myData.addChildData(itrs.getString(), newData);
		}
		myData = newData;
		myData.addIterator(s.getName());
	
		GraphModelData _ret=null;
		initProcess(n, argu);
		if(myData != null)	
			CollectoperandsandOperations(n.f4, myData);
		if(myData != null)
			CollectoperandsandOperations(n.f6, myData);
		n.f7.accept(this, argu);
		n.f8.accept(this, argu);
		endProcess(n, argu);
		
		/**
		 * Copy back
		 */
		myData = data;
		
		if(myStatus > insideWorkSharingConstruct) {
		
			updateMYSTATUS(myStatus-1);
		}
	
		
		
		return _ret;
	}

	/**
	 * f0 -> GotoStatement()
	 *       | ContinueStatement()
	 *       | BreakStatement()
	 *       | ReturnStatement()
	 */
	public GraphModelData visit(JumpStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <GOTO>
	 * f1 -> <IDENTIFIER>
	 * f2 -> ";"
	 */
	public GraphModelData visit(GotoStatement n, GraphModelData argu) {
		assert(0==1);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <CONTINUE>
	 * f1 -> ";"
	 */
	public GraphModelData visit(ContinueStatement n, GraphModelData argu) {
		assert(0==1);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <BREAK>
	 * f1 -> ";"
	 */
	public GraphModelData visit(BreakStatement n, GraphModelData argu) {
		// TODO assert(0 == 1);
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}

	/**
	 * f0 -> <RETURN>
	 * f1 -> ( Expression() )?
	 * f2 -> ";"
	 */
	public GraphModelData visit(ReturnStatement n, GraphModelData argu) {
		 GraphModelData _ret=null;
		initProcess(n, argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		endProcess(n, argu);
		return _ret;
	}
	
	
	

	
}
