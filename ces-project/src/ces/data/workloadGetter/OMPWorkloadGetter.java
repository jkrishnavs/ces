/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */

//
//

package ces.data.workloadGetter;

import java.util.*;
import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.GJDepthFirstProcess;
import ces.SystemConfigFile;
import ces.ast.info.CesFunctionDefinitionInfo;
import ces.data.constructData.*;
import ces.data.constructData.ConditionalWorkload.curLoad;
import ces.enums.JKPROJECT;
import ces.enums.Metric;
import ces.getter.HierarchyASTNodeGetter;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.analysis.type.Operator;
import imop.lib.cg.CallSite;
import imop.lib.util.Misc;
import imop.ast.info.*;

/**
 * Provides default methods which visit each node in the tree in depth-first
 * order.  Your visitors may extend this class.
 */
public class OMPWorkloadGetter extends GJDepthFirstProcess<OMPWorkload, OMPWorkload> {
	//
	// Auto class visitors--probably don't need to be overridden.
	//

	Stack<OMPFunctionWorkload> fnList;
	BlockWorkload externalWorkload;
	
	public OMPWorkloadGetter() {
		super();
		fnList = new Stack<OMPFunctionWorkload>();
    	externalWorkload = new ExternalWorkload(Main.root, null);
	}
	
	
	
	

	public BlockWorkload getNewBlockWorkload(Node n){
		return getCurrentFunctionWorkload().getNewBlockWorkload(n, true);
	}
	
	
	public OMPFunctionWorkload getFinalData (String functionName) {

		OMPWorkload callerdata = null;
		OMPFunctionWorkload calleedata = null;
		OMPFunctionWorkload callerFnData = null;
		for (UnresolvedFnCalls uFn : unresolved) {
			callerdata =  uFn.dataUpdated;
			
			for(OMPFunctionWorkload wl : fnList) {
				if (wl.node == uFn.callerFn) {
					callerFnData = wl;
					break;
				}
			}
			for(OMPFunctionWorkload wl : fnList) {
				if (wl.node == uFn.calleeFn) {
					calleedata = wl;
					break;
				}
			}
			
			if(callerFnData !=  null)
				Main.addlog(callerFnData.functionName);
			if(calleedata != null)
				Main.addlog(calleedata.functionName);			
			callerFnData.resolveFunctionCall(callerdata, calleedata);
		
		}
		
	
		for (OMPFunctionWorkload wl : fnList) {
			// Combine all incomplete barriers
			CesFunctionDefinitionInfo fndef = (CesFunctionDefinitionInfo) (wl.node.getInfo());
			if(fndef.getFunctionName().equals(functionName)) {
				return wl;
			}
		}
		return null;
	}
	
	public OMPFunctionWorkload getCurrentFunctionWorkload() {
		return fnList.peek();
	}
	
	
	
	
	public void constructNotHandledError(Node n) {
		Main.addlog(n.getInfo().getString());
		Main.addErrorMessage("This construct " + n.toString() +" is not handled for this getter " + this.toString());
	} 
	
	
	
	
	
	public OMPWorkload getCurrentBlockWorkload(Node n) {
		if(!fnList.isEmpty() && fnList.peek().isLive())
			return getCurrentFunctionWorkload().getCurrentBlockWorkload(n);
		else
			return externalWorkload;
	}
	
	
	
    private HashMap<Operator, Integer> coundOperator(Node n) {
		HashMap<Operator, Integer> opCountMap = new HashMap<Operator, Integer>();
		for(Operator op : Operator.values()) {
			opCountMap.put(op, new Integer(0));
		}

		for(Node exp : Misc.getExactEnclosee(n, Expression.class)) {
			if(exp instanceof Expression) {
			//	exp.getInfo().printNode();
				HashMap<Operator, Integer> thisOpCount = 
						CesSpecificMisc.countOperator((Expression)exp);
				for(Operator op: thisOpCount.keySet()) {
					opCountMap.put(op, opCountMap.get(op) + thisOpCount.get(op));
				}
			}
		}
		
		for(Node exp : Misc.getExactEnclosee(n, OmpForReinitExpression.class)) {
			if(exp instanceof OmpForReinitExpression) {
				//exp.getInfo().printNode();
				HashMap<Operator, Integer> thisOpCount = 
						CesSpecificMisc.countOperator((OmpForReinitExpression)exp);
				for(Operator op: thisOpCount.keySet()) {
					opCountMap.put(op, opCountMap.get(op) + thisOpCount.get(op));				
				}
			}
		}
	  return opCountMap;
	}
    
    private void collectAllArrayOperatorAccesses(Node n, OMPWorkload w) {
    	w.addArrayAccess(CesSpecificMisc.getAllWriteExpressions(n));
    	w.addReadArrayList(CesSpecificMisc.getAllReadExpressions(n));
    }
	
	private void collectALUMemoryOperations(Node n,OMPWorkload w) {
		assert(w != null);
		if(w == null){
			Main.addErrorMessage("hello");
		}
		ExprWorkload eW = w.getExprWorkload(n);
		HashSet<Symbol> sharedMemReads = n.getInfo().getSharedReads();
		HashSet<Symbol> sharedMemWrites = n.getInfo().getSharedWrites();
		Vector<Symbol> memReads = n.getInfo().getReads();
		Vector<Symbol> memWrites = n.getInfo().getWrites();
		eW.addConstantMetric(Metric.sharedRead, sharedMemReads.size());
		
		
		
		/*
		for (Symbol sharedRead: sharedMemReads) {
			Main.addlog("sharing read " + sharedRead.getName());
		}
		
		for (Symbol sharedWrite: sharedMemWrites) {
			Main.addlog("sharing Write " + sharedWrite.getName());
		}
		
		for (Symbol read: memReads) {
			Main.addlog("Read " + read.getName());
		}
		
		for (Symbol write: memWrites) {
			Main.addlog("Write "  + write.getName());
		}
		*/
		
		
		HashSet<Symbol> privateReads = ((CesInfo)n.getInfo()).getPrivateReads();
		HashSet<Symbol> privateWrites = ((CesInfo)n.getInfo()).getPrivateWrites();

		eW.addConstantMetric(Metric.sharedWrite, sharedMemWrites.size());
		eW.addConstantMetric(Metric.memread, memReads.size());
		eW.addConstantMetric(Metric.memwrite, memWrites.size());
		eW.addConstantMetric(Metric.privateRead, privateReads.size());
		eW.addConstantMetric(Metric.privateWrite, privateWrites.size());
	//	w.addConstantMetric(Metric., val);
		eW.addOperations(coundOperator(n));
		eW.addFunctionCalls(n.getInfo().getCallSites());
		eW.addConstantMetric(Metric.branches, 0);
	}
	
	private class UnresolvedFnCalls {
		FunctionDefinition callerFn;
		FunctionDefinition calleeFn;
		OMPWorkload dataUpdated;
		//int listpos;
		public UnresolvedFnCalls(FunctionDefinition caller,FunctionDefinition callee, OMPWorkload data) {
			callerFn = caller;
			calleeFn = callee;
			if(data == null) {
				Main.addErrorMessage("The data is NULL !!");
				Main.addlog(caller.getInfo().getString());
				Main.addlog(callee.getInfo().getString());
			}
			if(data.getClass() != OMPFunctionCallWorkload.class) {
				Main.addErrorMessage("Unexpected Call here");
			}
			dataUpdated = data;
		}
	}
	
	Stack<UnresolvedFnCalls> unresolved = new Stack<UnresolvedFnCalls>();
	

	
	public boolean addFunctionData(FunctionDefinition caller, FunctionDefinition callee,OMPWorkload sourcedata, CallSite callsite) {
	
		if(caller == null || callee == null) {
			return false;
		}
		/**
		 * TODO : greenmarl hack.
		 * 
		 */
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			
			if(((FunctionDefinitionInfo)callee.getInfo()).getFunctionName().contains("semi_sort") ||
			   ((FunctionDefinitionInfo)callee.getInfo()).getFunctionName().contains("make_reverse_edges")) {
				return false;
			}
		}
			
		
		
		OMPWorkload cW = getCurrentBlockWorkload(null);
		assert(cW == sourcedata);
		
		/**
		 * TODO: There can be new parallel regions defined inside the function called.
		 * to get The parallel region added to Function child list, when the function
		 * calls are resolved we close the current block workload if the workload is a 
		 * direct child of the function Workload.
		 * 
		 * NOT WOrking 
		 */
		/*
		if(! cW.isInsideParallelRegion() && cW.parent == getCurrentFunctionWorkload()) {
			getCurrentBlockWorkload(null).closeWorkload();
			cW = getCurrentFunctionWorkload();
		} 
		*/
		sourcedata =  cW.getNewFunctionCallWorkload(caller, cW, callsite); 
		int latestCallerpos = unresolved.size();
		int earliestCalleepos = -1;
		
		if(unresolved.isEmpty()) {
			unresolved.add(latestCallerpos, new UnresolvedFnCalls(caller, callee, sourcedata));
			return true;
		}
		
		
		for (int i = 0; i < unresolved.size(); i++) {
			if(unresolved.get(i).calleeFn == caller && latestCallerpos > i) {
					latestCallerpos = i;			
			} 
			
			if(unresolved.get(i).callerFn == callee) {
				 earliestCalleepos = i;
			}
		}
		if(earliestCalleepos == -1) {
			if(latestCallerpos == unresolved.size())
				earliestCalleepos = 0;
			else 
				earliestCalleepos = latestCallerpos - 1;
		}
		
		
		if(latestCallerpos <= earliestCalleepos) {
			Main.errWriter.println("Cannot add there is some back edge recursion? Caller :" +
					((FunctionDefinitionInfo)caller.getInfo()).getFunctionName() + " and Callee:" +
					((FunctionDefinitionInfo)callee.getInfo()).getFunctionName());
			return false;
		}
		
		//	OMPFunctionWorkload callerW = (OMPFunctionWorkload) ((CesInfo)caller.getInfo()).getOMPWorkload();
	//	Main.addlog("Insert position is " + (earliestCalleepos + 1));
	
		unresolved.add(earliestCalleepos + 1, new UnresolvedFnCalls(caller, callee, sourcedata));
		
		return true;
	}
	
	



	public OMPWorkload visit(NodeToken n, OMPWorkload argu) {
		initProcess(n, argu);
		endProcess(n, argu);
		return null;	
	}



	/**
	 * f0 -> ( DeclarationSpecifiers() )?
	 * f1 -> Declarator()
	 * f2 -> ( DeclarationList() )?
	 * f3 -> CompoundStatement()
	 */
	public OMPWorkload visit(FunctionDefinition n, OMPWorkload argu) {
		OMPFunctionWorkload fnWorkload = new OMPFunctionWorkload(n, null);
		fnList.add(fnWorkload);
		initProcess(n, fnWorkload);
		n.f0.accept(this, fnWorkload);
		n.f1.accept(this, fnWorkload);
		n.f2.accept(this, fnWorkload);
		n.f3.accept(this, fnWorkload);
		endProcess(n, fnWorkload);
		fnWorkload.closeWorkload();
		return argu;
	}





	/**
	 * f0 -> OmpPragma()
	 * f1 -> ParallelDirective()
	 * f2 -> Statement()
	 */
	public OMPWorkload visit(ParallelConstruct n, OMPWorkload argu) {
		OMPWorkload bW =  getCurrentBlockWorkload(n);
		OMPParallelWorkload parWorkload= new OMPParallelWorkload(n, bW);
		if(bW.getParent() == getCurrentFunctionWorkload()) {
			getCurrentBlockWorkload(n).closeWorkload();
			parWorkload.setParent(getCurrentFunctionWorkload());
			getCurrentFunctionWorkload().add(parWorkload);
		} else if(bW == getCurrentFunctionWorkload() ){
			getCurrentFunctionWorkload().add(parWorkload);
		} else {
			EmbeddedParallelRegions embedded = new EmbeddedParallelRegions(parWorkload, bW.getParent());
			CesSpecificMisc.collectParallelEmbeddingPath(bW, parWorkload, embedded);
			parWorkload.setEmbeddedRegion(embedded);
			Main.addlog("Parallel region " + n.getInfo().getString());
			bW.add(parWorkload);
			getCurrentFunctionWorkload().addEmbeddedRegion(embedded);
		}
		initProcess(n, parWorkload);
		n.f1.accept(this, parWorkload);
		n.f2.accept(this, parWorkload);
		endProcess(n, parWorkload);
		OMPBarrierWorkload endBarrier = new OMPBarrierWorkload(n, getCurrentBlockWorkload(n));
		parWorkload.add(endBarrier);
		getCurrentFunctionWorkload().closeBarrier(parWorkload.isEmbeddedRegion());
		parWorkload.closeWorkload();
		return argu;
	}

	
	
	/**
	 * f0 -> <PRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public OMPWorkload visit(OmpPrivateClause n, OMPWorkload argu) {
	
		getCurrentBlockWorkload(n).addPrivatizationList(new PrivatizationList(CesSpecificMisc.getVarList(n.f2)));
		
		return argu;
	}

	

	/**
	 * f0 -> <FIRSTPRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public OMPWorkload visit(OmpFirstPrivateClause n, OMPWorkload argu) {
		getCurrentBlockWorkload(n).addPrivatizationList(new PrivatizationList(CesSpecificMisc.getVarList(n.f2)));
		return argu;
	}

	/**
	 * f0 -> <LASTPRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public OMPWorkload visit(OmpLastPrivateClause n, OMPWorkload argu) {
		getCurrentBlockWorkload(n).addPrivatizationList(new PrivatizationList(CesSpecificMisc.getVarList(n.f2)));
		return argu;
	}



	/**
	 * f0 -> <COPYIN>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public OMPWorkload visit(OmpCopyinClause n, OMPWorkload argu) {
		OMPWorkload workload = getCurrentBlockWorkload(n);
		workload.addPrivatizationList(new PrivatizationList(CesSpecificMisc.getVarList(n.f2)));
		return argu;
	}



	/**
	 * f0 -> <REDUCTION>
	 * f1 -> "("
	 * f2 -> ReductionOp()
	 * f3 -> ":"
	 * f4 -> VariableList()
	 * f5 -> ")"
	 */
	public OMPWorkload visit(OmpReductionClause n, OMPWorkload argu) {
		OMPWorkload _ret=null;
		
		String rop = n.f2.f0.choice.toString();
		OMPReductionWorkload red =  new OMPReductionWorkload(n.f4.f0, rop, null);
		argu.add(red);
		NodeListOptional rest = n.f4.f1;
		for (int i = 1; i < rest.nodes.size(); i+=2) {
			red = new OMPReductionWorkload(rest.nodes.get(i), rop, null);
			argu.add(red);
		}
		return _ret;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> ForDirective()
	 * f2 -> OmpForHeader()
	 * f3 -> Statement()
	 */
	public OMPWorkload visit(ForConstruct n, OMPWorkload argu) {
	//	Main.addlog(n.getInfo().getString());
		OMPForWorkload fordata = new OMPForWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(fordata);
		n.f1.accept(this, fordata);
		n.f2.accept(this, fordata);
		fordata.setIterationDetails(n.f2);
		fordata.setClauseDetails(n.f1.f1.f0);
		n.f3.accept(this, fordata);
		fordata.closeWorkload();
		if(CesSpecificMisc.hasNowaitClause(n) == false) {
	     	OMPBarrierWorkload newBW = new OMPBarrierWorkload(n, getCurrentBlockWorkload(n));
			fordata.add(newBW);
		}
		return argu;
	}








	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTIONS>
	 * f2 -> NowaitDataClauseList()
	 * f3 -> OmpEol()
	 * f4 -> SectionsScope()
	 */
	public OMPWorkload visit(SectionsConstruct n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SINGLE>
	 * f2 -> SingleClauseList()
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public OMPWorkload visit(SingleConstruct n, OMPWorkload argu) {
		
		OMPSingleWorkload singleWorkload = new OMPSingleWorkload(n, getCurrentBlockWorkload(n));
		initProcess(n, singleWorkload);
		getCurrentBlockWorkload(n).add(singleWorkload);
		n.f2.accept(this, singleWorkload);
		n.f4.accept(this, singleWorkload);
		endProcess(n, singleWorkload);
		singleWorkload.closeWorkload();
		if(CesSpecificMisc.hasNowaitClause(n) == false) {
			OMPBarrierWorkload newBW =  new OMPBarrierWorkload(n, getCurrentBlockWorkload(n));
			singleWorkload.add(newBW);
		}
		return argu;
	}

	


	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASK>
	 * f2 -> ( TaskClauseList() )*
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public OMPWorkload visit(TaskConstruct n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	

	

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <FOR>
	 * f3 -> UniqueParallelOrUniqueForOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> OmpForHeader()
	 * f6 -> Statement()
	 */
	public OMPWorkload visit(ParallelForConstruct n, OMPWorkload argu) {
		
		OMPWorkload bW =  getCurrentBlockWorkload(n);
		OMPParallelWorkload barrierWorkload = new OMPParallelWorkload(n, bW);
		if(bW.getParent() == getCurrentFunctionWorkload()) {
			getCurrentBlockWorkload(n).closeWorkload();
			barrierWorkload.setParent(getCurrentFunctionWorkload());
			getCurrentFunctionWorkload().add(barrierWorkload);
		} else if(bW == getCurrentFunctionWorkload() ){
			getCurrentFunctionWorkload().add(barrierWorkload);
		} else {
			EmbeddedParallelRegions embedded = new EmbeddedParallelRegions(barrierWorkload, bW.getParent());
			CesSpecificMisc.collectParallelEmbeddingPath(bW, barrierWorkload, embedded);
			barrierWorkload.setEmbeddedRegion(embedded);
			Main.addlog("Parallel region " + n.getInfo().getString());
			bW.add(barrierWorkload);
			getCurrentFunctionWorkload().addEmbeddedRegion(embedded);
			
		}
		OMPForWorkload fordata = new OMPForWorkload(n, null);
		barrierWorkload.add(fordata);
		fordata.setIterationDetails(n.f5);
		fordata.setClauseDetails(n.f3.f0);
		n.f5.accept(this, fordata);
		n.f6.accept(this, fordata);
		OMPBarrierWorkload brW = new OMPBarrierWorkload(n, getCurrentBlockWorkload(n));
		fordata.add(brW);
		fordata.closeWorkload();
		getCurrentFunctionWorkload().closeBarrier(barrierWorkload.isEmbeddedRegion());
		return argu;
	}

	

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <SECTIONS>
	 * f3 -> UniqueParallelOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> SectionsScope()
	 */
	public OMPWorkload visit(ParallelSectionsConstruct n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <MASTER>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public OMPWorkload visit(MasterConstruct n, OMPWorkload argu) {
		
		OMPMasterWorkload masterdata =  new OMPMasterWorkload(n, getCurrentBlockWorkload(n));
	//	ompWorkloads.push(masterdata);
		getCurrentBlockWorkload(n).add(masterdata);
		n.f3.accept(this, masterdata);
		masterdata.closeWorkload();
//		ompWorkloads.pop();
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <CRITICAL>
	 * f2 -> ( RegionPhrase() )?
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public OMPWorkload visit(CriticalConstruct n, OMPWorkload argu) {
		
		OMPCriticalWorkload critical = new OMPCriticalWorkload(n, getCurrentBlockWorkload(n));
	//	ompWorkloads.push(critical);
		OMPWorkload workload = getCurrentBlockWorkload(n);
		workload.add(critical);
		n.f4.accept(this, critical);
		critical.closeWorkload();
	//	ompWorkloads.pop();
		return argu;
	}
	

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ATOMIC>
	 * f2 -> ( AtomicClause() )?
	 * f3 -> OmpEol()
	 * f4 -> ExpressionStatement()
	 */
	public OMPWorkload visit(AtomicConstruct n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <READ>
	 *       | <WRITE>
	 *       | <UPDATE>
	 *       | <CAPTURE>
	 */
	public OMPWorkload visit(AtomicClause n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <FLUSH>
	 * f2 -> ( FlushVars() )?
	 * f3 -> OmpEol()
	 */
	public OMPWorkload visit(FlushDirective n, OMPWorkload argu) {
		OMPFlushWorkload flushdata = new OMPFlushWorkload(n, getCurrentBlockWorkload(n));
		FlushVars vars = (FlushVars) n.f2.node;
		getCurrentBlockWorkload(n).add(flushdata);
		if(vars == null) {
			constructNotHandledError(n);
			return argu;
		}
		flushdata.addFlushVariables(CesSpecificMisc.getVarList(vars.f1));;
		return argu;
	}


	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ORDERED>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public OMPWorkload visit(OrderedConstruct n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <BARRIER>
	 * f2 -> OmpEol()
	 */
	public OMPWorkload visit(BarrierDirective n, OMPWorkload argu) {
		OMPBarrierWorkload newBW = new OMPBarrierWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(newBW);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKWAIT>
	 * f2 -> OmpEol()
	 */
	public OMPWorkload visit(TaskwaitDirective n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKYIELD>
	 * f2 -> OmpEol()
	 */
	public OMPWorkload visit(TaskyieldDirective n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <THREADPRIVATE>
	 * f2 -> "("
	 * f3 -> VariableList()
	 * f4 -> ")"
	 * f5 -> OmpEol()
	 */
	public OMPWorkload visit(ThreadPrivateDirective n, OMPWorkload argu) {
		getCurrentBlockWorkload(n).addPrivatizationList(new PrivatizationList(CesSpecificMisc.getVarList(n.f3)));
		return argu;
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <DECLARE>
	 * f2 -> <REDUCTION>
	 * f3 -> "("
	 * f4 -> ReductionOp()
	 * f5 -> ":"
	 * f6 -> ReductionTypeList()
	 * f7 -> ":"
	 * f8 -> Expression()
	 * f9 -> ")"
	 * f10 -> ( InitializerClause() )?
	 * f11 -> OmpEol()
	 */
	public OMPWorkload visit(DeclareReductionDirective n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> ( TypeSpecifier() )*
	 */
	public OMPWorkload visit(ReductionTypeList n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> AssignInitializerClause()
	 *       | ArgumentInitializerClause()
	 */
	public OMPWorkload visit(InitializerClause n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "="
	 * f4 -> Initializer()
	 * f5 -> ")"
	 */
	public OMPWorkload visit(AssignInitializerClause n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "("
	 * f4 -> ExpressionList()
	 * f5 -> ")"
	 * f6 -> ")"
	 */
	public OMPWorkload visit(ArgumentInitializerClause n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <IDENTIFIER>
	 *       | "+"
	 *       | "*"
	 *       | "-"
	 *       | "&"
	 *       | "^"
	 *       | "|"
	 *       | "||"
	 *       | "&&"
	 */
	public OMPWorkload visit(ReductionOp n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}



	/**
	 * f0 -> <CASE>
	 * f1 -> ConstantExpression()
	 * f2 -> ":"
	 * f3 -> Statement()
	 */
	public OMPWorkload visit(CaseLabeledStatement n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <DFLT>
	 * f1 -> ":"
	 * f2 -> Statement()
	 */
	public OMPWorkload visit(DefaultLabeledStatement n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> ( Expression() )?
	 * f1 -> ";"
	 */
	public OMPWorkload visit(ExpressionStatement n, OMPWorkload argu) {	
		collectALUMemoryOperations(n,getCurrentBlockWorkload(n));
		collectAllArrayOperatorAccesses(n, getCurrentBlockWorkload(n));	
		HashSet<CallSite> callSites = n.getInfo().getCallSites();
		for (CallSite cs : callSites) {
		//	Main.addlog(n.getInfo().getString());
			
			
			
			boolean sucess = addFunctionData(fnList.get(fnList.size()-1).function(),
						cs.getCalleeDefinition(), getCurrentBlockWorkload(n), cs);
			if(!sucess) {
		//		Main.addlog("Callee Name for statement is " + cs.getCalleeName());
				if(cs.getCalleeName().equals(SystemStringConstants.startProfile)) {
			//		Main.addlog(" Start point found here");
					BeginWorkload sW = new BeginWorkload(n, getCurrentFunctionWorkload());
					getCurrentFunctionWorkload().add(sW);
				} else if(cs.getCalleeName().equals(SystemStringConstants.endProfile)) {
			//		Main.addlog("Stop point found here");
					EndWorkload sW = new EndWorkload(n, getCurrentFunctionWorkload());
					getCurrentFunctionWorkload().add(sW);
				}
				 
				getCurrentBlockWorkload(n).getExprWorkload(n).addFunctionCall(cs);
			}
		}
		return argu;
	}

	/**
	 * f0 -> "{"
	 * f1 -> ( CompoundStatementElement() )*
	 * f2 -> "}"
	 */
	public OMPWorkload visit(CompoundStatement n, OMPWorkload argu) {
		BlockWorkload blockWorkload = getNewBlockWorkload(n);
		getCurrentBlockWorkload(n).add(blockWorkload);
		initProcess(n, blockWorkload);
		n.f0.accept(this, blockWorkload);
		n.f1.accept(this, blockWorkload);
		n.f2.accept(this, blockWorkload);
		endProcess(n, blockWorkload);
		blockWorkload.closeWorkload();
		return argu;
	}


	/**
	 * f0 -> <IF>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 * f5 -> ( <ELSE> Statement() )?
	 */
	public OMPWorkload visit(IfStatement n, OMPWorkload argu) {
		
		ConditionalWorkload condWorkload = new ConditionalWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(condWorkload);
		//n.f2.accept(this, condWorkload);
		collectALUMemoryOperations(n.f2, condWorkload);
		condWorkload.setLoad(curLoad.enumIfLoad);
		n.f4.accept(this, condWorkload);
		condWorkload.setLoad(curLoad.enumElseLoad);
		n.f5.accept(this, condWorkload);
		endProcess(n, condWorkload);
		condWorkload.closeWorkload();
		return argu;
	}

	/**
	 * f0 -> <SWITCH>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public OMPWorkload visit(SwitchStatement n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}



	/**
	 * f0 -> <WHILE>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public OMPWorkload visit(WhileStatement n, OMPWorkload argu) {
		//constructNotHandledError(n);
		WhileWorkload whileWork = new WhileWorkload(n, false, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(whileWork);
		initProcess(n, whileWork);
		ExprWorkload condWorkload = new ExprWorkload(n.f2, whileWork);
		collectALUMemoryOperations(n.f2, condWorkload);
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		n.f2.accept(nG, PrimaryExpression.class.getName());
		
		
		Main.addlog(n.getInfo().getString());
		
		String itrString = nG.getASTNodes().get(0).getInfo().getString();
		itrString = itrString.trim();
		Vector<Symbol> readSet = n.getInfo().getReads();
		Symbol itrSymbol = null;
		for(Symbol s: readSet ) {
			if(s.getName().equals(itrString)) {
				itrSymbol = s;
				break;
			}
		}
	//	assert(itrSymbol != null);
		whileWork.setIterator(itrSymbol);
		
		
		
		
		
		whileWork.setCondWorkload(condWorkload);
		n.f4.accept(this, whileWork);
		whileWork.closeWorkload();
		whileWork.setIterations(CesSpecificMisc.getWhileIterations(n));
		return argu;
	}

	/**
	 * f0 -> <DO>
	 * f1 -> Statement()
	 * f2 -> <WHILE>
	 * f3 -> "("
	 * f4 -> Expression()
	 * f5 -> ")"
	 * f6 -> ";"
	 */
	public OMPWorkload visit(DoStatement n, OMPWorkload argu) {
		WhileWorkload doWhileWorkload =  new WhileWorkload(n, true, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(doWhileWorkload);
		initProcess(n, doWhileWorkload);
		n.f1.accept(this, doWhileWorkload);
		ExprWorkload condWorkload = new ExprWorkload(n.f2, doWhileWorkload);
		collectALUMemoryOperations(n.f4, condWorkload);
		endProcess(n, doWhileWorkload);
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		n.f4.accept(nG, PrimaryExpression.class.getName());
		
		
		Main.addlog(n.getInfo().getString());
		
		String itrString = nG.getASTNodes().get(0).getInfo().getString();
		Vector<Symbol> readSet = n.getInfo().getReads();
		Symbol itrSymbol = null;
		for(Symbol s: readSet ) {
			if(s.getName().equals(itrString)) {
				itrSymbol = s;
				break;
			}
		}
		
		if(itrSymbol != null);
			doWhileWorkload.setIterator(itrSymbol);
		
		doWhileWorkload.closeWorkload();
		doWhileWorkload.setIterations(CesSpecificMisc.getWhileIterations(n));
		return argu;
	}

	/**
	 * f0 -> <FOR>
	 * f1 -> "("
	 * f2 -> ( Expression() )?
	 * f3 -> ";"
	 * f4 -> ( Expression() )?
	 * f5 -> ";"
	 * f6 -> ( Expression() )?
	 * f7 -> ")"
	 * f8 -> Statement()
	 */
	public OMPWorkload visit(ForStatement n, OMPWorkload argu) {
		//constructNotHandledError(n);
		
		Main.addlog(n.getInfo().getString());
		
		ForWorkload forWork = new ForWorkload(n, getCurrentBlockWorkload(n));
		forWork.setIterations(CesSpecificMisc.getForIterations(n));
		getCurrentBlockWorkload(n).add(forWork);
		ExprWorkload init = new ExprWorkload(n.f2, forWork);
		collectALUMemoryOperations(n.f2, init);
		forWork.setInitialExprWorkload(init);
		ExprWorkload cond = new ExprWorkload(n.f4, forWork);	
		collectALUMemoryOperations(n.f4, cond);
		forWork.setConditionalWorkload(cond);
		ExprWorkload increment = new ExprWorkload(n.f6, forWork);
		collectALUMemoryOperations(n.f6, increment);
		forWork.setIncrementalOMPWorkload(increment);
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		
		forWork.setIterator(CesSpecificMisc.getIterator(n));
		
		n.f8.accept(this, forWork);
		endProcess(n, forWork);
		forWork.closeWorkload();
		return argu;
	}


	/**
	 * f0 -> <GOTO>
	 * f1 -> <IDENTIFIER>
	 * f2 -> ";"
	 */
	public OMPWorkload visit(GotoStatement n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}

	/**
	 * f0 -> <CONTINUE>
	 * f1 -> ";"
	 */
	public OMPWorkload visit(ContinueStatement n, OMPWorkload argu) {
		ContinueWorkload retW =  new ContinueWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(retW);
		return argu;
	}

	/**
	 * f0 -> <BREAK>
	 * f1 -> ";"
	 */
	public OMPWorkload visit(BreakStatement n, OMPWorkload argu) {
		BreakWorkload retW =  new BreakWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(retW);
		return argu;
	}

	/**
	 * f0 -> <RETURN>
	 * f1 -> ( Expression() )?
	 * f2 -> ";"
	 */
	public OMPWorkload visit(ReturnStatement n, OMPWorkload argu) {
		ReturnWorkload retW =  new ReturnWorkload(n, getCurrentBlockWorkload(n));
		getCurrentBlockWorkload(n).add(retW);
		collectALUMemoryOperations(n, retW);
		return argu;
	}

	/**
	 * f0 -> AssignmentExpression()
	 * f1 -> ( "," AssignmentExpression() )*
	 */
	public OMPWorkload visit(Expression n, OMPWorkload argu) {
		constructNotHandledError(n);
		return argu;
	}
	
	
	/**
	 * f0 -> <FOR>
	 * f1 -> "("
	 * f2 -> OmpForInitExpression()
	 * f3 -> ";"
	 * f4 -> OmpForCondition()
	 * f5 -> ";"
	 * f6 -> OmpForReinitExpression()
	 * f7 -> ")"
	 */
	public OMPWorkload visit(OmpForHeader n, OMPWorkload argu) {
		
		OMPForWorkload forWork = (OMPForWorkload) argu;
		
		ExprWorkload init = new ExprWorkload(n.f2, forWork);
		collectALUMemoryOperations(n.f2, init);
		forWork.setInitialExprWorkload(init);
		ExprWorkload cond = new ExprWorkload(n.f4, forWork);	
		collectALUMemoryOperations(n.f4, cond);
		forWork.setConditionalWorkload(cond);
		ExprWorkload increment = new ExprWorkload(n.f6, forWork);
		collectALUMemoryOperations(n.f6, increment);
		forWork.setIncrementalOMPWorkload(increment);
	
		return argu;
	}
	
	
	/**
	 * f0 -> DeclarationSpecifiers()
	 * f1 -> ( InitDeclaratorList() )?
	 * f2 -> ";"
	 */
	public OMPWorkload visit(Declaration n, OMPWorkload argu) {
		OMPWorkload workload = argu;
		HashSet<AssignmentExpression> expList = CesSpecificMisc.getAllAssignmentExpressionsinNode(n);
		if(argu == null) {
			workload = externalWorkload;
		}
		for(AssignmentExpression exp: expList) {
			collectALUMemoryOperations(exp, workload);
		}
		return argu;
	}

	
	/**
	 * f0 -> <SCHEDULE>
	 * f1 -> "("
	 * f2 -> ScheduleKind()
	 * f3 -> ( "," Expression() )?
	 * f4 -> ")"
	 */
	public OMPWorkload visit(UniqueForClauseSchedule n, OMPWorkload argu) {
		OMPWorkload _ret=null;
		/*
		 * Skip this its a one tine expression job.
		 */
		return _ret;
	}

	
//	/**
//	 * f0 -> Declaration()
//	 *       | FunctionDefinition()
//	 *       | DeclareReductionDirective()
//	 *       | ThreadPrivateDirective()
//	 */
//	public OMPWorkload visit(ExternalDeclaration n, OMPWorkload argu) {
//		constructNotHandledError(n);
//		//		n.f0.accept(this, argu);
//		return argu;
//	}

}
