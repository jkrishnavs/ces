/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.aluoperations;

import java.util.HashMap;

import imop.Main;
import ces.enums.Metric;
import imop.lib.analysis.type.Operator;

public class ALUOperations {
	private double expectedARITHOperations;
	private double expectedMULTOperations;
	private double expectedBitwiseOperations;
	
	public void addOperations (HashMap<Operator, Integer> count) {
		for(Operator op: Operator.values()) {
			if(op == 	Operator.MUL ||
			   op ==	Operator.DIV ||
			   op ==    Operator.REM) {
				expectedMULTOperations += count.get(op);
			} else if(op == Operator.ADD 	 ||
			   op == Operator.SUB 	 || 
			   op == Operator.LSHIFT ||
			   op == Operator.RSHIFT || 
			   op == Operator.OR     ||
			   op == Operator.AND    ||
			   op == Operator.EQUALS ||
			   op == Operator.NOT_EQUALS ||
			   op == Operator.LT     ||
			   op == Operator.GT     ||
			   op == Operator.GEQ    ||
			   op == Operator.LEQ    ||
			   op == Operator.PREINC ||
			   op == Operator.PREDEC ||
			   op == Operator.POSTINC||
			   op == Operator.POSTDEC||
			   op == Operator.UNARY_NEGATION) {
				expectedARITHOperations += count.get(op);
			} else if(op == Operator.BITWISE_AND ||
					  op == Operator.BITWISE_XOR ||
					  op == Operator.BITWISE_OR ||
					  op == Operator.LOGICAL_NEGATION     ||
					  op == Operator.BITWISE_NEGATION) {
				expectedBitwiseOperations += count.get(op);				
			}	
		}
	}
   
	public void addConstantMetric(Metric m, double val) {
		switch (m) {
		case ops_arith:
			expectedARITHOperations += val;
			break;
		case ops_mult:
			expectedMULTOperations += val;
			break;
		case ops_bitwise:
			expectedBitwiseOperations += val;
			break;			
		default:
			Main.addErrorMessage("The option "+ m + " is not defined for " + this);
		}
	}
	
	
/*
	public void addStringMetric(Metric m, String val) {
		switch (m) {
		case ops_arith:
			expectedARITHOperations.addvarPart(val);
			break;
		case ops_mult:
			expectedMULTOperations.addvarPart(val);
			break;
		case ops_bitwise:
			expectedBitwiseOperations.addvarPart(val);
			break;			
		default:
			Main.addErrorMessage("The option "+ m + " is not defined for " + this);
		}
	}
	*/
	
	
	public double getVal(Metric m) {
		switch (m) {
		case ops_arith:
			return expectedARITHOperations;
		case ops_mult:
			return expectedMULTOperations;
		case ops_bitwise:
			return expectedBitwiseOperations;
		default:
			Main.addErrorMessage("The option "+ m + " is not defined for " + this);
		}
		return 0;
	}
	
	public void addIntegerMetrics(HashMap<Metric, Integer> op){
		expectedARITHOperations += op.get(Metric.ops_arith);
		expectedBitwiseOperations += op.get(Metric.ops_bitwise);
		expectedMULTOperations  += op.get(Metric.ops_mult);
	}
	
	public void dumpData(){
		Main.addlog("ARITH:" + expectedARITHOperations + 
				"\t MULT:" + expectedMULTOperations +
				"\t BIT:" + expectedBitwiseOperations);
	}
	
	
	
	public ALUOperations() {
			expectedARITHOperations = 0;
			expectedBitwiseOperations = 0;
			expectedMULTOperations = 0;
	}
	
	
	public ALUOperations(ALUOperations a) {
		expectedARITHOperations = a.expectedARITHOperations;
		expectedBitwiseOperations = a.expectedBitwiseOperations;
		expectedMULTOperations = a.expectedMULTOperations;
   }

	
	/*
	public void multiply(String S){
		expectedARITHOperations.multiply(S);
		expectedBitwiseOperations.multiply(S);
		expectedMULTOperations.multiply(S);
	}
	*/
	public void add(ALUOperations op) {
		expectedARITHOperations += op.expectedARITHOperations();
		expectedBitwiseOperations += op.expectedBitwiseOperations();
		expectedMULTOperations += op.expectedMULTOperations();
	}
	
	
	/*
	public void setexpectedMULTOperations(Counter op){
		expectedMULTOperations = op;
	}
	
	public void addtoexpectedMULTOperations(Counter op){
		expectedMULTOperations.add(op);
	}
	
	public void setexpectedBitwiseOperations(Counter op){
		expectedBitwiseOperations = op;
	}
	
	public void addtoexpectedBitwiseOperations(Counter op){
		expectedBitwiseOperations.add(op);
	}
	
	public void setexpectedArithOperation(Counter op){
		expectedARITHOperations = op;
	}
	
	public void addtoArithOperations(Counter add){
		expectedARITHOperations.add(add);
	}
	*/
	
	public double expectedBitwiseOperations(){
		return expectedBitwiseOperations;
	}
	

	
	public double expectedMULTOperations(){
		return expectedMULTOperations;
	}
	
	public double expectedARITHOperations(){
		return expectedARITHOperations;
	}
	
	
}
