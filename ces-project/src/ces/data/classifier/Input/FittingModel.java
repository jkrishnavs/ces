/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier.Input;

import ces.data.classifier.Classifier;
import ces.enums.CostFunction;

public interface  FittingModel {
	/*
	EigenFeature.ReductionFeature redF;
	EigenFeature.BarrierFeature barF;
	EigenFeature.BranchFeature branF;
	EigenFeature.CriticalFeature critF;
	EigenFeature.FlushFeature flushF;
	EigenFeature.MemOpsFeature memF;
	
	*/
	

	void update(Classifier cf);
	void updateReduction(Classifier c, CostFunction cf);
	void updateBarrier(Classifier c, CostFunction cf);
	void updateBranches(Classifier c, CostFunction cf);
	void updateCritical(Classifier c, CostFunction cf);
	void updateFlush(Classifier c, CostFunction cf);
	void updateMemops(Classifier c, CostFunction cf);
	
	
}