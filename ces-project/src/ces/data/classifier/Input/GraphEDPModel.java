/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier.Input;

import imop.Main;
import ces.data.base.GraphPropertiesData;
import ces.data.classifier.ClassificationDetails;
import ces.enums.HardwareConfig;

public class GraphEDPModel {
	
	
	 static final double[]coeffedptrainn4b2l2b1400000 =  {-1.531, -78.8319, 158.1219, -48.1399, 91.808, -84.8788, -8182.0428, -9.9295};
	 static final double[]coeffedptrainn4b2l2b1600000 =  {-1.7971, -92.6614, 186.0895, -56.662, 107.9365, -99.6111, -9614.8992, -11.8007};
	 static final double[]coeffedptrainn4b2l2b1800000 =  {-2.2131, -113.6618, 228.4129, -69.2076, 132.7907, -122.3507, -11822.9628, -14.4978};
	 static final double[]coeffedptrainn4b4l0b1400000 =  {-0.1861, -9.6482, 18.3758, -5.3753, 10.4238, -10.9259, -952.1006, -0.7881};
	 static final double[]coeffedptrainn4b4l0b1600000 =  {-0.1823, -9.4658, 18.0333, -5.3427, 10.1741, -10.6671, -930.9346, -0.7633};
	 static final double[]coeffedptrainn4b4l0b1800000 =  {-0.1905, -9.9807, 18.9385, -5.7734, 10.5484, -11.1122, -970.1262, -0.7614};
	 static final double[]coeffedptrainn6b2l4b1400000 =  {-0.845, -40.1401, 86.3434, -24.4946, 54.7209, -44.7283, -4693.0446, -7.4169};
	 static final double[]coeffedptrainn6b2l4b1600000 =  {-0.9944, -47.1292, 101.5357, -28.6558, 64.604, -52.7721, -5534.0501, -8.786};
	 static final double[]coeffedptrainn6b2l4b1800000 =  {-1.187, -56.3592, 121.3145, -34.4998, 77.0617, -62.7648, -6604.5055, -10.4741};
	 static final double[]coeffedptrainn8b4l4b1400000 =  {-0.3457, -17.1123, 38.4746, -14.2562, 22.801, -16.3132, -1972.4445, -3.4379};
	 static final double[]coeffedptrainn8b4l4b1600000 =  {-0.3255, -16.5333, 37.4296, -15.1798, 21.4171, -14.4766, -1866.6383, -3.3085};
	 static final double[]coeffedptrainn8b4l4b1800000 =  {-0.2821, -14.1621, 31.8961, -12.4913, 18.4295, -12.9328, -1604.9307, -2.7779};
	 static final double[]coeffedptrainn8b4l4b1200000 =  {-0.3832, -18.6212, 41.1417, -13.7834, 25.1395, -18.9654, -2164.7413, -3.632};
	 static final double[]coeffedptrainn8b4l4b2000000 =  {-0.3488, -17.5198, 39.3688, -15.3616, 22.8162, -16.3683, -1988.2343, -3.3875};
	
	/*
	 static final double[]coeffedptrainn4b2l2b1400000 =  {-1.7682, -84.9515, 146.4278, -43.2856, 75.6197, -79.6419, -7360.0203};
	 static final double[]coeffedptrainn4b2l2b1600000 =  {-2.079, -99.9341, 172.1916, -50.8929, 88.6975, -93.3874, -8637.97};
	 static final double[]coeffedptrainn4b2l2b1800000 =  {-2.5595, -122.5968, 211.3386, -62.1199, 109.1543, -114.7044, -10622.7434};
	 static final double[]coeffedptrainn4b4l0b1400000 =  {-0.205, -10.1339, 17.4476, -4.99, 9.139, -10.5103, -886.8602};
	 static final double[]coeffedptrainn4b4l0b1600000 =  {-0.2006, -9.9362, 17.1344, -4.9696, 8.9297, -10.2646, -867.7483};
	 static final double[]coeffedptrainn4b4l0b1800000 =  {-0.2087, -10.4499, 18.0418, -5.4012, 9.3071, -10.7106, -907.0938};
	 static final double[]coeffedptrainn6b2l4b1400000 =  {-1.0222, -44.7111, 77.6083, -20.8686, 42.6288, -40.8166, -4079.0254};
	 static final double[]coeffedptrainn6b2l4b1600000 =  {-1.2043, -52.544, 91.1883, -24.3605, 50.2799, -48.1384, -4806.6942};
	 static final double[]coeffedptrainn6b2l4b1800000 =  {-1.4373, -62.8144, 108.9789, -29.3792, 59.9853, -57.2407, -5737.3939};
	 static final double[]coeffedptrainn8b4l4b1400000 =  {-0.4279, -19.2311, 34.4258, -12.5755, 17.1961, -14.5001, -1687.8347};
	 static final double[]coeffedptrainn8b4l4b1600000 =  {-0.4046, -18.5723, 33.5331, -13.5623, 16.0231, -12.7316, -1592.738};
	 static final double[]coeffedptrainn8b4l4b1800000 =  {-0.3485, -15.8741, 28.6246, -11.1333, 13.9006, -11.4677, -1374.9617};
	 static final double[]coeffedptrainn8b4l4b1200000 =  {-0.47, -20.8596, 36.8642, -12.0077, 19.218, -17.0498, -1864.0595};
	 static final double[]coeffedptrainn8b4l4b2000000 =  {-0.4297, -19.6075, 35.3792, -13.7055, 17.2934, -14.5817, -1707.7955};
	 
	 /*
	  static final double[]coeff4b4l012Kedp =  {0.00039898, 0.0021, 0.0214, -0.057, -0.0139, 0.1014, 0.0598};
		 static final double[]coeff4b4l014Kedp =  {0.0075, -1.5338, 0.0202, -0.7775, 1.9119, -2.8635, -44.039};
		 static final double[]coeff4b4l016Kedp =  {0.013, -4.3709, 0.8766, -1.5802, 4.8198, -7.6957, -120.0203};
		 static final double[]coeff4b4l018Kedp =  {-0.0306, -7.1077, 2.053, 2.3713, 6.4121, -9.5322, -147.6279};
		 static final double[]coeff8b4l412Kedp =  {0.00028923, 0.0024, 0.0211, -0.0442, -0.0104, 0.0791, 0.002};
		 static final double[]coeff8b4l414Kedp =  {0.00027846, 0.0013, 0.025, -0.0447, -0.0108, 0.0823, -0.0156};
		 static final double[]coeff8b4l416Kedp =  {0.0003524, 0.0029, 0.0235, -0.0534, -0.0125, 0.098, 0.0325};
		 static final double[]coeff8b4l418Kedp =  {0.00039587, 0.003, 0.0285, -0.0615, -0.0147, 0.1167, 0.0385};
		 static final double[]coeff4b2l212Kedp =  {0.0004478, -0.00090604, 0.0408, -0.0736, -0.0188, 0.1473, -0.0088};
		 static final double[]coeff4b2l214Kedp =  {0.00046535, -0.00016236, 0.0406, -0.0749, -0.0188, 0.1449, -0.0231};
		 static final double[]coeff4b2l216Kedp =  {0.00045277, -0.00042112, 0.0445, -0.0755, -0.0192, 0.1495, -0.0455};
		 static final double[]coeff4b2l218Kedp =  {0.0088, -1.9937, -0.0035, -0.9366, 2.5126, -3.7251, -50.9767};
		 static final double[]coeff6b2l412Kedp =  {0.00025533, -0.00033045, 0.0255, -0.0394, -0.0089, 0.0588, -0.0911};
		 static final double[]coeff6b2l414Kedp =  {0.00026811, -0.00013213, 0.0259, -0.041, -0.009, 0.0599, -0.0898};
		 static final double[]coeff6b2l416Kedp =  {-0.0074, -2.9144, 0.7005, 0.4853, 2.9041, -4.412, -63.0525};
		 static final double[]coeff6b2l418Kedp =  {0.0058, -0.9311, 0.0205, -0.6066, 1.1793, -1.7865, -22.5808};
		 static final double[]coeff8b4l42Kedp =  {7.1495, 155.1457, -589.1159, 310.5912, 226.2739, -299.7316, -7786.8949};
	 
*/
	 
	 static final double[]coeffchunksn4b2l212k =  {-44.112, 2535.6465, 6477.0615, 3126.3275};
	 static final double[]coeffchunksn4b4l014k =  {6.6539, 248.5237, 9285.8073, -1126.3882};
	 static final double[]coeffchunksn6b2l416k =  {-11.1363, 2976.704, 3391.027, 199.9006};
	 static final double[]coeffchunksn8b4l418k =  {5.5463, -1287.8451, 3740.6388, -964.2293};
	 static final double[]coeffchunksn4b2l214k =  {-29.0527, 5744.9656, 4208.1621, 4111.3718};
	 static final double[]coeffchunksn4b4l016k =  {1.9625, 2909.5474, 5217.0298, -1100.0023};
	 static final double[]coeffchunksn6b2l418k =  {10.277, 3885.8102, 1109.128, -543.2622};
	 static final double[]coeffchunksn8b4l42k =  {17.5566, 12957.2363, 2499.8406, -1286.0227};
	 static final double[]coeffchunksn4b2l216k =  {-11.0581, -7014.7866, 10142.056, -144.1389};
	 static final double[]coeffchunksn4b4l018k =  {-11.4053, 1790.6592, 8634.6781, -332.6547};
	 static final double[]coeffchunksn8b4l412k =  {10.8897, -1316.0584, 5976.7103, -1632.0159};
	 static final double[]coeffchunksn4b2l218k =  {5.6788, 261.1901, 4860.0117, -1259.8678};
	 static final double[]coeffchunksn6b2l412k =  {3.6702, 867.9947, 3176.4734, -679.1311};
	 static final double[]coeffchunksn8b4l414k =  {-7.3853, 1786.2183, 2777.7701, 146.9502};
	 static final double[]coeffchunksn4b4l012k =  {10.7488, -524.72, 8106.2245, -2238.8276};
	 static final double[]coeffchunksn6b2l414k =  {5.5698, 68.7293, 2739.2483, -532.8855};
	 static final double[]coeffchunksn8b4l416k =  {22.865, -1862.2881, 6217.5352, -1822.3409};
	
	 /*
	 
	 static final double[][]coeffchunksn4b2l212k =  {{0.2282, 5.8204, 67.8534, -32.495},
			 										 {0.2453, 4.8755, 68.3165, -33.759},
			 										 {0.2623, 6.0111, 68.1111, -34.893},
			 										 {0.2336, 6.5282, 66.2586, -32.451},
			 										 {0.2644, 6.1093, 68.3073, -35.153},
			 										 {0.2714, 5.4886, 69.396, -35.884},
			 										 {0.2703, 6.1219, 69.949, -35.902}};
	 
	 static final double[][]coeffchunksn4b4l014k =  {{0.2146, 6.7368, 64.8666, -31.068},
			 									   {0.2665, 6.2341, 65.1956, -34.182},
			 									   {0.2241, 5.5481, 65.0188, -31.542},
			 									   {0.2193, 2.3622, 64.4099, -31.136},
			 									   {0.2768, 5.6117, 65.1771, -34.748},
			 									   {0.2787, 7.5214, 64.199, -34.254},
			 									   {0.2345, 2.6911, 63.1799, -31.636}};
	 
	 static final double[][]coeffchunksn6b2l416k = {{0.2362, 8.6781, 57.4177, -30.655},
			 									   {0.2376, 9.2533, 56.9363, -30.553},
			 									   {0.2563, 9.8038, 55.6089, -31.546},
			 									   {0.2434, 3.9098, 54.2912, -30.114},
			 									   {0.2611, 8.8855, 58.6432, -32.385},
			 									   {0.2577, 11.5056, 58.5892, -32.376},
			 									   {0.259, 10.2011, 59.5811, -32.523}};
	 
	 static final double[][]coeffchunksn8b4l418k =  {{0.7159, -36.5626, 135.8353, -80.572},
			 									   {0.6567, -19.7974, 117.7618, -70.393},
			 									   {0.6364, -9.6058, 111.1045, -67.913},
			 									   {0.2923, 5.973, 104.8361, -46.373},
			 									   {0.7823, -17.8671, 115.8877, -76.406},
			 									   {0.7903, -12.2008, 112.9989, -75.190},
			 									   {0.6007, -12.8856, 117.7592, -66.508}};
	 
	 static final double[][]coeffchunksn4b2l214k =  {{0.2287, 5.1728, 69.6579, -33.031},
			 										{0.2305, 11.7202, 67.9838, -32.834},
			 										{0.2633, 10.1514, 70.4234, -35.681},
			 										{0.2625, 5.3762, 69.9913, -35.194},
			 										{0.2505, 8.4503, 68.9685, -34.259},
			 										{0.2478, 10.3991, 70.0551, -34.368},
			 										{0.2773, 10.6586, 72.449, -37.110}};
	 
	 static final double[][]coeffchunksn4b4l016k =  {{0.8124, 29.2789, 105.3315, -82.605},
			 										 {0.7565, 21.4841, 104.5506, -79.271},
			 										 {0.5896, 25.5629, 93.2482, -64.969},
			 										 {0.7177, -7.3677, 103.3759, -76.092},
			 										 {0.8766, 30.0019, 110.6787, -89.188},
			 										 {0.735, 23.2974, 102.4584, -77.270},
			 										 {0.593, 19.6097, 95.263, -65.497}};
	 
	 static final double[][]coeffchunksn6b2l418k =  {{0.221, 4.0206, 62.1127, -30.646},
			 										 {0.2353, 3.7935, 59.5548, -31.021},
			 										 {0.2196, 6.3639, 59.2899, -29.816},
			 										 {0.2519, 5.2252, 61.0798, -32.382},
			 										 {0.2622, 5.9819, 61.3858, -33.250},
			 										 {0.2277, 5.817, 62.4315, -31.066},
			 										 {0.2204, 6.263, 62.803, -30.562}};
	 
	 static final double[][]coeffchunksn8b4l42k =  {{8.625, -78.7924, 616.0356, -665.072},
			 										{3.6292, 35.8188, 550.7024, -354.818},
			 										{4.04, 8.713, 606.0797, -395.101},
			 										{2.4595, 126.4987, 574.3234, -313.345},
			 										{7.1887, 70.3777, 609.0514, -599.516},
			 										{2.8735, 35.3083, 654.3962, -362.690},
			 										{3.2956, -102.0313, 706.036, -377.679}};
	 
	 static final double[][]coeffchunksn4b2l216k =  {{0.21, 7.0121, 69.888, -31.8995295},
			 										 {0.2187, 7.7558, 67.781, -32.0290147},
			 										 {0.2192, 8.4608, 67.6266, -32.0182117},
			 										 {0.2326, 7.5762, 70.0858, -33.4977797},
			 										 {0.2399, 8.2939, 69.2657, -33.8280552},
			 										 {0.2337, 9.1595, 68.7103, -33.2047318},
			 										 {0.2344, 8.1181, 69.1784, -33.335553}};
	 
	 static final double[][]coeffchunksn4b4l018k =  {{0.9572, -22.6184, 177.0764, -111.0987081},
			 										 {0.6014, -10.0662, 137.0001, -76.1812161},
			 										 {0.4045, -12.245, 136.7895, -57.9036466},
			 										 {0.1918, -40.4432, 137.1854, -46.3195234},
			 										 {0.802, -12.2276, 157.871, -95.5675935},
			 										 {0.5986, -8.4645, 137.5877, -75.8256862},
			 										 {0.4728, -9.1089, 134.3384, -63.0589569}};
	 
	 static final double[][]coeffchunksn8b4l412k =  {{0.2617, 5.4991, 72.7351, -35.9725012},
			                                         {0.2707, 6.8088, 70.9555, -36.2139558},
			                                         {0.2631, 6.1817, 72.0865, -35.956304},
			                                         {0.2853, 6.0115, 72.1075, -37.1166077},
			                                         {0.2688, 6.9561, 72.0712, -36.3420111},
			                                         {0.2632, 5.9488, 74.1329, -36.3436333},
			                                         {0.277, 6.9814, 74.3609, -37.4051147}};
	 
	 static final double[][]coeffchunksn4b2l218k = {{0.3551, 3.5194, 81.5825, -44.6440698},
			 										{0.344, 4.6431, 78.4821, -43.0404507},
			 										{0.3519, 2.9844, 79.3825, -43.7035665},
			 										{0.3539, 5.1545, 83.0372, -44.9149283},
			 										{0.3547, 6.6094, 79.5561, -44.1264148},
			 										{0.3781, 2.8204, 81.635, -46.1676142},
			 										{0.3644, 4.4244, 81.9434, -45.2680472}};
	 
	 static final double[][]coeffchunksn6b2l412k =  {{0.2561, 5.0264, 57.9659, -31.9594502},
	                                                 {0.2479, 6.2081, 55.492, -30.6477635},
	                                                 {0.2609, 5.1916, 55.9898, -31.6973425},
	                                                 {0.2194, 4.6932, 54.8009, -28.426256},
	                                                 {0.2876, 7.512, 57.3644, -34.0009138},
	                                                 {0.2819, 7.9613, 57.6839, -33.529342},
	                                                 {0.2726, 7.3929, 58.8349, -33.2405165}};
	 
	 static final double[][]coeffchunksn8b4l414k =  {{0.2539, 6.5534, 77.1603, -36.8507219},
			 										 {0.2459, 4.9016, 76.885, -36.1716623},
			 										 {0.2523, 4.4157, 77.8795, -36.8181544},
			 										 {0.2652, 5.3911, 78.0211, -37.7303433},
			 										 {0.273, 5.8662, 77.073, -38.0208403},
			 										 {0.2611, 3.9979, 79.7971, -37.8467681},
			 										 {0.2804, 4.0947, 79.9197, -38.8476943}};
	 
	 static final double[][]coeffchunksn4b4l012k = {{0.214, 7.9621, 60.9374, -29.5897047},
			                                        {0.2441, 3.2658, 59.5984, -30.8575079},
			                                        {0.195, 4.6275, 58.4654, -27.8553588},
			                                        {0.1929, 5.8821, 59.3909, -27.9772913},
			                                        {0.2505, 5.2771, 59.6861, -31.5138574},
			                                        {0.1917, 4.2696, 58.3082, -27.5154191},
			                                        {0.2208, 4.4428, 59.8295, -29.9834967}};
	 
	 static final double[][]coeffchunksn6b2l414k =  {{0.2535, 8.0443, 59.3355, -32.3554267},
			 										  {0.2632, 9.5173, 56.6942, -32.3427838},
			 										  {0.2672, 9.8248, 56.8434, -32.6185202},
			 										  {0.248, 7.529, 57.0289, -31.21519},
			 										  {0.2912, 10.2843, 58.5921, -34.5972019},
			 										  {0.2852, 9.5632, 60.8845, -34.7331206},
			 										  {0.2762, 9.7512, 60.2377, -33.881387}};
	 
	 static final double[][]coeffchunksn8b4l416k = {{0.2569, 5.0658, 88.6866, -39.7244302},
			 										{0.2895, 6.4542, 85.5893, -40.6529158},
			 										{0.2876, 7.1512, 85.7665, -40.9698085},
			 										{0.2795, 8.6854, 85.644, -40.7417659},
			 										{0.27, 8.4602, 86.4703, -40.0331855},
			 										{0.2765, 7.139, 87.7007, -40.6090814},
			 										{0.2886, 7.8813, 87.7535, -40.994775}};
	 
	 
	 */
	 static final int[] chunklist = {128, 256, 512, 1024, 2048, 4096, 8192};
	 
	 
	 
	 
	 
	 
	 
	 
	/* 
	 
	 static final double[]coeff4b4l012Kedp =  {0.01, -0.4527, 0.1866, -13.1868, -0.0366, -0.0095, 6.9627};
	 static final double[]coeff4b4l014Kedp =  {-0.0042, -0.3738, 0.2069, 2.2631, 0.1073, -0.0575, 2.612};
	 static final double[]coeff4b4l016Kedp =  {-0.0136, -1.0539, 0.6655, 8.4063, 0.2697, -0.2995, 6.7455};
	 static final double[]coeff4b4l018Kedp =  {-0.0256, -1.7362, 1.0875, 17.9306, 0.3732, -0.3103, 9.358};
	 static final double[]coeff8b4l412Kedp =  {0.00052206, -0.1217, 0.0921, -1.2394, 0.0575, -0.1202, 1.0994};
	 static final double[]coeff8b4l414Kedp =  {0.0006204, -0.1193, 0.0999, -1.3457, 0.0563, -0.1261, 1.0722};
	 static final double[]coeff8b4l416Kedp =  {0.00084977, -0.1158, 0.0992, -1.5933, 0.0559, -0.1198, 1.106};
	 static final double[]coeff8b4l418Kedp =  {0.00087681, -0.1404, 0.1126, -1.7552, 0.0684, -0.1356, 1.308};
	 static final double[]coeff4b2l212Kedp =  {-0.0062, -0.6397, 0.3328, 3.0144, 0.2129, -0.2118, 4.6644};
	 static final double[]coeff4b2l214Kedp =  {-0.0061, -0.6272, 0.3314, 2.9752, 0.2113, -0.2186, 4.5527};
	 static final double[]coeff4b2l216Kedp =  {-0.0069, -0.6739, 0.3746, 3.4869, 0.2235, -0.2437, 4.7981};
	 static final double[]coeff4b2l218Kedp =  {-0.0039, -0.4658, 0.2985, 1.5435, 0.165, -0.27, 3.3811};
	 static final double[]coeff6b2l412Kedp =  {-0.00096649, -0.194, 0.1175, -0.0469, 0.0756, -0.1208, 1.5226};
	 static final double[]coeff6b2l414Kedp =  {-0.0098, -0.6148, 0.4257, 6.8266, 0.2048, -0.2968, 3.378};
	 static final double[]coeff6b2l416Kedp =  {-0.0085, -0.689, 0.4544, 5.364, 0.1913, -0.2948, 3.9769};
	 static final double[]coeff6b2l418Kedp =  {-0.00092024, -0.2116, 0.1644, -0.2568, 0.0715, -0.1378, 1.6884};
	 static final double[]coeff8b4l42Kedp =  {6.8599, 175.7639, -778.4581, 5720.817, 165.6281, -73.8141, 847.1224};
	 */
	 
	 /*static final double[]coeff4b4l012Kedp =  {0.0144, -0.4777, 0.0645, -0.1708, -0.0201, -0.0755, 7.0996};
	 static final double[]coeff4b4l014Kedp =  {-0.0042, -0.3683, 0.1917, 0.0241, 0.116, -0.1395, 2.6562};
	 static final double[]coeff4b4l016Kedp =  {-0.0136, -1.0501, 0.6403, 0.0847, 0.2791, -0.3976, 6.9108};
	 static final double[]coeff4b4l018Kedp =  {-0.0269, -1.6265, 1.0455, 0.1967, 0.4011, -0.4844, 8.7788};
	 static final double[]coeff8b4l412Kedp =  {0.00065948, -0.1201, 0.0796, -0.0127, 0.0641, -0.1805, 1.1242};
	 static final double[]coeff8b4l414Kedp =  {0.00076642, -0.1185, 0.0874, -0.0139, 0.0632, -0.1899, 1.1026};
	 static final double[]coeff8b4l416Kedp =  {0.001, -0.1154, 0.0847, -0.0166, 0.0639, -0.1932, 1.1409};
	 static final double[]coeff8b4l418Kedp =  {0.0011, -0.1402, 0.0964, -0.018, 0.0777, -0.2219, 1.354};
	 static final double[]coeff4b2l212Kedp =  {-0.0061, -0.6271, 0.3048, 0.0307, 0.2253, -0.3293, 4.7232};
	 static final double[]coeff4b2l214Kedp =  {-0.006, -0.6163, 0.3045, 0.0303, 0.2233, -0.3345, 4.6218};
	 static final double[]coeff4b2l216Kedp =  {-0.0068, -0.662, 0.348, 0.036, 0.2357, -0.3643, 4.8714};
	 static final double[]coeff4b2l218Kedp =  {-0.0038, -0.4594, 0.2792, 0.0155, 0.1733, -0.3508, 3.4363};
	 static final double[]coeff6b2l412Kedp =  {-0.00089558, -0.1905, 0.1069, -0.00033337, 0.0812, -0.1718, 1.5399};
	 static final double[]coeff6b2l414Kedp =  {-0.0104, -0.6074, 0.4293, 0.075, 0.2083, -0.3548, 3.4612};
	 static final double[]coeff6b2l416Kedp =  {-0.0094, -0.6588, 0.4413, 0.0633, 0.2041, -0.3624, 3.8692};
	 static final double[]coeff6b2l418Kedp =  {-0.00080541, -0.2038, 0.1514, -0.0026, 0.0787, -0.2052, 1.6692};	
	 */
	 static final double[]bigscale =  {100000, 27.6348,100, 100, 100, 100,100};
	 
	 
	    
	    public double[] selectChunkArray(HardwareConfig config) {
	    	switch(config) {
	    	case n4b2l2b1200000:
	    		return coeffchunksn4b2l214k;
			case n4b2l2b1400000:
				return coeffchunksn4b2l214k;
			case n4b2l2b1600000:
				return coeffchunksn4b2l216k;
			case n4b2l2b1800000:
				return coeffchunksn4b2l218k;
			case n4b4l0b1200000:
				return coeffchunksn4b4l012k;
			case n4b4l0b1400000:
				return coeffchunksn4b4l014k;
			case n4b4l0b1600000:
				return coeffchunksn4b4l016k;
			case n4b4l0b1800000:
				return coeffchunksn4b4l018k;
			case n6b2l4b1200000:
				return coeffchunksn6b2l412k;
			case n6b2l4b1400000:
				return coeffchunksn6b2l414k;
			case n6b2l4b1600000:
				return coeffchunksn6b2l416k;
			case n6b2l4b1800000:
				return coeffchunksn6b2l418k;
			case n8b4l4b1200000:
				return coeffchunksn8b4l412k;
			case n8b4l4b1400000:
				return coeffchunksn8b4l414k;
			case n8b4l4b1600000:
				return coeffchunksn8b4l416k;
			case n8b4l4b1800000:
				return coeffchunksn8b4l418k;
			default:
				break;
			}
	    	assert(0 == 1);
	    	return null;
	    }
	 
	 
    
    public double[] selectArray(HardwareConfig config) {
    	switch(config) {
    	case n4b0l4b2000000:
    		return bigscale;
		case n4b2l2b1400000:
			return coeffedptrainn4b2l2b1400000;
		case n4b2l2b1600000:
			return coeffedptrainn4b2l2b1600000;
		case n4b2l2b1800000:
			return coeffedptrainn4b2l2b1800000;
		case n4b2l2b2000000:
			return bigscale;
		case n4b4l0b1400000:
			return coeffedptrainn4b4l0b1400000;
		case n4b4l0b1600000:
			return coeffedptrainn4b4l0b1600000;
		case n4b4l0b1800000:
			return coeffedptrainn4b4l0b1800000;
		case n4b4l0b2000000:
			return bigscale;
		case n6b2l4b1400000:
			return coeffedptrainn6b2l4b1400000;
		case n6b2l4b1600000:
			return coeffedptrainn6b2l4b1600000;
		case n6b2l4b1800000:
			return coeffedptrainn6b2l4b1800000;
		case n6b2l4b2000000:
			return bigscale;
		case n8b4l4b1200000:
			return coeffedptrainn8b4l4b1200000;
		case n8b4l4b1400000:
			return coeffedptrainn8b4l4b1400000;
		case n8b4l4b1600000:
			return coeffedptrainn8b4l4b1600000;
		case n8b4l4b1800000:
			return coeffedptrainn8b4l4b1800000;
		case n8b4l4b2000000:
			return coeffedptrainn8b4l4b2000000;
		default:
			break;
		}
    	assert(0 == 1);
    	return null;
    }

	
	
	 /**
	  * Since the some of x values were very small to make sure the values doesn't 
	  * phase out we scaled those values while training.  
	  * 
	  */
      
	  static final int densityScale = 1000;
	  static final int clusterScale = 1;
	  
	  public double getval(double coeff[], GraphPropertiesData v) {
	
		  return ((coeff[0] * v.getavgEdges() ) + (coeff[1] * v.getCluster() * clusterScale) + (coeff[2] * v.getaed()) 
				  + (coeff[3] * v.getdensity()* densityScale)
				  + (coeff[4] * v.getSeqVal() * 1 ) + (coeff[5] * v.getRandVal()* 1) + (coeff[6] *v.getAtomics() * 0.01) + coeff[7]);
	  }
	  
	  public double getGraphPropVal(double coeff[], GraphPropertiesData v) {
		  return ((coeff[0] * v.getavgEdges()) + (coeff[1] * v.getCluster() * clusterScale) + (coeff[2] * v.getaed()) 
				  + (coeff[3] * v.getdensity()* densityScale));
	  }
	  
	  /*
	   * The order of properties is 
	   * adjecency
	   * ClusterCoeff
	   * aed
	   * density
	   * memory
	   * array
	   * atomics
	   * */   	  	
	public HardwareConfig getbestConfigurationforEDP(GraphPropertiesData v) {
		
		HardwareConfig[] select= {HardwareConfig.n4b2l2b1400000, HardwareConfig.n4b2l2b1600000, 
		                          HardwareConfig.n4b2l2b1800000, HardwareConfig.n4b4l0b1400000,
		                          HardwareConfig.n4b4l0b1600000, HardwareConfig.n4b4l0b1800000, 
		                          HardwareConfig.n6b2l4b1400000, HardwareConfig.n6b2l4b1600000, 
		                          HardwareConfig.n6b2l4b1800000, HardwareConfig.n8b4l4b1200000, 
		                          HardwareConfig.n8b4l4b1400000, HardwareConfig.n8b4l4b1600000, 
		                          HardwareConfig.n8b4l4b1800000};
		
		double minVal = getval(selectArray(HardwareConfig.n8b4l4b2000000),v);
		HardwareConfig config = HardwareConfig.n8b4l4b2000000;
		
		for(HardwareConfig c: select ) {
			double val = getval(selectArray(c), v);
			if(minVal > val) {
				minVal = val;
				config = c;
			}
			
		}		
		return config;
	}
	
	
		
	public int getOptimalChunkConfiguration(GraphPropertiesData v, HardwareConfig h) {
		double[] chunkArray = selectChunkArray(h);
		
		int chunkSize = (int) getGraphPropVal(chunkArray, v);
		
		if(chunkSize <= 192)
			chunkSize= 128;
		else if(chunkSize <= 384)
			chunkSize = 256;
		else if(chunkSize <= 768)
			chunkSize = 512;
		else if(chunkSize <= 1536)
			chunkSize = 1024;
		else if (chunkSize <= 3072)
			chunkSize = 2048;
		else if (chunkSize <= 6144)
			chunkSize = 4096;
		else
			chunkSize = 8192;
		
		/*
		int chunkSize = chunklist[0];
		double minedp = getGraphPropVal(chunkArray[0], v);
		
		for(int i=1;i< chunklist.length;i++) {
			double edp =  getGraphPropVal(chunkArray[i], v);
			if(edp < minedp) {
				chunkSize = chunklist[i];			
			}
		}
		*/
		Main.addErrorMessage("The optimalChunkSize is " + chunkSize);
		return chunkSize;
	}
	
	
	

}
