/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.CompoundStatementElement;
import imop.ast.node.Expression;
import imop.ast.node.ForConstruct;
import imop.ast.node.FunctionDefinition;
import imop.ast.node.Node;
import imop.ast.node.NodeChoice;
import imop.ast.node.NodeList;
import imop.ast.node.NodeListOptional;
import imop.ast.node.NodeOptional;
import imop.ast.node.NodeToken;
import imop.ast.node.ParallelConstruct;
import imop.ast.node.ParallelForConstruct;
import imop.ast.node.UniqueForClauseSchedule;
import ces.SystemConfigFile;
import ces.ast.info.CesParallelConstructInfo;
import ces.data.base.GraphPropertiesData;
import ces.data.classifier.Input.GraphEDPModel;
import ces.data.workloadGetter.OMPGraphWorkloadGetter;
import ces.enums.HardwareConfig;
import ces.enums.OptimizationLevel;
import ces.util.CesSpecificMisc;
import imop.parser.CParser;

public class ClassifyGraphWorkload {
	

	public void execute(GraphPropertiesData data, Node wlRoot) {
		GraphEDPModel model = new GraphEDPModel();
		
		/* Collect Irregular Parallel Regions */
		
		
		
		
		
		HardwareConfig hc = model.getbestConfigurationforEDP(data);
		Main.addlog("The selected hardware configuration is " + hc.toString());
		
		FunctionDefinition def = (FunctionDefinition) wlRoot;
		assert(def.f3 != null);
		String configdata = "#pragma optimalConfig  " + hc.toString() + "\n";
		CompoundStatementElement s = (CompoundStatementElement) CParser.createCrudeASTNode(configdata, CompoundStatementElement.class);
		
		NodeListOptional nl = def.f3.f1;
		assert(nl.nodes != null);
		nl.nodes.add(0, s);
		
	
		if(SystemConfigFile.optimizationLevel == OptimizationLevel.DynamicScheduling) {
			int chunkSize = model.getOptimalChunkConfiguration(data, hc);
			
			// get irregular chunk Size
			//OMPDynamic
			
			String chunkData = "#pragma oprimalChunk "  + chunkSize + "\n";
			CompoundStatementElement cd = (CompoundStatementElement) CParser.createCrudeASTNode(chunkData, CompoundStatementElement.class);
			nl.nodes.add(0, cd);
			
			//ArrayList<Node> parallelWorkloads = CesSpecificMisc.collectAllChildrensofClass(wlRoot, ParallelConstruct.class.getName());
			ArrayList<Node> parallelForWorkloads = CesSpecificMisc.collectAllChildrensofClass(wlRoot, ParallelForConstruct.class.getName());
			ArrayList<Node> forWorkloads = CesSpecificMisc.collectAllChildrensofClass(wlRoot, ForConstruct.class.getName());
			//parallelWorkloads.addAll(parallelForWorkloads);
		
			for(Node parWorkload : parallelForWorkloads) {
				OMPGraphWorkloadGetter gwl = new OMPGraphWorkloadGetter();
				gwl.init(Main.root);
				parWorkload.accept(gwl, null);
				if(gwl.getGraphModel().isIrregular()) {
					// TODO use chunk size appropriate for gwl workload not overall workload
					ParallelForConstruct forc = (ParallelForConstruct) parWorkload;
					ArrayList<Node> list = CesSpecificMisc.collectAllChildrensofClass(forc, UniqueForClauseSchedule.class.getName());
					
					String scheduleString = "schedule( dynamic, "+ chunkSize + ")";
					UniqueForClauseSchedule scheduleNode = (UniqueForClauseSchedule) CParser.createCrudeASTNode(scheduleString, UniqueForClauseSchedule.class);
					
					if(!list.isEmpty()) {
						NodeChoice parent = (NodeChoice)list.get(0).parent;
						parent.updateChoice(scheduleNode, 1);
					}
					
				}
			}
			
			
			for(Node parWorkload : forWorkloads) {
				OMPGraphWorkloadGetter gwl = new OMPGraphWorkloadGetter();
				gwl.init(Main.root);
				parWorkload.accept(gwl, null);
				if(gwl.getGraphModel().isIrregular()) {
					// TODO use chunk size appropriate for gwl workload not overall workload
					ForConstruct forc = (ForConstruct) parWorkload;
					ArrayList<Node> list = CesSpecificMisc.collectAllChildrensofClass(forc, UniqueForClauseSchedule.class.getName());
					
					String scheduleString = " schedule (dynamic, "+ chunkSize + ")";
					String chunkSizeStr = String.valueOf(chunkSize); 
					Expression chunkExpression = (Expression)CParser.createRefinedASTNode(chunkSizeStr, Expression.class);
					NodeToken comma = new NodeToken(",");
					
				//	UniqueForClauseSchedule scheduleNode = (UniqueForClauseSchedule) CParser.createRefinedASTNode(scheduleString, UniqueForClauseSchedule.class);
					
					if(!list.isEmpty()) {
						UniqueForClauseSchedule oldScheduleNode  = (UniqueForClauseSchedule) list.get(0);
						NodeToken token = (NodeToken) oldScheduleNode.f2.f0.choice;
						token.tokenImage = "guided";
						NodeOptional opt = (NodeOptional) oldScheduleNode.f3;
						NodeList nodelist = new NodeList();
						nodelist.addNode(comma);
						nodelist.addNode(chunkExpression);
						opt.node = nodelist;
						
					//	NodeChoice parent = (NodeChoice)list.get(0).parent;
						
					//	parent.updateChoice(scheduleNode, 1);
					}
				}
			}
			
		}
		
		ArrayList<Node> parallelRegions = CesSpecificMisc.collectAllChildrensofClass(wlRoot, ParallelConstruct.class.getName());
		
		
		for(Node p : parallelRegions) {
			CesParallelConstructInfo info = (CesParallelConstructInfo) p.getInfo();
			if(info.isIrregularParallelRegion() == false) {
				/*
				 * TODO Custom Scheduling
				 * */
			}
		}
		
	}
 
}
