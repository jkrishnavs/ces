/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import imop.Main;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.data.CPPdata.CtoCPPFunctionMapping;
import ces.data.base.VariableRange;
import ces.data.constructData.*;
import ces.enums.*;
import ces.enums.OptimizationLevel;
import ces.getter.CPPFunctionGetter;
import ces.getter.CollectVariableRanges;
import ces.getter.GetMaxDimension;
import ces.getter.GetMaxNumber;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
import ces.util.SystemStringFunctions;
import imop.parser.CParser;

public class ClassifyWorkload {
	
	OMPFunctionWorkload ompFunctionWorkload;
	
	
	TranslationUnit __root;
	
	
	public static final int singleFlag = 1;
	public static final int ompForFlag = 2;
	public static final int masterFlag = 4;
	public static final int criticalFlag = 8;
	public static final int conditionalFlag = 16;
	public static final int breakFlag = 32;
	public static final int continueFlag = 64;
	public static final int blockFlag = 128;
	public static final int ompBlockFlag = 256;
	public static final int forFlag = 512;
	public static final int flushFlag = 1024;
	public static final int functionCallFlag = 2048;
	public static final int reductionFlag = 4096;
	public static final int returnFlag = 8196;
	public static final int whileFlag = 16392;
	
	private ArrayList<OMPParallelWorkload> parRegions;
	
	
	public void addToParRegions(OMPParallelWorkload pw){
		parRegions.add(pw);
	}
	
	
	//OMPParallelWorkload startBarrierWorkload;
	
	//OMPParallelWorkload endBarrierWorkload;
	

	public ClassifyWorkload(OMPFunctionWorkload ofw, TranslationUnit root) {
		ompFunctionWorkload = ofw;
		parRegions = new ArrayList<OMPParallelWorkload>();
		__root = root;
	}
	

	static final int  noneFound = 0;
	static final int startFound = 1;
	static final int endFound = 2;
	
	private int collectAllParallelRegions(Stack<OMPWorkload> wls, int status) {
		for(OMPWorkload child : wls) {
			if(child instanceof BeginWorkload) {
				assert(status == noneFound);
				status = startFound;
			} else if(child instanceof EndWorkload) {
				assert(status == startFound);
				status = endFound;
				assert(parRegions.size() > 0);
			//	endBarrierWorkload = parRegions.get(parRegions.size() - 1);
			}  else if(child.getClass() == OMPParallelWorkload.class) {
				OMPParallelWorkload bW = (OMPParallelWorkload) child;
				if(bW.getParallelregionWorkload() == null ) {
					ompFunctionWorkload.removeBarrierWorkload(bW);
				} else if(bW.getNode() == null) {
					Main.addErrorMessage(" The prallel region is not null but node is null");
				} else if(status == startFound) {
					boolean found = false;
					if(parRegions.isEmpty()) {
					//	startBarrierWorkload = (OMPParallelWorkload) child;
					}
					for(OMPParallelWorkload c : parRegions) {
						if(c == child) {
							found = true;
							c.incrementOccuranceFrequency();
							break;
						}
					}
					if(!found) {
						assert(((OMPParallelWorkload)child).getParallelregionWorkload() != null );
						addToParRegions((OMPParallelWorkload) child);
						((OMPParallelWorkload) child).setOccuranceFreqency(1);
					}	
				} else {
					//	Main.addlog("Outside start and end nodes");
				}
			} else if(child.getClass() == OMPBlockWorkload.class) {
				// do it				
//				if(((OMPBlockWorkload)child).hasParallelData()) {
//					Main.addlog("Has parallel region outside Barriers" + child.getNode().getInfo().getString());
//					Main.addErrorMessage("Has parallel region outside Barriers" + child.getNode().getInfo().getString());
//				} else {
//					int curStatus = status;
//					OMPBlockWorkload bW = (OMPBlockWorkload) child;
//					status = collectAllParallelRegions(bW.getChildWorkloads(), status);
//					if(status != curStatus)
//						Main.addlog("STATUS UPDATED");
//				}
				OMPBlockWorkload block = (OMPBlockWorkload) child;
				collectAllParallelRegions(block.getChildWorkloads(), status);
				
			} else if(child.getClass() == BlockWorkload.class) {
				BlockWorkload block = (BlockWorkload) child;
				collectAllParallelRegions(block.getChildWorkloads(), status);
			} else if(child.getClass() == ConditionalWorkload.class){
				ConditionalWorkload cW = (ConditionalWorkload) child;
				BlockWorkload block =  cW.getIfLoad();
				if(block != null)
					collectAllParallelRegions(block.getChildWorkloads(), status);
				block  = cW.getElseLoad();
				if(block != null)
					collectAllParallelRegions(block.getChildWorkloads(), status);
			} else if (child.getClass() == ForWorkload.class) {
				ForWorkload fW = (ForWorkload) child;
				collectAllParallelRegions(fW.getItrWorkload().getChildWorkloads(), status);
			} else if (child.getClass() == WhileWorkload.class) {
				WhileWorkload wW = (WhileWorkload) child;
				collectAllParallelRegions(wW.getItrWorkload().getChildWorkloads(), status);
			} else if (child.getClass() == ExprWorkload.class ||
					child.getClass() == ReturnWorkload.class  ||
					child.getClass() == OMPFunctionCallWorkload.class) {
				// DO Nothing.
			}
			
			/**
			 * Added as a part of 
			 * 
			 */
			
//			if(child.hasEmbeddedParallelRegion()) {
//				for (EmbeddedParallelRegions ePR : child.getEmbeddedDetails()) {	
//					OMPParallelWorkload pW = (OMPParallelWorkload) ePR.getParallelRegion();
//					addToParRegions(pW);
//				}
//			}
		}
		return status;	
	}

	
	public void execute() {
		
		for(OMPWorkload child : ompFunctionWorkload.getChildWorkloads()) {
			if(child.getClass() == OMPParallelWorkload.class) {
				OMPParallelWorkload bW = (OMPParallelWorkload) child;
				if(bW.getParallelregionWorkload() == null ) {
					ompFunctionWorkload.removeBarrierWorkload(bW);
				} else if(bW.getNode() == null) {
					Main.addErrorMessage(" The prallel region is not null but node is null");
				}
			} else if(child.getClass() == OMPBlockWorkload.class) {
				if(((OMPBlockWorkload)child).hasParallelData()) {
					Main.addErrorMessage("Has parallel region outside Barriers" + child.getNode().getInfo().getString());
				}
			} else {
		//		Main.addlog("The node is " + child);
			}
			
		}
		int status = noneFound;
		
		status = collectAllParallelRegions(ompFunctionWorkload.getChildWorkloads(), status) ;
	
		//Main.addlog(data);
		assert(status == endFound);
		
		
		
		
		verify();
		accumulateBranchOperations();
		accumulateReductionOperations();
		accumulateFalseSharing();
		accumulateSingle();
		accumulateCritical();
		accumulateMaster();
		accumulateData();
		accumulateUnevenWorkloads();
		accumulateReentrantNodes();
		
		
		
		
		//	printData();
		init();
		
		if(SystemConfigFile.project == JKPROJECT.CLASSIFIER) {
		//	cpe.setSelectedCostFunction(SystemConfigFile.selectedCostFunction);
			classify(ClassificationDetails.maxSize);
			getMaxNodes();
			setConfigDetails();
			tranformCode();
		} else if(SystemConfigFile.project == JKPROJECT.CES) {
			ces();
			transformCesCode();
		} else if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			irregularCES();
			transformIrregularCode();
		}
	}
	
	
	private ArrayList<VariableRange>  updateVariableList() {
		GetMaxDimension maxDim = new GetMaxDimension();
		__root.accept(maxDim);
		ClassificationDetails.maxSize = maxDim.getMaxSize();
		
		
		if(ClassificationDetails.maxSize == 0) {
		//	GetMaxNumber maxNumber  = new GetMaxNumber();
		//	__root.accept(maxNumber);
		//	ClassificationDetails.maxSize = maxNumber.getMaxSize();
			if(ClassificationDetails.maxSize == 0) {
				ClassificationDetails.maxSize = SystemConfigFile.defaultDimSize;
			}
		}
		
		
		ArrayList<PostfixExpression> varList = new ArrayList<PostfixExpression>();
		
		for(OMPParallelWorkload pwl : parRegions) {
			pwl.getVariablesofInterest(varList);
		}
		
		for(PostfixExpression e : varList) {
			Main.addlog("Variable of interest: " + e.getInfo().getString());
		}
		
		varList = cleanUpVariableList(varList);
		
		CollectVariableRanges collectVariableRanges =  new CollectVariableRanges(varList, null);
		__root.accept(collectVariableRanges);
		ArrayList<VariableRange> varRanges = collectVariableRanges.getVarList();		
		
		/**
		 * TODO this code is currently not tested for 
		 * other projects.
		 * But it should be tested and added.
		 */
	
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			ArrayList<PostfixExpression> unallocatedList = collectVariableRanges.getUnallocatedExprList();
			if(varRanges.isEmpty()) {
				VariableRange vr = new VariableRange(SystemConfigFile.defaultDimSize);
				varRanges.add(vr);
			}
			while(!unallocatedList.isEmpty()) {
				
				PostfixExpression exp = unallocatedList.remove(0);
				VariableRange vr = getRangeToAdd(varRanges);
				vr.addVariable(exp.getInfo().getString());
				collectVariableRanges = new CollectVariableRanges(unallocatedList, varRanges);
				__root.accept(collectVariableRanges);
				varRanges = collectVariableRanges.getVarList();
				unallocatedList = collectVariableRanges.getUnallocatedExprList();
			}	
		}
		/**
		 * Till here
		 * 
		 */
		return varRanges;
	}


	private VariableRange getRangeToAdd(ArrayList<VariableRange> vList) {
		/**
		 * TODO We are adding the unknown variable to the range 
		 * of SystemConfigFile.defaultDimSize.
		 * We can also add them to current largest VariableRange.
		 */
		

		for(VariableRange v : vList) {
			if(v.getMaxValue() >= SystemConfigFile.defaultDimSize)
				return v;
			
		}
		/**
		 * All created Variable ranges are smaller than defaultSize
		 * 
		 */
		VariableRange newRange = new VariableRange(SystemConfigFile.defaultDimSize);
		vList.add(newRange);
		return newRange;
	}


	private ArrayList<PostfixExpression> cleanUpVariableList(ArrayList<PostfixExpression> varList) {
		// Remove duplicates and constants
		ArrayList<PostfixExpression> condensedVarList =  new ArrayList<PostfixExpression>();
		for(PostfixExpression e : varList) {
			boolean flag = false;
			try {
				Double.parseDouble(e.getInfo().getString());
				flag = true;
			} catch (Exception execption){
				// Do Nothing
			}
			
			for(PostfixExpression newExp: condensedVarList) {
				if(newExp.getInfo().getString().equals(e.getInfo().getString())) {
					flag = true;
					break;
				}
			}
			
			if(!flag) {
				condensedVarList.add(e);
			}
			
		}
		return condensedVarList;
	}


	private void setConfigDetails() {
		
		
		HardwareConfig globalConfig = parRegions.get(0).getClassificationDetails().getSelected();
		mainFunctionCode = "\n {\n omp_set_num_threads("+ ClassificationDetails.getTotalCores(globalConfig) +");\n"
				+ "\n printf(\"THE CORE CONFIGURATIONS ARE BIG : " + ClassificationDetails.numberofBigCores(globalConfig)
				+ "\\n LITTLE : " + ClassificationDetails.numberofLITTLECores(globalConfig) 
				+ "\\n With big frequency " + ClassificationDetails.getBigFreqency(globalConfig) 
				+ "\\n " + globalConfig.name() + "  "
				+ "\\n\"); \n";
		String coresData = " cores = " + ClassificationDetails.getCoreIDFlag(globalConfig) +";";
		mainFunctionCode += coresData;
		if(SystemConfigFile.optimizationLevel == OptimizationLevel.CustomScheduling)
			mainFunctionCode += "affinityinitialize();\n"
					+ SystemStringConstants.baseSize +" = " + SystemConfigFile.defaultChunkSize + ";\n"
					+ SystemStringConstants.ratio + " = " + SystemConfigFile.globalbigr + ";\n";
		mainFunctionCode +="}";
		
	}

	private void transformCesCode() {
		SystemConfigFile.selectedCostFunction = CostFunction.EXECUTIONTIME;
		globalConfig = SystemConfigFile.globalHardwareConfig;
		mainFunctionCode = "\n {\n omp_set_num_threads("+ ClassificationDetails.getTotalCores(globalConfig) 
		+");\naffinityinitialize();\n}";
		addInitializationCode();
		for (OMPParallelWorkload cW : parRegions) {
			cW.getClassificationDetails().fixthreadtoCore();
			cW.transformCode(cW.getClassificationDetails());
			globalCode.addAll(cW.getClassificationDetails().getGlobalCode());
		}
		NodeList nl = Main.root.f0;
		int globalPos = Main.root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(Main.root));
		for(String gCode :globalCode) {
			ElementsOfTranslation tu = (ElementsOfTranslation) CParser.createCrudeASTNode(gCode, ElementsOfTranslation.class);
			nl.nodes.add(globalPos, tu);
		}
		addtoOutputCode();
	}


	private void accumulateReentrantNodes() {
		for(OMPParallelWorkload pwl : parRegions) {
			pwl.getParallelregionWorkload().accumulateReentrantNodes(1, SystemConfigFile.itrValue);
		}
	}


	public static Classifier cpe;
	
	public void init() {
		cpe = new Classifier();
		cpe.init();
		cpe.setVariableRanges(updateVariableList());
	}
	
	
	public void ces() {
		for(OMPParallelWorkload pwl : parRegions) {
			pwl.getParallelregionWorkload().ces();
		}
	}
	
	public void irregularCES() {
		for(OMPParallelWorkload pwl : parRegions) {
			pwl.getParallelregionWorkload().ces();
		}
	}

	public void classify(double maxSize) {
		for(OMPParallelWorkload pwl : parRegions) {
			pwl.getParallelregionWorkload().classify();
		}
	}
	
	
	HardwareConfig globalConfig;

	/**
	 *  If we have more than one Parallel regions this will 
	 *  help us identify the maximum weighted region.
	 *  We will select the maximum weighted region.
	 */
	private void getMaxNodes() {
		assert(!parRegions.isEmpty());
		if(parRegions.size() == 1 ) {
			ClassificationDetails cd = parRegions.get(0).getClassificationDetails();
			cd.setSelected(cd.getPreffered());	
			globalConfig = cd.getSelected();	
		
		} else {
			globalConfig = HardwareConfig.Unknown;
			double max = -1;
			for(OMPParallelWorkload pwl : parRegions) {
				double wldata = pwl.getAccumulatedData().totalOps().getBaseData();
				if(wldata > max) {
					max = wldata;
					globalConfig = pwl.getClassificationDetails().getPreffered();
				}
			}
		
			for(OMPParallelWorkload pwl : parRegions) {
				pwl.getClassificationDetails().setSelected(globalConfig);
			}
		}
		
	}

	public void verify() {
		for (OMPWorkload cW : parRegions) {
			assert(((OMPParallelWorkload)cW).getParallelregionWorkload() != null);
			cW.verify(returnFlag|breakFlag|continueFlag|blockFlag);
		}
		
	}
	
	public void accumulateReductionOperations() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateReductions(null);
		}
	}
	
	public void accumulateBranchOperations() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateBranches(null);
		}
	}
	
	
	public void accumulateFalseSharing() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateFalseSharing(null, null);
		}
	}
	
	
	public void accumulateSingle() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateSingle(null, false);
		}
	}
	public void accumulateMaster() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateMaster(null, false);
		}
	}
	
	public void accumulateCritical() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateCritical(null, false);
		}
	}
	
	
	public void accumulateData() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulatedata(null);
		}
	}
	
	public void accumulateUnevenWorkloads() {
		for (OMPWorkload cW : parRegions) {
			cW.accumulateUnevenWorkloads(null, false, null);
		}
	}

	public void printData() {
		for (OMPWorkload cW : parRegions) {
			Main.addlog(cW.dumpdata());
		}	
	}
	
	
	/*
	class ClusterDetails {
		ArrayList<OMPParallelWorkload> bwls;
		ClassificationDetails cd;
		public ClusterDetails(ArrayList<OMPParallelWorkload> b, ClassificationDetails c) {
			bwls = b;
			cd = c;
		}
		
	}
	
	*/
	/*
	private void culsterparregions() {	
		ArrayList<OMPParallelWorkload> bwls = ompFunctionWorkload.getParallelRegions();
		boolean newStart = true;
		ArrayList<OMPParallelWorkload> curList = null;
		
		ArrayList< ClusterDetails > clusterlist = new ArrayList<ClusterDetails>();
		
		
		for(OMPParallelWorkload bwl : bwls ) {
			Main.addlog("New Par start" + bwl.node.getInfo().getString());
			if(newStart) {
				assert(bwl.isStartPoint());
				curList = new ArrayList<OMPParallelWorkload>();
				newStart = false;
			}
			curList.add(bwl);
			if(bwl.isEndPoint()) {
				ClassificationDetails cd = getClassificationForCluster(curList);
				clusterlist.add(new ClusterDetails(curList, cd));
				newStart = true;
			}
		}
		
		for(ClusterDetails cd : clusterlist) {
			setClassificationForCluster(cd.bwls, cd.cd);
		}
		
	}
	*/
	

	
	public void setClassificationForCluster(ArrayList<OMPParallelWorkload> clusters, ClassificationDetails cd) {
		for(OMPParallelWorkload bW : clusters ) {
			bW.setClassificationDetails(cd, 1);
		}	
	}
	
	ArrayList<String> globalCode = new ArrayList<String>();
	String mainFunctionCode = "";
	
	public static CtoCPPFunctionMapping ccppmapping = null;
	
	private void transformIrregularCode() {
		SystemConfigFile.selectedCostFunction = CostFunction.EXECUTIONTIME;
		globalConfig = SystemConfigFile.globalHardwareConfig;
		mainFunctionCode = "\n {\n omp_set_num_threads("+ ClassificationDetails.getTotalCores(globalConfig) +");\n";
		globalCode.add("#pragma " + SystemStringConstants.globalCodeEnd + "\n");
		
		
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			ccppmapping = new CtoCPPFunctionMapping();
			CPPFunctionGetter mapper  = new CPPFunctionGetter();
			mapper.setCPPMap(ccppmapping);
			Main.root.accept(mapper);
		}
		
		IrregularityType globalIrregularity = IrregularityType.None;
		
		HashMap<OMPParallelWorkload, IrregularityType> nodeList =  new HashMap<OMPParallelWorkload, IrregularityType>();
		String edgeWorkloadString = "";
		ArrayList<String> edgeWorkloadStringArray = new ArrayList<String>();
		for (OMPParallelWorkload cW : parRegions) {
			// Fix thread to core mapping
			cW.getClassificationDetails().fixthreadtoCore();
			cW.transformIrregularCode(cW.getClassificationDetails());
			globalCode.addAll(cW.getClassificationDetails().getGlobalCode());
			
			IrregularityType localIR  = cW.getClassificationDetails().getIrregularityType();
			
			assert(localIR != IrregularityType.MultipleEdgeREdge && localIR != IrregularityType.MultipleUndefined);
			
			Main.addlog(cW.getNode().getInfo().getString());
			
			if(localIR != IrregularityType.None) {
				edgeWorkloadStringArray = cW.getClassificationDetails().getIrregularityString(globalIrregularity, edgeWorkloadString);
				if(globalIrregularity != IrregularityType.None && globalIrregularity != localIR) {
					if((globalIrregularity == IrregularityType.Edge || globalIrregularity == IrregularityType.REdge) && 
							(localIR == IrregularityType.Edge || localIR == IrregularityType.REdge)) {
						globalIrregularity = IrregularityType.MultipleEdgeREdge;
					} else if(globalIrregularity == IrregularityType.Edge && localIR == IrregularityType.Node) {
						globalIrregularity  = IrregularityType.MultipleEdgeNode;
						
					}else if(globalIrregularity == IrregularityType.MultipleEdgeNode && localIR == IrregularityType.Node){
						globalIrregularity = IrregularityType.MultipleEdgeREdgeNode;
					}	
				} else {
					globalIrregularity = localIR;
				
				}
				nodeList.put(cW, localIR);	
				
			}
		}
		
		Main.addlog("The irregularity type is  "+ globalIrregularity);
		Main.addlog("The Number of irregular loops is "+ nodeList.size());
		
		if(nodeList.size() > 1) {
			Main.addErrorMessage("Multiple parallel regions to optimize\n");
		}
		
		mainFunctionCode += SystemStringConstants.ratio + "  = "+ClassificationDetails.getIrrRatio()+"; \n";
		mainFunctionCode +=  SystemStringConstants.baseSize + " ="+ ClassificationDetails.getIrrChunkSize() + ";\naffinityinitialize(); \n";
		
		if(globalIrregularity ==  IrregularityType.Edge || globalIrregularity == IrregularityType.REdge
			|| globalIrregularity == IrregularityType.MultipleEdgeNode || globalIrregularity ==  IrregularityType.MultipleEdgeREdge) {
			mainFunctionCode += SystemStringConstants.callinitIrregular ;
		} else if(globalIrregularity !=  IrregularityType.None) {
		
			Main.addErrorMessage("The call to the worklist division is not appended to the main code.");
		}
		mainFunctionCode += "\n}";
		
		addInitializationCode();
		
		NodeList nl = Main.root.f0;
		
		
		int globalPos = Main.root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(Main.root));
	
		if( globalIrregularity  != IrregularityType.None && globalIrregularity != IrregularityType.Node 
			&& globalIrregularity != IrregularityType.MultipleUndefined) {
			globalCode.add(SystemStringFunctions.generateIrregularityCode(globalIrregularity, edgeWorkloadStringArray));
		} else if(globalIrregularity == IrregularityType.None) {
			// DO Nothing.
		} else {
			Main.addErrorMessage("The irregularity is not handled " + globalIrregularity);
		}
		
		/*
		 * TODO: We are currently removing the rest option. We can 
		 * add this back later. Currently it was not serving any purpose.		 
		if(globalIrregularity != IrregularityType.None) {
			globalCode.add(SystemStringConstants.irregularReset);
		} 
		*/
		if(globalIrregularity == IrregularityType.MultipleEdgeREdge) {
			globalCode.add(SystemStringConstants.IRR_EDGES_INIT);
			globalCode.add(SystemStringConstants.IRR_REDGES_INIT);
		}
		
		globalCode.add(SystemStringConstants.baseSizeInitial);
		globalCode.add(SystemStringConstants.ratioInitial);
		globalCode.add(SystemStringConstants.schedBlockInitial);
		globalCode.add(SystemStringConstants.bufferBlockInitial);
		globalCode.add(SystemStringConstants.bufferSizeInitial);
		globalCode.add(SystemStringConstants.listSizeInitial);
		globalCode.add(SystemStringConstants.startSchedInitial);
		globalCode.add(SystemStringConstants.irregularBlock);
		globalCode.add(SystemStringConstants.atomicHeader);
		globalCode.add(SystemStringConstants.BLOCKWLLIMIT_DEFINITION);
		globalCode.add(SystemStringConstants.GRAINFACTOR_DEFINITION);
		globalCode.add(SystemStringConstants.BIGCORE_DEFINE);
		globalCode.add(SystemStringConstants.LITCORE_DEFINE);
		
		globalCode.add("#pragma " + SystemStringConstants.globalCodeStart + "\n");

		
		for(String gCode :globalCode) {
			Main.addlog(gCode);
			ElementsOfTranslation tu = (ElementsOfTranslation) CParser.createCrudeASTNode(gCode, ElementsOfTranslation.class);
			nl.nodes.add(globalPos, tu);
		}
		addtoOutputCode();
	}
	
	
	private void tranformCode() {
		int globalPos = Main.root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(Main.root));
		for (OMPParallelWorkload cW : parRegions) {
			if(SystemConfigFile.optimizationLevel == OptimizationLevel.HardwareConfigOnly) {
				cW.getClassificationDetails().setNoOptimization();
			} else if(SystemConfigFile.optimizationLevel == OptimizationLevel.DynamicScheduling) {
				cW.getClassificationDetails().selectFreeExecution();
			} else {
				cW.getClassificationDetails().fixthreadtoCore();
			}
			cW.transformCode(cW.getClassificationDetails());
			globalCode.addAll(cW.getClassificationDetails().getGlobalCode());
		}
		NodeList nl = Main.root.f0;
		for(String gCode :globalCode) {
			
			ElementsOfTranslation tu = (ElementsOfTranslation) CParser.createCrudeASTNode(gCode, ElementsOfTranslation.class);
			nl.nodes.add(globalPos, tu);
		}
		
		if(SystemConfigFile.optimizationLevel == OptimizationLevel.CustomScheduling) {
			
			addtoOutputCode();
		}
		//mainFunctionCode		
		assert(!mainFunctionCode.isEmpty());
		addInitializationCode();
		Main.addlog("End of Story");
		
	}
	
	void addInitializationCode() {
		CompoundStatementElement cps = (CompoundStatementElement) CParser.createCrudeASTNode(mainFunctionCode, CompoundStatementElement.class);
		
		FunctionDefinition fd = ompFunctionWorkload.function();
		int pos = 0;
		NodeListOptional no = fd.f3.f1;
		for( Node ce : no.nodes) {
			CompoundStatementElement cel = (CompoundStatementElement) ce;
			if(cel.f0.choice.getClass() == Statement.class) {
				Statement s = (Statement) cel.f0.choice;
				if(s.f0.choice.getClass() == UnknownPragma.class)
					pos++;
				else		
					break;
			} else if(cel.f0.choice.getClass() == Declaration.class) {
				pos++;
			} else {
				Main.addErrorMessage("Unexpected class " + cel.f0.getClass().getName());
			}
		}
		
		
		if(pos == no.nodes.size())
			pos = 0;
		no.nodes.add(pos, cps);
		

		CompoundStatementElement cleanup = (CompoundStatementElement) CParser.createCrudeASTNode(SystemStringConstants.cleanup, 
											CompoundStatementElement.class);
		CompoundStatementElement cleanupschedule = (CompoundStatementElement) CParser.createCrudeASTNode(
				SystemStringConstants.cleanupschedule, CompoundStatementElement.class);
		pos = no.nodes.size();
		
		pos -= 1; // for return code;
		no.nodes.add(pos, cleanup);
		no.nodes.add(pos, cleanupschedule);
	}


	private void addtoOutputCode() {
		NodeList nl = Main.root.f0;
		SystemStringFunctions.addHelperFunctions();
		int globalPos = Main.root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(Main.root));
		/*
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			Node globalCodeEnd  = 
					CParser.createCrudeASTNode("# pragma " + SystemStringConstants.globalCodeEnd + " done here\n",
			 		ElementsOfTranslation.class);
			nl.addNodeatPosition(globalPos, globalCodeEnd);
		}
		*/
		Node affinityn = CParser.createCrudeASTNode(SystemStringConstants.affinity,
				ElementsOfTranslation.class);
		nl.addNodeatPosition(globalPos,affinityn);
		nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode(" cpu_set_t cpuset[NO_OF_THREADS];", 
			ElementsOfTranslation.class));
		nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode(" cpu_set_t allcpuset;", 
			ElementsOfTranslation.class));
		nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode(ClassificationDetails.getSelectedCores(globalConfig), 
				ElementsOfTranslation.class));
		
		
		String nThreadString = "#define NO_OF_THREADS  " + ClassificationDetails.getTotalCores(globalConfig) + "";
		
		/*
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			Node globalCodeStart  = 
			CParser.createCrudeASTNode("#pragma " + SystemStringConstants.globalCodeStart + "  done here\n",
			 		ElementsOfTranslation.class);
			nl.addNodeatPosition(globalPos, globalCodeStart);
		}
		*/
		
		/**
		 * Add headers for scheduling
		 * 
		 */
		
		if(SystemConfigFile.project != JKPROJECT.GRAPH) {
			String gnuSource = "#define _GNU_SOURCE";
			Main.outputWriter.println(gnuSource);
		}
		String syscalls = "#include <syscall.h>";
		String schduleh = "#include <sched.h>";
		String cescustom = "#define _CES_CUSTOM";
		
		Main.outputWriter.println(syscalls);
		Main.outputWriter.println(schduleh);
		Main.outputWriter.println(nThreadString);
		Main.outputWriter.println(cescustom);
		
		
	}
	
	

}
