/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import java.util.ArrayList;
import imop.ast.node.Node;
import ces.SystemConfigFile;
import ces.data.base.VariableRange;
import ces.enums.JKPROJECT;
import ces.getter.CollectGraphVariableRanges;
import ces.getter.GetMaxDimension;
import imop.lib.analysis.dataflow.Symbol;

public class GraphClassifier {
	
	Node __root;
	ArrayList<Node> varList;
	public GraphClassifier(Node r) {
		__root = r;
		varList = new ArrayList<Node>();
		GetMaxDimension maxDim = new GetMaxDimension();
		__root.accept(maxDim);
		ClassificationDetails.maxSize = maxDim.getMaxSize();
		
		
		if(ClassificationDetails.maxSize == 0) {
			if(ClassificationDetails.maxSize == 0) {
				ClassificationDetails.maxSize = SystemConfigFile.defaultDimSize;
			}
		}
		
		
		
	}
	
	
	public void addVariableOfInterest(Node n) {
		varList.add(n);
	}
	
	
	
	public static ArrayList<VariableRange> variableRanges = new ArrayList<VariableRange>();
	
	
	public double getVariableValueofVariable(Symbol s) {
		for(VariableRange v : variableRanges ) {
			if(v.variableFound(s.getName())) {
				return v.getMaxValue();
			} 
		}
		/* TODO variable list not working properly. */
		
		addVariableOfInterest(s.getDeclaringNode());
		updateVariableList();
		for(VariableRange v : variableRanges ) {
			if(v.variableFound(s.getName())) {
				return v.getMaxValue();
			} 
		}
		// Should not reach here by any chance.
		return ClassificationDetails.maxSize;
	}
	
	public ArrayList<VariableRange>  updateVariableList() {
		
		
		varList = cleanUpVariableList(varList);
		
		CollectGraphVariableRanges collectVariableRanges =  new CollectGraphVariableRanges(varList, null);
		__root.accept(collectVariableRanges);
		ArrayList<VariableRange> varRanges = collectVariableRanges.getVarList();
		ArrayList<Node> unallocatedList = collectVariableRanges.getUnallocatedExprList();
		if(varRanges.isEmpty()) {
			VariableRange vr = new VariableRange(SystemConfigFile.defaultDimSize);
			varRanges.add(vr);
		}
		while(!unallocatedList.isEmpty()) {
			
			Node exp = unallocatedList.remove(0);
			VariableRange vr = getRangeToAdd(varRanges);
			vr.addVariable(exp.getInfo().getString());
			collectVariableRanges = new CollectGraphVariableRanges(unallocatedList, varRanges);
			__root.accept(collectVariableRanges);
			varRanges = collectVariableRanges.getVarList();
			unallocatedList = collectVariableRanges.getUnallocatedExprList();
		}
		variableRanges = varRanges;
		return varRanges;
	}
	
	
	private ArrayList<Node> cleanUpVariableList(ArrayList<Node> varList) {
		// Remove duplicates and constants
		ArrayList<Node> condensedVarList =  new ArrayList<Node>();
		for(Node e : varList) {
			boolean flag = false;
			try {
				Double.parseDouble(e.getInfo().getString());
				flag = true;
			} catch (Exception execption){
				// Do Nothing
			}
			
			for(Node newExp: condensedVarList) {
				if(newExp.getInfo().getString().equals(e.getInfo().getString())) {
					flag = true;
					break;
				}
			}
			
			if(!flag) {
				condensedVarList.add(e);
			}
			
		}
		return condensedVarList;
	}

	

	private VariableRange getRangeToAdd(ArrayList<VariableRange> vList) {
		/**
		 * TODO We are adding the unknown variable to the range 
		 * of SystemConfigFile.defaultDimSize.
		 * We can also add them to current largest VariableRange.
		 */
		

		for(VariableRange v : vList) {
			if(v.getMaxValue() >= SystemConfigFile.defaultDimSize)
				return v;
			
		}
		/**
		 * All created Variable ranges are smaller than defaultSize
		 * 
		 */
		VariableRange newRange = new VariableRange(SystemConfigFile.defaultDimSize);
		vList.add(newRange);
		return newRange;
	}

}
