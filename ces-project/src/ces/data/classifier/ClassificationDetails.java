/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import java.util.ArrayList;
import java.util.Random;

import imop.Main;
import ces.SystemConfigFile;
import ces.data.base.Counter;
import ces.data.base.IrregularityData;
import ces.data.constructData.*;
import ces.enums.*;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;
/**
 * 
 * Classification Details of each parallel region
 * 
 * @author jkrishnavs
 *
 */
public class ClassificationDetails {
	private HardwareConfig preffered;
	private HardwareConfig selected;
//	private CostFunction selectedCostFunction;
	
	
	private static HardwareOrientation orientation;
	
	
	
	static {
		orientation = HardwareOrientation.normal;
		if(SystemConfigFile.project == JKPROJECT.GRAPH)
			orientation = HardwareOrientation.reverse;
	}

	static boolean onceFixed = false;
//	static boolean chunkSizeFixed = false;
	
	
	/* IRREGULAR PROJECT*/
	

	static double irrRatio = 0;
	static int irrChunkSize = 128;
	
	
	
	
	public void setBigLittleRatio(IrregularityData wl) {
		AccumulatedData data = wl.getAccumulatedData();
		double ratio = ClassifyWorkload.cpe.getBigLittleRatio(data, this);
		
		if(! Double.isNaN(ratio) && ratio < 100) {
			if(ratio > irrRatio) {
				irrRatio = ratio;
			}
		}
	}
	
	public static void setIrrChunkSize(int value) {
		if(value > irrChunkSize) {
			irrChunkSize = value;
		}
	}
	
	public static int getIrrChunkSize() {
		return irrChunkSize;
	}
	
	public static double getIrrRatio() {
		if(irrRatio == 0)
			return SystemConfigFile.globalbigr;
		return irrRatio;
	}
	
	ThreadScheduling schedule;

	public void setMaxSize(double maxSize) {
		this.maxSize = maxSize;
	}
	
	/*
	public void setBaseSize(int chunk) {
		if(baseSize < chunk )
			baseSize = chunk;
		chunkSizeFixed = true;
	}
	 */
	
	

	public ClassificationDetails() {
		preffered = HardwareConfig.Unknown;
		selected = HardwareConfig.Unknown;
		schedule = ThreadScheduling.none;
		parallelRegionCode = "";
		globalCode = new ArrayList<String>();
		loopboundaries = new ArrayList<String>();
		

		
	//	mainFunctionCode = new ArrayList<String>();
	}
	
	public ThreadScheduling getSchedulingDetails() {
		return schedule;
	}
	
	
	
	public void setNoOptimization(){
		schedule = ThreadScheduling.NoOptimization;
	}
	
	
	
	public void fixthreadtoCore() {
		assert(schedule == ThreadScheduling.none || schedule == ThreadScheduling.ThreadtoCoreFix);
		schedule = ThreadScheduling.ThreadtoCoreFix;
	}
	
	public void selectFreeExecution(){
		assert(schedule == ThreadScheduling.none || schedule == ThreadScheduling.Freeexecution);
		schedule = ThreadScheduling.Freeexecution;
	}
	
	public int getPrefferedCore(CoreType c){
		if(c == CoreType.big && getMaxBigCoreID(selected) != -1) {
			int i = generator.nextInt(getMaxBigCoreID(selected) - getMinBigCoreID(selected));
			return i+getMinBigCoreID(selected);
		} 
		
		if(c == CoreType.LITTLE && getMinBigCoreID(selected) != -1) {
			return generator.nextInt(getMaxLittleCoreID(selected) - getMinLittleCoreID(selected)) 
					+ getMinLittleCoreID(selected);
		}
		return -1;
	}

	
	Random generator = new Random();
	
	
	public static CoreType getCoreType(int coreid, HardwareConfig config) {
		switch (config) {
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			assert(coreid < 4);
			return CoreType.big;
		default:
			if(coreid >= numberofAllCores(config))
				Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
			if(coreid >= numberofAllCores(config)) {
				Main.addlog(SystemStringConstants.genericErrorMessage);
			}
			assert(coreid < numberofAllCores(config));
			if(coreid<4)
				return CoreType.LITTLE;
			else
				return CoreType.big;
		}
	}
	
	
	public static int numberofAllCores(HardwareConfig config) {
		switch (config) {
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
		case n4b0l4b2000000:
			return 4;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return 6;
		case n8b4l4b1200000:
		case n8b4l4b1400000:
		case n8b4l4b1600000:
		case n8b4l4b1800000:
		case n8b4l4b2000000:
			return 8;
		default:
			Main.addErrorMessage(SystemStringConstants.unknownConfig);
		}
		return 0;
	} 
	
	public static int numberofLITTLECores(HardwareConfig config){
		switch (config) {
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return 0;
		default:
			return 4;
		}
	}

	
	public static int numberofBigCores(HardwareConfig config){
		switch (config) {
		case n4b0l4b2000000:
			return 0;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return 2;
		default:
			return 4;
		}
	}
	
	/*
	 * Should return -1 if the core type is not available. 
	 * */
	public static int getMaxBigCoreID(HardwareConfig config) {
		switch (config) {
		case n4b0l4b2000000:
			return -1;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return 5;
		default:
			return 7;
		}
	}
	
	
	
	/*
	 * Should return -1 if the core type is not available. 
	 * */
	public static int getCoreIDFlag(HardwareConfig config) {
		switch (config) {
		case n4b0l4b2000000:
			return 15;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return 63;
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return 240;
		case n8b4l4b1200000:
		case n8b4l4b1400000:
		case n8b4l4b1600000:
		case n8b4l4b1800000:
		case n8b4l4b2000000:
			return 255;
		default:
			Main.addErrorMessage(SystemStringConstants.unknownConfig);
		}
		return -1;
	}
	
	
	public static int getMinBigCoreID(HardwareConfig config) {
		switch (config) {
		case n4b0l4b2000000:
			return -1;
		default:
			return 4;
		}
	}
	
	public static int getMinLittleCoreID(HardwareConfig config) {
		switch (config) {
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return -1;

		default:
			return 0;
		}
		
	}
	
	public int getMaxLittleCoreID(HardwareConfig config) {
		switch (selected) {
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return -1;
		default:
			return 3;	
		}
	}
	
	
	
	
	
	public int scheduleSingle(OMPSingleWorkload s) {
		s.getAccumulatedData().evaluate();
		return getPrefferedCore(ClassifyWorkload.cpe.getPreferredCoreType(selected, s.getAccumulatedData()));
	}
	
	
	public int scheduleMaster(OMPMasterWorkload s) {
		s.getAccumulatedData().evaluate();
		return getPrefferedCore(ClassifyWorkload.cpe.getPreferredCoreType(selected, s.getAccumulatedData()));	
	}
	
	public int getNoOfBigCores() {
		return getMaxBigCoreID(selected) - getMinBigCoreID(selected);
	}
	
	public int getNoOfLittleCores() {
		return getMaxLittleCoreID(selected) - getMinLittleCoreID(selected);
	}
	
	public double getRatioofbigLittleScheduling(OMPForWorkload f) {
		if(getMinBigCoreID(selected) != -1 && getMaxLittleCoreID(selected) != -1)
			return 2;
		else return -1;
	}
	
	/*
	public int scheduleMasterNodes() {
		
		if(getMinBigCoreID() != -1) {
			int i = generator.nextInt(getMaxBigCoreID() - getMinBigCoreID());
			return i+getMinBigCoreID();
		}
		// -1 means don't touch the code.
		return -1;
	}
	
	
	*/
	public HardwareConfig getPreffered() {
		return preffered;
	}
	public void setPreffered(HardwareConfig preffered) {
		this.preffered = preffered;
	}
	public HardwareConfig getSelected() {
		return selected;
	}
	public void setSelected(HardwareConfig selected) {
		this.selected = selected;
	}
	
	
	private String parallelRegionCode;
	private ArrayList<String> globalCode;

	ArrayList<String> loopboundaries;
	
//	ArrayList<String> mainFunctionCode;
	
	
	public ArrayList<String> getGlobalCode() {
		globalCode.addAll(loopboundaries);
		return globalCode;
	}
	
	public String debugCode(){
		String retVal = CesSpecificMisc.getNewVariableName();
		String str = "int "+ retVal + " = 0;";
		globalCode.add(str);
		return retVal;
	}
	
	/*
	public ArrayList<String> getMainFunctionCode() {
		
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			mainFunctionCode.add("\n" + SystemStringConstants.baseSize + " = " + baseSize + ";");
		}
		return mainFunctionCode;
	}
	*/
	/**
	 * 
	 * 
	 * @param wl
	 * @param itrValue
	 * @param decrement
	 * @return Array name
	 */
	public String setLoopBoundaries(OMPForWorkload wl, double itrValue, boolean decrement) {
		double ratio = ClassifyWorkload.cpe.getBigLittleRatio(wl.getAccumulatedData(), this);
		double factor = (ratio * ClassificationDetails.numberofBigCores(selected) ) + 
					ClassificationDetails.numberofLITTLECores(selected);
		int tot = ClassificationDetails.numberofAllCores(selected);
		
		
		/***
		 * If decrement we will set from big to little (N - 0)
		 * else we will set from little to big (0-N)
		 */
		
		String arrayStr = "";
		String arrName = CesSpecificMisc.getNewVariableName();
		
		int start, end; 
		int chunkSize;
		int coreid;

		if(decrement)
			start = (int) itrValue;
		else
			start = 0;
		arrayStr = "{" + start;
		
		for(int i =0; i< tot-1; i++ ) {
			if(orientation == HardwareOrientation.reverse)
				coreid = tot-(i+1);
			else
				coreid = i;
			if(getCoreType(coreid, selected) ==CoreType.big)
					chunkSize = (int) (factor * ratio);
			else
				chunkSize = (int) factor;
			if(decrement)
				end = start - chunkSize; 
			else
				end = start + chunkSize;
			arrayStr += ", " + end;
			start = end;
		}
		
		if(decrement)
			arrayStr += ", " + 0;
		else
			arrayStr += ", " + (int)itrValue;
		
			
		arrayStr += " }";
		globalCode.add(new String("\n int " + arrName +  "["+ (getTotalCores(selected)+1) +"] = " + arrayStr + ";"));
		return arrName;
	}
	
	public String setLoopBoundaries(OMPForWorkload wl, String totalIterations, boolean decrement) {
		double ratio = ClassifyWorkload.cpe.getBigLittleRatio(wl.getAccumulatedData(), this);
		
		if(Double.isNaN(ratio) || ratio > 100) {
			Main.addErrorMessage(SystemStringConstants.RatioNanErrorMessage);
//			ClassifyWorkload.cpe.getBigLittleRatio(wl.getAccumulatedData(), this);
			return equalDistribution();
		}
				
		double totalSet = (ratio * ClassificationDetails.numberofBigCores(selected) ) + 
					ClassificationDetails.numberofLITTLECores(selected);
		
		if(totalSet == 0)
			Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
		
		int tot = ClassificationDetails.numberofAllCores(selected);
		double factor = 1/(totalSet);
		
		/***
		 * If decrement we will set from big to little (N - 0)
		 * else we will set from little to big (0-N)
		 */
		
		String arrayStr = "";
		String arrName = CesSpecificMisc.getNewVariableName();
		
		double start, end; 
		double chunkSize;
		int coreid;

		if(decrement)
			start = 1;
		else
			start = 0;
		arrayStr = "{" + start;
		
		for(int i =0; i< tot-1; i++ ) {
			if(orientation == HardwareOrientation.reverse)
				coreid = tot-(i+1);
			else
				coreid = i;
			if(getCoreType(coreid, selected) ==CoreType.big)
					chunkSize =(factor * ratio);
			else
				chunkSize =  factor;
			if(decrement)
				end = start - chunkSize; 
			else
				end = start + chunkSize;
			if(end>=0 && end <= 1) {
				
			} else {
				Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
			}
			
			
			assert(end >= 0 && end <= 1);
			arrayStr += ", " + end;
			start = end;
		}
		
		if(decrement)
			arrayStr += ", " + 0;
		else
			arrayStr += ", " + 1;
		
			
		arrayStr += " }";
		globalCode.add(new String("\n double " + arrName +  "["+ (getTotalCores(selected)+1) +"] = " + arrayStr + ";"));
		return arrName;
	
	}
	
	private String equalDistribution() {
		int tot = ClassificationDetails.numberofAllCores(selected);
		String arrName = CesSpecificMisc.getNewVariableName();
		String arrayStr = "{ 0";
		double ratio = 1.0/tot;
		double start = ratio;
		for(int i =0; i< tot-1; i++ ) {
			arrayStr += "," + start; 
			start += ratio; 
		}
		arrayStr += "," +1;
		arrayStr += " }";
		globalCode.add(new String("\n double " + arrName +  "["+ (getTotalCores(selected)+1) +"] = " + arrayStr + ";"));
		
		return arrName;
	}


	ArrayList<String> unknownVariables;
	/**
	 *  The max size of all variables in the program.
	 * 
	 */
	public static double maxSize = 0;
	

	
	
/*
	public boolean transformForConstruct(String iterString, double ratio) {
		if(ratio == 0  || iterString.equals(SystemConfigFile.INFINITY))
			return false;
		else
			return true;
	}
	
	*/
	
	//double biglittleratio=0;
	
//	String ratio = "";
	
//	public void setbiglittleRatio(double d) {
//		biglittleratio = d;
//	}
	
	
	public int getMaxChunkSize(String totalIterations,OMPForWorkload f) {
		Counter iter = new Counter(totalIterations);
		int iterSize = (int) iter.evaluate();
		int cores = ClassificationDetails.getTotalCores(selected);
		int falseSharingMinStride = f.getFalseSharingMaximumStride();
		int maxBlockSize = iterSize / cores;
		if(falseSharingMinStride == 0)
			return 1;
		else {
			int itrClub = SystemConfigFile.cacheLineSize / falseSharingMinStride;
			if(itrClub < maxBlockSize)
				return itrClub;
			else 
				return 1;
		}
	}
	
	
	@SuppressWarnings("unused")
	public ArrayList<String> getDynamicChunkSize(OMPForWorkload s) {
		ArrayList<String> retValues = new ArrayList<String>();
		double ratio = ClassifyWorkload.cpe.getBigLittleRatio(s.getAccumulatedData(), this);
		if(ratio == 0 || SystemConfigFile.dynamicChunkSize == false) {
			retValues.add(", " + getMaxChunkSize(s.getTotalIterations().getString(), s));
		} else {
			int tot = (int) (ratio* SystemConfigFile.dynamicChunkAccuracy + 
					SystemConfigFile.dynamicChunkAccuracy);	
			String varName = "__ces" + CesSpecificMisc.getToken();
			String initialization  = "\n int " + varName + " = " + s.getTotalIterations() + " / " + tot + ";\n";
			retValues.add("," + varName);
			retValues.add(initialization);
		}
		return retValues;
	}
	
	
	public String getParallelRegionCode() {
		if(!onceFixed) {
			if(schedule == ThreadScheduling.ThreadtoCoreFix) {
				parallelRegionCode += "\n int " +  SystemStringConstants.threadid + " = omp_get_thread_num();";
				parallelRegionCode += " int __t = sched_setaffinity(syscall(SYS_gettid), sizeof(cpuset["+ 
						SystemStringConstants.threadid + "]), &cpuset["+ SystemStringConstants.threadid +"]);\n" +
						"if(__t == -1) printf(\"Error in setting affinity\"); \n" ;
			}
			if(!parallelRegionCode.isEmpty()) {
				parallelRegionCode =  " { " + parallelRegionCode + " } ";
				onceFixed = true;
			}
			return parallelRegionCode;
		} else {
			return "";
		}
	}

	public boolean isSMP(HardwareConfig m) {
		switch (m) {
		case Unknown:
		case n4b0l4b2000000:
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return true;
		default:	
			return false;
		}
		
	}





	public static int getTotalCores(HardwareConfig globalConfig) {
		switch(globalConfig) {
		case n4b2l2b1200000: 
		case n4b2l2b1400000:
		case n4b2l2b1600000: 
		case n4b2l2b1800000: 
		case n4b2l2b2000000:
		case n4b0l4b2000000:	
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return 4;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return 6;
		case n8b4l4b1200000:
		case n8b4l4b1400000:
		case n8b4l4b1600000:
		case n8b4l4b1800000:
		case n8b4l4b2000000:
			return 8;
		default:
			Main.addErrorMessage(SystemStringConstants.unknownConfig + globalConfig.name());
		
		}
		return 0;
	}


	public static String getSelectedCores(HardwareConfig globalConfig) {
		switch(globalConfig) {
		case n4b0l4b2000000:	
			return SystemStringConstants.scheduleLittleOnly;
		case n4b4l0b1200000:
		case n4b4l0b1400000:
		case n4b4l0b1600000:
		case n4b4l0b1800000:
		case n4b4l0b2000000:
			return SystemStringConstants.scheduleBigOnly;
		case n6b2l4b1200000:
		case n6b2l4b1400000:
		case n6b2l4b1600000:
		case n6b2l4b1800000:
		case n6b2l4b2000000:
			return SystemStringConstants.schedule4L2b;
		case n8b4l4b1200000:
		case n8b4l4b1400000:
		case n8b4l4b1600000:
		case n8b4l4b1800000:
		case n8b4l4b2000000:
			if(orientation == HardwareOrientation.reverse)
				return SystemStringConstants.scheduleAllReverse;
			else
				return SystemStringConstants.scheduleAll;
		default:
			Main.addErrorMessage(SystemStringConstants.unknownConfig + globalConfig.name());
		
		}
		if(orientation == HardwareOrientation.reverse)
			return SystemStringConstants.scheduleAllReverse;
		else
			return SystemStringConstants.scheduleAll;
	}



	public static String getBigFreqency(HardwareConfig globalConfig) {
		switch(globalConfig) {
		case Unknown:
			break;
		case n4b0l4b2000000:
			return SystemStringConstants.bf0;
		case n4b4l0b2000000:
		case n8b4l4b2000000:
		case n6b2l4b2000000:
			return SystemStringConstants.bf2000;
		case n4b4l0b1200000:
		case n6b2l4b1200000:
		case n8b4l4b1200000:
			return SystemStringConstants.bf1200;
		case n4b4l0b1400000:
		case n6b2l4b1400000:
		case n8b4l4b1400000:
			return SystemStringConstants.bf1400;
		case n4b4l0b1600000:
		case n6b2l4b1600000:
		case n8b4l4b1600000:
			return SystemStringConstants.bf1600;
		case n4b4l0b1800000:
		case n6b2l4b1800000:
		case n8b4l4b1800000:
			return SystemStringConstants.bf1800;
		default:
			Main.addErrorMessage(" big Frequency asked for" + SystemStringConstants.unknownConfig);
		
		}
		

		return SystemStringConstants.bf0;	
	}

	public double getMaxSize() {
		return ClassificationDetails.maxSize;
	}
	
	IrregularityType irType = IrregularityType.None;
	String irrString;
	String workloadString;
	String iterationLimit;
	String irrStatement;
	
	public void setIrregularityDetails(IrregularityType it, String isS, String workload, String itrLimit, String irrStmt) {
		this.irType = it;
		this.irrString = isS;
		this.workloadString = workload;
		this.iterationLimit = itrLimit;
		this.irrStatement = irrStmt;
	} 

	public IrregularityType getIrregularityType() {
		return irType;
	}
	
	public ArrayList<String> getIrregularityString(IrregularityType globalIrregularity, String edgeWorkloadString) {
		
		ArrayList<String> retVal = new ArrayList<>();
		if(globalIrregularity == irType && edgeWorkloadString.equals(irrString)) {
			retVal.add(irrString);
			retVal.add(workloadString);
			retVal.add(iterationLimit);
			retVal.add(irrStatement);
			return retVal;
		}
		
		if(! edgeWorkloadString.isEmpty() && ! irrString.isEmpty() ) {
			Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
			// TODO : For time being we will hard code it.
			String retString =  " if(flag == "+ SystemStringConstants.IRR_EDGES+") "
					+ " sum += G . begin [ " + SystemStringConstants.mainIteratorString  
					+ " + 1 ]  - G . begin [ " + SystemStringConstants.mainIteratorString  + "];\n"
					+ " else "
					+ " sum += G.r_begin [ " + SystemStringConstants.mainIteratorString  
					+ " + 1 ]  - G.r_begin [ " + SystemStringConstants.mainIteratorString  + " ];\n";
			
			
			assert(!retString.isEmpty());
			retVal.add(retString);
			retVal.add("G.num_edges()");
			retVal.add("G.num_nodes()");
			return retVal;
		} else  if (edgeWorkloadString.isEmpty()){
			retVal.add(irrString);
			retVal.add(workloadString);
			retVal.add(iterationLimit);
			retVal.add(irrStatement);
			return retVal;
		} else
			return retVal;
		
	}
}
