/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import java.util.ArrayList;
import java.util.HashMap;

import imop.Main;
import ces.SystemConfigFile;
import ces.data.base.VariableRange;
import ces.data.classifier.Input.FittingModel;
import ces.data.classifier.Input.GaussianFit;
import ces.data.classifier.Input.GaussianFitWindow;
import ces.data.classifier.Input.LinearFit;
import ces.data.classifier.Input.LinearFitWindow;
import ces.data.classifier.Input.QuadFit;
import ces.data.classifier.Input.QuadFitWindow;
import ces.data.constructData.AccumulatedData;
import ces.enums.CoreType;
import ces.enums.EigenMetric;
import ces.enums.HardwareConfig;
import ces.enums.RegressionFunctionChoice;
import ces.util.SystemStringConstants;

public class Classifier {
	public Classifier() {
		 mem = new CostMap();
		barrier = new CostMap();
		critical = new CostMap();
		reduction =  new CostMap();
		flush = new CostMap();
		branches =  new CostMap();
		
		memSpeedup =  new CostMap();
		barrierSpeedup  =  new CostMap();
		criticalSpeedup  =  new CostMap();
		reductionSpeedup  =  new CostMap();
		flushSpeedup  =  new CostMap();
		branchesSpeedUp  =  new CostMap();

	}
	
	

	public static final double memweight = 1;
	public static final double barrierweight = 1;
	public static final double branchweight = 1;
	public static final double criticalweight = 1;
	public static final double reductionweight = 1;
	public static final double flushweight = 1;
	public static final double singleweight = 1;
	public static final double falseshareweight = 1;
	
	
	/**
	 * Barriers are trained at a very low x  scale.
	 * if we save the same x then regression function will find it difficult.
	 * So we scale it by a factor and store.
	 */
	public static final double barrierScale = 1000000;
	public static final double criticalScale = 1000000;
	public static final double memScale = 0.25;
	
	public static ArrayList<VariableRange> variableRanges = new ArrayList<VariableRange>();
	
	
	public void setVariableRanges(ArrayList<VariableRange> data) {
		variableRanges = data;
	}
	
	
	public static double getHolderValueofVariable(String var, boolean sign) {
		for(VariableRange v : variableRanges) {
			if(v.variableFound(var))
				return v.getVariableValue(sign, var);
		}
		
		if(var.equals(SystemStringConstants.numThreadsString))
			return SystemConfigFile.no_of_cores;
		return ClassificationDetails.maxSize;
	}
	
	

	// one layer of runtime afterwards;
	

	
	@SuppressWarnings("serial")
	public class CostMap extends HashMap<HardwareConfig, RegressionFunction> {}
	
	public CostMap mem;
	public CostMap barrier;
	public CostMap critical;
	public CostMap reduction;
	public CostMap flush;
	public CostMap branches;
	
	public CostMap memSpeedup;
	public CostMap barrierSpeedup;
	public CostMap criticalSpeedup;
	public CostMap reductionSpeedup;
	public CostMap flushSpeedup;
	public CostMap branchesSpeedUp;

	
	/*
	public void setMemRegressionFunction(ArrayList<RegressionFunction> rf) {
		mem = rf;
	}
	public void setBarrierRegressionFunction(ArrayList<RegressionFunction> rf) {
		barrier = rf;
	}
	public void setCriticalRegressionFunction(ArrayList<RegressionFunction> rf) {
		critical = rf;
	}
	public void setReductionRegressionFunction(ArrayList<RegressionFunction> rf) {
		reduction = rf;
	}
	public void setflushRegressionFunction(ArrayList<RegressionFunction> rf) {
		flush = rf;
	}
	public void setBranchesRegressionFunction(ArrayList<RegressionFunction> rf) {
		branches = rf;
	}
	
	*/
	
	/**
	 * 
	 * TODO: NOT Calculated right now
	 */
	ArrayList<RegressionFunction> falseSharing;
	ArrayList<RegressionFunction> single;
	
	FittingModel model;
	public void init() {
		switch(SystemConfigFile.regressionFunctionChoice){
		case gaussian:
			model = new GaussianFit();
			model.update(this);
			break;
		case linear:
			model = new LinearFit();
			model.update(this);
			break;
		case quad:
			model = new QuadFit();
			model.update(this);
			break;
	//	case bestFit:
	//		model = new BestFitModel();
	//		model.update(this);
	//		break;
		case linearWindow:
			model = new LinearFitWindow();
			model.update(this);
			break;
		case quadWindow:
			model = new QuadFitWindow();
			model.update(this);
			break;
		case gaussianWindow:
			model = new GaussianFitWindow();
			model.update(this);
			break;
		default:
			break;		
		} 
		
	}
	
	
	// TODO use Weights.
	public double getALUCost( AccumulatedData data, EigenMetric metric) {
		return data.getALUEigenFactorValue(metric);
	} 
	
	public double getCost(HardwareConfig selected, AccumulatedData data) {
		double retVal = 0;
		double barVal = data.getEigenFactorValue(EigenMetric.ompBarriers);
		double memVal = data.getEigenFactorValue(EigenMetric.MemoryOps);
		double criticalVal = data.getEigenFactorValue(EigenMetric.ompCritical);
		double reductionVal = data.getEigenFactorValue(EigenMetric.ompReductions);
		double branchesVal = data.getEigenFactorValue(EigenMetric.branches);
		double flushVal = data.getEigenFactorValue(EigenMetric.ompFlush);
		if(SystemConfigFile.ALUFACTOR){
			barVal = getALUCost(data, EigenMetric.ompBarriers);
			memVal = getALUCost(data, EigenMetric.MemoryOps);
			criticalVal = getALUCost(data, EigenMetric.ompCritical);
			reductionVal = getALUCost(data, EigenMetric.ompReductions);
			branchesVal = getALUCost(data, EigenMetric.branches);
			flushVal  = getALUCost(data, EigenMetric.ompFlush);
			
		}
		barVal = barVal * barrierScale;
		criticalVal = criticalVal * criticalScale;
		memVal = memVal * memScale;
		RegressionFunction rfBar = barrier.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("Barrier regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfBran = branches.get(selected);
		if(rfBran == null){
			Main.addErrorMessage("Branches regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfCrit = critical.get(selected);
		if(rfCrit == null){
			Main.addErrorMessage("Critical regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfRed = reduction.get(selected);
		if(rfRed == null){
			Main.addErrorMessage("Reduction regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfMem = mem.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("Memory regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfFlush = flush.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("flush regression factor not defined for hardware config "+ selected.name());
		}
		/**
		 * We need to scale back barrier and critical to reduce its over dependence
		 * 
		 */
		double retValbase = (memweight * rfMem.getVal(0) + 
				( rfBar.getVal(0) * barrierweight/barrierScale) +
				rfBran.getVal(0) * branchweight+
				rfRed.getVal(0)  * reductionweight+
				( rfCrit.getVal(0) * criticalweight/criticalScale) +
				rfFlush.getVal(0) * flushweight); 
		
		retVal = memweight * rfMem.getVal(memVal) + 
				( rfBar.getVal(barVal) * barrierweight / barrierScale) +
				rfBran.getVal(branchesVal) * branchweight +
				rfRed.getVal(reductionVal) * reductionweight +
				( rfCrit.getVal(criticalVal) * criticalweight/criticalScale) +
				rfFlush.getVal(flushVal) * flushweight;
		retVal = retVal-(retValbase );
		
		/**
		 * The cost functions seems to be too small for plotting we are scaling it by 1000 
		 * for readability
		 */
		return retVal* 1000;
	} 
	
	

	
	public double getCostSpeedUP(HardwareConfig selected, AccumulatedData data) {
		double retVal = 0;
		double barVal = data.getEigenFactorValue(EigenMetric.ompBarriers);
		double memVal = data.getEigenFactorValue(EigenMetric.MemoryOps);
		double criticalVal = data.getEigenFactorValue(EigenMetric.ompCritical);
		double reductionVal = data.getEigenFactorValue(EigenMetric.ompReductions);
		double branchesVal = data.getEigenFactorValue(EigenMetric.branches);
		double flushVal = data.getEigenFactorValue(EigenMetric.ompFlush);
		if(SystemConfigFile.ALUFACTOR){
			barVal = getALUCost(data, EigenMetric.ompBarriers);
			memVal = getALUCost(data, EigenMetric.MemoryOps);
			criticalVal = getALUCost(data, EigenMetric.ompCritical);
			reductionVal = getALUCost(data, EigenMetric.ompReductions);
			branchesVal = getALUCost(data, EigenMetric.branches);
			flushVal  = getALUCost(data, EigenMetric.ompFlush);
			
		}
		RegressionFunction rfBar = barrierSpeedup.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("Barrier regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfBran = branchesSpeedUp.get(selected);
		if(rfBran == null){
			Main.addErrorMessage("Branches regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfCrit = criticalSpeedup.get(selected);
		if(rfCrit == null){
			Main.addErrorMessage("Critical regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfRed = reductionSpeedup.get(selected);
		if(rfRed == null){
			Main.addErrorMessage("Reduction regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfMem = memSpeedup.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("Memory regression factor not defined for hardware config "+ selected.name());
		}
		
		RegressionFunction rfFlush = flushSpeedup.get(selected);
		if(rfBar == null){
			Main.addErrorMessage("flush regression factor not defined for hardware config "+ selected.name());
		}
		
		double retValbase = (rfMem.getVal(0) + 
				rfBar.getVal(0) +
				rfBran.getVal(0) +
				rfRed.getVal(0) +
				rfCrit.getVal(0) +
				rfFlush.getVal(0)) / SystemConfigFile.TOTALFEATURES; 
		
		retVal = rfMem.getVal(memVal) + 
				rfBar.getVal(barVal) +
				rfBran.getVal(branchesVal) +
				rfRed.getVal(reductionVal) +
				rfCrit.getVal(criticalVal) +
				rfFlush.getVal(flushVal);
		retVal = retVal - (retValbase* (SystemConfigFile.TOTALFEATURES - 1) );
		return retVal;
	} 
	
	/***
	 * Classifier Hardware configurations
	 */
	
	HardwareConfig[] ClassifierValues = {HardwareConfig.n4b0l4b2000000,
			                             HardwareConfig.n4b4l0b1200000,
			                             HardwareConfig.n4b4l0b1400000,
			                             HardwareConfig.n4b4l0b1600000,
			                             HardwareConfig.n4b4l0b1800000,
			                             HardwareConfig.n4b4l0b2000000,
			                             HardwareConfig.n6b2l4b1200000,
			                             HardwareConfig.n6b2l4b1400000,
			                             HardwareConfig.n6b2l4b1600000,
			                             HardwareConfig.n6b2l4b1800000,
			                             HardwareConfig.n6b2l4b2000000,
										 HardwareConfig.n8b4l4b1200000,
										 HardwareConfig.n8b4l4b1400000,
										 HardwareConfig.n8b4l4b1600000,
										 HardwareConfig.n8b4l4b1800000};
	
	
	public HardwareConfig Classify(AccumulatedData data) {
	
		double cost =  getCost(HardwareConfig.n8b4l4b2000000, data);
		HardwareConfig selected = HardwareConfig.n8b4l4b2000000;
		// skip unknown
		for(int i=0;i< ClassifierValues.length-1;i++) {
		//	Main.addErrorMessage(String.valueOf(i) + vals[i].name());
			double curCost = getCost(ClassifierValues[i], data);
			Main.addlog(" COST FUNCTION VALUE FOR "+ ClassifierValues[i] + " is " + curCost);
			if(curCost < cost) {
				cost = curCost;
				selected = ClassifierValues[i];
			}
			
		}
		return selected;
	}

	
	
	
	/**
	 * @param accumulatedData
	 * @return
	 */
	public CoreType getPreferredCoreType(HardwareConfig selected, AccumulatedData accumulatedData) {
		double bigCost =  getCost(selected, accumulatedData);
		double littleCost =  getCost(selected, accumulatedData);
		if(bigCost <= littleCost)
			return CoreType.big;
		else
			return CoreType.none;
	}
	
	
	
	/**
	 * @param accumulatedData
	 * @param sd
	 * @return
	 */
	public double getBigLittleRatio(AccumulatedData accumulatedData, ClassificationDetails sd) {
		
		assert(accumulatedData != null);
		accumulatedData.evaluate();
		double speedup =  getCostSpeedUP(sd.getSelected(), accumulatedData);
		return speedup;
	}
	
}
