/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.classifier;

import imop.Main;
import ces.enums.RegressionFunctionChoice;

public class RegressionFunction {
	
//	public CostFunction cf;
	RegressionFunctionChoice choice;
	
	double[] coeffients;
	int degree;
	
	public RegressionFunction(double[] coeff, RegressionFunctionChoice choice, int deg) {
		this.coeffients = coeff;
		/**
		 * IMPORTATNT: degree actually store a value 1 higher than the actual degree
		 * of the polynomial to accommodate the constant.
		 * For example Linear eqn has value 2 stored as degree.
		 */
		if(deg == -1)
			degree = coeff.length;
		else
			degree = deg;
		
		
		this.choice = choice;
	//	this.cf = cf;
//		this.m = m;
	}
	
	public int getDegree() {
		return degree-1;
	}
	
	private double getValPoly(int start, double val){
		double tot = 0;
		
//		if(start != 0) {
//			Main.addlog("hello");
//		}
		
		for(int i=0;i<degree;i++) {
			tot += coeffients[i+ start] * Math.pow(val, i);
		}
		return tot;
	}
	
	
	public double getVal(double val) {
		if(coeffients.length == degree) {
			if(choice != RegressionFunctionChoice.gaussian)
				return getValPoly(0, val);
			else
				return getValGaussian(0, val);
		} else {
			int startIndex = 0;
			int windowIndex = degree;
			while(windowIndex < coeffients.length) {
				if(coeffients[windowIndex] < val) {
					startIndex = windowIndex +1;
					windowIndex += degree + 1;		
				} else {
					break;
				}
			}
			if(choice != RegressionFunctionChoice.gaussian)
				return getValPoly(startIndex, val);
			else 
				return getValGaussian(startIndex, val);
		} 
	}

	private double getValGaussian(int start, double val) {
		// mean sd y scale
		double FI = (Math.sqrt(2*Math.PI)*coeffients[1 + start]);
		double exp = Math.pow(Math.E,(-0.5*((val-coeffients[0 + start])/(coeffients[1+ start]*coeffients[1+ start]))));
		double data = coeffients[2+ start]/ (FI*exp);
		return data;
	}
	
	
	/*
	public static void tryFlanagan() {
	    // x1 data array
        double[] xArray = {0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5};
        // x2 data array
        double[] zArray = {0.0,0.13,0.26,0.39,0.52,0.65,0.78,0.91,1.04,1.17,1.3,1.43,1.56,1.69,1.82,1.95};
        // observed y data array
        double[] yArray = {12.1379,10.1886,8.9674,7.7824,7.9645,7.3671,6.8216,7.2602,7.1105,7.2873,7.9745,8.5188,9.1492,9.3891,10.4923,11.8675};
        // estimates of the standard deviations of y
        double[] sdArray = {0.25,0.22,0.27,0.23,0.21,0.26,0.28,0.24,0.25,0.23,0.27,0.22,0.23,0.26,0.28,0.24};

        Regression reg = new Regression(xArray, yArray);
        reg.polynomial(2);
       // reg.plotXY();
        reg.getBestEstimates();	
        //reg.
      //  reg.bestPolynomial();
        reg.print("false1d.txt");
	}
	*/
	
}
