/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.memoryOps;

import java.util.ArrayList;
import java.util.HashSet;

import imop.Main;
import ces.enums.Metric;
import imop.lib.cg.CallSite;

public class MemoryOperations {
	double reads;
	double writes;
	double branches;
	double sharedReads;
	double sharedWrites;
	double privateReads;
	double privateWrites;
	ArrayList<CallSite> unKnownFnCalls;
	// Counter unknownfunctionCalls;
	
	
//	ArrayList<Node> functionCalls;
	
	
	public void addFunctionCall(CallSite s) {
		unKnownFnCalls.add(s);
	}
	
	public void addFunctionCalls(HashSet<CallSite> s) {
		unKnownFnCalls.addAll(s);
	}
	
	
	
	
	public void addConstantMetric(Metric m, double val) {
		switch(m) {
			case memread:
					reads += val;
				break;
			case memwrite:
					writes += val;
				break;
			case branches:
					branches += val;
				break;
			case sharedRead:
					sharedReads += val;
					break;
			case sharedWrite:
					sharedWrites += val;
					break;
			case privateRead:
					privateReads += val;
					break;
			case privateWrite:
					privateWrites += val;
					break;
			default:
				Main.addErrorMessage("Unknown option "+ m + " for " + this);
		
		}
	}


	
	
	public double getVal(Metric m) {
		switch(m) {
		case memread:
			return reads;
		case memwrite:
			return writes;
		case branches:
			return branches;
		case sharedRead:
			return sharedReads;
		case sharedWrite:
			return sharedWrites;
		case funcCalls:
			return unKnownFnCalls.size();
		case privateRead:
			return privateReads;
		case privateWrite:
			return privateWrites;
		default:
			Main.addErrorMessage("Unknown option "+ m + " for " + this);
	
		}
		return 0;
	}
	
	public String dumpAnalysisData(){
		String retVal = "cachereads:" + reads;
		retVal += "\tcacheWrites:" + writes ;
		retVal += "\tsharedReads: " + sharedReads ;
		retVal += "\t sharedWrites: " + sharedWrites;
		retVal += "branches:" + branches;
		retVal += "FuncCalls:" + unKnownFnCalls.size();
		
		return retVal;
	}
	
	public void dumpData() {
		Main.addlog("cachereads:" + reads + "\t cacheWrites:" 
				+ writes + "\t sharedReads: " + sharedReads
				+ "\t sharedWrites: " + sharedWrites + "branches:" + branches 
				+ "FuncCalls:" + unKnownFnCalls.size());
		
	}
	
	public MemoryOperations() {
		reads = 0;
		writes = 0;
		branches =  0;
		sharedReads = 0;
		sharedWrites = 0;
		privateReads = 0;	
		privateWrites = 0;
		unKnownFnCalls = new ArrayList<CallSite>();
	}
			
	public MemoryOperations(MemoryOperations a) {
		reads = a.reads;
		writes = a.writes;
		branches = a.branches;
		sharedReads = a.sharedReads;
		sharedWrites = a.sharedWrites;
		privateReads = a.privateReads;
		privateWrites = a.privateWrites;
		unKnownFnCalls = new ArrayList<CallSite>(a.unKnownFnCalls);
	}
	
	public void addMemoryOperations(MemoryOperations s) {
		reads += s.reads;
		writes += s.writes;
		branches += s.branches;
		sharedReads += s.sharedReads;
		sharedWrites += s.sharedWrites;
		privateReads += s.privateReads;
		privateWrites += s.privateWrites;
		unKnownFnCalls.addAll(unKnownFnCalls);
	}

}
