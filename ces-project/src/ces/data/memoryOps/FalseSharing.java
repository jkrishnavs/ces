/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.memoryOps;

import java.util.ArrayList;

import ces.data.base.Counter;
import ces.data.node.ArrayExpression;

public class FalseSharing {
	public class ArrayNode {
		String base;
		String stride;
		Counter counter;
		
		public int getStrideValue(){
			Counter stCounter = new Counter(stride);
			return (int) stCounter.evaluate();
		}
		
		
		public ArrayNode(String b, String s) {
			base = b;
			if(s!= null)
				stride = s;
			else 
				stride = "";
			counter = new Counter();
			counter.addconstPart(1);
		}
		
		public ArrayNode(ArrayNode a) {
			base = a.base;
			stride = a.stride;
			counter = new Counter(a.counter);
		}
	}
	
	ArrayList<ArrayNode> sharing;
	
	
	public FalseSharing(){
		sharing = new ArrayList<FalseSharing.ArrayNode>();
	}
	
	
	public void addSharingFalseSharing(FalseSharing s) {
		for(ArrayNode a : s.sharing) {
			addSharing(a);
		}
		
	}
	
	public void addSharingList(ArrayList<ArrayExpression> s, String displacement) {
		for(ArrayExpression a : s) {
			if(! a.getCoefficient(displacement).isEmpty())
		    	addSharing(a.getBaseName(), a.getCoefficient(displacement));
		}
	}
	
	
	
	void addSharing(ArrayNode a) {
		boolean found = false;
		for(ArrayNode s: sharing) {
			if(s.base.equals(a.base) && s.stride.equals(a.stride)) {
				s.counter.add(a.counter);
				found  = true;
				break;	
			}
		}
		if(!found) {
			ArrayNode newNode = new ArrayNode(a.base, a.stride);
			sharing.add(newNode);
		}
		
	}
	
	void addSharing(String base, String stride) {
		boolean found = false;
		
		for(ArrayNode s: sharing) {
			if(s.base == base && s.stride == stride) {
				s.counter.addconstPart(1);
				found  = true;
				break;	
			}
		}
		if(!found) {
			ArrayNode newNode = new ArrayNode(base, stride);
			sharing.add(newNode);
		}
	}
	
	public String dumpData() {
		String str="\n";
		for(ArrayNode a: sharing) {
			str += a.base + " " + a.stride + " " +a.counter.getdata(); 
		}
		return str;
	}
	
	void multiply(String s) {
		for(ArrayNode a: sharing) {
			a.counter.multiply(s);
		}
	}

	double data;
	
	public double getEvaluatedData() {
		return data;
	}

	public double evaluate() {
		for(ArrayNode a: sharing) {
			data += a.counter.evaluate();
		}
		return data;
	}


	public int getMaximumStride() {
		int stride = 0;	
		for(ArrayNode n : sharing) {
			if(stride < n.getStrideValue())
				stride  = n.getStrideValue();
		}
		return stride;
	}
	
}
