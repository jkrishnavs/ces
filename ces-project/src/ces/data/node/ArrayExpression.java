/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.node;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.*;
import ces.data.base.Counter;
import ces.util.CesStringMisc;
import stringexp.CESString;

public class ArrayExpression {
	Node baseNode;
	ArrayList<BracketExpression> bracketExpressions;
	String baseStr;
	String index;
	
	Counter counter;
	
	boolean write;
	
	public ArrayExpression(Node base, String b, ArrayList<BracketExpression> brackets,boolean w) {
		baseNode = base;
		if(b == null) {
			Main.addlog("base is " + base);
		}
		baseStr = b;
		bracketExpressions = new ArrayList<BracketExpression>();
		bracketExpressions.addAll(brackets);
		index = bracketExpressions.get(0).f1.getInfo().getString();
		write = w;
		counter = new Counter();
		counter.addconstPart(1);
	}
	
	public String getBaseName() {
		return baseNode.getInfo().getString();
	}
	
	
	public boolean isWrite(){
		return write;
	}

	
	public String getIndex() {
		return index;
	}
	
	public String getBracketExpression(){
		String brExpr = "";
		for(BracketExpression b : bracketExpressions){
			brExpr += b.getInfo().getString();
		}
		return brExpr;
	}
	
	public int matrixDimension(){
		return bracketExpressions.size();
	}
	
	public boolean addtoFalseSharing(String displacement) {
		String dist = bracketExpressions.get(bracketExpressions.size()-1 ).f1.toString();
		return CESString.findVariable(dist, displacement);
	}
	
	
	public String getCoefficient(String iteration) {
		assert(bracketExpressions.size() != 0);
		BracketExpression leastSignificant = bracketExpressions.get(bracketExpressions.size() -1);
		String s = leastSignificant.f1.getInfo().getBareMinimumString();
		/*
		 * TODO : assuming flalse sharing only with ompfo r
		 * 
		 */
		
		if(iteration != null && s.contains(iteration)) {
			return CesStringMisc.getAdduntant(leastSignificant.f1.getInfo().getString(), iteration) ;
		}
		
		return "";
	}
}
