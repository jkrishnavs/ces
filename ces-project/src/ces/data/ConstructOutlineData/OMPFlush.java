/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ConstructOutlineData;

import imop.ast.node.Node;

public class OMPFlush extends OMPConstructData{
	public OMPFlush(Node d) {
		super(d);
	}

	int ompFlushVariables;
}