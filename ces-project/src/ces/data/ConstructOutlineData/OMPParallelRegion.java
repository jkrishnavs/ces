/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ConstructOutlineData;

import java.util.ArrayList;

import imop.ast.node.ExpressionStatement;
import imop.ast.node.IterationStatement;
import imop.ast.node.Node;
import imop.ast.node.SelectionStatement;

public class OMPParallelRegion extends OMPConstructData{
	public OMPParallelRegion(Node d) {
		super(d);
		cdata = new ArrayList<OMPCritical>();
		fdata = new ArrayList<OMPFlush>();
		mdata = new ArrayList<OMPMaster>();
		sdata = new ArrayList<OMPSingle>();
		bdata = new ArrayList<OMPBarrier>();
		fordata = new ArrayList<OMPFor>();
		
	}
	//	ArrayList<Node> statments;
	int statements;
	ArrayList<OMPCritical> cdata; 
	ArrayList<OMPFlush> fdata;
	ArrayList<OMPMaster> mdata;
	ArrayList<OMPSingle> sdata;
	ArrayList<OMPBarrier> bdata;
	ArrayList<OMPFor> fordata;
	OMPReduction reduction;
	
	
	@Override
	public void addStatement(ExpressionStatement n) {
		statements++;
	}
	
	@Override
	public void addJumpStament(SelectionStatement n) {
		statements++;	
	}

	@Override
	public void addLoopStatement(IterationStatement n) {
		statements++;
	}
	

	@Override
	public void add(OMPConstructData n) {
		if(n instanceof OMPReduction){
			if(reduction == null)
				reduction = new OMPReduction(n.data);
			reduction.reductionList.addAll(((OMPReduction) n).reductionList);
		}else if(n instanceof OMPCritical){
			cdata.add((OMPCritical) n);
		}else if(n instanceof OMPFlush){
			fdata.add((OMPFlush) n);
		}else if(n instanceof OMPMaster){
			mdata.add((OMPMaster) n);
		}else if(n instanceof OMPSingle){
			sdata.add((OMPSingle) n);
		}else if(n instanceof OMPBarrier){
			bdata.add((OMPBarrier)n);
		}else if(n instanceof OMPFor){
			fordata.add((OMPFor)n);
		}else if(n instanceof OMPForNoWait){
			fordata.add((OMPFor)n);
		}else if(n instanceof OMPParallelRegion){
			if( ((OMPParallelRegion) n).reduction != null)
				add(((OMPParallelRegion) n).reduction);
			ArrayList<OMPCritical> clist = ((OMPParallelRegion) n).cdata;
			for (int i = 0; i < clist.size(); i++) {
				add(clist.get(i));	
			}
			ArrayList<OMPFlush> flist = ((OMPParallelRegion) n).fdata;
			for (int i = 0; i < flist.size(); i++) {
				add(flist.get(i));
			}
			ArrayList<OMPMaster> mlist =((OMPParallelRegion) n).mdata;
			for (int i = 0; i < mlist.size(); i++) {
				add(mlist.get(i));
			}
			ArrayList<OMPSingle> slist = ((OMPParallelRegion) n).sdata;
			for (int i = 0; i < slist.size(); i++) {
				add(slist.get(i));
			}
			ArrayList<OMPBarrier> blist =  ((OMPParallelRegion) n).bdata;
			for (int i = 0; i < blist.size(); i++) {
				add(blist.get(i));
			}
			ArrayList<OMPFor> forList =  ((OMPParallelRegion) n).fordata;
			for (int i = 0; i < forList.size(); i++) {
				add(forList.get(i));
			}
			statements += ((OMPParallelRegion) n).statements;
		} else if(n instanceof OMPFunctionData){
			add(((OMPFunctionData) n).addition);
		} else { 
			super.add(n);
		}
	}
	
	@Override
	public String dumpdata() {
		return "\nNo of Critical region " + cdata.size() + "\nNo of Flush region " + fdata.size() 
			+ "\nNo of Master region " + mdata.size() + "\nNo fo Single region " + sdata.size()
			+ "\nNo of Barriers " + bdata.size() + "\nNo of fordata " + fordata.size()
			+ "\n Reductions is " + fordata.size() + "\nNo of Statements " + statements + "\n";
	}
}