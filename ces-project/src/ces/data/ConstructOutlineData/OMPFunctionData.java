/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ConstructOutlineData;

import java.util.ArrayList;

import imop.ast.node.ExpressionStatement;
import imop.ast.node.IterationStatement;
import imop.ast.node.Node;
import imop.ast.node.SelectionStatement;

public class OMPFunctionData extends OMPConstructData {
	
	
	OMPParallelRegion addition = null;
	
	public OMPFunctionData(Node d) {
		super(d);
		addition = new OMPParallelRegion(d);
		parregions = new ArrayList<>();
	}
	
	ArrayList<OMPParallelRegion> parregions;
	
	

	@Override
	public void add(OMPConstructData n) {
		if(n instanceof OMPParallelRegion){				   
			parregions.add((OMPParallelRegion) n);
		}else if(n instanceof OMPFunctionData){
			addition.add(((OMPFunctionData) n).addition);
			parregions.addAll(((OMPFunctionData) n).parregions);
		}else {
			if(parregions.size() == 0){
				addition.add(n);
			}else 
				super.add(n);
		}
		
	}
	
	@Override
	public void addStatement(ExpressionStatement n) {
		if(parregions.size() == 0){
			addition.addStatement(n);
		}
	}
	
	@Override
	public void addJumpStament(SelectionStatement n) {
		if(parregions.size() == 0){
			addition.addJumpStament(n);
		}	
	}

	@Override
	public void addLoopStatement(IterationStatement n) {
		if(parregions.size() == 0){
			addition.addLoopStatement(n);
		}	
	}
	
	
	
	@Override
	public String dumpdata() {
		String retVal = "";
		for (int i = 0; i < parregions.size(); i++) {
			retVal += "******* PARALLEL REGION " + i + "********************";
	
			retVal += parregions.get(i).dumpdata();	
		}	
		return retVal;
	}
}
