/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ConstructOutlineData;

import imop.ast.node.ExpressionStatement;
import imop.ast.node.IterationStatement;
import imop.ast.node.Node;
import imop.ast.node.SelectionStatement;

public class OMPSingle extends OMPConstructData{
	public OMPSingle(Node d) {
		super(d);
	}

	int ompSingleStmts;
	@Override
	public void addStatement(ExpressionStatement n) {
		ompSingleStmts++;
	}
	
	@Override
	public void addJumpStament(SelectionStatement n) {
		ompSingleStmts++;	
	}

	@Override
	public void addLoopStatement(IterationStatement n) {
		ompSingleStmts++;
	}
}