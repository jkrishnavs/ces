/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ConstructOutlineData;

import imop.Main;
import imop.ast.node.ExpressionStatement;
import imop.ast.node.IterationStatement;
import imop.ast.node.Node;
import imop.ast.node.SelectionStatement;

public class OMPConstructData {
	Node data;
	
	
	public OMPConstructData(Node d){
		data  = d;
	}
	
	public void add(OMPConstructData n){
		Main.errWriter.println("The add function is not defined for this " + this);
	}

	public void addStatement(ExpressionStatement n) {
		Main.errWriter.println("The Statment function is not defined for this " + this);
	}

	public void addJumpStament(SelectionStatement n) {
		Main.errWriter.println("The Jump function is not defined for this " + this);
			
	}

	public void addLoopStatement(IterationStatement n) {

		Main.errWriter.println("The Iteration statement is not defined for this " + this);
		
	}

	public String dumpdata() {
		return "ERROR:The dump data is not defined for this ";
	}
}
