/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.data.ces;

import imop.ast.node.*;
import ces.util.CesSpecificMisc;
import ces.util.SystemStringConstants;

public class ReductionData {
	String opSymbol; // reduction symbol
	String varName; // variable Name
	String tempName;
	Node enclosingNode;
	Node reductionNode;
	
	
	public void setEnclosingNode(Node n) {
		enclosingNode  = n;
	}
	
	
	public ReductionData(String sym, String var, Node node) {
		opSymbol = sym;
		opSymbol = opSymbol.replaceAll("\\s","");
		varName = var;
		this.reductionNode = node;
	//	tempName = CesSpecificMisc.GenerateReplaceName(varName);
	}
	
	/**
	 * @return the temp variable name that is created private for each thread.
	 */
	/*
	public String getTempVarName() {
		return tempName;
	}
	*/
	public Node getCurrentVar() {
		return reductionNode;
	}
	/**
	 * 
	 * @return the initialisation statement for the private variable 
	 * needs to be added before the for construct
	 */
	public String getinitilizationStatement(String replacementName){
		tempName = replacementName;
		String varType = CesSpecificMisc.getTypeStringWithoutAnySpecifiers(varName, enclosingNode);
		String statement = " \t " + varType + " " + replacementName + " = " 
				+ CesSpecificMisc.getInitialValueforTheSymbol(opSymbol) + ";"; 
		return statement;
	}
	
	/**
	 * 
	 * @return returns the atomic statement that needs to be added 
	 * after the for construct to obtain the final result to the original variable.
	 */
	public String getReductionStatement() {
		String atomicStmt = SystemStringConstants.atomicConstruct 
				+ "\t " + varName + " " +  opSymbol + "="
				+ " " + tempName + ";";
		return atomicStmt;
	}
	
}

