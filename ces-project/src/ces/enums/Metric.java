/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.enums;

public enum Metric {
		memread,
		memwrite,
		privateRead,
		privateWrite,
		sharedRead,
		sharedWrite,
		branches,
		ops_arith,
		ops_mult,
		ops_bitwise,
		ops_alu,
		funcCalls,
		ompbarrier,
		ompDirectives,
		
		/**  Used for classifiers **/
}
