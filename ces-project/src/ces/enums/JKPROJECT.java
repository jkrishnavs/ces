/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.enums;

import ces.SystemConfigFile;

public enum JKPROJECT {
	/**
	 * CES project is meant for optimizing a regular OpenMP
	 * program for the given asymmetric hardware. 
	 * Example Benchmark is NPB.
	 * The ces project is not designed to handle OpenMP
	 * tasks and recursions. 
	 */
	CES,
	/**
	 * The CLASSIFIER project is utilised to find the best 
	 * hardware configuration for a given regular OpenMP
	 * program. This is build on top of the CES project
	 * and hence will have similar restrictions like
	 * CES.  
	 * Example Benchmark is NPB.
	 * The classifier project will not transform the input code
	 * unless We choose customized optimization 
	 * (See {@link SystemConfigFile}.optimizationLevel) for more details.
	 * When the optimization level is selected as CUSTOM
	 * we use the ces optimization once the selection is done.
	 */
	CLASSIFIER,
	/**
	 * The GRAPH project is intended to optimize graph algorithms 
	 * for a given asymmetric hardware.
	 * This has similar end goals like CES and is build on top of CES. 
	 * Example benchmark is GreenMarl
	 * Why this flag is needed?
	 * GreenMarl is originally written in CPP with classes and Templates
	 * which are not handled properly by IMOP which is based 
	 * on ANSI C. So we use a ROSE based project to parse the original 
	 * GreenMarl code to to create an ANSI C compliant skeletal code.
	 * We use #pragma s to retain the information about the original
	 * CPP program to final output the cpp program.
	 */	
	GRAPH,
	/**
	 * CES flag for Tasks optimizations. 
	 * 
	 */
	TASK,
	/**
	 * siam 
	 * 
	 */
	SIAM,
	/**
	 * Dummy project
	 */
	NONE,
}
