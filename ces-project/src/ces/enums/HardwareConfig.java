/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.enums;

public enum HardwareConfig {
	n4b0l4b2000000,
	
	n4b2l2b1200000, 
	n4b2l2b1400000,
	n4b2l2b1600000, 
	n4b2l2b1800000, 
	n4b2l2b2000000,
	
	n4b4l0b1200000, 
	n4b4l0b1400000,
	n4b4l0b1600000, 
	n4b4l0b1800000, 
	n4b4l0b2000000,
	
	n6b2l4b1200000, 
	n6b2l4b1400000,
	n6b2l4b1600000, 
	n6b2l4b1800000, 
	n6b2l4b2000000,
	
	n8b4l4b1200000, 
	n8b4l4b1400000,
	n8b4l4b1500000,
	n8b4l4b1600000,
	n8b4l4b1700000,
	n8b4l4b1800000,
	n8b4l4b1900000,
	n8b4l4b2000000,
	
	Unknown,
}
