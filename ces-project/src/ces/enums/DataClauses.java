/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.enums;

public enum  DataClauses{ 
	
	OMPPRIVATECLAUSE(0),
	OMPFIRSTPRIVATECLAUSE(1),
	OMPLASTPRIVATECLAUSE(2),
	OMPSHAREDCLAUSE(3),
	OMPCOPYINCLAUSE(4),
	OMPDFLTSHAREDLAUSE(5),
	OMPDFLTNONECLAUSE(6),
	OMPREDUCTIONCLAUSE(7);


private int value;    
private DataClauses(int value) {
this.value = value;
}
public int getValue() {
    return value;
  }

};