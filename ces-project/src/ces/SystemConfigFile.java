/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces;

import ces.enums.*;

/**
 * 
 * 
 * 
 * @author jkrishnavs
 *
 */
public class SystemConfigFile {

	
	/**
	 * Hardware configurations.
	 * 
	 */
	public static final int no_of_cores = 8;
	public static final int no_of_big = 4;
	public static final int no_of_little = 4;
	public static final int cacheLineSize = 16;

	
	
	/**
	 * Software configurations
	 * 
	 */
	
	/***
	 * If we are using the command line arguments we can set 
	 * values of these parameters
	 */
	//Project type
	public static JKPROJECT project = JKPROJECT.CES;
		
	// Cost function
	public static CostFunction selectedCostFunction = CostFunction.EXECUTIONTIME;
	// Optimization level requested. Valid only for Project Classifier
	public static OptimizationLevel  optimizationLevel = OptimizationLevel.DynamicScheduling;
	// regression choice
	public static RegressionFunctionChoice regressionFunctionChoice = RegressionFunctionChoice.linear;
	// input graph is irregular
	public static  boolean irregularGraph = false;
	//Choice to use ALUFator or totalOps for getting the feature vector
	public static boolean ALUFACTOR = true;
	// Should we choose a dynamic chunk size for different 
	// input sizes
	public static boolean dynamicChunkSize = false;
	// Choose to optimize single and master while optimizing for ces
	public static boolean optimizeSinglAndMasterForCes = false;
	
	// The file in which we store all graph properties.
	public static String graphPropertyFile  = "";
	
	
	
	/**
	 * Graph min and max chunk sizes
	 */
	public static int minChunkSizeGraph = 1024;
	public static int maxChunkSizeGraph = 8192;
	
	

	
	
	
	public static final int TOTALFEATURES = 6; // IMPORTANT always keep this updated
	/**
	 * Default Values
	 */
	 // Default Chunk Size for Dynamic chunking.
	public static final int dynamicChunkAccuracy = 10;	
	public static final double globalbigr = 2.7;
	// For deciding the Variable Range limits.
	public static final int logBase = 10;
	//Default dimension max size. Only if max SIze fails.
	public static final int defaultDimSize = 128;
	 //Update occurrences limit beyond which we need to use update logic in ces
	public static final int UpdateOccuranceLimit = 3;
	//The definition of big loop
	public static final int BigLoopSize = 50;
	// Minimum iteration size for ces scheduling.
	public static final int forScheduleMinimumSize  = 8;
	// Default iteration value
	public static final long itrValue = 10;
	// Default main function
	public static String baseFunction = "main";
	// Equation length  limit beyond which we call reduce function on the equation.
	public static final int equationLimit = 1000;
	// TODO remove this
	public static double btSize = 18;
	// Default Chunk Size
	public static final int defaultChunkSize = 4096;
	public static final int INVALID_POS = -1;
	
	
	public static final HardwareConfig globalHardwareConfig = HardwareConfig.n6b2l4b1800000;
	

	
}
