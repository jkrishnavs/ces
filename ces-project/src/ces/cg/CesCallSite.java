/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.cg;

import imop.ast.node.FunctionDefinition;
import imop.ast.node.PostfixExpression;
import imop.lib.cg.CallSite;
/**
 *  inherited variables
 * 	public String calleeName;
 *	public FunctionDefinition calleeDefinition; // is NULL if calleeDef not available
 *	public PostfixExpression callerExp;	// The actual PostfixExpression having a call within it.
 *	public Node callerCFGNode;		// The actual CFG node which contains this PostfixExpression.
 *
 */
public class CesCallSite extends CallSite{
	
	
	
	private boolean _callInsideParallelconstruct; /*This flag is set when the call site is in parallel region.*/
	private boolean _callInsideSequentialregion; /* This flag is set when the call site is in sequential region 
	 												Note : both flags are not complementary as we can have multiple
	 												Control Flow paths there by calling from both sequential and 
	 												parallel paths*/
	

	public CesCallSite(String calleeName, FunctionDefinition calleeDef,
			PostfixExpression callerExp) {
		
		super(calleeName, calleeDef, callerExp);
		_callInsideParallelconstruct(false);
		_callInsideSequentialregion(false);
	}


	public void _callInsideParallelconstruct(boolean val){
		_callInsideParallelconstruct = val;
	}
	public boolean _callInsideParallelconstruct(){
		return _callInsideParallelconstruct;
	}
	
	public boolean _callInsideSequentialregion(){
		return _callInsideSequentialregion;
	}
	
	public void _callInsideSequentialregion(boolean val){
		_callInsideSequentialregion = val;
	}
	


}
