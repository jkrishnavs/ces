/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.util;
import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import imop.baseVisitor.GJVoidDepthFirst;
import ces.SystemConfigFile;
import ces.enums.JKPROJECT;

/**
 * Printing the code corresponding to the current state of AST.
 * Needs 0 as the Integer argument
 */
public class PrintCPPCodeVisitor extends GJVoidDepthFirst<Integer> {
	
	/**
	 * This code emits CPP code based on the pragmas structures associated with 
	 * The input code (C code with pargams from the original CPP file)
	 * is  generated using https://github.com/jkrishnavs/CPPtoANSIC project.
	 * Any queries mail to jkrishnavs@gmail.com .
	 */
	
	
	/**
	 * These are set of function that has the body defined in the .h files. If we
	 * expand them here we will have multiple function definitions.
	 */ 
	String[] skiplist = {"num_nodes", "num_edges", "ATOMIC_OR", "abs"};

	private static boolean replaceifTest;
	private static String ifTestString;
	
	private static boolean replaceWhileTest;
	private static String whileTestString;
	
	static boolean printCode = false;
	static boolean globalPrintCode = false;
	
	static int skipStatement = 0;
	
	static boolean replaceForinit = false;
	static boolean replaceForincrement = false;
	static boolean replaceForTest = false;
	
	static String forInitString;
	static String forIncrementString;
	static String forTestString;
	
	static String functionHeaderString;
	static String className;
	static String functionName;
	
	// 
	static String templateString = "\n\n";
	static boolean templateBoolean = false; 
	public static String removeSemicolon(String s) {
		return s.replaceAll(";", "");
	}
	
	public static String returnForHeaderString(String forInit, String forTest, String forIncrement){
		String header="";
		if(replaceForinit) {
			header += forInitString.trim();
		} else {
			header += forInit;
		}
		header +=";";
		if(replaceForTest) {
			header += forTestString.trim();
		} else {
			header += forTest;
		}
		header += ";";
		if(replaceForincrement) {
			header += forIncrementString.trim();
		} else {
			header += forIncrement;
		}
		replaceForincrement = false;
		replaceForinit = false;
		replaceForTest = false;
		
		return header;
	}
	
	

	public static void printTabs(int num) {
		if(printCode) {
			Main.outputWriter.println();
			for (int i = 0; i < num; i++) {
				Main.outputWriter.print("    ");
			}
		}
	}
	
	public static void printString(String code, int tab, boolean skipTab) {
		/*
		 *  TODO greenmarl hack remove
		 */
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			if(code.indexOf("((size_type )p)") != -1) {
				code = code.replace("((size_type )p)", "p");
			} else if(code.indexOf("seq_iter ") != -1) {
				code = code.replace("seq_iter ", "gm_node_seq_vec::seq_iter ");
			}
			
			//Main.addlog(code);
		}
		
		
		if(printCode) {
			if(!skipTab)
				printTabs(tab);
			Main.outputWriter.print(code);
		} else {
			Main.addErrorMessage("The print code called when the printCode flag is not set");
		}
		
	}


	/**
	 * This method can be overridden to print some logs.
	 */
	public void endProcess(Node n, Integer argu) {
		
	}
	

	public void visit(NodeToken n, Integer argu) { 
		if(printCode) {
			if(n.toString().equals("#"))
				Main.outputWriter.println();
			/**
			 * TODO remove these hacks.
			 * Green marl hack.
			 */
			if(n.toString().equals("NIL_NODE") && SystemConfigFile.project == JKPROJECT.GRAPH){
				Main.outputWriter.print( "gm_graph::"+n.toString() + " "); 
						
			} else if(n.toString().equals("_gm_sort_indices") && SystemConfigFile.project == JKPROJECT.GRAPH) {
				Main.outputWriter.print( n.toString()+"<node_t>" + " "); 
			}else {
				Main.outputWriter.print( n.toString() + " "); 
			}
		}
	}
	

	//
	// User-generated visitor methods below
	//

	/**
	 * f0 -> ( ElementsOfTranslation() )+
	 */
	public void visit(TranslationUnit n, Integer argu) {
		n.f0.accept(this, new Integer(0));
	}

	/**
	 * f0 -> ExternalDeclaration()
	 *       | UnknownCpp()
	 *       | UnknownPragma()
	 */
	public void visit(ElementsOfTranslation n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> Declaration()
	 *       | FunctionDefinition()
	 *       | DeclareReductionDirective()
	 *       | ThreadPrivateDirective()
	 */
	public void visit(ExternalDeclaration n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( DeclarationSpecifiers() )?
	 * f1 -> Declarator()
	 * f2 -> ( DeclarationList() )?
	 * f3 -> CompoundStatement()
	 */
	public void visit(FunctionDefinition n, Integer argu) {
//		assert(printCode == false);
		boolean updateCode = false;
		//if(!printCode) {
		updateCode = decideToPrintTheFunction(n.f3);
		//}
		printCode |= updateCode;
		if(printCode) {
			printTabs(argu);
		 if(!updateCode) {
			 n.f0.accept(this, argu);
			 n.f1.accept(this, argu);
			 n.f2.accept(this, argu + 1);
		 } else {
			 printFunctionHeader(n, argu);
			 skipStatement = 2;
		 }
			n.f3.accept(this, argu);
		}
		if(updateCode && globalPrintCode == false)
			printCode = false;
	}

	private void printFunctionHeader(FunctionDefinition n, Integer argu) {
		CompoundStatement funBody = n.f3;
		Vector<Node> nodeList = funBody.f1.nodes;
		functionHeaderString = "";
		nodeList.get(0).accept(this, argu);
		nodeList.get(1).accept(this, argu);
		assert(!functionHeaderString.isEmpty());
		printString(functionHeaderString, argu, false);
	}

	private boolean decideToPrintTheFunction(CompoundStatement funBody) {
		
		if(funBody.f1.nodes.isEmpty())
			return false;
		Vector<Node> nodeList = funBody.f1.nodes;
		
		
		Node s =  ((CompoundStatementElement)nodeList.get(0)).f0.choice;
		if(s.getClass() == Statement.class) {
			Node mychoice = ((Statement)s).f0.choice;
			if(mychoice.getClass() == UnknownPragma.class) {
				String unknownCpp = ((UnknownPragma) mychoice).f2.getInfo().getString();
				int startPoint = unknownCpp.indexOf(" function ") + 10; // size of " function "
				String funcString = unknownCpp.substring(startPoint);
				funcString = funcString.trim();
				for (int i = 0; i < skiplist.length; i++) {
					if(funcString.equals(skiplist[i])) {
						return false;
					}
				}
				if(! templateBoolean){
					printCode = true;
					printString(templateString, 0, false);
					templateBoolean = true;
					printCode = false;
				}	
				
				return true;
			}
		}
		
		return false;
	}

	/**
	 * f0 -> DeclarationSpecifiers()
	 * f1 -> ( InitDeclaratorList() )?
	 * f2 -> ";"
	 */
	public void visit(Declaration n, Integer argu) {
		printTabs(argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( Declaration() )+
	 */
	public void visit(DeclarationList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( ADeclarationSpecifier() )+
	 */
	public void visit(DeclarationSpecifiers n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> StorageClassSpecifier()
	 *       | TypeSpecifier()
	 *       | TypeQualifier()
	 */
	public void visit(ADeclarationSpecifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <AUTO>
	 *       | <REGISTER>
	 *       | <STATIC>
	 *       | <EXTERN>
	 *       | <TYPEDEF>
	 */
	public void visit(StorageClassSpecifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( <VOID> | <CHAR> | <SHORT> | <INT> | <LONG> | <FLOAT> | <DOUBLE> | <SIGNED> | <UNSIGNED> | StructOrUnionSpecifier() | EnumSpecifier() | TypedefName() )
	 */
	public void visit(TypeSpecifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <RESTRICT>
	 *       | <CONST>
	 *       | <VOLATILE>
	 *       | <INLINE>
	 *       | <CCONST>
	 *       | <CINLINED>
	 *       | <CINLINED2>
	 *       | <CSIGNED>
	 *       | <CSIGNED2>
	 */
	public void visit(TypeQualifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**

	 * f0 -> ( StructOrUnionSpecifierWithList() | StructOrUnionSpecifierWithId() )
	 */
	public void visit(StructOrUnionSpecifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> StructOrUnion()
	 * f1 -> ( <IDENTIFIER> )?
	 * f2 -> "{"
	 * f3 -> StructDeclarationList()
	 * f4 -> "}"
	 */
	public void visit(StructOrUnionSpecifierWithList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu + 1);
		printTabs(argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> StructOrUnion()
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(StructOrUnionSpecifierWithId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <STRUCT>
	 *       | <UNION>
	 */
	public void visit(StructOrUnion n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( StructDeclaration() )+
	 */
	public void visit(StructDeclarationList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> InitDeclarator()
	 * f1 -> ( "," InitDeclarator() )*
	 */
	public void visit(InitDeclaratorList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> Declarator()
	 * f1 -> ( "=" Initializer() )?
	 */
	public void visit(InitDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> SpecifierQualifierList()
	 * f1 -> StructDeclaratorList()
	 * f2 -> ";"
	 */
	public void visit(StructDeclaration n, Integer argu) {
		printTabs(argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( ASpecifierQualifier() )+
	 */
	public void visit(SpecifierQualifierList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> TypeSpecifier()
	 *       | TypeQualifier()
	 */
	public void visit(ASpecifierQualifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> StructDeclarator()
	 * f1 -> ( "," StructDeclarator() )*
	 */
	public void visit(StructDeclaratorList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> StructDeclaratorWithDeclarator()
	 *       | StructDeclaratorWithBitField()
	 */
	public void visit(StructDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> Declarator()
	 * f1 -> ( ":" ConstantExpression() )?
	 */
	public void visit(StructDeclaratorWithDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ":"
	 * f1 -> ConstantExpression()
	 */
	public void visit(StructDeclaratorWithBitField n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> EnumSpecifierWithList()
	 *       | EnumSpecifierWithId()
	 */
	public void visit(EnumSpecifier n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <ENUM>
	 * f1 -> ( <IDENTIFIER> )?
	 * f2 -> "{"
	 * f3 -> EnumeratorList()
	 * f4 -> "}"
	 */
	public void visit(EnumSpecifierWithList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <ENUM>
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(EnumSpecifierWithId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> Enumerator()
	 * f1 -> ( "," Enumerator() )*
	 */
	public void visit(EnumeratorList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ( "=" ConstantExpression() )?
	 */
	public void visit(Enumerator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}
	
	 /**
	    * f0 -> TypeofKeyword()
	    * f1 -> UnaryExpression()
	    */
	   public void visit(TypeofSpecifier n, Integer argu) {
	      n.f0.accept(this, argu);
	      n.f1.accept(this, argu);
	   }

	   /**
	    * f0 -> <TYPEOF>
	    *       | <CTYPEOF>
	    *       | <CTYPEOF2>
	    */
	   public void visit(TypeofKeyword n, Integer argu) {
	      n.f0.accept(this, argu);
	   }

	/**
	 * f0 -> ( Pointer() )?
	 * f1 -> DirectDeclarator()
	 */
	public void visit(Declarator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> IdentifierOrDeclarator()
	 * f1 -> DeclaratorOpList()
	 */
	public void visit(DirectDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ( ADeclaratorOp() )*
	 */
	public void visit(DeclaratorOpList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> DimensionSize()
	 *       | ParameterTypeListClosed()
	 *       | OldParameterListClosed()
	 */
	public void visit(ADeclaratorOp n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "["
	 * f1 -> ( ConstantExpression() )?
	 * f2 -> "]"
	 */
	public void visit(DimensionSize n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> ( ParameterTypeList() )?
	 * f2 -> ")"
	 */
	public void visit(ParameterTypeListClosed n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> ( OldParameterList() )?
	 * f2 -> ")"
	 */
	public void visit(OldParameterListClosed n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 *       | "(" Declarator() ")"
	 */
	public void visit(IdentifierOrDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( "*" | "^" )
	 * f1 -> ( TypeQualifierList() )?
	 * f2 -> ( Pointer() )?
	 */
	public void visit(Pointer n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( TypeQualifier() )+
	 */
	public void visit(TypeQualifierList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ParameterList()
	 * f1 -> ( "," "..." )?
	 */
	public void visit(ParameterTypeList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ParameterDeclaration()
	 * f1 -> ( "," ParameterDeclaration() )*
	 */
	public void visit(ParameterList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> DeclarationSpecifiers()
	 * f1 -> ParameterAbstraction()
	 */
	public void visit(ParameterDeclaration n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> Declarator()
	 *       | AbstractOptionalDeclarator()
	 */
	public void visit(ParameterAbstraction n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( AbstractDeclarator() )?
	 */
	public void visit(AbstractOptionalDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ( "," <IDENTIFIER> )*
	 */
	public void visit(OldParameterList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AssignmentExpression()
	 *       | ArrayInitializer()
	 */
	public void visit(Initializer n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "{"
	 * f1 -> InitializerList()
	 * f2 -> ( "," )?
	 * f3 -> "}"
	 */
	public void visit(ArrayInitializer n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> Initializer()
	 * f1 -> ( "," Initializer() )*
	 */
	public void visit(InitializerList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> SpecifierQualifierList()
	 * f1 -> ( AbstractDeclarator() )?
	 */
	public void visit(TypeName n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AbstractDeclaratorWithPointer()
	 *       | DirectAbstractDeclarator()
	 */
	public void visit(AbstractDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> Pointer()
	 * f1 -> ( DirectAbstractDeclarator() )?
	 */
	public void visit(AbstractDeclaratorWithPointer n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AbstractDimensionOrParameter()
	 * f1 -> DimensionOrParameterList()
	 */
	public void visit(DirectAbstractDeclarator n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AbstractDeclaratorClosed()
	 *       | DimensionSize()
	 *       | ParameterTypeListClosed()
	 */
	public void visit(AbstractDimensionOrParameter n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> AbstractDeclarator()
	 * f2 -> ")"
	 */
	public void visit(AbstractDeclaratorClosed n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( ADimensionOrParameter() )*
	 */
	public void visit(DimensionOrParameterList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> DimensionSize()
	 *       | ParameterTypeListClosed()
	 */
	public void visit(ADimensionOrParameter n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 */
	public void visit(TypedefName n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ( LabeledStatement() | ExpressionStatement() | CompoundStatement() | SelectionStatement() | IterationStatement() | JumpStatement() | UnknownPragma() | OmpConstruct() | OmpDirective() | UnknownCpp() )
	 */
	public void visit(Statement n, Integer argu) {
		if(! n.f0.choice.getClass().getSimpleName().equals("CompoundStatement")) {
			if(n.f0.choice.getClass().getSimpleName().equals("OmpDirective") || n.f0.choice.getClass().getSimpleName().equals("OmpConstruct")) {
				if(printCode)
					Main.outputWriter.println();
			}
			else {
				printTabs(argu);
			}
		}
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "#"
	 * f1 -> <UNKNOWN_CPP>
	 */
	public void visit(UnknownCpp n, Integer argu) {
		printTabs(argu);
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <OMP_CR>
	 *       | <OMP_NL>
	 */
	public void visit(OmpEol n, Integer argu) {
		//n.f0.accept(this, argu);
		printTabs(argu);
	}

	/**
	 * f0 -> ParallelConstruct()
	 *       | ForConstruct()
	 *       | SectionsConstruct()
	 *       | SingleConstruct()
	 *       | ParallelForConstruct()
	 *       | ParallelSectionsConstruct()
	 *       | TaskConstruct()
	 *       | MasterConstruct()
	 *       | CriticalConstruct()
	 *       | AtomicConstruct()
	 *       | OrderedConstruct()
	 */
	public void visit(OmpConstruct n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> BarrierDirective()
	 *       | TaskwaitDirective()
	 *       | TaskyieldDirective()
	 *       | FlushDirective()
	 */
	public void visit(OmpDirective n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> ParallelDirective()
	 * f2 -> Statement()
	 */
	public void visit(ParallelConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu + 1);
	}

	/**
	 * f0 -> "#"
	 * f1 -> <PRAGMA>
	 * f2 -> <OMP>
	 */
	public void visit(OmpPragma n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "#"
	 * f1 -> <PRAGMA>
	 * f2 -> <UNKNOWN_CPP>
	 */
	public void visit(UnknownPragma n, Integer argu) {
		printTabs(argu);
		String unknownCpp = n.f2.getInfo().getString();
		if(unknownCpp.startsWith("skip fnDef")) {
			if( unknownCpp.contains("class") ) {
				int startPoint = unknownCpp.indexOf("class") + 5; // size of "class"
				int endPoint = unknownCpp.indexOf(" function ");
				className = unknownCpp.substring(startPoint, endPoint);
				startPoint = unknownCpp.indexOf(" function ") + 10; // size of " function "
				functionName = unknownCpp.substring(startPoint);
			} else {
				className = "";
			}
				
		} else if (unknownCpp.startsWith("Fnsignature")) {
			String funDefString = unknownCpp.substring(11); // size of "Fnsignature"
			funDefString = removeSemicolon(funDefString);
				if(!className.isEmpty()) {
				functionName = functionName.trim();
				int insertPoint = funDefString.indexOf(functionName);
				functionHeaderString =  funDefString.substring(0, insertPoint)
						+ className + "::" + funDefString.substring(insertPoint);
			} else
				functionHeaderString = funDefString;
		} else if (unknownCpp.startsWith("replace fnCall normal")) {
			String functionCall = unknownCpp.replaceFirst("replace fnCall normal", "");
			printString(functionCall, argu, false);
			skipStatement = 1;
		} else if (unknownCpp.startsWith("replace fnCall ifcondition")) {
			replaceifTest = true;
			ifTestString = unknownCpp.replaceFirst("replace fnCall ifcondition", "");
			ifTestString = removeSemicolon(ifTestString);
		} else if (unknownCpp.startsWith("replace fnCall whilecondition")) {
			replaceWhileTest = true;
			whileTestString = unknownCpp.replaceFirst("replace fnCall whilecondition", "");
			whileTestString = removeSemicolon(whileTestString);
		}  else if (unknownCpp.startsWith("replace fnCall fortest")) {
			replaceForTest = true;
			forTestString = unknownCpp.replaceFirst("replace fnCall fortest", "");
			forTestString = removeSemicolon(forTestString);
		} else if (unknownCpp.startsWith("replace fnCall for_init")) {
			replaceForinit = true;
			forInitString = unknownCpp.replaceFirst("replace fnCall for_init", "");
			forInitString = removeSemicolon(forInitString);
		} else if (unknownCpp.startsWith("replace fnCall  increment")) {
			replaceForincrement = true;
			forIncrementString = unknownCpp.replaceFirst("replace fnCall  increment", "");
			forIncrementString = removeSemicolon(forIncrementString);
		} else if (unknownCpp.startsWith(SystemStringConstants.globalCodeStart)) {
			// assert(globalPrintCode == false);
			// assert(printCode == false);
			globalPrintCode = true;
			printCode = true;
		} else if (unknownCpp.startsWith(SystemStringConstants.globalCodeEnd)) {
		//	assert(globalPrintCode == true);
		//	assert(printCode == true);
			globalPrintCode = false;
			printCode = false;
			
		} else if(unknownCpp.startsWith("skip 1")) {
			String functionCall = unknownCpp.replaceFirst("skip 1 ", "");
			printString(functionCall, argu, false);
			skipStatement = 1;
		} else if(unknownCpp.startsWith("skip 0")) {
			String functionCall = unknownCpp.replaceFirst("skip 0 ", "");
			printString(functionCall, argu, false);
		} else {
			Main.addErrorMessage("pragma instruction not handled " + unknownCpp);
		}
//		Main.addlog(unknownCpp);
	}

	/**
	 * f0 -> <PARALLEL>
	 * f1 -> UniqueParallelOrDataClauseList()
	 * f2 -> OmpEol()
	 */
	public void visit(ParallelDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( AUniqueParallelOrDataClause() )*
	 */
	public void visit(UniqueParallelOrDataClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> UniqueParallelClause()
	 *       | DataClause()
	 */
	public void visit(AUniqueParallelOrDataClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> IfClause()
	 *       | NumThreadsClause()
	 */
	public void visit(UniqueParallelClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IF>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 */
	public void visit(IfClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <NUM_THREADS>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 */
	public void visit(NumThreadsClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPrivateClause()
	 *       | OmpFirstPrivateClause()
	 *       | OmpLastPrivateClause()
	 *       | OmpSharedClause()
	 *       | OmpCopyinClause()
	 *       | OmpDfltSharedClause()
	 *       | OmpDfltNoneClause()
	 *       | OmpReductionClause()
	 */
	public void visit(DataClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <PRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpPrivateClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <FIRSTPRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpFirstPrivateClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <LASTPRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpLastPrivateClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <SHARED>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpSharedClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <COPYIN>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpCopyinClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <DFLT>
	 * f1 -> "("
	 * f2 -> <SHARED>
	 * f3 -> ")"
	 */
	public void visit(OmpDfltSharedClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <DFLT>
	 * f1 -> "("
	 * f2 -> <NONE>
	 * f3 -> ")"
	 */
	public void visit(OmpDfltNoneClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <REDUCTION>
	 * f1 -> "("
	 * f2 -> ReductionOp()
	 * f3 -> ":"
	 * f4 -> VariableList()
	 * f5 -> ")"
	 */
	public void visit(OmpReductionClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> ForDirective()
	 * f2 -> OmpForHeader()
	 * f3 -> Statement()
	 */
	public void visit(ForConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <FOR>
	 * f1 -> UniqueForOrDataOrNowaitClauseList()
	 * f2 -> OmpEol()
	 */
	public void visit(ForDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( AUniqueForOrDataOrNowaitClause() )*
	 */
	public void visit(UniqueForOrDataOrNowaitClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> UniqueForClause()
	 *       | DataClause()
	 *       | NowaitClause()
	 */
	public void visit(AUniqueForOrDataOrNowaitClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <NOWAIT>
	 */
	public void visit(NowaitClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <ORDERED>
	 *       | UniqueForClauseSchedule()
	 *       | UniqueForCollapse()
	 */
	public void visit(UniqueForClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <COLLAPSE>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 */
	public void visit(UniqueForCollapse n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <SCHEDULE>
	 * f1 -> "("
	 * f2 -> ScheduleKind()
	 * f3 -> ( "," Expression() )?
	 * f4 -> ")"
	 */
	public void visit(UniqueForClauseSchedule n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <STATIC>
	 *       | <DYNAMIC>
	 *       | <GUIDED>
	 *       | <RUNTIME>
	 */
	public void visit(ScheduleKind n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <FOR>
	 * f1 -> "("
	 * f2 -> OmpForInitExpression()
	 * f3 -> ";"
	 * f4 -> OmpForCondition()
	 * f5 -> ";"
	 * f6 -> OmpForReinitExpression()
	 * f7 -> ")"
	 */
	public void visit(OmpForHeader n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		String init = n.f2.getInfo().getString();
		String test = n.f4.getInfo().getString();
		String update = n.f6.getInfo().getString();
		
		String headerString= returnForHeaderString(init, test, update);
		printString(headerString, 0, true);
		n.f7.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "="
	 * f2 -> Expression()
	 */
	public void visit(OmpForInitExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpForLTCondition()
	 *       | OmpForLECondition()
	 *       | OmpForGTCondition()
	 *       | OmpForGECondition()
	 */
	public void visit(OmpForCondition n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "<"
	 * f2 -> Expression()
	 */
	public void visit(OmpForLTCondition n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "<="
	 * f2 -> Expression()
	 */
	public void visit(OmpForLECondition n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ">"
	 * f2 -> Expression()
	 */
	public void visit(OmpForGTCondition n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ">="
	 * f2 -> Expression()
	 */
	public void visit(OmpForGECondition n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> PostIncrementId()
	 *       | PostDecrementId()
	 *       | PreIncrementId()
	 *       | PreDecrementId()
	 *       | ShortAssignPlus()
	 *       | ShortAssignMinus()
	 *       | OmpForAdditive()
	 *       | OmpForSubtractive()
	 *       | OmpForMultiplicative()
	 */
	public void visit(OmpForReinitExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "++"
	 */
	public void visit(PostIncrementId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "--"
	 */
	public void visit(PostDecrementId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "++"
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(PreIncrementId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "--"
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(PreDecrementId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "+="
	 * f2 -> Expression()
	 */
	public void visit(ShortAssignPlus n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "-="
	 * f2 -> Expression()
	 */
	public void visit(ShortAssignMinus n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "="
	 * f2 -> <IDENTIFIER>
	 * f3 -> "+"
	 * f4 -> AdditiveExpression()
	 */
	public void visit(OmpForAdditive n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "="
	 * f2 -> <IDENTIFIER>
	 * f3 -> "-"
	 * f4 -> AdditiveExpression()
	 */
	public void visit(OmpForSubtractive n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> "="
	 * f2 -> MultiplicativeExpression()
	 * f3 -> "+"
	 * f4 -> <IDENTIFIER>
	 */
	public void visit(OmpForMultiplicative n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTIONS>
	 * f2 -> NowaitDataClauseList()
	 * f3 -> OmpEol()
	 * f4 -> SectionsScope()
	 */
	public void visit(SectionsConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu + 1);
	}

	/**
	 * f0 -> ( ANowaitDataClause() )*
	 */
	public void visit(NowaitDataClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> NowaitClause()
	 *       | DataClause()
	 */
	public void visit(ANowaitDataClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "{"
	 * f1 -> ( Statement() )?
	 * f2 -> ( ASection() )*
	 * f3 -> "}"
	 */
	public void visit(SectionsScope n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		printTabs(argu - 1);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SECTION>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public void visit(ASection n, Integer argu) {
		if(printCode)
			Main.outputWriter.println();
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <SINGLE>
	 * f2 -> SingleClauseList()
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public void visit(SingleConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> ( ASingleClause() )*
	 */
	public void visit(SingleClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> NowaitClause()
	 *       | DataClause()
	 *       | OmpCopyPrivateClause()
	 */
	public void visit(ASingleClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <COPYPRIVATE>
	 * f1 -> "("
	 * f2 -> VariableList()
	 * f3 -> ")"
	 */
	public void visit(OmpCopyPrivateClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASK>
	 * f2 -> ( TaskClauseList() )*
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public void visit(TaskConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> DataClause()
	 *       | UniqueTaskClause()
	 */
	public void visit(TaskClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> IfClause()
	 *       | FinalClause()
	 *       | UntiedClause()
	 *       | MergeableClause()
	 */
	public void visit(UniqueTaskClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <FINAL>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 */
	public void visit(FinalClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <UNTIED>
	 */
	public void visit(UntiedClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <MERGEABLE>
	 */
	public void visit(MergeableClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <FOR>
	 * f3 -> UniqueParallelOrUniqueForOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> OmpForHeader()
	 * f6 -> Statement()
	 */
	public void visit(ParallelForConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
	}

	/**
	 * f0 -> ( AUniqueParallelOrUniqueForOrDataClause() )*
	 */
	public void visit(UniqueParallelOrUniqueForOrDataClauseList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> UniqueParallelClause()
	 *       | UniqueForClause()
	 *       | DataClause()
	 */
	public void visit(AUniqueParallelOrUniqueForOrDataClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <PARALLEL>
	 * f2 -> <SECTIONS>
	 * f3 -> UniqueParallelOrDataClauseList()
	 * f4 -> OmpEol()
	 * f5 -> SectionsScope()
	 */
	public void visit(ParallelSectionsConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <MASTER>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public void visit(MasterConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <CRITICAL>
	 * f2 -> ( RegionPhrase() )?
	 * f3 -> OmpEol()
	 * f4 -> Statement()
	 */
	public void visit(CriticalConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> <IDENTIFIER>
	 * f2 -> ")"
	 */
	public void visit(RegionPhrase n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ATOMIC>
	 * f2 -> ( AtomicClause() )?
	 * f3 -> OmpEol()
	 * f4 -> ExpressionStatement()
	 */
	public void visit(AtomicConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <READ>
	 *       | <WRITE>
	 *       | <UPDATE>
	 *       | <CAPTURE>
	 */
	public void visit(AtomicClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <FLUSH>
	 * f2 -> ( FlushVars() )?
	 * f3 -> OmpEol()
	 */
	public void visit(FlushDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> VariableList()
	 * f2 -> ")"
	 */
	public void visit(FlushVars n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <ORDERED>
	 * f2 -> OmpEol()
	 * f3 -> Statement()
	 */
	public void visit(OrderedConstruct n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <BARRIER>
	 * f2 -> OmpEol()
	 */
	public void visit(BarrierDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKWAIT>
	 * f2 -> OmpEol()
	 */
	public void visit(TaskwaitDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <TASKYIELD>
	 * f2 -> OmpEol()
	 */
	public void visit(TaskyieldDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <THREADPRIVATE>
	 * f2 -> "("
	 * f3 -> VariableList()
	 * f4 -> ")"
	 * f5 -> OmpEol()
	 */
	public void visit(ThreadPrivateDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
	}

	/**
	 * f0 -> OmpPragma()
	 * f1 -> <DECLARE>
	 * f2 -> <REDUCTION>
	 * f3 -> "("
	 * f4 -> ReductionOp()
	 * f5 -> ":"
	 * f6 -> ReductionTypeList()
	 * f7 -> ":"
	 * f8 -> Expression()
	 * f9 -> ")"
	 * f10 -> ( InitializerClause() )?
	 * f11 -> OmpEol()
	 */
	public void visit(DeclareReductionDirective n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
		n.f7.accept(this, argu);
		n.f8.accept(this, argu);
		n.f9.accept(this, argu);
		n.f10.accept(this, argu);
		n.f11.accept(this, argu);
	}

	/**
	 * f0 -> ( TypeSpecifier() )*
	 */
	public void visit(ReductionTypeList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> AssignInitializerClause()
	 *       | ArgumentInitializerClause()
	 */
	public void visit(InitializerClause n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "="
	 * f4 -> Initializer()
	 * f5 -> ")"
	 */
	public void visit(AssignInitializerClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
	}

	/**
	 * f0 -> <INITIALIZER>
	 * f1 -> "("
	 * f2 -> <IDENTIFIER>
	 * f3 -> "("
	 * f4 -> ExpressionList()
	 * f5 -> ")"
	 * f6 -> ")"
	 */
	public void visit(ArgumentInitializerClause n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 *       | "+"
	 *       | "*"
	 *       | "-"
	 *       | "&"
	 *       | "^"
	 *       | "|"
	 *       | "||"
	 *       | "&&"
	 */
	public void visit(ReductionOp n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ( "," <IDENTIFIER> )*
	 */
	public void visit(VariableList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> SimpleLabeledStatement()
	 *       | CaseLabeledStatement()
	 *       | DefaultLabeledStatement()
	 */
	public void visit(LabeledStatement n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 * f1 -> ":"
	 * f2 -> Statement()
	 */
	public void visit(SimpleLabeledStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <CASE>
	 * f1 -> ConstantExpression()
	 * f2 -> ":"
	 * f3 -> Statement()
	 */
	public void visit(CaseLabeledStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> <DFLT>
	 * f1 -> ":"
	 * f2 -> Statement()
	 */
	public void visit(DefaultLabeledStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> ( Expression() )?
	 * f1 -> ";"
	 */
	public void visit(ExpressionStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "{"
	 * f1 -> ( CompoundStatementElement() )*
	 * f2 -> "}"
	 */
	public void visit(CompoundStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu + 1);
		printTabs(argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> Declaration()
	 *       | Statement()
	 */
	public void visit(CompoundStatementElement n, Integer argu) {
		if(skipStatement > 0)
			skipStatement --;
		else
			n.f0.accept(this, argu);
	}

	/**
	 * f0 -> IfStatement()
	 *       | SwitchStatement()
	 */
	public void visit(SelectionStatement n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <IF>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 * f5 -> ( <ELSE> Statement() )?
	 */
	public void visit(IfStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		if(! replaceifTest) {
			n.f2.accept(this, argu);
		} else {
			assert(!ifTestString.isEmpty());
			replaceifTest = false;
			ifTestString = ifTestString.trim();
			printString(ifTestString, 0, true);
			ifTestString = "";
		}
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
	}

	/**
	 * f0 -> <SWITCH>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public void visit(SwitchStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> WhileStatement()
	 *       | DoStatement()
	 *       | ForStatement()
	 */
	public void visit(IterationStatement n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <WHILE>
	 * f1 -> "("
	 * f2 -> Expression()
	 * f3 -> ")"
	 * f4 -> Statement()
	 */
	public void visit(WhileStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		// TODO
		// TODO
		if(!replaceWhileTest) {
			n.f2.accept(this, argu);
		} else {
			assert(!whileTestString.isEmpty());
			replaceWhileTest = false;
			whileTestString = whileTestString.trim();
			printString(whileTestString, 0, true);
			whileTestString = "";
		}		
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
	}

	/**
	 * f0 -> <DO>
	 * f1 -> Statement()
	 * f2 -> <WHILE>
	 * f3 -> "("
	 * f4 -> Expression()
	 * f5 -> ")"
	 * f6 -> ";"
	 */
	public void visit(DoStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
		n.f4.accept(this, argu);
		n.f5.accept(this, argu);
		n.f6.accept(this, argu);
	}

	/**
	 * f0 -> <FOR>
	 * f1 -> "("
	 * f2 -> ( Expression() )?
	 * f3 -> ";"
	 * f4 -> ( Expression() )?
	 * f5 -> ";"
	 * f6 -> ( Expression() )?
	 * f7 -> ")"
	 * f8 -> Statement()
	 */
	public void visit(ForStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		String init = n.f2.getInfo().getBareMinimumString();
		String test = n.f4.getInfo().getBareMinimumString();
		String update = n.f6.getInfo().getBareMinimumString();
		
		String headerString= returnForHeaderString(init, test, update);
		printString(headerString, 0, true);
		n.f7.accept(this, argu);
		n.f8.accept(this, argu);
	}

	/**
	 * f0 -> GotoStatement()
	 *       | ContinueStatement()
	 *       | BreakStatement()
	 *       | ReturnStatement()
	 */
	public void visit(JumpStatement n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <GOTO>
	 * f1 -> <IDENTIFIER>
	 * f2 -> ";"
	 */
	public void visit(GotoStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> <CONTINUE>
	 * f1 -> ";"
	 */
	public void visit(ContinueStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <BREAK>
	 * f1 -> ";"
	 */
	public void visit(BreakStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <RETURN>
	 * f1 -> ( Expression() )?
	 * f2 -> ";"
	 */
	public void visit(ReturnStatement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> AssignmentExpression()
	 * f1 -> ( "," AssignmentExpression() )*
	 */
	public void visit(Expression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> NonConditionalExpression()
	 *       | ConditionalExpression()
	 */
	public void visit(AssignmentExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> UnaryExpression()
	 * f1 -> AssignmentOperator()
	 * f2 -> AssignmentExpression()
	 */
	public void visit(NonConditionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "="
	 *       | "*="
	 *       | "/="
	 *       | "%="
	 *       | "+="
	 *       | "-="
	 *       | "<<="
	 *       | ">>="
	 *       | "&="
	 *       | "^="
	 *       | "|="
	 */
	public void visit(AssignmentOperator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> LogicalORExpression()
	 * f1 -> ( "?" Expression() ":" ConditionalExpression() )?
	 */
	public void visit(ConditionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ConditionalExpression()
	 */
	public void visit(ConstantExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> LogicalANDExpression()
	 * f1 -> ( "||" LogicalORExpression() )?
	 */
	public void visit(LogicalORExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> InclusiveORExpression()
	 * f1 -> ( "&&" LogicalANDExpression() )?
	 */
	public void visit(LogicalANDExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ExclusiveORExpression()
	 * f1 -> ( "|" InclusiveORExpression() )?
	 */
	public void visit(InclusiveORExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ANDExpression()
	 * f1 -> ( "^" ExclusiveORExpression() )?
	 */
	public void visit(ExclusiveORExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> EqualityExpression()
	 * f1 -> ( "&" ANDExpression() )?
	 */
	public void visit(ANDExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> RelationalExpression()
	 * f1 -> ( EqualOptionalExpression() )?
	 */
	public void visit(EqualityExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> EqualExpression()
	 *       | NonEqualExpression()
	 */
	public void visit(EqualOptionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "=="
	 * f1 -> EqualityExpression()
	 */
	public void visit(EqualExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "!="
	 * f1 -> EqualityExpression()
	 */
	public void visit(NonEqualExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ShiftExpression()
	 * f1 -> ( RelationalOptionalExpression() )?
	 */
	public void visit(RelationalExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> RelationalLTExpression()
	 *       | RelationalGTExpression()
	 *       | RelationalLEExpression()
	 *       | RelationalGEExpression()
	 */
	public void visit(RelationalOptionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "<"
	 * f1 -> RelationalExpression()
	 */
	public void visit(RelationalLTExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ">"
	 * f1 -> RelationalExpression()
	 */
	public void visit(RelationalGTExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "<="
	 * f1 -> RelationalExpression()
	 */
	public void visit(RelationalLEExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ">="
	 * f1 -> RelationalExpression()
	 */
	public void visit(RelationalGEExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AdditiveExpression()
	 * f1 -> ( ShiftOptionalExpression() )?
	 */
	public void visit(ShiftExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ShiftLeftExpression()
	 *       | ShiftRightExpression()
	 */
	public void visit(ShiftOptionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> ">>"
	 * f1 -> ShiftExpression()
	 */
	public void visit(ShiftLeftExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "<<"
	 * f1 -> ShiftExpression()
	 */
	public void visit(ShiftRightExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> MultiplicativeExpression()
	 * f1 -> ( AdditiveOptionalExpression() )?
	 */
	public void visit(AdditiveExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> AdditivePlusExpression()
	 *       | AdditiveMinusExpression()
	 */
	public void visit(AdditiveOptionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "+"
	 * f1 -> AdditiveExpression()
	 */
	public void visit(AdditivePlusExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "-"
	 * f1 -> AdditiveExpression()
	 */
	public void visit(AdditiveMinusExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> CastExpression()
	 * f1 -> ( MultiplicativeOptionalExpression() )?
	 */
	public void visit(MultiplicativeExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> MultiplicativeMultiExpression()
	 *       | MultiplicativeDivExpression()
	 *       | MultiplicativeModExpression()
	 */
	public void visit(MultiplicativeOptionalExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "*"
	 * f1 -> MultiplicativeExpression()
	 */
	public void visit(MultiplicativeMultiExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "/"
	 * f1 -> MultiplicativeExpression()
	 */
	public void visit(MultiplicativeDivExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "%"
	 * f1 -> MultiplicativeExpression()
	 */
	public void visit(MultiplicativeModExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> CastExpressionTyped()
	 *       | UnaryExpression()
	 */
	public void visit(CastExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> TypeName()
	 * f2 -> ")"
	 * f3 -> CastExpression()
	 */
	public void visit(CastExpressionTyped n, Integer argu) {
		/**
		 * TODO greenmarl hack remove
		 * 
		 */
//		Main.addlog(n.f1.getInfo().getString() + "|");
		if(( n.f1.getInfo().getString().equals("size_type ") )
		    && SystemConfigFile.project == JKPROJECT.GRAPH ) {
			// skip
		} else {
			n.f0.accept(this, argu);
			n.f1.accept(this, argu);
			n.f2.accept(this, argu);
		}
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> UnaryExpressionPreIncrement()
	 *       | UnaryExpressionPreDecrement()
	 *       | UnarySizeofExpression()
	 *       | UnaryCastExpression()
	 *       | PostfixExpression()
	 */
	public void visit(UnaryExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "++"
	 * f1 -> UnaryExpression()
	 */
	public void visit(UnaryExpressionPreIncrement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "--"
	 * f1 -> UnaryExpression()
	 */
	public void visit(UnaryExpressionPreDecrement n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> UnaryOperator()
	 * f1 -> CastExpression()
	 */
	public void visit(UnaryCastExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> SizeofTypeName()
	 *       | SizeofUnaryExpression()
	 */
	public void visit(UnarySizeofExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> <SIZEOF>
	 * f1 -> UnaryExpression()
	 */
	public void visit(SizeofUnaryExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <SIZEOF>
	 * f1 -> "("
	 * f2 -> TypeName()
	 * f3 -> ")"
	 */
	public void visit(SizeofTypeName n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
		n.f3.accept(this, argu);
	}

	/**
	 * f0 -> "&"
	 *       | "*"
	 *       | "+"
	 *       | "-"
	 *       | "~"
	 *       | "!"
	 */
	public void visit(UnaryOperator n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> PrimaryExpression()
	 * f1 -> PostfixOperationsList()
	 */
	public void visit(PostfixExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> ( APostfixOperation() )*
	 */
	public void visit(PostfixOperationsList n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> BracketExpression()
	 *       | ArgumentList()
	 *       | DotId()
	 *       | ArrowId()
	 *       | PlusPlus()
	 *       | MinusMinus()
	 */
	public void visit(APostfixOperation n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "++"
	 */
	public void visit(PlusPlus n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "--"
	 */
	public void visit(MinusMinus n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "["
	 * f1 -> Expression()
	 * f2 -> "]"
	 */
	public void visit(BracketExpression n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "("
	 * f1 -> ( ExpressionList() )?
	 * f2 -> ")"
	 */
	public void visit(ArgumentList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
		n.f2.accept(this, argu);
	}

	/**
	 * f0 -> "."
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(DotId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> "->"
	 * f1 -> <IDENTIFIER>
	 */
	public void visit(ArrowId n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <IDENTIFIER>
	 *       | Constant()
	 *       | ExpressionClosed()
	 */
	public void visit(PrimaryExpression n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> "(" Expression() ")"
	 * 		| "(" CompundStatment() ")"
	 */
	public void visit(ExpressionClosed n, Integer argu) {
		n.f0.accept(this, argu);
	}

	/**
	 * f0 -> AssignmentExpression()
	 * f1 -> ( "," AssignmentExpression() )*
	 */
	public void visit(ExpressionList n, Integer argu) {
		n.f0.accept(this, argu);
		n.f1.accept(this, argu);
	}

	/**
	 * f0 -> <INTEGER_LITERAL>
	 *       | <FLOATING_POINT_LITERAL>
	 *       | <CHARACTER_LITERAL>
	 *       | ( <STRING_LITERAL> )+
	 */
	public void visit(Constant n, Integer argu) {
		n.f0.accept(this, argu);
	}

}
