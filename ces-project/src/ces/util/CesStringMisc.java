/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.util;

import imop.Main;
import ces.data.base.Counter;

import java.util.ArrayList;
import java.util.StringTokenizer;
import stringexp.CESString;
import stringexp.Minterms;
public class CesStringMisc {
	
	/***   STRing parser based functions ****/
	
	public static boolean isSingleIdentifier(String s) {
	//	Main.addlog("CESSTRING singleIdentifier "+ s);
		return CESString.isSingleIdentifier(s);
	}
	
	
	public static int getNumberofLiterals(String s) {
	//	Main.addlog("The number of tokens asked for string "+ s);
		return CESString.getasTokens(s).size();
	}
	
	
	 
	public static ArrayList<String> getStringTokens(String s) {
		return CESString.getasTokens(s);
	}
	

	public static String getTruthProbabilityEvaluated(String string, ArrayList<String> baseIterator, String max) {
		return CESString.getTruthProbabilityEvaluated(string, baseIterator, max);
	}
	
	
	
	
	
	
	
	
	
	
	/********************* String based functions ************/
	/**
	 * The function returns the highest degree term in the 
	 * counter c.
	 * @param c
	 * @return
	 */
	/*
	public static String getHighestdegreeterm(Counter c){
		c.reduce();
		if(c.getVarPart().isEmpty())
			return String.valueOf(c.getConstPart());
	//	Main.addlog("CESSTRING most significant Term "+ c.getVarPart());
		return CESString.mostSignificantTerm(c.getVarPart());
	}
	
	*/
	public static boolean islargerthan(String a,String c){
		
		if(a.isEmpty())
			return false;
		
		int mydegree = getdegree(a);
		int cdegree = getdegree(c);
		if(mydegree > cdegree)
			return true;
		if(cdegree > mydegree)
			return false;
		
		double mycoefficient = getmaxcoefficient(a);
		double ccoefficient = getmaxcoefficient(c);
		
		if(mycoefficient > ccoefficient)
			return true;
		if(ccoefficient > mycoefficient)
			return true;
		else
			return islargerthan(a.substring(a.indexOf('+')+1), c.substring(c.indexOf('+')+1));
	}
	
	
	
	
	/**
	 * Check if string represents a Number 
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isNumber(String s) {
	    return isNumber(s,10);
	}

	public static boolean isNumber(String s, int radix) {
	    if(s.isEmpty()) return false;
	    
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }if(s.charAt(i) == '.')
	        	continue;
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}
	
	/**
	 * The max variable degree.
	 * 
	 * @param s
	 * @return
	 */
	public static int getStringDegree(String s){
		StringTokenizer st = new StringTokenizer(s,"*");
		int degree = 0;
		while (st.hasMoreTokens()) {
			String cur = st.nextToken();
			if(!isNumber(cur))
				degree++;
		}
		return degree;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * 
	 * @param divdent : of the form sum of products.
	 * @param divisor
	 * @return
	 */
	
	public static String divideSOPString(String divdent, String divisor){
		String retVal = null;
		boolean started = false;
		StringTokenizer st = new StringTokenizer(divdent,"+");
		while (st.hasMoreTokens()) {
			String cur = st.nextToken();
			String partialq = dividestring(cur, divisor);
			if(partialq.isEmpty()){
				return new String();
			}else{
				if(!started){
					retVal = new String(partialq);
					started = true;
				}else
					retVal += " + " + partialq;
			}
		}	
		return retVal;
	} 
	
	
	
	public String getStringFromMinterms(ArrayList<Minterms> minterms) {
		String s = ""; 
		boolean plus = false;
		for (Minterms m : minterms) {
			if(plus) {
				String minStr = m.getString();
				if(!minStr.isEmpty())
					s += " + ("+  m.getString() + " ) ";
			} else {
				if(minterms.size() == 1) 
					s = m.getString();
				else {
					String minStr = m.getString();
					if(!minStr.isEmpty()) {
						s = " ( " + m.getString() + " ) ";
						plus = true;
					}

				}
			}
		}
		return s;
	}
	
	
	
	public static boolean isThreadMigrationfavourable(Counter s){
		s.reduce();
		if(s.getVarPart().isEmpty())
			return false;
		return true;
	}
	
	
	public static String dividestring(String divdent, String divisor){
		String retVal = null;
		StringTokenizer st = new StringTokenizer(divdent,"*");
		StringTokenizer div = new StringTokenizer(divisor,"*");
		
		ArrayList<String> allvalues = new ArrayList<String>();
		
		while (st.hasMoreTokens()) {
			allvalues.add(st.nextToken());
		}
		boolean found;
		while (div.hasMoreTokens()) {
			String cur = div.nextToken();
			 found = false;
			for (int i = 0; i < allvalues.size(); i++){
				if(allvalues.get(i).equals(cur)){
					allvalues.remove(i);
					found = true;
				}
				if(found == false){
					Main.addlog("Unable to divide the divisor and divident");
					Main.addlog("Divisor = "+ divisor + " and divident = " + divdent);
					return new String();
				}
			}
		}
		if(allvalues.isEmpty()) {
			return new String("1");
		}
		retVal = new String(allvalues.get(0));
		for (String val : allvalues) {
			retVal += "*" + val; 
		}
		return retVal;		
	}
	
	public static double getCoefficient(String s){
		StringTokenizer st = new StringTokenizer(s,"*");
		double coefficient  = 1;
		while (st.hasMoreTokens()) {
			String cur = st.nextToken();
			if(isNumber(cur)){
				coefficient = coefficient * Double.parseDouble(cur);	
			}
		}
		return coefficient;
	}
	
	/**
	 * highestdegree
	 * 
	 * @return
	 */
	public static int getdegree(String variablePart){
		if(variablePart.isEmpty())
			return 0;
		StringTokenizer dg = new StringTokenizer(variablePart, "+");
		return getStringDegree(dg.nextToken());
	}
	
	
	

	public static double getmaxcoefficient(String variablePart){
		if(variablePart.isEmpty())
			return 0;
		StringTokenizer dg = new StringTokenizer(variablePart, "+");
		return getCoefficient(dg.nextToken());
	}
	
	
	
	
	/**
	 * this function return three possible values
	 * 
	 * 1 : meaning the value of the term asymptotically increases when iterator increases.
	 * 0 : change unknown;
	 * -1: means the value of term asymptotically decreases when iterator changes.
	 * @param term
	 * @param iterator
	 * @return
	 * NOTE: We are just looking at sign now
	 */
	
	public static int isAsymptoticallyincreasing(String term,String iterator){
		if(CESString.findVariable(term, iterator) == false)
			return 0;
		String coeff = CESString.getCoefficient(term, iterator);
		if(CESString.isNegative(coeff))
				return -1;
		else
			return 1;
	}
	



	
	
	/*

	
	public static boolean isOperand(String s){
	    return (Character.isAlphabetic(s.charAt(0)) || (s.charAt(0) == '_'));
	}
	
	public static boolean isOperator(Object s){
		if(s.equals("*") || s.equals("+") || s.equals("-") || s.equals("/")){
			return true;
		}
		return false;
	}


	
	public static Counter reduceStringtoCounter(String S){
		
		return new Counter();
	}




*/
	
	
	
	
	
	
	
	/********************* STRING FUNCTIONS END HERE ****************************************/
	
	
	
	/******************************** Obsolute Functions ************************************/
	/**
	 * The variable occurring maximum in the equations 
	 * is termed as base variable.
	 * 
	 * 
	 * @param var
	 * @return
	 */
	/*
	public static String getBaseVar(String var){
		if(var.length() ==0)
			return null;
		HashMap<String, Integer> hmap = new  HashMap<String, Integer>();
		StringTokenizer st = new StringTokenizer(var," ");  
		while (st.hasMoreTokens()) {  
		      String cur   =  st.nextToken();
		      if(cur.equals("+") || cur.equals("(") || cur.equals(")")  || cur.equals("*")){
		    	continue;  
		      }
		      if(hmap.containsKey(cur)){
		    	  hmap.put(cur, hmap.get(cur)+1);
		      }else{
		    	  hmap.put(cur, 1);
		      }
		}  
		Map.Entry<String, Integer> baseVal = null;
		for (Map.Entry<String, Integer> itr : hmap.entrySet()) {
			if(baseVal == null || itr.getValue() > baseVal.getValue()){
				if(isNumber(itr.getKey())){
					continue;
				}else{
					baseVal = itr;
				}
			}
		}
		if(baseVal!=null)
			return baseVal.getKey();
		else
			return null;
	}
	
	/**
	 * 
	 * @param divdent: of the form sum of products.
	 * @param divisor: of the form sum of products.
	 * @return
	 * 
	 * eg : (A + B) / (C + D) = A/(C+D) + B/(C+D);
	 * 
	 * else
	 *   (4 A + 6 B) / (2A + 3 B ) = 2
	 *   
	 */
	/*
	public static String divideSOPStringSOP(String divident, String divisor){
		
	//	HashMap<String, Integer> hmap = new  HashMap<String, Integer>();
		StringTokenizer st = new StringTokenizer(divisor,"+");  
		String soln = new String();
		Scanner scan = new Scanner(System.in);
		while (st.hasMoreTokens()) {  
		//      String cur   =  st.nextToken();
		      System.err.println("The divident is :"+ divident);
		      System.err.println("The divisor is :"+ divisor);
		      soln = scan.next();    
		}
	    scan.close();
		return soln;
	}
	*/
	
	
	/*
	
	public static String addTwoTerms(String mst, String cur) {

//		Main.addlog("CESSTRING coefficient "+ mst);
		double coeff1 = CESString.getCoefficient(mst);
//		Main.addlog("CESSTRING coefficient "+ cur);
		double coeff2 =  CESString.getCoefficient(cur);
		double sum =  coeff1 + coeff2;
		String s = String.valueOf(sum);
		ArrayList<String> varlist = CESString.getvarlistinTerm(mst);
		for (String var : varlist) {
			s += " * " + var;
		}
		return s;
	}

	
	
	public static String getMostSignificantTermWithVariable(String data,String var) {
		
		
		ArrayList<String> tokenString = CESString.getTokensofOuterExpression(data);
		ArrayList<ArrayList<String>> list = new ArrayList<>();
		ArrayList<String> curlist = new ArrayList<String>(); 
		ArrayList<String> mst  = new ArrayList<String>(); 
		for (int i = 0; i < tokenString.size(); i++) {
			String cur = tokenString.get(i);
			if(isOperand(cur)){
				curlist.add(cur);
				if(i+1 == tokenString.size()){
					list.add(curlist);
				}else if(!(tokenString.get(i+1).equals("*")|| tokenString.get(i+1).equals("/"))){
					list.add(curlist);
					curlist = new ArrayList<String>();
					if(!isOperand(tokenString.get(i+1))){
						i++;
					}
				}else{
					if(tokenString.get(i+1).equals("*")|| tokenString.get(i+1).equals("/")){
						curlist.add(tokenString.get(i+1));
					}
				}
			}
		}
		
		for (ArrayList<String> cl : list) {
			boolean found = false;
			for (String cur : cl) {
				if(cur.equals(var)){
					found = true;
					break;
				}
			}
			if(found && cl.size() > mst.size()){
				mst = cl;
			}
		}
		String mstStr = "";
		for (String m : mst) {
			mstStr += m;
		}
		return mstStr;
		// return CESString.mostSignificantTermWIthVariable(data, var);
	}
	
	*/
	public static String getAdduntant(String string, String iteration) {
		if(string.trim().equals(iteration.trim()))
			return "1";
		else if(string.contains(iteration)) {
			return CESString.getCoefficient(string, iteration);
		} 
		
		Main.addErrorMessage("Unknown comparision " + string + " with iterator " + iteration);
		return "1"; // Conservative approach ?
	}


	public static String addTwoTerms(String mst, String cur) {
		 String tot = mst + " + " + cur;
		 ArrayList<Minterms> list = reduce(tot);
		 
		 boolean started = false;
		 String reduced = "";
		 for(Minterms l : list) {
			 if(started)
				 reduced += " + " + l.getString();
			 else
				 reduced += l.getString();
		 }
		 
		 return reduced;
	}
	
	
	
	public static ArrayList<Minterms> reduce(String s) {
		/**
		 * Our strings can contain sizeof functions which in turn might be 
		 * trying to get the size of pointers which will contain
		 * a dangling * at the end of variable
		 * eg: sizeof(void *)
		 * 
		 * Even though functions are accepted by our parser these
		 * dangling pointers are not accepted.
		 * Also sizeof variables can be replaced by actual values 
		 * size of char : 1
		 * size of bool : 1
		 * size of pointers : 4
		 * size of int : 4
		 * size of size_t : 4
		 */
		while(s.contains(SystemStringConstants.sizeoffunctionName)) {
			int index = s.indexOf(SystemStringConstants.sizeoffunctionName);
			int start = s.indexOf("(", index);
			boolean found  = false;
			int balanced = 0;
			int end = start;
			do{
				if(s.charAt(end) == '(') {
					found  =true;
					balanced ++;
				} else if(s.charAt(end) == ')'){
					balanced --;
				}
				end ++;
			} while (!found || balanced !=0);
			
			String function =  s.substring(start+1, end-1);
			function = function.trim();
			
			String replacementString = "4";
			
			if(function.charAt(function.length()-1) == '*' || 
			   function.contains("size_t") ||
			   function.contains("int")) {
				replacementString = "4";
			} else if (function.contains("char")) {
				replacementString = "1";
			}
			
			String sizeoffunction = s.substring(index, end);
			s = s.replace(sizeoffunction, replacementString);
			
			/**
			 * TODO size of array
			 * 
			 */
		}
		/**
		 * We also do not allow casting in function.
		 * TODO remove casting. 
		 */
		s = s.replace("( int )", ""); 
		
		Main.addlog("To Reduce " + s);
		return CESString.Reduce(s);
	}
	
	
	public static String getReducedString(String s) {
		  ArrayList<Minterms> list = reduce(s);
		 
		 boolean started = false;
		 String reduced = "";
		 for(Minterms l : list) {
			 if(started) {
				 String str = l.getString();
				 if(! str.isEmpty())
					 reduced += " + ( " + l.getString() + ") ";
			 } else {
				 if(list.size() == 1)
					 reduced = l.getString();
				 else {
					 String lt = l.getString();
					 if(!lt.isEmpty()) {
						 reduced = " ( " + l.getString() + " ) ";
						 started = true;
					 }
				 }
			 }
		 }
		 
		 if(reduced.contains("+"))
			 reduced = "(" + reduced + ")";
		 
		 return reduced;
		
	}


	public static double getscale(String string) {
		ArrayList<Minterms> minterms = reduce(string);
		assert(minterms.size() > 0);
		for(int i=0;i < minterms.size(); i++) {
			Minterms m = minterms.get(i);
			 if(m.getConstant() != 0)
				 return (1/m.getConstant());
		}
		return 1;
	}


	public static String scale(double scale, String string) {
		Counter count = new Counter(string);
		count.multiply(String.valueOf(scale));
		count.reduce();
		String s = count.getdata();
		return s;
	}


	public static String removeExtraTokens(String str) {
		String newString = "";
		int level = 0;
		int i=0;
		for(; i< str.length();i++) {
			if((str.charAt(i) == ')' || str.charAt(i) == ']')) {
				if(level == 0)
					break;
				else
					level --;
			} if((str.charAt(i) == '(' || str.charAt(i) == '[')) {
				level ++;
			}
		}
		newString = str.substring(0, i);
		return newString;
	}


	
	/**
	 * Multiply two product forms
	 * 
	 * @param multiplicant
	 * @param multiplier
	 * @return
	 */
	/*
	public static ArrayList<String> multiply(ArrayList<String> multiplicant, String multiplier){
		ArrayList<String> pp = new ArrayList<String>();
		for (String m : multiplicant) {
			String s = new String(multiplier + "*" + m);
			pp.add(s);
		}
		return pp;
	}
	
	*/
	
}