/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.util;

import java.util.*;

import imop.Main;
import imop.ast.info.FunctionDefinitionInfo;
import imop.ast.info.RootInfo;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.data.base.Counter;
import ces.data.base.TotalIterations;
import ces.data.constructData.EmbeddedParallelRegions;
import ces.data.constructData.ForWorkload;
import ces.data.constructData.OMPFunctionWorkload;
import ces.data.constructData.OMPParallelWorkload;
import ces.data.constructData.OMPWorkload;
import ces.data.constructData.WhileWorkload;
import ces.data.node.ArrayExpression;
import ces.getter.*;
import imop.lib.analysis.dataflow.Definition;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.getter.*;
import imop.lib.util.Misc;
import imop.parser.CParser;
import imop.lib.analysis.type.*;
import imop.lib.cg.CallSite;

public class CesSpecificMisc {
	
	public enum copyType{
		noCopy,
		copyin,
		copyout,
		INVALID
	};
	
	
	/**
	 * Function is added for handling extracting reduction clause
	 * from For construct before its transformation.
	 * A private variable is created for reach Thread whose initial
	 * value will depend on the operation.
	 * +	0
	 * *	1
	 * -	0
	 * &	~0
	 * |	0
	 * ^	0
	 * &&	1
	 * ||	0
	 * @param symbol : operation
	 * @return initial value.
	 */
	
	public static String getInitialValueforTheSymbol(String symbol){
		
		if(symbol.equals("+") ||
		   symbol.equals("-") ||
		   symbol.equals("|") ||
		   symbol.equals("^") ||
		   symbol.equals("||")){
			return "0";
		}else if(symbol.equals("*") ||
				 symbol.equals("&&")) {
			return "1";
		}else if(symbol.equals("&")) {
			return "~0";
		}else{
			Main.addErrorMessage(" Unexpected symbol found in the reduction clause" + symbol);
			return "0";
		}
	} 
	
	
	
	/**
	 * Returns a set of all assignment operations in the tree rooted at Node n. 
	 * 
	 **/
	public static HashSet<AssignmentExpression> getAllAssignmentExpressionsinNode(Node n) {
		AssignmentExpressionGetter exprGetter = new AssignmentExpressionGetter();
		n.accept(exprGetter);
		return exprGetter.getAllAssignmentExpressions();
	}
	
	
	public static boolean sizeofAGreaterthanB(Node A, Node B) {
		CodeSizeGetter cgsA = new CodeSizeGetter();
		A.accept(cgsA);
		CodeSizeGetter cgsB = new CodeSizeGetter();
		B.accept(cgsB);
		if(cgsA.getSize() > cgsB.getSize())
			return true;
		return false;
	}
	
	
	/***
	 * Returns all ArrayExpressions in the code rooted at n.
	 * to distinguish between reads and writes look into {@link ArrayExpression}.
	 */
//	public static ArrayList<ArrayExpression> getAllArrayExpressions(Node n) {
//		Write exprGetter =  new ArrayExpressionGetter();
//		n.accept(exprGetter);
//		return exprGetter.getAllArrayExprssions();
//	}
//	
	
	
	public static ArrayList<String> getVarList(VariableList s) {
		ArrayList<String> privateVars = new ArrayList<String>();
		privateVars.add(s.f0.tokenImage);
		Vector<Node> nlo = s.f1.nodes;
		for (int i = 1; i < nlo.size(); i+=2) {
			privateVars.add(nlo.get(i).toString());
		}
		return privateVars;
	} 
	
	
	
	

	public static ArrayList<ArrayExpression> getAllWriteExpressions(Node n) {
		ArrayExpressionWritesGetter exprGetter = new ArrayExpressionWritesGetter();
		n.accept(exprGetter, 0);
		return exprGetter.getAllArrayExpressions();
	}
	
	public static ArrayList<ArrayExpression> getAllReadExpressions(Node n) {
		ArrayExpressionReadsGetter exprGetter = new ArrayExpressionReadsGetter();
		n.accept(exprGetter, 0);
		return exprGetter.getAllArrayExpressions();
	}
	

	
	
	/**
	 * A counter that generates unique number every time its called. 
	 * 
	 */
	
	private static int counter = 0;
	public static int getToken(){
		return counter++;
	} 
	
	/**
	 * Checks if argument ancestor is an ancestor of argument child.
	 * @param ancestor
	 * @param child
	 * @return
	 */
	public static boolean isancestorOf(Node ancestor,Node child){
		if((child.parent == null) || (child instanceof FunctionDefinition)){
			return false;
		}
		if(child.parent == ancestor)
			return true;
		else{
			return isancestorOf(ancestor, child.parent);
		}
		
	}
	
	
	
	/**
	 * We are looking upwards only until we find nodes will multiple children.
	 * Returns an ancestor in hierarchy until we reach a list/Node list/ Node list optional
	 * @param className : the ancestor Node class name
	 * @param child : the child node whose ancestor we need to find.
	 * @return
	 */
	public static Node getAncestorOfType(String className, Node child) {
		assert(! className.equals(NodeList.class.getName()) && 
				! className.equals(NodeListOptional.class.getName()) &&
				! className.equals(NodeSequence.class.getName()));
		Node cur = child;
		while(! cur.getClass().getName().equals(className) ) {
			if( className.equals(NodeList.class.getName()) || 
				className.equals(NodeListOptional.class.getName()) ||
			    className.equals(NodeSequence.class.getName()))
				return null;
			cur = cur.parent;
		}
		return cur;
	}
	
	/**
	 * 
	 * @param root : root of AST search Tree.
	 * @param childClassName
	 * @return list of Nodes that of the given class that are decendant of the given node.
	 */
	public static ArrayList<Node> collectAllChildrensofClass(Node root, String childClassName) {
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		root.accept(nG, childClassName);
		
		return nG.getASTNodes();
	}
	
	

	
	
	
	
	public static String getTypeString(String varName,FunctionDefinition enclosingFunction){
		return "int";
	}
	
	
	
	public static FunctionDefinition getFunctionDefinitionofName(Node root, String functionName) {
		Vector<FunctionDefinition> defs = ((RootInfo)root.getInfo()).getAllFunctionDefinitions();
		FunctionDefinition retVal = null;
		for(FunctionDefinition d : defs) {
			FunctionDefinitionInfo info = (FunctionDefinitionInfo) d.getInfo();
			if(info.getFunctionName().equals(functionName)) {
				retVal = d;
				break;
			}
		}
		return retVal;
	}

	
	
	
	/**
	 * If the current parallel construct has nowait clause it will return true.
	 * @param pc : parallelconstruct
	 * @return
	 */
	public static boolean hasNowaitClause(OmpConstruct pc) {
		Vector<Node> clauseList = null;
		if (pc instanceof ParallelConstruct) {
		     clauseList = ((ParallelConstruct)pc).f1.f1.f0.nodes;
		} else if (pc instanceof ParallelForConstruct) {
			clauseList = ((ParallelForConstruct)pc).f3.f0.nodes;
		} else if (pc instanceof ParallelSectionsConstruct) {
			clauseList = ((ParallelSectionsConstruct)pc).f3.f0.nodes;
		} else if (pc instanceof ForConstruct) {
			clauseList  = ((ForConstruct)pc).f1.f1.f0.nodes;
			for (Node clause : clauseList) {
				Node directive = ((AUniqueForOrDataOrNowaitClause)clause).f0.choice; 
				if(directive instanceof NowaitClause){
					return true;
				}
			}
			return false;
		} else if(pc instanceof SectionsConstruct) {
			clauseList = ((SectionsConstruct)pc).f2.f0.nodes;
		} else if(pc instanceof SingleConstruct) {
			clauseList = ((SingleConstruct)pc).f2.f0.nodes;
			for (Node clause : clauseList) {
				Node directive = ((ASingleClause)clause).f0.choice;
				if(directive instanceof NowaitClause){
					return true;
				}
			}
		} else{
			Main.errWriter.println("The nowait check not handled for the construct "+ pc.getClass());
		}
		
		for (Node clause : clauseList) {
			Node directive = ((AUniqueParallelOrDataClause)clause).f0.choice;
			if(directive instanceof NowaitClause){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * This function returns true if the tree rooted at node
	 * has a parallel construct inside.
	 * @param node
	 * */
	public static boolean hasParallelConstruct(Node node){
		ParallelConstructGetter pcg = new ParallelConstructGetter();
		node.accept(pcg);
		if(pcg.getList().size()!= 0)
			return true;
		return false;
	}	
	
	
	/**
	 * This returns the Number of parallel tasks (N) in OMP For
	 * 
	 */
	
	public static TotalIterations getNoofParallelIterationsinOmpFor(OmpForHeader h) {
		String startPointStr= h.f2.f2.getInfo().getBareMinimumString();
		Node startPoint = h.f2.f2;
		NodeChoice forConditionChoice = h.f4.f0;
		NodeChoice  reinitExprChoice = h.f6.f0;
		String str = "";
		
		Node lowerLimit = null;
		Node upperLimit = null;
		
		if(reinitExprChoice.choice instanceof  ShortAssignPlus  ||
		   reinitExprChoice.choice instanceof  ShortAssignMinus ||
		   reinitExprChoice.choice instanceof  OmpForAdditive   ||
		   reinitExprChoice.choice instanceof  OmpForSubtractive||
		   reinitExprChoice.choice instanceof  OmpForMultiplicative) {
			Main.addErrorMessage("The reinit expression for ofr constrauct not handled here"+ reinitExprChoice.choice.toString());
		}
		
		if(forConditionChoice.choice instanceof OmpForLTCondition) {
			OmpForLTCondition cond =  (OmpForLTCondition)forConditionChoice.choice;
			str = "(" + cond.f2.getInfo().getBareMinimumString() + "  "+"- "+ startPointStr + ")";
			
			lowerLimit = startPoint;
			upperLimit = cond.f2;
			
			
		} else if(forConditionChoice.choice instanceof OmpForLECondition) {
			OmpForLECondition cond =  (OmpForLECondition)forConditionChoice.choice;
			str = "(" + cond.f2.getInfo().getBareMinimumString() + "  "+"- "+ startPointStr + " + 1 )"; 
			
			lowerLimit = startPoint;
			upperLimit = cond.f2;
			
			
		} else if(forConditionChoice.choice instanceof OmpForGTCondition) {
			OmpForGTCondition cond =  (OmpForGTCondition)forConditionChoice.choice;
			str = "(" + startPointStr + "  "+"- "+ cond.f2.getInfo().getBareMinimumString() + ")";
			
			lowerLimit = cond.f2;
			upperLimit = startPoint;
			
		}else if(forConditionChoice.choice instanceof OmpForGECondition) {
			OmpForGECondition cond =  (OmpForGECondition)forConditionChoice.choice;
			str = "(" + startPointStr + "  "+"- "+ cond.f2.getInfo().getBareMinimumString() + " + 1 )"; 
			
			lowerLimit = cond.f2;
			upperLimit = startPoint;
			
		} else {
			Main.addErrorMessage("Unkown for condition "+ forConditionChoice.choice.toString());
		}
		
		assert( upperLimit != null && lowerLimit !=  null);
		
		TotalIterations retVal =  new TotalIterations(upperLimit, lowerLimit, str);
		
		return retVal;
	}
	
	
	/***
	 * This function would return true if the called function is called inside
	 * a parallel construct.
	 * @param node
	 * @return
	 */
	public static boolean isinsideParallelConstruct(Node node){
		if((node instanceof FunctionDefinition) || (node instanceof TranslationUnit))
			return false;
		else if((node.parent instanceof ParallelConstruct) || 
				(node.parent instanceof ParallelForConstruct) ||
				(node.parent instanceof ParallelSectionsConstruct)){
			return true;
		}else
			return isinsideParallelConstruct(node.parent);			
	}
	
	/*
	 * This Function purges all duplicate entries from the vector of Symbols given.
	 * */
	public static boolean purgeDuplicateSymbols(Vector<Symbol> list){
		int i =1; 
		
		while(i<list.size()){
			Symbol cur = list.get(i);
			boolean found  = false;
			for (int j = 0; j < i; j++) {
				if(list.get(j).getName().equals(cur.getName())){
					found = true;
					break;
				}
			}
			if(found){
				list.remove(i);
			}
		}
		
		return false;
	}
	/**
	 * Generate new unique name for string replace.
	 * @param curvar
	 * @return __ces_<curvar>
	 */
	public static String GenerateReplaceName(String curvar,FunctionDefinition enclosingFunction){
		
		return (SystemStringConstants.cesTag + curvar);
	}
	
	
	/**
	 * Generate new unique name for string replace.
	 * @param curvar
	 * @return __ces_<curvar>
	 */
	public static String GenerateReplaceName(String curvar){
		return (SystemStringConstants.cesTag + curvar);
	}
	
	public static String getNewVariableName() {
		return (SystemStringConstants.cesTag + getToken());
	}
	
	/**
	 * returns the varaible type : int bool char etc
	 * @param varName
	 * @param enclosingFunction
	 * @return
	 */
	
	public static String getTypeStringWithoutAnySpecifiers(String varName,Node node){
		
		Node enclosingNode = node;
		Symbol curSymbol = null;
		curSymbol =  Misc.getSymbolEntry(varName, enclosingNode);
		TypeSpecifier typespecifier = null;
		/***
		 * We want only the type and not any extra declaration(like static, external) 
		 * etc of the original variable. since we are using this to just create 
		 * a local temporary copy of the variable.
		 */
		
		if(curSymbol == null)
			Main.addErrorMessage("The Symbol for "+ varName + " is not found in any of the symbol table");
		else{
			NodeList varDecList = curSymbol.getType().declarationSpecifiers.f0;
			for (Node n : varDecList.nodes) {
				ADeclarationSpecifier decspec = (ADeclarationSpecifier) n;
				if(decspec.f0.choice instanceof TypeSpecifier){
					typespecifier = (TypeSpecifier) decspec.f0.choice;
					break;
				}
			}
		}
		if(typespecifier == null){
			Main.addErrorMessage("Could not find the declaration of the variable "+ varName);
			return "int";
		}else
			return typespecifier.f0.choice.toString();
	}
	
	/**
	 * This function return The first Function Declared iour file 
	 * @param root : Translation Unit
	 * @return
	 */
	
	public static Node getFirstExternalDeclaration(TranslationUnit root){
		Vector<Node> nodelist = root.f0.nodes;
		for (Node n : nodelist) {
			if(n instanceof ElementsOfTranslation){
				NodeChoice nC = (NodeChoice) ((ElementsOfTranslation)n).f0; 
				if(nC.choice instanceof  ExternalDeclaration){
					return n;
				}
			}
		}
		return null;
	}
	
	
	/**
	 * Find and replace all instances of variable curVar under root with
	 * replaceVar 
	 * @param root
	 * @param curVar
	 * @param enclosingFunction
	 * @param copyType : specifies if initialization or copying required.
	 * @param repalcementVariable : The replacement variable name
	 * @param haveInitilization : add initialization statement inside the scope.
	 * 							: true add Intialization statment
	 * 							: else skip.
	 */
	/*
	public static void findandReplaceVariable(Node root,String curVar, 
			FunctionDefinition enclosingFunction, copyType t,
			String replacementVariable,boolean haveInitilization){
		String replaceVar = replacementVariable;
		if(replacementVariable == null)
			replaceVar = GenerateReplaceName(curVar);
		if(haveInitilization){
			String initializationString  = new String();
			String typeString = getTypeString(replaceVar, enclosingFunction);
			if(t == copyType.copyin){
				initializationString =  typeString + " " + replaceVar + " = " + curVar + ";" ;		
			}else{
				initializationString =  typeString + " " + replaceVar + ";";
			}
			Node initStatement = CParser.createCrudeASTNode(initializationString, Declaration.class);
			CompoundStatement cst = (CompoundStatement) ((Statement)(root)).f0.choice;
			cst.f1.nodes.add(0, initStatement);
		}
		
		NodeTokenGetter nodeGetter = new NodeTokenGetter(curVar);
		root.accept(nodeGetter);
		Vector<NodeToken> nodeList = nodeGetter.getList();
		for (NodeToken nt : nodeList) {
			nt.tokenImage = replaceVar;
		}
		
	//	Misc.dumpCurrentProgramSnippet(enclosingFunction);
		
	} 
	*/
	/**
	 * Find and replace all instances of variable curVar under root with
	 * replaceVar
	 * @param root
	 * @param curVar
	 * @param enclosingFunction
	 * @param copyType : specifies if initialization or copying required.
	 * @param init : initialise the variable inside root.
	 * @return replacement string. 
	 */
	public static String findandReplaceVariable(Statement root,Node curVar, 
			FunctionDefinition enclosingFunction, copyType t, boolean init ){
		if(t == copyType.INVALID)
			return "";
		String replaceVar = GenerateReplaceName(curVar.toString(),enclosingFunction);
		String initializationString  = new String();
		String typeString = getTypeStringWithoutAnySpecifiers(curVar.toString(), curVar);
		if(t == copyType.copyin){
			initializationString =  typeString + " " + replaceVar + " = " + curVar + ";" ;		
		}else{
			initializationString =  typeString + " " + replaceVar + ";";
		}
		
		/**
		 * get all node tokens 
		 * and replace the curVar with replaceVar
		 * */
		NodeTokenGetter nodeGetter = new NodeTokenGetter(curVar.toString());
		root.accept(nodeGetter);
		Vector<NodeToken> nodeList = nodeGetter.getList();
		for (NodeToken nt : nodeList) {
			nt.tokenImage = replaceVar;
		}
		
		/***
		 * Big issue here
		 * TODO: Should find some other fix
		 * 
		 * The #pragma skip statements might contain references to the variables that is to be 
		 * replaced. In such cases we need to replace the variables in the pragmas also
		 * with the new variable names. 
		 * 
		 * For more refer to kosaraju 
		 * 
		 * This is a string based manipulation which will bring in more errors.
		 */
		
		ArrayList<Node> pragmaList  = collectAllChildrensofClass(root,UnknownPragma.class.getName());
		String str = curVar.toString().trim();
		for(Node n : pragmaList) {
			UnknownPragma pragmaStmt = (UnknownPragma) n;
			pragmaStmt.f2.tokenImage = pragmaStmt.f2.tokenImage.replaceAll(str, replaceVar);
		}
		
		
		
		
		/*
		 * Add the declaration statement into the list.
		 */
		if(init) {
			Node initStatement = CParser.createCrudeASTNode(initializationString, Declaration.class);
			CompoundStatement cst=null;
			if(root.f0.choice instanceof CompoundStatement)
				cst = (CompoundStatement) root.f0.choice;
			else{
				cst = ((CompoundStatement) CParser.createCrudeASTNode("{}",CompoundStatement.class));
				cst.f1.nodes.add(root.f0.choice);
				root.f0.choice = cst;
			}	
			cst.f1.nodes.add(0, initStatement);
		}
		return replaceVar;
		
	} 
	
	
	
	
	/**
	 * Returns the node from the nodelistclass which is a
	 * parent to the node n. Basically we are searching for the node
	 * "nlNode" which contains n and is present in nl.
	 * @param nl : The nodelist class we need to search in
	 * @param n : The node we are searching for.
	 * @return
	 */
	public static Node getEnclosingNodeFromNodeList(NodeListClass nl,Node n){
		Node nlNode = null;
		IsParentNode isParent = new IsParentNode(n);
		for (int i = 0; i < nl.size(); i++) {
			Node curNode = nl.elementAt(i);
			curNode.accept(isParent);
			if(isParent.isParent())
				return curNode;
		}
		return nlNode;
	}
	 
	
	/**
	 * Return the Map of counter;
	 * @param exp

	 */
	public static HashMap<Operator, Integer> countOperator(Expression exp) {
		OperatorCounter opCounter = new OperatorCounter();
		exp.accept(opCounter);
		return opCounter.operatorCounter;
	}	
	
	/**
	 * Return the Map of counter;
	 * @param exp

	 */
	public static HashMap<Operator, Integer> countOperator(OmpForReinitExpression exp) {
		OperatorCounter opCounter = new OperatorCounter();
		exp.accept(opCounter);
		return opCounter.operatorCounter;
	}

	/**
	 * Prints the map of operator to its count.
	 * @param countMap
	 */
	public static void printOperatorCount(HashMap<Operator, Integer> countMap) {
		for(Operator op: countMap.keySet()) {
			Main.addlog(op + "\t: " + countMap.get(op));
		}
	}
	
	
	
	/*************************Counter Based Function *****************/

	
	
	public static boolean llargerthanr(Counter a,Counter c){
//		if(!a.isReduced())
			a.reduce();
//		if(!c.isReduced())
			c.reduce();
		if(a.isconstant() && c.isconstant())
			return (a.getConstPart()> c.getConstPart());
		else
			CesStringMisc.islargerthan(a.getVarPart(), c.getVarPart());
		return false;
	}
	
	
	
	
	public static boolean foundVariable(Counter c,String v){
		StringTokenizer st = new StringTokenizer(c.getString()," +()*");  
		while (st.hasMoreTokens()) {  
		      String cur   =  st.nextToken();
		      if(cur.equals(v))
		    	  return true;
		}  
		
		return false;
	}
	
	
	/*
	 * get ratio of Counter t/b.
	 * 
	 */
	/*
	public static String getRatio(Counter t, Counter b){
		String tms = CesStringMisc.getHighestdegreeterm(t);
		String bms = CesStringMisc.getHighestdegreeterm(b);
		return CesStringMisc.dividestring(tms,bms);
	}
	*/
	
	private static final int monotonicdecrease = 1;
	private static final int monotonicincrease = 2;
	private static final int exponentialincrease =1;
	private static final int linearincrease =1;
	
	
	public static TotalIterations getWhileIterations(WhileStatement w) {
		/*
		 * We are taking the following assumptions while calculating the 
		 * number of iterations of while loop. 
		 * 1. The iterator is initialised in the immediately preceding 
		 * Node.
		 * 2. The iterator is updated in only one of the statements 
		 * inside while loop.
		 * 3. The while loop does not contain any break, return.
		 * .
		 * If any of these fails we return infinity.
		 */
		boolean fail = false;
		
		
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		w.accept(nG, ReturnStatement.class.getName());
		//w.accept(nG, ContinueStatement.class.getName());
		w.accept(nG, BreakStatement.class.getName());
		
		if(nG.getASTNodes().size() > 0) 
			fail = true;
		
		
		Vector<Node> nl = Misc.getContainingList(w);
		Node wr = Misc.getWrapperInList(w);
		int pos = Misc.getIndexInContainingList(wr);
		if(pos == 0)
			return new TotalIterations(SystemStringConstants.INTSIZE);
		
		Node initial = nl.elementAt(pos-1);
		nG.reset();
		initial.accept(nG, NonConditionalExpression.class.getName());
		if(nG.getASTNodes().isEmpty()) {
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		
		
		NonConditionalExpression init =  (NonConditionalExpression) nG.getASTNodes().get(0);
		Expression exp = w.f2;
		nG.reset();
		exp.accept(nG, RelationalExpression.class.getName());
		RelationalExpression r = (RelationalExpression) nG.getASTNodes().get(0);
		
		String itr =  r.f0.getInfo().getBareMinimumString();
		Main.addlog("Iterator is   "+ itr + " While loop is " + w.getInfo().getString());
		String itrString = init.f0.getInfo().getBareMinimumString(); 
		String initValueStr = null;
		Node initValue = null;
		if(itrString.equals(itr)) {
			initValueStr = init.f2.getInfo().getBareMinimumString();
			initValue = init.f2;
		}
		
		int condition = 0;
		int incrementrelation = 0;
		
		nG.reset();
		
	//	Main.addlog(r.getInfo().getString());
		
		exp.accept(nG, RelationalLTExpression.class.getName() );
		exp.accept(nG, RelationalGTExpression.class.getName());
		exp.accept(nG, NonEqualExpression.class.getName());
		
		if(nG.getASTNodes().isEmpty()) {
			Main.addErrorMessage("The while condition not handled "+ w.f2.getInfo().getBareMinimumString());
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		
		TotalIterations itrs;
		
		String s = null;
		
		
		if(nG.getASTNodes().get(0).getClass() == RelationalGTExpression.class) {
			if(initValueStr != null)
				s = initValueStr;
			else
				s = SystemStringConstants.INFINITY;
			condition = monotonicdecrease;
		} else if (nG.getASTNodes().get(0).getClass() == RelationalLTExpression.class) {
			// we assume != has a monotolically
			// reducing node.
			condition = monotonicincrease;
			s = ((RelationalLTExpression)nG.getASTNodes().get(0)).f1.getInfo().getBareMinimumString();
		} else if (nG.getASTNodes().get(0).getClass() == NonEqualExpression.class) {
			condition = monotonicincrease;
			if(initValueStr != null)
				s = initValueStr;
			else
				s = " 0";
		}
		
		
		nG.reset();
		w.f4.accept(nG, NonConditionalExpression.class.getName());
		String updateExpr = "1";
		ArrayList<Node> assignlist = nG.getASTNodes();
		for(Node node : assignlist) {
			NonConditionalExpression nce = (NonConditionalExpression) node;
			Main.addlog(nce.f2.getInfo().getBareMinimumString());
			if(nce.f0.getInfo().getBareMinimumString().equals(itr)) {
				if(nce.f2.getInfo().getBareMinimumString().contains("/") ||
				   nce.f2.getInfo().getBareMinimumString().contains("<<") ||
				   nce.f2.getInfo().getBareMinimumString().contains(">>") ){
					if(incrementrelation < exponentialincrease)
						incrementrelation = exponentialincrease;
				} 
				String nceStr = nce.f2.getInfo().getBareMinimumString();
				if(nceStr.contains("+") ) {
					updateExpr = nceStr.substring(nceStr.indexOf('+')+1);
							if(incrementrelation < linearincrease)
								incrementrelation = linearincrease;
				} else if ( nceStr.contains("-")	) {
					updateExpr = nceStr.substring(nceStr.indexOf('-')+1);
						if(incrementrelation < linearincrease)
							incrementrelation = linearincrease;
				}
			} 
		}

		if(incrementrelation == 0) {
			Main.addErrorMessage("Increment relation not handled " + w.getInfo().getBareMinimumString());
			updateExpr = "1";
		}
		
		
		if(incrementrelation == 0 || condition == 0) {
			fail = true;
		}
		
		if(incrementrelation == 0 && (condition == 2) ) {
			fail = false;
		}
		
		if(incrementrelation == exponentialincrease) {
			s =  SystemStringConstants.INTSIZE;
		}
		
		if(incrementrelation == linearincrease  && !( (updateExpr.equals("1 ") || updateExpr.equals("1")
				|| updateExpr.equals(" 1 ")))) {
			s = " ( (" + s + " ) / " + updateExpr +" )";
		}else if( CesStringMisc.getNumberofLiterals(s.trim()) > 1) {
			s = "( " + s + " ) ";
		}
		
		
		if(fail) {
			s = SystemStringConstants.INFINITY;
		}
		assert(s != null);	
		
		itrs = new TotalIterations(s);
		
		return itrs;
	}
	
	
	public static TotalIterations getWhileIterations(DoStatement w) {
		
		/*
		 * We are taking the following assumptions while calculating the 
		 * number of iterations of while loop. 
		 * 1. The iterator is initialised in the immediately preceding 
		 * Node.
		 * 2. The iterator is updated in only one of the statements 
		 * inside while loop.
		 * 3. The while loop does not contain any break, return.
		 * .
		 * If any of these fails we return infinity.
		 */
		
	//	Main.addlog(w.getInfo().getString());
		boolean fail = false;
		String s = null;
		
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		w.accept(nG, ReturnStatement.class.getName());
		//w.accept(nG, ContinueStatement.class.getName());
		w.accept(nG, BreakStatement.class.getName());
		
		if(nG.getASTNodes().size() > 0) 
			fail = true;
		
		
		Vector<Node> nl = Misc.getContainingList(w);
		Node wr = Misc.getWrapperInList(w);
		int pos = Misc.getIndexInContainingList(wr);
		Node initial = nl.elementAt(pos-1);
		nG.reset();
		initial.accept(nG, NonConditionalExpression.class.getName());
		if(nG.getASTNodes().isEmpty()) {
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		
		
		NonConditionalExpression init =  (NonConditionalExpression) nG.getASTNodes().get(0);
		Expression exp = w.f4;
		nG.reset();
		exp.accept(nG, RelationalExpression.class.getName());
		RelationalExpression r = (RelationalExpression) nG.getASTNodes().get(0);
		
		String itr =  r.f0.getInfo().getBareMinimumString();
		String checkStr = init.f0.getInfo().getBareMinimumString();
		if(checkStr.equals(itr) == false) {
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		String initValue = init.f2.getInfo().getBareMinimumString();
		
		int condition = 0;
		int incrementrelation = 0;
		
		nG.reset();
		
	//	Main.addlog(r.getInfo().getString());
		
		exp.accept(nG, RelationalLTExpression.class.getName() );
		exp.accept(nG, RelationalGTExpression.class.getName());
		exp.accept(nG, NonEqualExpression.class.getName());
		
		if(nG.getASTNodes().isEmpty()) {
			Main.addErrorMessage("The do while condition not handled " + w.f4.getInfo().getString());
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		
		
		
		if(nG.getASTNodes().get(0).getClass() == RelationalGTExpression.class) {
			s = initValue;
			condition = monotonicdecrease;
		} else if (nG.getASTNodes().get(0).getClass() == RelationalLTExpression.class) {
			// we assume != has a monotolically
			// reducing node.
			condition = monotonicincrease;
			s = ((RelationalLTExpression)nG.getASTNodes().get(0)).f1.getInfo().getBareMinimumString();
		} else if (nG.getASTNodes().get(0).getClass() == NonEqualExpression.class) {
			condition = monotonicincrease;
			s = initValue;
		}
		
		
		nG.reset();
		w.f1.accept(nG, NonConditionalExpression.class.getName());
		String updateExpr = "1";
		ArrayList<Node> assignlist = nG.getASTNodes();
		for(Node node : assignlist) {
			NonConditionalExpression nce = (NonConditionalExpression) node;
			String condString = nce.f0.getInfo().getBareMinimumString();
			Main.addlog(condString);
			String rest = nce.f2.getInfo().getBareMinimumString();
			if(condString.equals(itr)) {
				if(nce.f2.getInfo().getString().contains("/") ||
				   nce.f2.getInfo().getString().contains("<<")||
				   nce.f2.getInfo().getString().contains("2 * ")){
					if(incrementrelation < exponentialincrease)
						incrementrelation = exponentialincrease;
				} 
				String nceStr = nce.f2.getInfo().getString();
				if(nceStr.contains("+") ) {
					updateExpr = nceStr.substring(nceStr.indexOf('+')+1);
							if(incrementrelation < linearincrease)
								incrementrelation = linearincrease;
				} else if ( nceStr.contains("-")	) {
					updateExpr = nceStr.substring(nceStr.indexOf('-')+1);
						if(incrementrelation < linearincrease)
							incrementrelation = linearincrease;
				}
			} 
		}
		
		if(incrementrelation == 0) {
			Main.addErrorMessage("Increment relation not handled");
			updateExpr = "1";
		}
		
		
		if(incrementrelation == 0 || condition == 0) {
			fail = true;
		}
		
		if(incrementrelation == exponentialincrease) {
			s =  SystemStringConstants.INTSIZE;
		}
		
		if(incrementrelation == linearincrease  && !( (updateExpr.equals("1 ") || updateExpr.equals("1")
				|| updateExpr.equals(" 1 ")))) {
			s = " ( (" + s + " ) / " + updateExpr +" )";
		}else if( CesStringMisc.getNumberofLiterals(s) > 1) {
			s = "( " + s + " ) ";
		}
		
		
		
		
		if(fail) {
			s = SystemStringConstants.INFINITY;
		}
		assert(s != null);
		TotalIterations itrs = new TotalIterations(s);
		
		return itrs;
		
	}
	
	
	public static TotalIterations getForIterations(ForStatement f) {
		Main.addlog(f.getInfo().getBareMinimumString());
		assert(f.f6.node != null);
		Node incrementor  =  f.f6.node;
		
		
		
				
		assert(f.f4.node != null);
		Node condition = f.f4.node;
		
		Node initializer = null;
		
		if(f.f2.node != null) {
			initializer = f.f2.node;
		} else {
			FunctionDefinition funcDef = (FunctionDefinition) Misc.getEnclosingFunction(f);
			ReachingDefinitionGetter defgetter = new ReachingDefinitionGetter();
			defgetter.setSinkNode(f);
			Vector<Symbol> writes = f.f6.getInfo().getWrites();
			defgetter.setSourceSymbol(writes.get(0));
			funcDef.accept(defgetter);
			ArrayList<Definition> defs  = defgetter.getDefList();
			if(defs.size() != 1) {
				/**
				 * Either multiple or no defs
				 * 
				 */
				
			} else {
				initializer = defs.get(0).definingNode;
			}
			
		}
		
	//	assert(initializer != null);
		
	/*	Main.addlog(" incrementor " + f.f6.getInfo().getString() 
				+ " initializer " + f.f2.getInfo().getString()
				+ " condition " + f.f4.getInfo().getString());	
		*/
		HierarchyASTNodeGetter nG = new HierarchyASTNodeGetter();
		nG.reset();
		boolean plusplus = false;
		boolean minusminus = false;
		boolean addPlus = false;
		incrementor.accept(nG, PlusPlus.class.getName());
		assert(nG.getASTNodes().size() < 2 );
		if(nG.getASTNodes().size() == 1)
			plusplus = true;
		nG.reset();
		
		incrementor.accept(nG, MinusMinus.class.getName());
		assert(nG.getASTNodes().size() < 2 );
		if(nG.getASTNodes().size() == 1)
			minusminus = true;
		nG.reset();
		
		
		String jumpsize ="";
		incrementor.accept(nG, NonConditionalExpression.class.getName());
		assert(nG.getASTNodes().size() < 2 );
		if(nG.getASTNodes().size() == 1) {
			NonConditionalExpression nce = ((NonConditionalExpression)nG.getASTNodes().get(0));
			String op = nce.f1.getInfo().getBareMinimumString();
			if(op.equals("+= "))
				addPlus = true;
			else {
				Main.addlog("Unhandled exr");
				return new TotalIterations(SystemStringConstants.INFINITY);
			}
			jumpsize = nce.f2.getInfo().getBareMinimumString();
		}
		nG.reset();
		
		if(!(plusplus || minusminus || addPlus)) {
			Main.addErrorMessage(" Unhandled Expression for for Iteration count where for loop is " + f.getInfo().getString());
		}

		
		Node upperLimit = null;
		Node lowerLimit = null;
		
		
		
		
		
	//	Main.addlog(condition.getInfo().getBareMinimumString());
		condition.accept(nG,RelationalOptionalExpression.class.getName());
		assert(nG.getASTNodes().size() < 2 );
		RelationalOptionalExpression rel  = null;
		if(nG.getASTNodes().size() != 0) {
			rel	= (RelationalOptionalExpression) nG.getASTNodes().get(0);
		} 
		
		String endPoint = "";
		NonEqualExpression neq  = null;
		if(rel == null) {
			nG.reset();
			condition.accept(nG,NonEqualExpression.class.getName());
			assert(nG.getASTNodes().size() < 2 );
			if(nG.getASTNodes().size() != 0) {
				neq	= (NonEqualExpression) nG.getASTNodes().get(0);
			} 
		}
		nG.reset();	
		
		
		String startPoint = null;
		if(initializer != null) {
			initializer.accept(nG, ConditionalExpression.class.getName());
			//	Main.addlog(initializer.getInfo().getString());
			//	Main.addlog("The ConditionalNodes are:::::");
			for (int i = 0; i < nG.getASTNodes().size(); i++) {
				//	Main.addlog(nG.getASTNodes().get(i).getInfo().getString());
			}
			// assert(nG.getASTNodes().size() < 2 );
		  startPoint = nG.getASTNodes().get(0).getInfo().getString();
		} else {
			if(endPoint.equals("0"))
				startPoint = SystemStringConstants.INFINITY;
			else
				startPoint = "0";
		} 
		
		
		String s ="";
	//	Main.addlog("For loop is " + f.getInfo().getString());
		
		if(neq == null && rel == null) {
			// unkown
			return new TotalIterations(SystemStringConstants.INFINITY);
		}
		
		if(plusplus) {
			lowerLimit  = initializer;
			
			if(startPoint.equals("0 ")) 
				startPoint = "";
			if(rel != null && rel.f0.choice.getClass() == RelationalLTExpression.class) {
				endPoint = (( RelationalLTExpression)rel.f0.choice).f1.getInfo().getString();
				upperLimit = (( RelationalLTExpression)rel.f0.choice).f1;
				s = endPoint;
			} else if(rel != null && rel.f0.choice.getClass() == RelationalLEExpression.class) {
				endPoint = (( RelationalLEExpression)rel.f0.choice).f1.getInfo().getString();
				s = endPoint + " + 1";
				upperLimit = (( RelationalLEExpression)rel.f0.choice).f1;
			} else if(neq != null){
				endPoint = (( NonEqualExpression)neq).f1.getInfo().getString();
				s = endPoint;
				upperLimit = (( NonEqualExpression)neq).f1;
			}
			
		//	Main.addlog("Start Point is " + startPoint + " and end point is " + endPoint);
			
			assert(! s.isEmpty());
			
			if(! startPoint.isEmpty())
				s += " - " + startPoint;
			
			
		} else  if (minusminus){
			
			upperLimit = initializer;
			
			if(rel != null && rel.f0.choice.getClass() == RelationalGTExpression.class) {
				endPoint = (( RelationalGTExpression)rel.f0.choice).f1.getInfo().getString();
				s = startPoint;
				lowerLimit = (( RelationalGTExpression)rel.f0.choice).f1;
			} else if(rel != null && rel.f0.choice.getClass() == RelationalGEExpression.class) {
				endPoint = (( RelationalGEExpression)rel.f0.choice).f1.getInfo().getString();
				s = startPoint + " + 1 ";
				lowerLimit = (( RelationalGEExpression)rel.f0.choice).f1;
			} else if (neq != null) {
				endPoint = (( NonEqualExpression)neq).f1.getInfo().getString();
				s = startPoint;
				lowerLimit = (( NonEqualExpression)neq).f1;
			}
			if(endPoint.equals("0 ") && ! endPoint.isEmpty()) {
				s += " - " + endPoint;
			}
		//	Main.addlog("Start Point is " + startPoint + " and end point is " + endPoint);
			
			assert(!s.isEmpty() );
		} else if (addPlus) {
			lowerLimit = initializer;
			if(startPoint.equals("0 ")) 
				startPoint = "";
			if(rel.f0.choice.getClass() == RelationalLTExpression.class) {
				endPoint = (( RelationalLTExpression)rel.f0.choice).f1.getInfo().getString();
				s = endPoint;
				upperLimit = (( RelationalLTExpression)rel.f0.choice).f1;
			} else if(rel.f0.choice.getClass() == RelationalLEExpression.class) {
				endPoint = (( RelationalLEExpression)rel.f0.choice).f1.getInfo().getString();
				s = endPoint + " + 1";
				upperLimit = (( RelationalLEExpression)rel.f0.choice).f1;
			}
			
		//	Main.addlog("Start Point is " + startPoint + " and end point is " + endPoint);
			
			assert(! s.isEmpty());
			
			if(! startPoint.isEmpty())
				s += " - " + startPoint;
			if(! jumpsize.equals("1")) {
				s = "(" + s + ")  / " + jumpsize + ""; 
			}
			
		}

		
		if( CesStringMisc.getNumberofLiterals(s) > 1) {
			s = "( " + s + " ) ";
		}
		
		TotalIterations itr = null;
		if(upperLimit == null || lowerLimit == null) {
			 itr = new TotalIterations(s);
		} else {
			itr =  new TotalIterations(upperLimit, lowerLimit, s);
		}
		
		return itr;
	}


	public static OmpForCondition getForConditionExpression(Node node) {
		if(node.getClass() == ForConstruct.class) {
			ForConstruct forC = (ForConstruct)node;
			return forC.f2.f4;
		} else if(node.getClass() == ParallelForConstruct.class) {
			ParallelForConstruct forC = (ParallelForConstruct) node;
			return forC.f5.f4;
		} else {
			Main.addErrorMessage("Unknown for class");
			return null;
		}
	}



	public static void collectParallelEmbeddingPath(OMPWorkload bW, OMPParallelWorkload barrierWorkload,
			EmbeddedParallelRegions embedded) {
		OMPWorkload curW = bW;
		ArrayList<OMPWorkload> parentList =  new ArrayList<OMPWorkload>();
		while(curW.getClass() !=  OMPFunctionWorkload.class) {
			if(curW.getClass() == ForWorkload.class ||
			   curW.getClass() == WhileWorkload.class) {
				embedded.setInsideLoop();
			}
			parentList.add(0, curW);
			curW = curW.getParent();
		}
		parentList.get(0).hasEmbeddedParallelRegion(embedded);
		assert(curW.getClass() == OMPFunctionWorkload.class);
		embedded.setParentFunction((OMPFunctionWorkload) curW);
		embedded.addPathList(parentList);
		return;
	}



	public static void setBaseFunction(TranslationUnit root) {
		Vector<FunctionDefinition> defs = ((RootInfo)root.getInfo()).getAllFunctionDefinitions();
		for(FunctionDefinition def : defs) {
			FunctionDefinitionInfo fInfo = (FunctionDefinitionInfo) def.getInfo();
			CallSiteGetter callGetter = new CallSiteGetter();
			def.accept(callGetter);
			HashSet<CallSite> calls = callGetter.getCallSiteList();	
			for(CallSite cs : calls) {
				if(cs.getCalleeName().equals(SystemStringConstants.startProfile)) {
				//	Main.addErrorMessage("The base function is " + fInfo.getFunctionName());
					SystemConfigFile.baseFunction = fInfo.getFunctionName();
				}
			}
		}
	}



	public static String getInitialValueforForConstructIterator(ForConstruct forConstruct) {
		String intData = forConstruct.f2.f2.getInfo().getString();
		intData = intData.substring(intData.indexOf("=") +1);
		intData = intData.trim();
		return intData;
	}



	public static String getFinalValueforForConstructIterator(ForConstruct forConstruct) {
		String fData = forConstruct.f2.f4.getInfo().getString();
		fData = fData.substring(fData.indexOf("<") +1);
		fData = fData.trim();
		return fData;
	}
	
	
	public static Symbol getIterator(ParallelForConstruct n) {
		OmpForHeader header = n.f5;
		Expression choicd  =  (Expression) header.f6.f0.choice;
		Main.addlog(choicd.getInfo().getString());
		Vector<Symbol> writes =  choicd.getInfo().getWrites();
		
		assert(writes.size() == 1);
		
		return writes.get(0);
		
	}
	
	
	public static Symbol getIterator(ForConstruct n) {
		OmpForHeader header = n.f2;
		/*
		 * Can be one of these
		 *  * f0 -> PostIncrementId()
		 *       | PostDecrementId()
		 *       | PreIncrementId()
		 *       | PreDecrementId()
		 *       | ShortAssignPlus()
		 *       | ShortAssignMinus()
		 *       | OmpForAdditive()
		 *       | OmpForSubtractive()
		 *       | OmpForMultiplicative()
		 */
		
		
		Node choicd  =  (Node) header.f6.f0.choice;
		Main.addlog(choicd.getInfo().getString());
		Vector<Symbol> writes =  choicd.getInfo().getWrites();
		
		assert(writes.size() == 1);
		
		return writes.get(0);
	}



	public static Symbol getIterator(ForStatement n) {
		/**
		 * Grammar production:
		 * f0 -> <FOR>
		 * f1 -> "("
		 * f2 -> ( Expression() )?
		 * f3 -> ";"
		 * f4 -> ( Expression() )?
		 * f5 -> ";"
		 * f6 -> ( Expression() )?
		 * f7 -> ")"
		 * f8 -> Statement()
		 */

		Expression choicd  =  (Expression) n.f6.node;
		Main.addlog(choicd.getInfo().getString());
		Vector<Symbol> writes =  choicd.getInfo().getWrites();
		
		assert(writes.size() == 1);
		
		return writes.get(0);
		
		/*
		if(choicd != null) {
			HierarchyASTNodeGetter nodeGetter =  new HierarchyASTNodeGetter();
			choicd.accept(nodeGetter, NonConditionalExpression.class.getName());
			ArrayList<Node> list = nodeGetter.getASTNodes();
			assert(list.size() <= 1);
			NonConditionalExpression exp =  (NonConditionalExpression)list.get(0);
			nodeGetter.reset();
			exp.accept(nodeGetter, PrimaryExpression.class.getName());
			list = nodeGetter.getASTNodes();
			PrimaryExpression primExp = (PrimaryExpression) list.get(0);
			return primExp;
		} 
		
		*/
	}



	public static String getFunctionName(String cFunctionCall) {
		PostfixExpression e = null;
		e = (PostfixExpression) CParser.tryCrudeASTNode(cFunctionCall, PostfixExpression.class);
		if(e == null)
			return null;
		
		if(e.f1.getInfo().getString().isEmpty())
			return null;
		
		ArrayList<Node> nodes = collectAllChildrensofClass(e, ArgumentList.class.getName());
		if(!nodes.isEmpty())	
			return e.f0.getInfo().getString();
		
		return null;
	}
	



	public static ArrayList<String> getArgLists(String cFunctionCall) {
		PostfixExpression e = null;
		e = (PostfixExpression) CParser.tryCrudeASTNode(cFunctionCall, PostfixExpression.class);
		if(e == null)
			return null;
		
		if(e.f1.getInfo().getString().isEmpty())
			return null;
		ArrayList<Node> nodes = collectAllChildrensofClass(e, ArgumentList.class.getName());
		if(!nodes.isEmpty()) {
			ArrayList<String> argList = new ArrayList<>();
			ArgumentList list = (ArgumentList) nodes.get(0);
			if (list.f1.node != null) {
				ExpressionList exlist = (ExpressionList) list.f1.node; 
				argList.add(exlist.f0.getInfo().getString());
				Vector<Node> argnodes = exlist.f1.nodes;
				for(int i=0;i<argnodes.size();i++) {
					NodeSequence node = (NodeSequence) argnodes.get(i);
					argList.add(node.nodes.get(1).getInfo().getString());
				}
			}
			return argList;
			
		}	
		
		return null;
		
	}



	public static boolean isbranchNode(Node n) {
		if(n instanceof IfStatement || n instanceof ForStatement || n instanceof WhileStatement || 
		   n instanceof DoStatement || n instanceof ParallelForConstruct || n instanceof ForConstruct)
			return true;
		return false;
	}



	public static ArrayList<String> collectArguments(Node node) {
		// TODO Auto-generated method stub
		return null;
	}
}
