/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.util;

import ces.SystemConfigFile;


/**
 * 
 * @author jkrishna
 *
 */


public class SystemStringConstants {
	
	
	/**
	 * Config values
	 * 
	 */
	

	public static final String worklistSizeLimit = "128";
	public static final String grainLimit = "0.1";
	public static final String wlUpperLimit = "2";
	public static final String bucketLimit = "1.1";
	
	public static final String threadid = "__ces_tid";
	
	public static final String cesTag = "_ces_";

	//	public static final String UNKNOWN = ""
	public static final String INTSIZE = " 32 ";


	/*	 static strings   */
	public static final String INFINITY = "cesinfces";
	public static final String GRAPHSIZE = "siamGraphSize";
	public static final String barrierString =  " \n #pragma omp barrier \n";
	public static final String parallelString = "\n #pragma omp parallel \n";
	public static final String getThreadIDfunction = "omp_get_thread_num()";
	public static final String singleConstruct = "\n#pragma omp single \n ";
	public static final String parForStaticSchedule = "#pragma omp parallel for schedule (static) private(__i) \n";
	public static final String numThreadsString = "NO_OF_THREADS";
	public static final String atomicConstruct = "\n#pragma omp atomic \n";
	public static final String cesTotalIterationPlaceHolder = "__ces__totalIterations";
	
	public static final String updateVariable = "__ces_update";
	public static final String bufVariable = "__ces_buf";
	public static final String iterationVar = "__ces_Iterator";
	public static final String endIterationVar = "__ces_end";
	public static final String locksVar = "__ces_locks";
	public static final String doIterFunctionName = "__ces_doIter";
	public static final String getThreadFuncName  = "__ces_getThread";
	public static final String threadLockVar = "__ces_threadLocks";
	public static final String stealVar = "__ces_Steal";
	public static final String criticalStr = "\n #pragma omp critical  \n";
	public static final String globalCodeStart ="globalCodeStart";
	public static final String globalCodeEnd = "globalCodeEnd";
	public static final String startProfile  = "energymonitor__startprofiling";
	public static final String endProfile = "energymonitor__stopprofiling";
	public static final String arraySize = "__arraySize";
	public static final String baseSize = "__baseSize";
	public static final String ratio = "__ratio";
	public static final String schedBlock = "__scheduling";
	public static final String bufferBlock = "__buffer";
	public static final String listSize = "__listSize";
	public static final String startSched = "__startSched";
	public static final String bufferSize = "__bufferSize";
	public static final String cleanup = "\n#pragma skip 0 delete "+ bufferBlock + ";\n";
	public static final String cleanupschedule = "\n\n#pragma skip 0 delete " + schedBlock + ";\n\n";
	
	/* Error codes in the project */
	public static final String unknownConfig = "Unknown Config";
	public static final String genericErrorMessage =  "Error arghh !!!";
	public static final String RatioNanErrorMessage = "Error Message: The calculated ratio is NaN, reverting to equal contribution";
	
	/* big core frequencies */
	
	public static final String bf2000 = "2.0 GHz";
	public static final String bf1800 = "1.8 GHz";
	public static final String bf1600 = "1.6 GHz";
	public static final String bf1400 = "1.4 GHz";
	public static final String bf1200 = "1.2 GHz";
	public static final String bf0 = "0 GHz";
	
	
	public static final String edgesString = " G.num_edges()";
	public static final String nodeString = "G.num_nodes()";
	
	
	public static final String sizeoffunctionName = "sizeof";

	
	/* core to thread mappings*/
	
	public static final String scheduleLittleOnly = "int selected_cores[NO_OF_THREADS] = {0,1,2,3};";
	public static final String scheduleBigOnly = "int selected_cores[NO_OF_THREADS] = {4,5,6,7};";
	public static final String schedule4L2b = "int selected_cores[NO_OF_THREADS] = {0,1,2,3,4,5};";
	public static final String scheduleAll = "int selected_cores[NO_OF_THREADS] = {0,1,2,3,4,5,6,7};";
	public static final String scheduleAllReverse = "int selected_cores[NO_OF_THREADS] = {7,6,5,4,3,2,1,0};";
	
	/**
	 * TODO: update parent of each node
	 * 
	 */
	static final String updateintial = "int "+ updateVariable +"[NO_OF_THREADS];\n";
	public static final String baseSizeInitial = "int "+ baseSize + ";\n";
	public static final String ratioInitial =  "double "+ ratio + ";\n";
	public static final String schedBlockInitial = "#pragma skip 0 sched *"+schedBlock+";\n";
	public static final String bufferBlockInitial = "#pragma skip 0 sched *"+bufferBlock+";\n";
	
	/***
	 * IMPORTANT: We are putting initial buffer Size as 1.
	 * We put the last chunk (after dividing the rest of the iterations  equally)  
	 * into the buffer block.
	 * Since we are not categorising this chunk into big/little,
	 * we are unsure of the workload of this chunk, It can be very large 
	 * and hence We would want it to be executed ASAP.
	 * So we put it at the start of the buffer block.
	 * This is a small optimisation we have though of ourself.
	 */
	public static final String bufferSizeInitial = "#pragma skip 0 std::atomic<int> " + bufferSize+"(1);\n"; 
	
	
	
	public static final String schedBlockDummy = "int "+schedBlock+";\n";
	public static final String listSizeInitial = "int " + listSize +";\n";
	public static final String startSchedInitial = "int " + startSched + "[NO_OF_THREADS + 2];\n";
	public static final String grainFactorTerm = "__GRAINFACTOR";
	public static final String BlockWLLimit = "__BlockWLLimit"; 
	public static final String workloadStrip = "__workloadStrip";
	public static final String divisionString = "__division";
	public static final String grainLimitStr = "__grainLimit";

	public static final String upperLimit = "__upperLimit";

	
	
	public static final String BLOCKWLLIMIT_DEFINITION = "#define " + BlockWLLimit + " " + wlUpperLimit + "\n";
	public static final String GRAINFACTOR_DEFINITION = "#define " + grainFactorTerm + " " + grainLimit + "\n";
	
	public static final String irregularBlock =  "typedef struct sched{\n node_t start; node_t end; int wl; \n} sched; \n";
	
	
	public static final String atomicHeader = "\n#include <atomic>\n";
	
	/**
	 * TODO 
	 * the graph name hard coded as gm.
	 */
	public static final String callinitIrregular = "initiateNodeData(G, 0);\n";
	
	public static final String initIrregularName = "initiateNodeData";
	
	public static final String callinitreset = "__graph__reset();\n";


	public static final String IRR_EDGES = "IRR_EDGES";
	public static final String IRR_REDGES = "IRR_REDGES";
	public static final String bigCoreStr = "__NO_BIG_CORES";
	public static final String litCoreStr = "__NO_LITTLE_CORES";
			
	public static final String IRR_EDGES_INIT  = "#define IRR_EDGES 0\n";
	public static final String IRR_REDGES_INIT = "#define IRR_REDGES 1\n";
	
	public static final String BIGCORE_DEFINE = "#define  " + bigCoreStr + "  " + SystemConfigFile.no_of_big +"\n";
	public static final String LITCORE_DEFINE = "#define  " + litCoreStr + "  " + SystemConfigFile.no_of_little + "\n";
	
	
	
	
	
	/********************
	 * CPP info pragmas *
	 ********************/
	
	public static final String pragmaSkip1Str = "#pragma skip 1";
	
	
	
	
	
	public static final String mainIteratorString = "v_t";
	public static final String mainEndString = "end";
	public static final String mainSumString = "sum";
	
	public static final String chunkBlockSize = "(4*( "+ratio +" + 1))";
	public static final String chunkSizeString = "chunkSize";
	
	public static final String ifContribution = "0.5";

	

			
	
	

	
	
	
	public static final String irregularReset = "void __graph__reset() {\n"
			+ "#pragma skip fnDef function __graph__reset()\n"
			+ "#pragma Fnsignature void __graph__reset();\n"
			+ "\n\nint i;\n"
			+ "#pragma omp parallel for\n"
			+ "for(i=0;i<__listSize;i++) {\n"
			+ "__scheduling[i].sched = 0;\n"
			+ "}\n"
			+ "}\n";
	
	public static final String affinity = "void affinityinitialize(){ \n"
			+ "int i; \n "
			+ " for(i=0;i<NO_OF_THREADS; i++) {\n "
			+ " CPU_ZERO(&cpuset[i]);"
			+ "  CPU_SET(selected_cores[i], &cpuset[i]);"
			+ " } \n}";
	/*
	public static final String affinity = "void affinityinitialize(int argv[]){"
			+ "CPU_ZERO(&allcores);\n" 
			+ "int i; \n "
			+ " for(i=0;i<NO_OF_THREADS;)\n "
			+ " CPU_ZERO(&cpuset[i]);"
			+ "CPU_SET(_allcores, argv[2+i]);\n" 
			+ "CPU_SET(cpuset[i], argv[2+i]);"
			+ "}";
	*/
	static final String getthreadfunc = "int "+getThreadFuncName+"() {\n"
		+ "int t = "+numThreadsString+ ";\n"
		+ "int diff = "+bufVariable+";\n"
		+ "int i;\n"
		+ "for ( i =0; i < "+numThreadsString+ "; i ++){\n"
		+ "   if("+locksVar+"[i] > NEVER_LOCK){\n"
		+ "   if ( diff < ("+endIterationVar+" [ i ] - "+iterationVar+"[ i ])){\n"
		+ "       diff = "+endIterationVar+" [ i ] - "+iterationVar+" [ i ];\n"
		+ "       t = i; \n"
		+ "   }else if("+locksVar+"[i] == LOCK && "+bufVariable+" > ("+endIterationVar+" [ i ] - "+iterationVar+" [ i ])){\n"
		+ "	#pragma omp atomic \n"
		+ " 	"+locksVar+"[i] &= NEVER_LOCK;\n"
		+ "   }"
		+ "}\n"
		+ "}\n"
		+ "if( t != "+numThreadsString+ "){\n"
		+ "#pragma omp atomic \n"
		+ "    "+locksVar+"[t] |= LOCK; \n"
		+ "}"
		+ "return t ;\n"
		+ "}";
	
	
	
	static final String doiterfunc = 
						"int "+doIterFunctionName+"(int tid) {\n"
						+ "if ( "+iterationVar+" [ tid ] < "+endIterationVar+" [ tid ]){\n"
						+ " int nextiter  =  "+iterationVar+" [ tid ];\n"
						+ " if("+locksVar+"[tid] < LOCK) /* either initial or never lock*/\n"
						+ "  "+iterationVar+" [ tid ] ++; \n"
						+ "   else{ \n"
						+ "    omp_set_lock(&"+threadLockVar+"[tid]); \n"
						+ "    "+iterationVar+" [ tid ] ++; \n"
						+ "     omp_unset_lock(&"+threadLockVar+"[tid]); \n"
						+ "   }"
						+ " return nextiter; \n"
						+ "  } else { \n"
						+ " if("+locksVar+"[tid] > NEVER_LOCK) \n"
						+ "#pragma omp atomic \n"
						+ "   "+locksVar+"[tid] &= NEVER_LOCK; \n"
						+ "   int steal = 0; \n"
						+ "    do { \n"
						+ "     steal =0;"
						+ "     int i = "+ getThreadFuncName + "();\n"
						+ "     if(i == "+numThreadsString+ ")       return -1;"
						+ "     int end,newend;"
						+ "     do{ \n"
						+ "       end = "+endIterationVar+" [ i ];"
						+ "       newend = "+endIterationVar+" [ i ] - "+bufVariable+" ;"
						+ "       omp_set_lock(&"+threadLockVar+"[i]);\n"
						+ "       if("+endIterationVar+"[i] == end && newend > "+iterationVar+"[i]){\n"
						+ "          "+endIterationVar+"[i] = newend;"
						+ "          "+stealVar+"[i] --;"
						+ "          "+stealVar+"[tid]++;"
						+ "          steal = 0;\n"
						+ "        }else \n"
						+ "          steal = 1;\n"
						+ "        omp_unset_lock(&"+threadLockVar+"[i]);"
						+ "      }while(steal > 0 && "+endIterationVar+"[i] > ("+iterationVar+"[i] + "+bufVariable+")); \n"
						+ "    if ( steal == 0 ){\n"
						+ "       "+endIterationVar+" [ tid ] = end ;\n"
						+ "       "+iterationVar+" [ tid ] = newend + 1;\n"
						+ "      return newend;\n"
						+ "     }\n"
						+ "    } while ( steal > 0);\n"
						+ "  } \n"
						+ "  return -1; \n"
						+ "} \n\n";

	


}
