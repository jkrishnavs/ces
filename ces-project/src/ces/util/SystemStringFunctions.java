/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.util;

import java.util.ArrayList;

import imop.Main;
import imop.ast.node.Declaration;
import imop.ast.node.FunctionDefinition;
import imop.ast.node.Node;
import imop.ast.node.NodeList;
import imop.ast.node.TranslationUnit;
import imop.ast.node.UnknownCpp;
import ces.SystemConfigFile;
import ces.data.classifier.ClassifyWorkload;
import ces.enums.IrregularityType;
import ces.enums.JKPROJECT;
import imop.lib.builder.Builder;
import imop.parser.CParser;
import stringexp.CESString;
import stringexp.Minterms;

public class SystemStringFunctions {
	
	public static String generateIrregularityCode(IrregularityType irType, ArrayList<String> edgeWorkloadString) {
		
		
		/**
		 * TODO :assumed all 8 cores are used.
		 * Also we assume the block size is larger than the number of cores 
		 * and constant given by SystemConfug file.
		 * This is not a requirement for the algorithm. This is just for easy coding.
		 */
		
		String buffSize = "__bufSize";
		
		// assert(edgeWorkloadString.size() ==4); 
		
		/**
		 * The edge Workload String list containg the following.
		 * 0: for buketizing, and for buffer list
		 * 1: workload Strip value
		 * 2: loop bound value.
		 * 3: chunk Workload as a whole.
		 */		
		
		for(String s : edgeWorkloadString){
			Main.addlog(s);
		}
		
		
		String loopBound = edgeWorkloadString.get(2);
		
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			ArrayList<Minterms> tokens = CesStringMisc.reduce(loopBound);
			
			
			loopBound = "";
			boolean flag = false;
			for(Minterms min : tokens) {
				String s = min.getString();
				if(!s.isEmpty()) {
					if(flag == false)
						flag = true;
					else 
						loopBound += " + ";
					String updated = ClassifyWorkload.ccppmapping.getCPPFunctionCall(s);
					if( updated != null) {
						loopBound += updated;
					} else {
						loopBound += s;
					}
				}
			}
		}
		
		
		Main.addlog(loopBound);
		
		String initIrregularEdges = "void "+SystemStringConstants.initIrregularName +"() {\n"
				+ "#pragma skip fnDef function "+SystemStringConstants.initIrregularName+"\n"
				+ "#pragma Fnsignature void "+SystemStringConstants.initIrregularName+" (gm_graph &G, int flag);\n"
				+ "node_t v;\n"
				+ SystemStringConstants.listSize +" =  "+ SystemStringConstants.worklistSizeLimit + "; \n int "
				+  SystemStringConstants.chunkSizeString + " = ceil((double)G.num_nodes()/" + SystemStringConstants.listSize +");\n"
			
				+ "int "+buffSize+" = "+ SystemStringConstants.listSize+ ";";
		
		initIrregularEdges	+= SystemStringConstants.pragmaSkip1Str + " " + SystemStringConstants.bufferBlock+" = new sched["+SystemStringConstants.listSize+"];\n";
		initIrregularEdges	+= "bufferBlock;\n";
	
		
		if(!edgeWorkloadString.get(0).isEmpty()) {
			initIrregularEdges	+= "int "+SystemStringConstants.workloadStrip+" = ("+ edgeWorkloadString.get(1) +");\n";
		}

		initIrregularEdges +=  " int " + SystemStringConstants.arraySize + " = " + SystemStringConstants.listSize + "* 1.5;\n";
		initIrregularEdges	+= SystemStringConstants.pragmaSkip1Str + " " + SystemStringConstants.schedBlock+" = new sched["+SystemStringConstants.arraySize+"];\n";
		initIrregularEdges	+= "schedBlock;\n";
		
		initIrregularEdges		+= SystemStringConstants.pragmaSkip1Str + " std::atomic<int> atomicstart(0);\n"
				+ "int atomicstart;\n"
				+ SystemStringConstants.pragmaSkip1Str + " std::atomic<int> atomicend("+SystemStringConstants.arraySize+"-1);\n"
				+ "int atomicend;\n"
				+ "int bigSum = 0;\n"
				+ "int litSum = 0;\n";
		
		if(edgeWorkloadString.get(0).isEmpty()) {
			Main.addErrorMessage(SystemStringConstants.genericErrorMessage);
			initIrregularEdges	+=  "int "+SystemStringConstants.grainLimitStr  +" = (( G.num_edges() / G . num_nodes ( ) ) * ( "
			+SystemStringConstants.chunkSizeString+" * "
			+SystemStringConstants.grainFactorTerm+" ) );";
	
		} else {
		/*	initIrregularEdges	+= "int bigLimit = ((workLoadStrip /G.num_nodes()) * ("
					+SystemStringConstants.chunkSizeString+"* " +SystemStringConstants.bigFactor
					+" /"+SystemStringConstants.ratio+"));\n";
			initIrregularEdges	+= "int litLimit = ((workLoadStrip /G.num_nodes()) * ("
					+SystemStringConstants.chunkSizeString+"* " +SystemStringConstants.littleFactor
					+" /"+SystemStringConstants.ratio+"));\n";	*/
			
			initIrregularEdges += "int "+SystemStringConstants.grainLimitStr 
					+" = (( "+SystemStringConstants.workloadStrip+" / "
					+ loopBound
					+ " ) * ( "+SystemStringConstants.chunkSizeString+" * "
					+SystemStringConstants.grainFactorTerm+" ) );"
					+ "int "+ SystemStringConstants.divisionString 
					+" = (( "+SystemStringConstants.workloadStrip+" / "
					+ loopBound
					+ ") * (  "+SystemStringConstants.chunkSizeString
					+" / sqrt("+SystemStringConstants.ratio+")) );"
					+ "int "+ SystemStringConstants.upperLimit
					+" = (( "+SystemStringConstants.workloadStrip+" / "
					+ loopBound
					+" ) * (  "+SystemStringConstants.chunkSizeString
					+"* "+SystemStringConstants.BlockWLLimit+") );";
			
		}	
		initIrregularEdges	+= SystemStringConstants.parallelString
				+ "\n{"
				+ " int "+SystemStringConstants.mainSumString+";\n"
				+ " node_t " + SystemStringConstants.mainIteratorString +";"
				+ " node_t " + SystemStringConstants.mainEndString +";"
				+ " int pos;"
				+ " node_t startPoint; \n"
				+ "#pragma omp 	for  schedule(dynamic,4) reduction (+:bigSum,litSum) \n"
				+ " for (v = 0;v< "+ SystemStringConstants.listSize+"-1; v++) {\n"
				+ " " + SystemStringConstants.mainIteratorString 
				+" = v * "+SystemStringConstants.chunkSizeString 
				+" ; \n " + SystemStringConstants.mainEndString 
				+" = " + SystemStringConstants.mainIteratorString 
				+" + "+SystemStringConstants.chunkSizeString
				+" ;\n startPoint = " 
				+ SystemStringConstants.mainIteratorString +" ;\n";
			
		if(edgeWorkloadString.get(0).isEmpty()) {
			initIrregularEdges	+= "for ( ;" + SystemStringConstants.mainIteratorString 
					+" < " + SystemStringConstants.mainEndString 
					+" ;" + SystemStringConstants.mainIteratorString 
					+" ++ ) {";
			
			if(irType == IrregularityType.MultipleEdgeREdge 
			|| irType == IrregularityType.MultipleEdgeREdgeNode) {
				initIrregularEdges += " if(flag == "+ SystemStringConstants.IRR_EDGES+") "
						+ " sum += G . begin [ " + SystemStringConstants.mainIteratorString  
						+ " + 1 ]  - G . begin [ " + SystemStringConstants.mainIteratorString  
						+ "];\n" + " else "
						+ " sum += G.r_begin [ " + SystemStringConstants.mainIteratorString  
						+ " + 1 ]  - G.r_begin [ " + SystemStringConstants.mainIteratorString  
						+ " ];\n";
			} else if (irType == IrregularityType.Edge 
					|| irType == IrregularityType.MultipleEdgeNode) {
				initIrregularEdges +=
						" sum += G . begin [ " 
						+ SystemStringConstants.mainIteratorString  + " + 1 ]  - G . begin [ " 
					    + SystemStringConstants.mainIteratorString  + " ];\n";
			}  else if (irType == IrregularityType.REdge ) {
				initIrregularEdges +=
						" sum += G . r_begin [" + SystemStringConstants.mainIteratorString  
						+ " + 1 ]  - G . r_begin [" + SystemStringConstants.mainIteratorString  + " ];\n";
			} else {
				Main.addErrorMessage("The Irregularity is not handled currently " + irType);
			}
			initIrregularEdges += "}\n";
		} else {
			initIrregularEdges += edgeWorkloadString.get(3);
		} 
		
		// Secondary chunking & buketizing
		
		initIrregularEdges += " if ( "+SystemStringConstants.mainSumString 
				+ " > "+SystemStringConstants.divisionString
				+ " ) { if("+SystemStringConstants.mainSumString +" > "+SystemStringConstants.upperLimit
				+ ") { int stepSum = 0;int val;int stepStart;int stepEnd;"
				+ "" + SystemStringConstants.mainIteratorString +" = startPoint;"
				+ "int __initial = " + edgeWorkloadString.get(0) + ";\n"
				+ "" + SystemStringConstants.mainIteratorString +" = " 
				+ SystemStringConstants.mainEndString +";\n"
				+ "int __enddetails = " + edgeWorkloadString.get(0) + ";\n"
				+ " if(__initial < __enddetails) {" + SystemStringConstants.mainIteratorString 
				+ " = " + SystemStringConstants.mainEndString +"; stepEnd= " 
				+ SystemStringConstants.mainEndString +";"
				+ "while ("+SystemStringConstants.mainSumString+" > "
				+ SystemStringConstants.upperLimit  +") {" 
				+ SystemStringConstants.mainIteratorString +"--;"
				+ "val = "+ edgeWorkloadString.get(0) +";"+SystemStringConstants.mainSumString
				+ " -= val;stepSum += val;}"
				+ "" + SystemStringConstants.mainEndString +" = " 
				+ SystemStringConstants.mainIteratorString 
				+ " + 1;stepStart= " + SystemStringConstants.mainEndString +";} else {"
				+ " " + SystemStringConstants.mainIteratorString 
				+ " = startPoint;stepStart = startPoint;"
				+ "while ("+SystemStringConstants.mainSumString+" > "
				+ SystemStringConstants.upperLimit  +") {" 
				+ SystemStringConstants.mainIteratorString +"++;"
				+ "val = "+ edgeWorkloadString.get(0) +";"+SystemStringConstants.mainSumString 
				+ " -= val;stepSum += val;}"
				+ "startPoint = " + SystemStringConstants.mainIteratorString +";stepEnd = startPoint;}"
				+ " if(stepSum > "+ SystemStringConstants.grainLimitStr  +") {"
				+ "pos = atomicstart ++ ;"
				+ SystemStringConstants.schedBlock + " [ pos ] . start = stepStart;"
				+ SystemStringConstants.schedBlock + " [ pos ] . end = stepEnd;"
				+ SystemStringConstants.schedBlock + " [ pos ] . wl = stepSum;"
				+ "bigSum += stepSum ;} else { pos = __bufferSize ++ ;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . start = stepStart;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . end = stepEnd;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . wl = stepSum;}}pos = atomicstart ++ ;"
				+ SystemStringConstants.schedBlock + "[ pos ] . start = startPoint ;"
				+ SystemStringConstants.schedBlock + "[ pos ] . end = " 
				+ SystemStringConstants.mainEndString +" ;"
				+ SystemStringConstants.schedBlock + " [ pos ] . wl = "
				+SystemStringConstants.mainSumString+" ;bigSum += "+SystemStringConstants.mainSumString+" ;} else {"
				+ "if ( "+SystemStringConstants.mainSumString+" > "+ SystemStringConstants.grainLimitStr  +") { pos = atomicend -- ;"
				+ SystemStringConstants.schedBlock + "[ pos ] . start = startPoint ;"
				+ SystemStringConstants.schedBlock + " [ pos ] . end = " 
				+ SystemStringConstants.mainEndString +" ;"
				+ SystemStringConstants.schedBlock + " [ pos ] . wl = "
				+SystemStringConstants.mainSumString+" ;"
				+ "litSum += "+SystemStringConstants.mainSumString+"; } else { pos = __bufferSize ++ ;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . start = startPoint ;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . end = " 
				+ SystemStringConstants.mainEndString +" ;"
				+ SystemStringConstants.bufferBlock + " [ pos ] . wl = "
				+SystemStringConstants.mainSumString+" ;}}}}";
		
		
		
		// Type Workload Balancing 
		
		initIrregularEdges +="assert(atomicstart <= atomicend-1);"
				+ "int bigSumLimit = (bigSum + litSum)* ("
				+ SystemStringConstants.ratio + "/("
				+ SystemStringConstants.ratio + "+1));"
				+ "int litSumLimit = (bigSum + litSum)/("
				+ SystemStringConstants.ratio + " +1);"
				+ "if(bigSum > ( bigSumLimit)) {int diff = bigSum - bigSumLimit;\n"
				+ SystemStringConstants.pragmaSkip1Str +"  int* list = new int[atomicstart]; \n"
				+ "int list;"
				+ "int sum = 0; "
				+ "int id = 0; "
				+ "int walker  = atomicstart -1;"
				+ "int avg = "+ SystemStringConstants.bucketLimit
				+" * ("+SystemStringConstants.workloadStrip
				+" /"+ loopBound
				+") * "+SystemStringConstants.chunkSizeString +";"
				+ "while(sum < diff && walker >= 0 ) {"
				+ "if("+ SystemStringConstants.schedBlock +"[walker] .wl < avg) {"
				+ "sum += "+ SystemStringConstants.schedBlock + " [walker].wl;"
				+ "list[id++] = walker;}walker --;}int i;for(i=0;i<id;i++) {"
				+ "int pos = --atomicstart;int wl ="
				+ SystemStringConstants.schedBlock + "[pos].wl;"
				+ "int start = "+ SystemStringConstants.schedBlock + "[pos].start;"
				+ "int end = "+ SystemStringConstants.schedBlock + "[pos].end;"
				+ "pos = list[i];int nextpos = atomicend--;"
				+ SystemStringConstants.schedBlock + "[nextpos].start = "
			    + SystemStringConstants.schedBlock + "[pos].start; "
				+ SystemStringConstants.schedBlock + "[nextpos].end = "
				+ SystemStringConstants.schedBlock + "[pos].end ;"
				+ SystemStringConstants.schedBlock + "[nextpos].wl = "
				+ SystemStringConstants.schedBlock + "[pos].wl ;"
				+ SystemStringConstants.schedBlock + "[pos].start = start;"
				+ SystemStringConstants.schedBlock + "[pos].end = end;"
				+ SystemStringConstants.schedBlock + "[pos].wl = wl;"
				+ "}bigSum -= sum;litSum += sum;\n #pragma skip 0 delete list;\n} else if(litSum > litSumLimit) "
				+ "{ int diff = litSum - litSumLimit;\n"
				+ SystemStringConstants.pragmaSkip1Str + " int* list = new int[("+SystemStringConstants.arraySize+"- atomicend)];\n"
				+ "int* list;"
				+ "int sum = 0; int id = 0; int walker  = atomicend + 1;"
				+ " int avg = ("+SystemStringConstants.workloadStrip
				+ " /"+ loopBound +") * "
				+ SystemStringConstants.chunkSizeString + "/"+ SystemStringConstants.ratio + ";"
				+ "while(sum < diff && walker < "+SystemStringConstants.arraySize +") {if("
				+ SystemStringConstants.schedBlock + "[walker].wl > avg) {sum +="
				+ SystemStringConstants.schedBlock + "[walker].wl;list[id++] = walker;}"
				+ "walker ++;}int i;for(i=0;i<id;i++) {int pos = ++atomicend;"
				+ "int wl = "
				+ SystemStringConstants.schedBlock + "[pos].wl;"
				+ "int start = "
				+ SystemStringConstants.schedBlock + "[pos].start;"
				+ "int end = "
				+ SystemStringConstants.schedBlock + "[pos].end;"
				+ "pos = list[i];"
				+ "int nextpos = atomicstart++;"
				+ SystemStringConstants.schedBlock + "[nextpos].start = "
			    + SystemStringConstants.schedBlock + "[pos].start; "
				+ SystemStringConstants.schedBlock + "[nextpos].end = "
				+ SystemStringConstants.schedBlock + "[pos].end ;"
				+ SystemStringConstants.schedBlock + "[nextpos].wl = "
				+ SystemStringConstants.schedBlock + "[pos].wl ;"
				+ SystemStringConstants.schedBlock + "[pos].start = start;"
				+ SystemStringConstants.schedBlock + "[pos].end = end;"
				+ SystemStringConstants.schedBlock + "[pos].wl = wl;}"
				+ "bigSum += sum;litSum -= sum;\n #pragma skip 0 delete list;\n}";
		
		//  Individual Core Division
		
		initIrregularEdges += "int bigLimit = bigSum / __NO_BIG_CORES ;"
				+ "bigSum = 0 ;int pos = 0 ;int i ; int cumLimit = bigLimit;"
				+ "__startSched [ pos ++ ] = 0 ;int back = 0;"
				+ " for ( i = 0 ;i < atomicstart && pos < 4 ;i ++ ) {"
				+ "bigSum += "
				+ SystemStringConstants.schedBlock + " [ i ] . wl ;"
				+ "if ( bigSum > cumLimit ) {"
				+ SystemStringConstants.startSched+ " [ pos ++ ] = i;"
				+ "cumLimit += bigLimit; back = bigSum; } }"
				+ SystemStringConstants.startSched + "[ pos ++] = atomicstart;"
				+ "int litLimit = litSum / __NO_LITTLE_CORES ;"
				+ "cumLimit = litLimit ;"
				+ SystemStringConstants.startSched + "[ pos ++ ] = atomicend + 1 ;"
				+ " litSum = 0; back = 0;for ( i = atomicend+1;i < " +SystemStringConstants.arraySize +" "
				+ "&& pos < NO_OF_THREADS+1 ;i ++ ) { litSum +="
				+ SystemStringConstants.schedBlock + " [ i ] . wl ;"
				+ "if ( litSum > cumLimit ) { "
				+ SystemStringConstants.startSched+ "  [ pos ++ ] = i ;"
				+ "cumLimit += litLimit ; back = litSum; } }"
				+ SystemStringConstants.startSched+ " [ pos ++ ] = " +SystemStringConstants.arraySize +";\n "
				+ SystemStringConstants.bufferBlock + " [ 0 ] . start ="
				+ " ( " +SystemStringConstants.listSize +" - 1 ) * chunkSize ;"
				+ SystemStringConstants.bufferBlock + " [ 0 ] . end = "
				+ loopBound +" ; return ; }";
		
		return initIrregularEdges;
		
	}
	
	
	public static String getWorkloadString(String typString) {
		
		
		if(SystemStringConstants.mainEndString.equals(typString)) {
			if(typString.contains("r_begin")) {
				return "(G.r_begin [ " + SystemStringConstants.mainEndString  
						+ " + 1 ]  - G.r_begin [ " 
						+ SystemStringConstants.mainIteratorString  + " ])";
			} else {
				return "(G.begin [ " 
						+ SystemStringConstants.mainEndString  + " + 1 ]  - G.begin [ " 
						+ SystemStringConstants.mainIteratorString  + " ])";
			}
		} else {
			if(typString.contains("r_begin")) {
				return "(G.r_begin [ " + typString  
						+ " + 1 ]  - G.r_begin [ " 
						+typString + " ])";
			} else {
				return "(G.begin [ " 
						+ typString  + " + 1 ]  - G.begin [ " 
						+ typString + " ])";
			}
			
		}
	}

	
	
	private static boolean shouldUpdate = false; // has a re visited parallel for
	private static boolean shouldSteal = false; // the for loop should implement 


	
	
	
	
	public static boolean shouldSteal() {
		return shouldSteal;
	}
	
	public static boolean ShouldUpdate() {
		return shouldUpdate;
	}
	
	public static void shouldSteal(boolean b) {
		
		
		
		shouldSteal = b;
	}
	
	public static void shouldUpdate(boolean b) {
		shouldUpdate = b;
	}
	
	
	TranslationUnit __root;
	int globalPos; // position in Translation unit list to add functions.


	public SystemStringFunctions(Node root) {
		__root  = (TranslationUnit) root;
	}
	
	
	
	
	static public void addHelperFunctions() {
		int globalPos = Main.root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(Main.root));
		NodeList nl = Main.root.f0;
		if(shouldSteal){
			Node doiterfunction = CParser.createCrudeASTNode(SystemStringConstants.doiterfunc, FunctionDefinition.class);
			nl.addNodeatPosition(globalPos, doiterfunction);
			Node getThread = CParser.createCrudeASTNode(SystemStringConstants.getthreadfunc, FunctionDefinition.class);
			nl.addNodeatPosition(globalPos, getThread);

			nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode("int "+SystemStringConstants.stealVar
					+"["+SystemStringConstants.numThreadsString+ "];", Declaration.class));
			nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode("omp_lock_t "+SystemStringConstants.threadLockVar
					+"["+ SystemStringConstants.numThreadsString+ "];", Declaration.class));
			nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode("int "+ SystemStringConstants.bufVariable+" = 1;"
					, Declaration.class));
			nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode("int "+ SystemStringConstants.locksVar
					+"["+SystemStringConstants.numThreadsString+ "]; \n", Declaration.class));
			nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode("int "+ SystemStringConstants.iterationVar +"["
					+SystemStringConstants.numThreadsString+ "];  \n", Declaration.class));
			nl.addNodeatPosition(globalPos,CParser.createCrudeASTNode("int "+ SystemStringConstants.endIterationVar 
					+"["+ SystemStringConstants.numThreadsString+ "];  \n", Declaration.class));
				nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode("#define NEVER_LOCK 0\n",UnknownCpp.class));
			nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode("#define INITIAL 1\n",UnknownCpp.class));
			nl.addNodeatPosition(globalPos, CParser.createCrudeASTNode("#define LOCK 2 \n",UnknownCpp.class));
			if(shouldUpdate){
				Node update = CParser.createCrudeASTNode(SystemStringConstants.updateintial, Declaration.class);
				nl.addNodeatPosition(globalPos, update);
			}
			
		}
		
	}
	
	
	String addFooFunction(Node forStatement){
		String funcName = "_foo_for_" + Builder.getNewTempName();
		String fooString = "\n void "+ funcName+"(int iter[][],int tid){"
				+ "\n for(iter[tid][2] = iter[tid][0];iter[tid][2] <iter[tid][1];iter[tid][2]++)"
				+ forStatement.getInfo().getString()
				+ "\n}";

		NodeList nl = __root.f0;
		Node fooFunction = CParser.createCrudeASTNode(fooString, FunctionDefinition.class);
		nl.addNodeatPosition(globalPos,fooFunction);
		
		return funcName;
	}
	
	/**
	 * function to add code for updating the inital ratio for revisted for construct.
	 * @return
	 */
	
	public boolean execute() {
		globalPos = __root.f0.nodes.indexOf(CesSpecificMisc.getFirstExternalDeclaration(__root));
		addHelperFunctions();
		return true;
	}

}
