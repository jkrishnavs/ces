package ces.util;

import java.util.HashSet;

import imop.ast.info.*;
import imop.ast.info.CesInfo;
import imop.ast.node.BarrierDirective;
import imop.ast.node.*;
import ces.ast.info.*;
import ces.cg.CesCallSite;
import ces.data.StringExpression.CesStringGetter;
import ces.getter.PrivateLvalueAccessGetter;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.cg.CallSite;
import imop.lib.util.CesImopBridge;

public class CesImopBridgeImp implements CesImopBridge {

	@Override
	public CallSite getNewCallSite(String calleeName, FunctionDefinition calleeDef, PostfixExpression callerExp) {
		if(Node.cesCompilation) {
			return new CesCallSite(calleeName, calleeDef, callerExp);
		} else {
			return new CesCallSite(calleeName, calleeDef, callerExp);		
		}
	}
	
	
	@Override
	public String bareMinimumString(Node n) {
		assert(Node.cesCompilation == true);
		CesStringGetter str = new CesStringGetter();
		n.accept(str);
		return str.getString();
	}


	@Override
	public void addParameters(FunctionDefinitionInfo info, Symbol sym) {
		((CesFunctionDefinitionInfo)info).addParameter(sym);
	}


	@Override
	public HashSet<Symbol> privateReads(Node node) {

		PrivateLvalueAccessGetter accessGetter = new PrivateLvalueAccessGetter();
		node.accept(accessGetter);
		return accessGetter.privateLvalueReadList;
	}


	@Override
	public HashSet<Symbol> privateWrites(Node node) {
		PrivateLvalueAccessGetter accessGetter = new PrivateLvalueAccessGetter();
		node.accept(accessGetter);
		return accessGetter.privateLvalueWriteList;
	}


	@Override
	public Info getCesInfo(Node node) {
		if(node.getClass() == FunctionDefinition.class) {
			return new CesFunctionDefinitionInfo((FunctionDefinition)node);
		} else if(node.getClass() ==  TranslationUnit.class) {
			return  new RootInfo(node);
		} else if(node.getClass() ==  ParallelConstruct.class) {
			return new CesParallelConstructInfo(node);
	    } else if(node.getClass()  == ForConstruct.class
		  || node.getClass() ==  ParallelForConstruct.class) {
		  return new CesForConstructinfo(node); 
	    } else if(node.getClass() == SectionsConstruct.class
		  ||   node.getClass() ==  ParallelSectionsConstruct.class) {
		  return new CesSectionsConstructInfo(node);
        } else if(node.getClass() ==  SingleConstruct.class) {
		  return new CesSingleConstructInfo(node);
	    } else if (node.getClass() == TaskConstruct.class) {
		  return new CesOmpConstructInfo(node);
	    } else if(node.getClass() ==  MasterConstruct.class){
		   return new CesMasterConstructInfo(node);
	   }  else if(node.getClass() ==  CriticalConstruct.class
		  || node.getClass() == AtomicConstruct.class){
		  return  new CesCriticalConstructinfo(node);
	   }  else if (node.getClass() ==  OrderedConstruct.class)
		   return new CesOmpConstructInfo(node);
	    else if (node.getClass() ==  OmpConstruct.class)
		 return new CesOmpConstructInfo(node);
     	else if(node.getClass() ==  IfStatement.class)
		return new CesSelectionInfo(node);
    	else if(node.getClass() == SwitchStatement.class)
		return new CesSelectionInfo(node);
    	else if(node.getClass() ==  JumpStatement.class)
		return new CesJumpStatementInfo(node);
	    else if (node.getClass() ==  CompoundStatement.class)
		return new CesCompoundStatementInfo((CompoundStatement) node);
	    else if (node.getClass() == WhileStatement.class
		  || node.getClass() == DoStatement.class 
		  || node.getClass() == ForStatement.class) {
		       return new CesIterationInfo(node);
	   } else if (node.getClass() == BarrierDirective.class){
		return new CesBarrierInfo(node);
	   } else
		return  new CesInfo(node);
	}
}
