/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.preprocess;

import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import ces.SystemConfigFile;
import ces.enums.JKPROJECT;
import ces.getter.InfiParallelForConstructGetter;
import ces.util.CesSpecificMisc;
import imop.parser.CParser;

public class SeparateParallelFor {
	private Node rootNode = null;
	
	
	public SeparateParallelFor(Node root){	
		rootNode= root;
	}
	
	private String  pullDownThePragmaDetails( ParallelForConstruct parConst, String outStr) {
		
		if(SystemConfigFile.project == JKPROJECT.GRAPH) {
			/**
			 * We could have multiple pragma directives due to the conversion from CPP to C.
			 * These needs to be inside the parallel region just above the parallel for 
			 */
			CompoundStatementElement parentNode = 
					(CompoundStatementElement) CesSpecificMisc.getAncestorOfType(CompoundStatementElement.class.getName(), parConst);
			
			CompoundStatement compS = 
					(CompoundStatement) CesSpecificMisc.getAncestorOfType(CompoundStatement.class.getName(), parentNode);
			Vector<Node> nl = compS.f1.nodes;
			
			int constPos = nl.indexOf(parentNode);
			assert(constPos !=  -1);
			int startPos = constPos;
			if(startPos != 0) {
				CompoundStatementElement cse = (CompoundStatementElement) nl.get(startPos -1);
				while(! CesSpecificMisc.collectAllChildrensofClass(cse, UnknownPragma.class.getName()).isEmpty()) {
					startPos --;
					cse = (CompoundStatementElement) nl.get(startPos -1);
				}
			}
			if(startPos != constPos) {
				// We have some pragmas to be moved.
				while(startPos != constPos) {
					Node unKnownPragma = nl.remove(startPos);
					outStr += "\n" + unKnownPragma.getInfo().getString();
					constPos --;
				}	
			}
			
			
		}
		
		
		return outStr;
	}
	
	
	private boolean separateParallelFor(ParallelForConstruct parConst){
		Vector<Node> optionList = parConst.f3.f0.nodes;
		Node parent = parConst.parent;
		
		/*
		if(parConst.parent instanceof OmpConstruct)
			parent = (OmpConstruct)parConst.parent;
		else
			Main.addErrorMessage("Error expecting a Ompconstruct here got "+ parConst.parent);
			*/
		
		Vector<Node> parallelOptionList = new Vector<Node>();
		Vector<Node> forOptionList = new Vector<Node>();
		for(Node curNode : optionList){
			Node clause = ((AUniqueParallelOrUniqueForOrDataClause)curNode).f0.choice;
			if(clause instanceof UniqueParallelClause)
				parallelOptionList.add(clause);
			else if(clause instanceof UniqueForClause){
				forOptionList.add(clause);
			}else if(clause instanceof DataClause){
				parallelOptionList.add(clause);
			}else{
				Main.addErrorMessage("Unexpected clause at this level");
			}
		}
		/*Add omp parallel string first */
		String outStr = new String("#pragma omp parallel "); 
		for (Node option : parallelOptionList ) {
			outStr += option.getInfo().getString();
			outStr += " ";
		}
		outStr += "\n{ \n";
		
		outStr = pullDownThePragmaDetails(parConst, outStr);
		
		outStr += "#pragma omp for ";
		for (Node option : forOptionList ) {
			outStr += option.getInfo().getString();
			outStr += " ";
		} 
		outStr +="\n";
		outStr += parConst.f5.getInfo().getString();
		outStr += parConst.f6.getInfo().getString();
		
		outStr += "\n}\n";	
		
		
	//	Main.addlog(outStr);
		
		Node newparallelConstruct = CParser.createCrudeASTNode(outStr, ParallelConstruct.class);
		
		
		/*
		 * Parallel Construct has the choice id of 0.
		 * Refer CParser for more details.
		 * 
		 */
		if(parent.getClass() == OmpConstruct.class){
			((OmpConstruct)parent).f0.updateChoice(newparallelConstruct,0);
		}else if(parent.getClass() == NodeChoice.class){
			((NodeChoice)parent).updateChoice(newparallelConstruct,0);
		}else
			Main.addErrorMessage("Error expecting a Ompconstruct here got "+ parConst.parent);
		
		return false;
	}
	
	
	public boolean execute(){
		InfiParallelForConstructGetter parGetter = new InfiParallelForConstructGetter();
		rootNode.accept(parGetter);
		for(ParallelForConstruct parConst : parGetter.getList()){
			separateParallelFor(parConst);
		}
		return true;
	}
	
}
