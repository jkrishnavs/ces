/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.preprocess;

import java.util.Vector;

import imop.Main;
import imop.ast.node.*;
import ces.getter.*;
import imop.parser.CParser;

public class SeparateParallelSections {
	private Node rootNode = null;
	
	
	public SeparateParallelSections(Node root){	
		rootNode= root;
	}
	
	private boolean separateParallelSectionConstructs(ParallelSectionsConstruct parConst){
		Vector<Node> optionList = parConst.f3.f0.nodes;
		Node parent = parConst.parent;
		
		
		Vector<Node> parallelOptionList = new Vector<Node>();
		Vector<Node> dataOptionsList = new Vector<Node>();
		for(Node curNode : optionList){
			Node clause = ((AUniqueParallelOrDataClause)curNode).f0.choice;
			if(clause instanceof UniqueParallelClause)
				parallelOptionList.add(clause);
			else if(clause instanceof DataClause){
				parallelOptionList.add(clause);
			}else{
				Main.addErrorMessage("Unexpected clause at this level");
			}
		}
		/*Add omp parallel string first */
		String outStr = new String("#pragma omp parallel "); 
		for (Node option : parallelOptionList) {
			outStr += option.getInfo().getString();
			outStr += " ";
		}
		outStr += "\n{ \n";
		outStr += "#pragma omp sections\n";
		for (Node option : dataOptionsList) {
			outStr += option.getInfo().getString();
			outStr += " ";
		} 
		outStr +="\n";
		outStr += parConst.f5.getInfo().getString();
		outStr += "\n}\n";	/* closing #pragma omp parallel */
		
		Node newparallelConstruct = CParser.createCrudeASTNode(outStr, OmpConstruct.class);
		
		
		
		/*
		 * Parallel Construct has the choice id of 0.
		 * Refer CParser for more details.
		 * 
		 */
		if(parent.getClass() == OmpConstruct.class) {
			((OmpConstruct)parent).f0.updateChoice(newparallelConstruct, 0);
		} else if(parent.getClass() == NodeChoice.class) {
			((NodeChoice)parent).updateChoice(newparallelConstruct, 0);
		} else
			Main.addErrorMessage("Error expecting a Ompconstruct here got "+ parConst.parent);
		
	
		return false;
	}
	
	
	public boolean execute(){
		InfiParallelSectionsConstructGetter parGetter = new InfiParallelSectionsConstructGetter();
		rootNode.accept(parGetter);
		for(ParallelSectionsConstruct parConst : parGetter.getList()){
			separateParallelSectionConstructs(parConst);
		}
		return true;
	}
	
}
