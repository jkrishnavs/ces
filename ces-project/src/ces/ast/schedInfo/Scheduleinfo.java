/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.schedInfo;

import imop.ast.node.*;

/**
 * 
 * @author jkrishna
 * This class will store all the informations 
 * required for each parallel construct
 * which are calculated step by step based on our requirement.
 */
public class Scheduleinfo {
	Node  node;
	int expectedThreads; /* specifies the expected number of threads 
						if the information is given earlier to the compiler. */
	
	int _affinity; /*
	 					affinity will have values ranging from 0 to 1024
						0 Specifying no work done at all where as
						1024 specifying only computations done and
						no waiting happens.
	 				*/
	
	public Scheduleinfo(Node n) {
		node = n;
		setexpectedThreads(-1);
		
	}
	
	public Scheduleinfo(Node n,int threads){
		node = n;
		setexpectedThreads(threads);
	}
	
	public void setexpectedThreads(int threads){
		expectedThreads = threads;
	}
	
	public void setAffinity(int affinity){
		_affinity = affinity;	
	}
	
	public int getAffinity(){
		return _affinity;
	}
}
