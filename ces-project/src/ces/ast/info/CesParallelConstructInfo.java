/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.info.ParallelConstructInfo;
import imop.ast.node.Node;

public class CesParallelConstructInfo extends ParallelConstructInfo {
	
	private boolean irregularParallelRegion = false;
	
	
	public boolean isIrregularParallelRegion() {
		return irregularParallelRegion;
	}
	
	public void setIrregularParallelRegion(boolean pr) {
		irregularParallelRegion = pr;
	}
	
	
	public CesParallelConstructInfo(Node owner) {
		super(owner);
	}

}
