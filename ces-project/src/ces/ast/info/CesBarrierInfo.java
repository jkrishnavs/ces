/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.info.OmpConstructInfo;
import imop.ast.node.Node;

public class CesBarrierInfo extends CesOmpConstructInfo {

	public Node previousBarrierNode;
	
	public Node getPreviousBarrierNode(){
		return previousBarrierNode;
	}
	
	public void setPreviousBarrierNode(Node n){
		previousBarrierNode = n;
	}
	
	
	public CesBarrierInfo(Node owner) {
		super(owner);
	}

}
