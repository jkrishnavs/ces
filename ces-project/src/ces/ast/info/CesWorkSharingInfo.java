/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.node.Node;
import ces.util.CesSpecificMisc;

public class CesWorkSharingInfo extends CesOmpConstructInfo {

	boolean hasImplicitBarrier;
	
    public CesWorkSharingInfo(Node owner) {
		super(owner);
	}
    
    public boolean hasImplicitBarrier(){
    	return hasImplicitBarrier;
    }
    public void hasImplicitBarrier(boolean b){
    	hasImplicitBarrier = b;
    }
    
    protected String ratioName = "";
    
	public String getRatioName(){
		if(ratioName.isEmpty()){
			ratioName  = "__ratio" + CesSpecificMisc.getToken();
		}
		return ratioName;
	}
	
    
 
    	
}
