/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.info.CesInfo;
import imop.ast.node.Node;

/*
 * This class is maintained specifically for the 
 * sequential block transformations
 * 
 * This will contain the information about 
 * the starting Node and ending node of the 
 * sequential block.
 * 
 * We have two boolean values that needs to be set
 * 
 * __combinewithpredparallel : is set when the analysis
 * phase directs us to combine the sequential region 
 * with the preceding parallel region.
 * 
 * __combinewithsuccparallel : is set when the analysis
 * phase directs us to combine the sequential region 
 * with the succeeding parallel region.
 * 
 * */

public class CesSequentialBlockInfo extends CesInfo {
	
	public CesSequentialBlockInfo(Node owner) {
		super(owner);
		startingNode = owner;
		
	}

	private boolean __combinewithpredparallel = false;
	private boolean __combinewithsuccparallel = false;
	
	private Node startingNode = null;
	private Node endingNode = null;
	private Node predParallelNode = null;
	private Node succParallelNode = null;
	
	public void setPredParallelNode(Node n){
		predParallelNode = n;
	}
	
	public void setSuccParallelNode(Node n){
		succParallelNode = n;
	}
	
	public Node getPredParallelNode(Node n){
		return predParallelNode;	
	}
	
	public Node getSucccParallelNode(Node n){
		return succParallelNode;
	}
	
	
	public void setcombinewithpred(boolean v){
		__combinewithpredparallel = v;
	}
	
	
	public void setcombinewithsucc(boolean v){
		__combinewithsuccparallel = v;
	}
	
	public boolean getcombinewithpred(){
		return __combinewithpredparallel;
	}
	
	public boolean getcombinewithsucc(){
		return __combinewithsuccparallel;
	}

	public void setStartingNode(Node n){
		startingNode = n;
	}
	
	public void setEndingNode(Node n){
		endingNode = n;
	}
	
	public Node getStartNode(){
		return startingNode;
	}
	
	public Node getEndingNode(){
		return endingNode;
	}
}
