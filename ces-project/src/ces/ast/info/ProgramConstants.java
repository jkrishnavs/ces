/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

public class ProgramConstants {
	String varName;
//	String valueStr;
	boolean intVal;
	double value;
	
	public ProgramConstants (String varName, double value, boolean intVal) {
		this.varName = varName;
		this.value = value;
		this.intVal = intVal;
	}
	
	public String getString() {
		if(intVal)
			return String.valueOf((int)value);
		return String.valueOf(value);
	} 
	
	public boolean sameVariable(String varName) {
		if(this.varName.equals(varName))
			return true;
		return false;
	}
}
