/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.info.CesInfo;
import imop.ast.node.Node;

public class CesJumpStatementInfo extends CesInfo{

	/**
	 * We need to analyze if we need to process the jump 
	 * statement conditionally or uncconditionally.
	 * 
	 */
	private Node targetNode;
	
	public void setTargetNode(Node n){
		targetNode = n;
	}
	
	
	public Node getTargetNode(){
		return targetNode;
	}
	
	public CesJumpStatementInfo(Node owner) {
		super(owner);
	}

}
