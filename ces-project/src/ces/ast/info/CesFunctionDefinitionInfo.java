/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import java.util.ArrayList;
import java.util.Vector;

import imop.ast.info.*;
import imop.ast.node.FunctionDefinition;
import ces.cg.CesCallSite;
import imop.lib.analysis.dataflow.Symbol;
import imop.lib.analysis.type.Type;

/**
 * 
 * 	public boolean runInParallel = false; // This flag is set for those function 
 *                                         defs which may become a part of a parallel region.
 *	public String functionName;
 *	public ArrayList<CallSite> calledFunctions = new ArrayList<CallSite>();  // list of all callsites in this function 
 */

public class CesFunctionDefinitionInfo extends FunctionDefinitionInfo{
	private ArrayList<CesCallSite> allCallsites;  /*list of all call sites from which this function is called */
	private boolean _hasParallelCode; /* This flag is set when */
	private boolean _preanalysis; /* This flag  is set when the pre-analysis is done
	 								 We might skip the analysis of few function if
	 								 we feel its wasteful. But we need to track these
	 								 for later check.*/
	
	
	private boolean revisited; /*True if there is a chance of the function to be revisited.*/
	
	public boolean revisited(){
		return revisited;
	}
	
	public void revisited(boolean b){
		revisited = b;
	}
	
	
	private Vector<Info> blockInfos; /* Stores data of all blocks for sequential calls*/
	
	private Vector<CesSequentialBlockInfo> seqList;
	
	private Symbol returnSymbol;
	
	private Vector<Symbol> parameters = new Vector<Symbol>();
	
	public Type getreturnType(){
		return returnSymbol.getType();
	}
	
	public void setreturnSymbol(Symbol sym){
		returnSymbol = sym;
	}
	


	
	public Symbol getreturnSymbol(){
		return returnSymbol;
	}
	
	public void addParameter(Symbol par){
		parameters.add(par);
	}
	
	public Vector<Symbol> getparameterlist(){
		return parameters;
	}
	
	public int noofParamters(){
		return parameters.size();
	}
	
	public Vector<CesSequentialBlockInfo> getSequentialBlockList(){
		return seqList;
	}
	
	public void addtoSequentialBlockList(CesSequentialBlockInfo info){
		seqList.add(info);
	}
	
	
	
	public void seqcall_addBlock(Info info){
		blockInfos.add(info);
	}
	
	
	public Info seqcall_getBlockat(int i){
		return blockInfos.get(i);
	}
	
	public Vector<Info> getBlockListInfo(){
		return blockInfos;
	}
	
	
	/***
	 * Current understanding is that there is no effect of 
	 * having a parallel section inside a single or a 
	 * master construct.
	 * Also single construct has an inherent barrier but master 
	 * does not.
	 */
	
	public boolean isCalledFromParallelRegion(){
		for (CesCallSite cs : allCallsites) {
			if(cs._callInsideParallelconstruct()){
				return true;
			}
		}
		return false;
	}
	public boolean isCalledFromSequentialRegion(){
		if(allCallsites.isEmpty())
			return true;
		for (CesCallSite cs : allCallsites){
			if(cs._callInsideSequentialregion()){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	public CesFunctionDefinitionInfo(FunctionDefinition owner) {
		super(owner);
		allCallsites = new ArrayList<CesCallSite>();
		_preanalysis(false);
		
	}
	
	public void addcallsite(CesCallSite s){
		allCallsites.add(s);
	}
	
	public ArrayList<CesCallSite> getAllCallerSite(){
		return allCallsites;
	}
	
	public boolean _preanalysis(){
		return _preanalysis;
	}
	
	public void _preanalysis(boolean val){
		_preanalysis = val;
	}
	
	public boolean _hasParallelCode(){
		return _hasParallelCode;
	}
	
	public void _hasParallelCode(Boolean val){
		_hasParallelCode = val;
	}
	
}
