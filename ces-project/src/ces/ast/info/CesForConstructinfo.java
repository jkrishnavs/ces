/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import java.util.Vector;

import imop.ast.node.ForConstruct;
import imop.ast.node.Node;
import ces.data.base.Counter;

public class CesForConstructinfo extends CesWorkSharingInfo {

	/* Scaled ratio for small or big cores if end points unknown*/
	private String scaledRatioBig;
	private String scaledRatioSmall;
	
	
	private boolean endpointVaraible;
	

	private boolean hasscaledendpoints;// set when we have schedules a scaled end point. false actual end points
	private boolean shouldsteal; // should we implement the stealing function.
	private String iterationVariable;
	
	
	
	private boolean revisitedloop; // the loop is revisited , update initial 
	
	
	

	public void setIterationVariable(String itr){
		if(itr == null){
			iterationVariable  = ((ForConstruct)node).f2.f2.f0.tokenImage;
			
		}
		iterationVariable = itr;
	}
	
	
	public String getIterationVariable(){
		if(iterationVariable == null)
			iterationVariable  = ((ForConstruct)node).f2.f2.f0.tokenImage;
		return iterationVariable;
	}
	
	
	public boolean hasscaledendpoints(){
		return hasscaledendpoints;
	}
	public void hasscaledendpoints(boolean v){
		hasscaledendpoints = v;
	}
	
	public boolean revisitedloop(){
		return revisitedloop; 
	}
	
	public void revisitedloop(boolean v){
		revisitedloop = v;
	}
	
	public boolean shouldsteal(){
		return shouldsteal;
	}
	
	public void shouldsteal(boolean v){
		shouldsteal = v;
	}
	
	
	public void setScaledRatio(String b, String s){
		scaledRatioBig = b;
		scaledRatioSmall = s;
		endpointVariable(true);
		
	}
	public String getScaledRatiobig(){
		return scaledRatioBig;
	}
	
	public String getScaledRatiosmalll(){
		return scaledRatioSmall;
	}
	
	private Vector<Counter>  _ratio; /* The scaled end points for 
	    the loop*/

	public Vector<Counter> getRatios(){
		return _ratio;
	}	
	
	
	
	
	public void endpointVariable(boolean b){
		endpointVaraible = b;
	}
	public boolean endpointVariable(){
		return endpointVaraible;
	}
	
	
	
	public CesForConstructinfo(Node owner) {
		super(owner);
		hasscaledendpoints = false;
	}


	public boolean isscheduled() {
		return false;
	}
	
}
