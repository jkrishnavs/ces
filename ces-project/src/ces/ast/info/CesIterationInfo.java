/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.info.CesBlockInfo;
import imop.ast.node.Node;

public class CesIterationInfo extends CesBlockInfo {


	public CesIterationInfo(Node owner) {
		super(owner);
	}

}
