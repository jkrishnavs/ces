/*
 * Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
 * This file is a part of the project CES, licensed under the MIT license.
 * See CESLICENSE.md for the full text of the license.
 * 
 * The above notice shall be included in all copies or substantial
 * portions of this file.
 */
package ces.ast.info;

import imop.ast.node.*;
import ces.data.base.Counter;

public class CesSectionsConstructInfo extends CesWorkSharingInfo{
	
	
	public class IndSecData{
		private ASection indSection;
		//	private int _ID;
		private Counter affinity; /*
			affinity is set as the ratio of bigWorkload and little workload.
		*/
		
		
		public Counter getAffinity(){
			return affinity;
		}
		
		public void setAffinity(Counter c){
			affinity  = c;
		}
		
		public ASection getSectionPointer(){
			return indSection;
		}
		public IndSecData(ASection sec) {
				indSection = sec;
		}
		
		public void setGroup(int g){
		}
	}
	
	public CesSectionsConstructInfo(Node owner) {
		super(owner);
	}

}
