/*
 * Copyright (c) 2018 Aman Nougrahiya, V Krishna Nandivada, IIT Madras.
 * This file is a part of the project IMOP, licensed under the MIT license.
 * See LICENSE.md for the full text of the license.
 *
 * The above notice shall be included in all copies or substantial 
 * portions of this file.
 */
package ces;
import java.io.*;

//import java.util.*;
import imop.ast.node.Node;
import imop.ast.node.TranslationUnit;
import ces.SystemConfigFile;
import ces.enums.CostFunction;
import ces.enums.JKPROJECT;
import ces.enums.OptimizationLevel;
import ces.enums.RegressionFunctionChoice;
import ces.util.CesImopBridgeImp;
import ces.util.CesSpecificMisc;
import imop.parser.CParser;


public class MainforCESCompilation {
	
	public static void InitWriter(){
		initCES();
		Main.logWriter   = new PrintWriter(System.err);
		Main.errWriter   = new PrintWriter(System.err);
		Main.outputWriter= new PrintWriter(System.out);	
		imop.Main.logWriter = Main.logWriter;
		imop.Main.errWriter = Main.errWriter;
		imop.Main.outputWriter = Main.outputWriter;
	}

	

	public static void initCES() {
		Node.setcesCompilation(true);
		Node.cesImopBridge = new CesImopBridgeImp();
	}
	


	


	public static void main(String [] args){
		FileInputStream fis = null;
		InitWriter();
		
		if(args.length > 0) {
			Main.setparameters(args);
		}
		
		
		try {
			Main.root = (TranslationUnit) CParser.createRefinedASTNode(System.in, TranslationUnit.class);
			Main.preprocess();
				
			
			if(SystemConfigFile.project == JKPROJECT.GRAPH) {
				Main.executeGraph();
			} else {
				Main.execute();
			}
			Main.printCode(Main.root);
			Main.addErrorMessage("Ended Successfully");
			}catch (Exception e) {
				Main.addErrorMessage(e.getMessage());
				Main.addErrorMessage(e.toString());
				e.printStackTrace();
			}finally{
				try{
					if(fis != null)
						fis.close();
					Main.errWriter.close();
				}catch(IOException e){
					e.printStackTrace();
				}		
			}
		
	}


} 



