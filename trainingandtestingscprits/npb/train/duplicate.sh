#!/bin/bash
bigfreq=(b2000000 b1800000 b1600000 b1400000 b1200000)
folderpath=(readingsfor readingsbranches readingsflush readingsmemory  readingsreductions)
three=3
four=4
five=5
for freq in "${bigfreq[@]}"
do
    for folder in "${folderpath[@]}"
    do
	for filename in $freq/$folder/*
	do
	    id=${filename:${#filename}-5:1}
	    ext=${filename:${#filename}-4:4} 
	   # echo $ext
	    name=${filename::-5}
	  #  echo $name
	    fname=${filename%.*}
#	    echo $id
	    if [ "$id" == "1" ]
	    then
	    	cp $filename $name$three$ext
		cp $filename $name$five$ext
	    elif [ "$id" == "2" ]
	    then
	    	cp $filename $name$four$ext
	    else
	    	echo "Error " $filename
	    fi
	done
    done
done
