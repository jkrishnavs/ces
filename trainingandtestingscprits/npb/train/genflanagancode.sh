#!/bin/bash 
FILENAME="codegen"
JAVACODE=$FILENAME".java"
#echo $JAVACODE
cp="-cp ./:/home/jkrishnavs/odroiddata/eigenRead/flanagan.jar"
rm $JAVACODE || true
contains() {
    string="$1"
    substring="$2"
    if test "${string#*$substring}" != "$string"
    then
	return 1    # $substring is in $string
    else
	return 0    # $substring is not in $string
    fi
}

# uncommend this later
bigfreq=(b2000000 b1800000 b1600000 b1400000 b1200000)
#bigfreq=(b1200000)
#declare -a UPDATEFUNCTION=("updatebarrier"
# 			   "updatecritical") 
declare -a UPDATEFUNCTION=("updateBarrier"
  			   "updateBranches"
  			   "updateCritical"
  			   "updateFlush"
  			   "updateMemops"
  			   "updateReduction")

#declare -a UPDATEEIGEN=("barrier"
#		        "critical")
declare -a UPDATEEIGEN=("barrier"
  		        "branches"
  		        "critical"
  		        "flush"
  		        "memory"
  		        "reduction")
declare -a FITLIST=("linear" "gaussian" "quad")

# windowlimits show be same as bnaDivisionList in getdata.py
#declare -a barrierwindow (0.01  1), [1, 10], [0.01, 1], [0.5, 1], [30, 60], [1, 10]]
#Noofwindows=3
declare -a barrierWindow=("Barrier" "0.01" "1")
declare -a branchesWindow=("Branches" "1" "10")
declare -a criticalWindow=("Critical" "0.01" "1")
declare -a flushWindow=("Flush" "0.5" "1")
declare -a memoryWindow=("Mem" "30" "60")
declare -a reductionWindow=("Reduction" "1" "10")



#declare -a FITLIST=("linear" )
declare -a FITCLASS=("LinearFit" "GaussianFit" "QuadFit")
#declare -a FITCLASS=("LinearFit" )
#declare -a UPDATEFUNCTION=("updateBarrier")
#declare -a UPDATEEIGEN=("barrier")
#declare -a COSTFN=("EXECUTIONTIME" "ENERGY" "EDP" "SPEEDUP")
#declare -a COSTNAMES=("executiontime" "energy" "edp" "speedup")

declare -a COSTFN=("EXECUTIONTIME" "ENERGY" "EDP" )
declare -a COSTNAMES=("executiontime" "energy" "edp")


# echo "import flanagan.analysis.Regression;" >> $JAVACODE
# echo "public class " $FILENAME  " {" >> $JAVACODE
# echo "  public static void main (String[] args) {" >> $JAVACODE
export PATH=/usr/lib/jvm/java-9-openjdk-amd64/bin:$PATH

for freq in "${bigfreq[@]}"
do
    NEWLIST=()
    for d in */
    do
	if [[ "$d" == "$freq"* ]]
	then
	    NEWLIST+=(${d::-1})
	fi
    done
    echo $NEWLIST
    for update in "${UPDATEEIGEN[@]}"
    do
	echo  $update
	python getdata.py $update "${NEWLIST[@]}"
	
	javac $cp $JAVACODE
	java -ea $cp codegen &>out
	test -s out
	if [ $? -eq 0 ]
	then
	    cat out &> out.log
	    #cat out
	    if grep -q "Nan" out.log
	    then
		echo  $JAVACODE
		exit
	    fi
	    # exit
	fi
	mv $JAVACODE  codegenbackup/$FILENAME$update$freq".java"	
    done
done
#echo "}}" >> $JAVACODE
mv *.csv csvfolder/

# now write the code for generating files

index=0
for CLASSFIT in "${FITCLASS[@]}"
do
    LFILE=$CLASSFIT".java"
    rm $LFILE || true
    echo "package imop.ces.data.classifier.Input;" >> $LFILE
    echo "import imop.ces.data.classifier.*;" >> $LFILE
    echo "import imop.Main;" >> $LFILE
    echo "import imop.ces.enums.*;" >> $LFILE
    echo "import imop.ces.SystemConfigFile;" >> $LFILE
    echo "public class "$CLASSFIT" implements FittingModel {" >> $LFILE
    FITNAME=${FITLIST[$index]}
    echo "@Override" >> $LFILE
    echo "public void update(Classifier cf) {" >> $LFILE
    for FNNAME in "${UPDATEFUNCTION[@]}"
    do
	echo "     "$FNNAME"(cf, SystemConfigFile.selectedCostFunction);" >> $LFILE
	echo "     "$FNNAME"(cf, CostFunction.SPEEDUP);" >> $LFILE
    done
    echo "}" >> $LFILE
    eigenindex=0
    COEFFNAME="coeff"
    for FNNAME in "${UPDATEFUNCTION[@]}"
    do
	
	echo "@Override" >> $LFILE
	echo "public void "$FNNAME"(Classifier c, CostFunction cf) {" >>$LFILE
	echo "    switch(cf) {" >>$LFILE
	costindex=0
	EIGENNAME=${UPDATEEIGEN[$eigenindex]}
	#echo ${barrierWindow[0]}
	#	echo $FNNAME
	declare -a cutoffs
	declare -a barrierWindow=("barrier" "0.01" "1")
        if [[ $FNNAME == *"${barrierWindow[0]}"* ]]
	then
	    cutoffs=("${barrierWindow[@]}")
	elif [[ $FNNAME == *"${branchesWindow[0]}"*  ]]
	then
	    cutoffs=("${branchesWindow[@]}")
	elif [[ $FNNAME == *"${criticalWindow[0]}"*  ]]
	then
	    cutoffs=("${criticalWindow[@]}")
	elif [[ $FNNAME == *"${flushWindow[0]}"*  ]]
	then
	    cutoffs=("${flushWindow[@]}")
	elif [[ $FNNAME == *"${memoryWindow[0]}"*  ]]
	then
	    cutoffs=("${memoryWindow[@]}")
	elif [[ $FNNAME == *"${reductionWindow[0]}"*  ]]
	then
	    cutoffs=("${reductionWindow[@]}")
	fi
	echo "The cutoff Window for the given cost function is " ${cutoffs[@]} 

	
	for COST in "${COSTFN[@]}"
	do
	    echo "        case "$COST":" >>$LFILE
	    COSTNAME=${COSTNAMES[$costindex]}
	    FILENAME=$EIGENNAME$COSTNAME$FITNAME
	    #echo $FILENAME
	    #    echo $COST, $FNNAME
	    for f in *.txt
	    do
		if [[ $f == *"$FILENAME"* ]]
		then
		    #echo $f " " $LFILE " " $COST
		    python getEigenValues.py $f $LFILE $COST ${cutoffs[@]}
		fi
	    done
	    costindex=$(( $costindex + 1))
	    echo "       break;" >> $LFILE
	done
	echo "   default:" >> $LFILE
	echo "              Main.addErrorMessage(\"unkown cost function\" + cf );" >> $LFILE
	echo "   }" >> $LFILE
	echo "}" >> $LFILE
	eigenindex=$(( $eigenindex + 1 ))
    done
    echo "}" >> $LFILE
    index=$(( $index + 1 ))
done

mv *.txt flanagandata/
