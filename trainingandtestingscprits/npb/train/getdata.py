from os import listdir
from os.path import isfile, join
import sys
import csv
import math

SAMPLESPERSECOND = 10
SCALEDATA = 1000000

def bigcoreStaticPower(frequency):
    if frequency == "b2000000":
        return 0.2
    elif frequency == "b1800000":
        return 0.1
    elif frequency == "b1600000":
        return 0.1
    elif frequency == "b1400000":
        return 0.1
    elif frequency == "b1200000":
        return 0.1
    else :
        print "unknown frequency " + frequency
        return 777777
    

def outp(mypath, filen, total, n, b, bn, num, eigenval):
    inputfile = mypath + "/" + filen
    lit = 0
    big = 0
    with open(inputfile) as fp:
        for line in fp:
            fc = line.split(" ")
            if "Instructions" in line:
                total[num][bn][3] = long(fc[len(fc) -1]) 
            if "percentage" in line:
                total[num][bn][4] = float(fc[len(fc) -1])
            elif "number of barrier operations" in line:
                total[num][bn][4] = float(fc[len(fc) -1])
            elif "Elapsed " in line:
                total[num][bn][5] = float(fc[len(fc) -2])
            elif "scheduled" in line:
                core = int(fc[len(fc) -3])
                if core < n-b:
                    lit = lit + int(fc[len(fc)-1])
                else:
                    big = big + int(fc[len(fc) -1])
    fp.close()
    if eigenval == "barrier" or eigenval == "critical":
        total[num][bn][4] = (total[num][bn][4] * SCALEDATA )/(total[num][bn][3] * 3)  
    if n != b:
        total[num][bn][6] = lit/(n-b)
        total[num][bn][9] = float(big)/lit 
    if b != 0:
        total[num][bn][7] = big/b
    total[num][bn][8] = ((float)(lit)/(lit+big)) * (100)
     

        
    
def prof(mypath, filen, total, n, b, bn, num, freq):
    tot = 0
    big = 0
    lit = 0
    peaklit= 0
    peakbig = 0
    peaktot = 0
    samples = 0
    samplespersecond = SAMPLESPERSECOND    
    inputfile = mypath + "/" + filen
    i = 0
    with open(inputfile) as fp:
        for line in fp:
            i = i+1;
            fc = line.split(",")
            if(len(fc)>3 and i > 2):
                tot = tot + float(fc[len(fc)-1])
                big = big + float(fc[len(fc)-4])
                lit = lit + float(fc[len(fc)-5])
                samples = samples +1;
                if peaklit < float(fc[len(fc)-5]):
                    peaklit = float(fc[len(fc)-5])
                if peakbig < float(fc[len(fc)-4]):
                    peakbig = float(fc[len(fc)-4])
                if peaktot < float(fc[len(fc)-1]):
                    peaktot = float(fc[len(fc)-1])
    fp.close()
    if samples == 0:
        print "error samples 0 " + filen
        sys.exit()
    totAvg = tot/samples
    bigAvg = big/samples
    litAvg = lit/samples    
    if b == 0:
        totAvg = totAvg - bigAvg
        bigAvg = 0
    if b == 2:
        reduction = bigcoreStaticPower(freq)
        #print reduction
        totAvg = totAvg - reduction
        bigAvg = bigAvg - reduction
    if n == b:
        totAvg = totAvg - litAvg
        litAvg = 0
    #print peaklit, peakbig, peaktot
    #print num, bn
    total[num][bn][10] = litAvg
    total[num][bn][11] = bigAvg
    total[num][bn][12] = totAvg
    total[num][bn][13] = peaklit
    total[num][bn][14] = peakbig
    total[num][bn][15] = peaktot



def run(argv):
    folderpath=["readingsfor", "readingsbranches", "readingscritical", "readingsflush", "readingsmemory", "readingsreductions"]
    #should be same as UPDATEEIGEN in genflanagan code
    eigennames=["barrier", "branches", "critical", "flush", "memory", "reduction"]
    costfnList=["executiontime", "energy", "edp"]
    
    #should be same as COSTNAMES in genflanagan code
#    costfnList=["executiontime", "energy", "edp", "speedup"]

    #should be same as FITLIST in genflanagan code
    fitlist=["linear", "gaussian", "quad"]
    
    #should be in the same order of eigenlist
    bnaList= [["1", "5", "10", "20", "25", "50", "75", "100", "200", "500", "750", "1000", "2000", "4000", "8000", "16000", "32000", "64000", "128000"],
              ["r05","r07","r09","br1","1.5","br2","br5","r10","r15","r20","r25","r30","r35","r40","r45","r50"],
              ["1", "2", "3", "5", "10", "20", "25", "50", "75", "100", "200", "250", "500", "600", "750", "1000", "2000", "4000", "8000", "16000", "32000", "64000", "128000" ],
              ["us10", "us20","us30","us40","us50","us60","us70","us80","us90","s100","s200","s300","s400","s500","s700","s900"],
              ["m10","m15","m20","m25","m30","m35","m40","m45","m50","m55","m60","m65","m70","m75","m80","m90","100"],
              ["d01","d02","d03","d05","d07","d09","rd1","1.5","rd2","rd5","d10","d15","d20","d25","d30","d35"]]

    bnaDivisionList=[[0.01, 1], [1, 10], [0.01, 1], [0.5, 1], [30, 60], [1, 10]]

    # for barriers
    #perarray =["0.5","0.7", "0.9", "1", "1.5", "2", "2.5","5","10","15","20","25","30","35","40" ]
    perarrayRed= ["0.1","0.2","0.3","0.5","0.7","0.9","1","1.5","2","5","10","15","20","25","30","35"]
    hardwareChoices=["n4b0", "n4b4", "n6b2", "n8b4"]
    overallcount=0
    curEigenIndex=eigennames.index(argv[0])
    eigenFeature=argv[0]
    totfolders= len(argv)- 1
    bna = bnaList[curEigenIndex]
    bnaDivision= bnaDivisionList[curEigenIndex]
    #print bnaDivision
    #print argv[0], argv[1]
    #print "Samples per second taken as ", SAMPLESPERSECOND
    freq=argv[1][:8]
    #print freq
    csvfile= freq+argv[0] +".csv"
    total = [[[ 0 for j in xrange (30)]for i in xrange (93)] for k in xrange(20) ]
    for itrfolder in range(totfolders):
        outerfolder = argv[itrfolder + 1]
        maxCount = 0
        mypath= outerfolder + "/" + folderpath[curEigenIndex]
        #print mypath        
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for filen in onlyfiles:
            #print filen
            br = filen[:-4]
            #print br
            num = int(br[-1:]) 
            if num > maxCount:
                maxCount = num
            num = num + overallcount
            br = br[:-2]
            profstr =  br[-4:]
            if profstr == "prof":
                if eigenFeature == "barrier":
                    br = br[:-8]
                else:
                    br = br[:-5]
                b = int(br[-1:])
                n = int(br[-3:-2])
                bn = 0
                if n == 4 and b == 0: 
                    mul = 0
                elif n == 4 and b == 4:
                    mul = 1
                elif n == 6 and b == 2:
                    mul = 2
                elif n == 8 and b == 4:
                    mul = 3
                else:
                    print "error in values of na and b ", n, b
                    sys.exit(0)
                for bnv in bna:
                    #print br[3:br.index('n')]
                    if (eigenFeature == "barrier" and bnv == br[3:br.index('n')]):
                        break;
                    elif (eigenFeature == "critical" and bnv == br[3:br.index('n')]):
                        break
                    elif (eigenFeature == "flush" and bnv == br[-8:-4]):
                        break
                    elif bnv == br[-7:-4]:
                        break
                    else:
                        bn = bn + 1
                if bn == len(bna):
                    print "error no bna " + br + " with bn " + str(bn)
                    continue
                    #sys.exit()
                bn = (mul * len(bna))  + bn
                prof(mypath, filen, total, n, b, bn, num, freq)
            else:
                if eigenFeature == "barrier":
                    b = int(br[-4:-3])
                    n = int(br[-6:-5])
                else:
                    b = int(br[-1:])
                    n = int(br[-3:-2])
                bn = 0
                for bnv in bna:
                    if (eigenFeature == "barrier" and bnv == br[3:br.index('n')]):
                        break;
                    elif (eigenFeature == "critical" and bnv == br[3:br.index('n')]):
                        break
                    elif eigenFeature == "flush" and bnv == br[-8:-4]:
                        break
                    elif bnv == br[-7:-4]:
                        break
                    else:
                        bn = bn + 1
                if n == 4 and b == 0: 
                    mul = 0
                elif n == 4 and b == 4:
                    mul = 1
                elif n == 6 and b == 2:
                    mul = 2
                elif n == 8 and b == 4:
                    mul = 3
                else:
                    print "error in values of na and b ", n, b
                    sys.exit(0)
                if bn == len(bna):
                    print "error no bna " + br
                    continue
                    #sys.exit(0)
                bn = (mul * len(bna))  + bn
                #print num,bn
                total[num][bn][0] = br
                total[num][bn][1] = n
                total[num][bn][2] = b
                outp(mypath, filen, total, n, b, bn, num, argv[0])
        overallcount= overallcount + maxCount        
    bnsize= 4 * len(bna)
    for bni in range(bnsize):
        count = 0
        time5 = 0
        lit6 = 0
        big7 = 0
        perlit8 = 0
        speedbig9 = 0
        litp10 = 0
        bigp11 = 0
        totp12 = 0
        litpp13 = 0
        bigpp14 = 0
        totpp15 = 0
        lite16 = 0
        bige17 = 0
        tote18 = 0
        timesd19 = 0
        litppsd20 = 0
        bigppsd21 = 0
        totppsd22 = 0
        litesd23  = 0
        bigesd24  = 0
        totesd25  = 0
        EDP26 = 0
        EDPSD27 = 0
        BIGSPEEDSD28 = 0
        
        for num in range(1,overallcount):
            if total[num][bni][0] != '0' :
                count = count + 1
                time5 = time5 + total[num][bni][5]
                lit6 = lit6 + total[num][bni][6]
                big7 = big7 + total[num][bni][7]
                perlit8 = perlit8 + total[num][bni][8]
                speedbig9 = speedbig9 + total[num][bni][9]
                litp10 = litp10 + total[num][bni][10]
                bigp11 = bigp11 + total[num][bni][11]
                totp12 = totp12 + total[num][bni][12]
                litpp13 = litpp13 + total[num][bni][13]
                bigpp14 = bigpp14 + total[num][bni][14]
                totpp15 = totpp15 + total[num][bni][15]
                lite16 = lite16 + ((total[num][bni][5] * total[num][bni][10]))
                bige17 = bige17 + ((total[num][bni][5] * total[num][bni][11]))
                tote18 = tote18 + ((total[num][bni][5] * total[num][bni][12]))
                EDP26 = EDP26 + ((total[num][bni][5] * total[num][bni][12]) * total[num][bni][5])
        #print "count = ", count 
        total[overallcount+1][bni+1][0]  = total[1][bni][0]
        total[overallcount+1][bni+1][1]  = total[1][bni][1]
        total[overallcount+1][bni+1][2]  = total[1][bni][2]
        total[overallcount+1][bni+1][3]  = total[1][bni][3]
        total[overallcount+1][bni+1][4]  = total[1][bni][4]
        total[overallcount+1][bni+1][5]  = time5/count 
        total[overallcount+1][bni+1][6]  = lit6/count
        total[overallcount+1][bni+1][7]  = big7/count
        total[overallcount+1][bni+1][8]  = perlit8/count
        total[overallcount+1][bni+1][9]  = speedbig9/count
        total[overallcount+1][bni+1][10]  = litp10/count
        total[overallcount+1][bni+1][11] = bigp11/count
        total[overallcount+1][bni+1][12] = totp12/count
        total[overallcount+1][bni+1][13] = litpp13/count
        total[overallcount+1][bni+1][14] = bigpp14/count
        total[overallcount+1][bni+1][15] = totpp15/count
        total[overallcount+1][bni+1][16] = lite16/count
        total[overallcount+1][bni+1][17] = bige17/count
        total[overallcount+1][bni+1][18] = tote18/count
        total[overallcount+1][bni+1][26] = EDP26/count
        # finding standard deviation now
        for ct in range(0,count):
            timesd19 = timesd19 + ( (total[overallcount+1][bni+1][5] - total[ct][bni][5]) ** 2)
            litppsd20 = litppsd20 + ( (total[overallcount+1][bni+1][13] - total[ct][bni][13]) ** 2)
            bigppsd21 = bigppsd21 + ( (total[overallcount+1][bni+1][14] - total[ct][bni][14]) ** 2)
            totppsd22 = totppsd22 + ( (total[overallcount+1][bni+1][15] - total[ct][bni][15]) ** 2)
            litesd23  = litesd23  + ( (total[overallcount+1][bni+1][16] - total[ct][bni][16]) ** 2)
            bigesd24  = bigesd24  + ( (total[overallcount+1][bni+1][17] - total[ct][bni][17]) ** 2)
            totesd25  = totesd25  + ( (total[overallcount+1][bni+1][18] - total[ct][bni][18]) ** 2)
            EDPSD27   = EDPSD27   + ( (total[overallcount+1][bni+1][26] - total[ct][bni][26]) ** 2)
            BIGSPEEDSD28 = BIGSPEEDSD28 + ( (total[overallcount+1][bni+1][9] - total[ct][bni][9]) ** 2)
        total[overallcount+1][bni+1][19] = math.sqrt((timesd19/count) )
        total[overallcount+1][bni+1][20] = math.sqrt((litppsd20/count) )
        total[overallcount+1][bni+1][21] = math.sqrt((bigppsd21/count) )
        total[overallcount+1][bni+1][22] = math.sqrt((totppsd22/count) )
        total[overallcount+1][bni+1][23] = math.sqrt((litesd23/count) )
        total[overallcount+1][bni+1][24] = math.sqrt((bigesd24/count) )
        total[overallcount+1][bni+1][25] = math.sqrt((totesd25/count) )
        total[overallcount+1][bni+1][27] = math.sqrt((EDPSD27/count) )
        total[overallcount+1][bni+1][28] = math.sqrt((BIGSPEEDSD28/count) )

    total[overallcount+1][0][0] = "benchmark"
    total[overallcount+1][0][1] = "#cores"
    total[overallcount+1][0][2] = "#big"
    total[overallcount+1][0][3] = "# itrs"
    total[overallcount+1][0][4] = "%"
    total[overallcount+1][0][5] = "Time"
    total[overallcount+1][0][6] = "#Lit Itr"
    total[overallcount+1][0][7] = "#Big Itr"
    total[overallcount+1][0][8] = "% Lit"
    total[overallcount+1][0][9] = "Big speed"
    total[overallcount+1][0][10] = "Lit avg P"
    total[overallcount+1][0][11] = "Big avg P"
    total[overallcount+1][0][12] = "Tot avg P"
    total[overallcount+1][0][13] = "Lit peak P"
    total[overallcount+1][0][14] = "Big peak P"
    total[overallcount+1][0][15] = "Tot peak P"
    total[overallcount+1][0][16] = "Lit Energy"
    total[overallcount+1][0][17] = "Big Energy"
    total[overallcount+1][0][18] = "Tot Energy"
    total[overallcount+1][0][19] = "Time sd"
    total[overallcount+1][0][20] = "Lit pp sd"
    total[overallcount+1][0][21] = "Big pp sd"
    total[overallcount+1][0][22] = "Tot pp sd"
    total[overallcount+1][0][23] = "Lit en sd"
    total[overallcount+1][0][24] = "Big en sd"
    total[overallcount+1][0][25] = "Tot en sd"
    total[overallcount+1][0][26] = "EDP "
    total[overallcount+1][0][27] = "EDP SD"
    total[overallcount+1][0][28] = "SPEEDUP SD"
    datapos=[5, 18, 26, 9]
    sdpos = [19, 25, 27, 28]
    
    # hardwareconfigs 4B4L, 4B, 4L, 2B4L
    # runtime
    f = open("codegen.java", "w+")
    f.write("import flanagan.analysis.Regression;\n")
    f.write("public class codegen {\n")
    f.write("  public static void main (String[] args) { \n\n")


    
    

    # if eigenFeature == "barrier":
    #     for j in range(2):
    #         for i in range (1,len(perarray)):
    #             total[overallcount+1][j* len(perarray) +i][4] = perarray[i-1]
    if eigenFeature == "reduction":
        for j in range(2):
            for i in range(1, len(perarrayRed)+1):
                total[overallcount+1][j* len(perarrayRed) +i][4] = float(perarrayRed[i-1])

    slidingWindowLengthList=[]
    slidingWindowid = 0;
    # We use bnaDivision List to create multiple windows of regression to implement
    # sliding regression in
    slidingWindowCount=0
    bnaDivision.append(100)
    # print bnaDivision
    for j in range(len(bna)):
        ind = j+1
        # print  total[overallcount+1][len(bna) + ind][datapos[0]-1]
        if(bnaDivision[slidingWindowid] < total[overallcount+1][len(bna) + ind][datapos[0]-1]):
            slidingWindowLengthList.append(slidingWindowCount)
            slidingWindowCount = 0
            slidingWindowid += 1
        slidingWindowCount += 1

    slidingWindowLengthList.append(slidingWindowCount)
    #print slidingWindowLengthList

    for window in range(len(slidingWindowLengthList)):
        regVal = "reg" + eigenFeature + outerfolder;
        xVal = "x_"+eigenFeature +  outerfolder
        f.write("\n Regression " + regVal  + "_" + str(window)  + ";\n")



    # x vals
    
    skiplength = 0
    if freq != "b2000000":
       # print "Skipping n4b0 for xvalue " + freq
        skiplength = len(bna)
       # print "Length of bna", len(bna)
    flag = None
    #print skiplength
    appendSize = 0
    for window in range(len(slidingWindowLengthList)):
        windowSize = slidingWindowLengthList[window]
        flag = None
        windowStr = "_" + str(window)
        f.write("\n\n\n double " + xVal + windowStr +"[] = {")
        #print "windowSize", windowSize 
        for j in range(windowSize):
            ind = (skiplength + j + 1 + appendSize)
            if flag:
                f.write(", " + str(total[overallcount+1][ind][4]))
            else:
                f.write(str(total[overallcount+1][ind][4]))
                flag = True
        f.write("};\n\n\n")
        appendSize += windowSize


    # y vals
    for costfn in range(len(costfnList)):
        costFnName = costfnList[costfn]
        varName =["" for x in range(len(hardwareChoices))] # for n4b0 n4b4 n6b2 and n8b4
        for i in range (len(hardwareChoices)):
            #need to skip n4b4 and n4b0 for speed up
            if costFnName == "speedup" and total[overallcount+1][i*len(bna) + 1][1] == total[overallcount+1][i*len(bna) + 1][2]:
                continue
            if costFnName == "speedup" and total[overallcount+1][i*len(bna) + 1][2] == 0:
                continue            
            coreconfig = "n" + str(total[overallcount+1][i*len(bna) + 1][1]) + "b" + str(total[overallcount+1][i* len(bna) + 1][2])
            varname = coreconfig+outerfolder+ eigenFeature + costFnName
            flag = None
            varName[i] = varname

            appendVal=0
            for window in range(len(slidingWindowLengthList)):
                windowSize = slidingWindowLengthList[window]
                flag = None
                slidingWindowStr="_" + str(window)
                varname = varname  
                f.write("double " + varname + slidingWindowStr + "[] = { ")
                for j in range(windowSize):
                    ind = (i* len(bna) + j + appendVal + 1)
                    if flag:
                        f.write(", " + str(total[overallcount+1][ind][datapos[costfn]]))
                    else:
                        f.write(str(total[overallcount+1][ind][datapos[costfn]]))
                        flag = True
                f.write("};\n")
                appendVal += windowSize   
                

        for i in range (len(hardwareChoices)):
            # for frequncies other than b2000000 we need to skip n4b0
            if freq != "b2000000" and i == 0:
                #print "Skipping n4b0 for " + freq
                continue
            if costFnName == "speedup" and total[overallcount+1][i*len(bna) + 1][1] == total[overallcount+1][i*len(bna) + 1][2]:
                continue
            if costFnName == "speedup" and total[overallcount+1][i*len(bna) + 1][2] == 0:
                continue
            coreconfig = "n" + str(total[overallcount+1][i*len(bna) + 1][1]) + "b" + str(total[overallcount+1][i* len(bna) + 1][2])
            varname = coreconfig+outerfolder+ eigenFeature + costFnName + "_sd"
            if costFnName != "speedup":
                flag = None
                appendVal = 0
                for window in range(len(slidingWindowLengthList)):
                    windowSize = slidingWindowLengthList[window]
                    flag = None
                    slidingWindowStr="_" + str(window)
                    varname = varname 
                    f.write("double " + varname + slidingWindowStr  + "[] = { ")
                    for j in range(windowSize):
                        ind = (i* len(bna) + j + appendVal +1)
                        if flag:
                            f.write(", " + str(total[overallcount+1][ind][sdpos[costfn]]))
                        else:
                            f.write(str(total[overallcount+1][ind][sdpos[costfn]]))
                            flag = True
                    f.write("};\n")
                    appendVal += windowSize
                    f.write(regVal + slidingWindowStr + " = new Regression(" + xVal + slidingWindowStr +", "+ varName[i] + slidingWindowStr  + ", " + varname + slidingWindowStr  +");\n")
            else :
                f.write(regVal + slidingWindowStr  + " = new Regression(" + xVal + slidingWindowStr +", "+ varName[i] + slidingWindowStr + ");\n")
            for window in range(len(slidingWindowLengthList)):
                slidingWindowStr = "_" + str(window)
                f.write(regVal + slidingWindowStr +  ".linear();\n")
                f.write(regVal + slidingWindowStr + ".print(\"" + varName[i] + fitlist[0] + slidingWindowStr + ".txt\");\n")
                f.write(regVal + slidingWindowStr + ".polynomial(2);\n")
                f.write(regVal + slidingWindowStr + ".print(\"" + varName[i] + fitlist[2] + slidingWindowStr +".txt\");\n")
                f.write(regVal + slidingWindowStr + ".gaussian();\n")
                f.write(regVal + slidingWindowStr + ".print(\"" + varName[i] + fitlist[1] + slidingWindowStr +".txt\");\n")
    f.write("}}")
                 
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(total[overallcount+1])
    return


if __name__ == '__main__':
    run(sys.argv[1:])

