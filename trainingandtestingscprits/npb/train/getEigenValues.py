import sys

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
    
filename = sys.argv[1]
outfilename = sys.argv[2]
costName = sys.argv[3]
# skip 4
limits = []
for j in range(5, len(sys.argv)):
    limits.append(sys.argv[j])
# now file out the limit of the current file.
# I currently have no idea but alway a 1 gets added to the filename
# as a result we need to append the 1 to our search string
#print "The limits are ", limits
limitid=-1
for j in range(len(limits)):
    searchstr = str(j) + "1"
    if(filename.find(searchstr) != -1):
        limitid = j

eigenValues=["barrier", "branches", "critical", "flush", "memory", "reduction"]
Fit=["linear", "gaussian", "quad"]
#TODO
#varName=["barrier", "branches", "critical", "flush", "mem", "reduction", "barrierSpeedup", "branchesSpeedUp" , "criticalSpeedup",  "flushSpeedup", "memSpeedup" , "reductionSpeedup"]

varName=["barrier", "branches", "critical", "flush", "mem", "reduction" ]

classifiername="c."
fitEnumName="RegressionFunctionChoice."
hardwareEnumName="HardwareConfig."

# coeff name should be same as COEFFNAME in genflanagan code
coeffName="coeff"
Hardware=["n4b0b2000000", "n4b4b1200000", "n4b4b1400000", "n4b4b1600000", "n4b4b1800000", "n4b4b2000000", "n6b2b1200000", "n6b2b1400000", "n6b2b1600000", "n6b2b1800000", "n6b2b2000000" , "n8b4b1200000", "n8b4b1400000", "n8b4b1600000", "n8b4b1800000", "n8b4b2000000"]


# we have to store corresponding enum values
#eigenEnum=["ompBarriers", "branches", "ompCritical", "ompFlush", "MemoryOps", "ompReductions"] 
#Cost=["EXECUTIONTIME", "ENERGY", "EDP", "SPEEDUP"]


with open(filename) as f:
    contents = f.readlines()
    l=list()
    e=list()
    myHardware = None
    varPos = 0
    
    if sys.argv[3] == "SPEEDUP":
        varPos = len(eigenValues)
    myFit = None
    name = contents[0].split()[2]
    for val in Fit:
        if val in name:
            myFit = val
    for val in eigenValues:
        if val in name:
            myeigen = val
            pos = eigenValues.index(myeigen)
            myVarName = varName[pos + varPos]
            
    for val in Hardware:
        if val in name:
            myHardware = val
            myPos = Hardware.index(myHardware)

    if myHardware != None:
        for content in contents:
            words = content.split()
            if len(words) == 6:
                if isfloat(words[0]) == True:
                    # print words[1], words[2]
                    trpVal = 0
                if words[0][:2] == "c[":
                    l.append(float(words[1]))
                    e.append(float(words[2]))
                elif words[0][:4] == "mean":
                    l.append(float(words[1]))
                    e.append(float(words[2]))
                elif words[0][:2] == "sd":
                    l.append(float(words[1]))
            elif len(words) == 7:
                if words[0] == "y" and words[1] == "scale":
                    l.append(float(words[2]))
        f = open(outfilename,"a+")
        # f.write("case  " + myHardware + ": \n")
        # f.write("\t\t "+myeigen+"["+ str(myPos)+"] = )")
        # arrayStr =  "{"
        arrayStr = ""
        found = None
        for i in range(len(l)):
            if found == None and limitid == 0:
                arrayStr = arrayStr + str(l[i])
                found = True
            else:
                arrayStr = arrayStr + ", " + str(l[i])
        if limitid != -1:
            arrayStr = arrayStr + "," + limits[limitid]
        
        # arrayStr = arrayStr + "}";
        if myVarName == None:
            print "was not able to find appropriate Variable Name "+ name
            sys.exit()
        doubleName = coeffName+ myHardware+ costName
        #if limitid == 0:
        #    f.write("\t\t if ( eigenVal < " + limits[limitid] + ") { \n")
        #elif limitid != -1:
        #    f.write("\t\t else if ( eigenVal < " + limits[limitid] + ") { \n")
        #else:
        #    f.write ("\t\t else { \n")
        if limitid == 0:  
            f.write("\t\t\t double[]" + doubleName + " =  { " + arrayStr)
        elif limitid == -1:
            f.write( arrayStr + "};\n")
            f.write("\t\t\t "+classifiername+ myVarName + ".put ("+ hardwareEnumName + myHardware + ",  new RegressionFunction(" + doubleName + ", "+ fitEnumName + myFit +" ) );\n")
        else:
            f.write(arrayStr)
        
