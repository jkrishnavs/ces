import sys
outfilename = sys.argv[1]

# coeff name should be same as COEFFNAME in genflanagan code
coeffName="coeff"

configfolder=["edptrainn4b2l2b1400000", "edptrainn4b2l2b1600000", "edptrainn4b2l2b1800000",
              "edptrainn4b4l0b1400000", "edptrainn4b4l0b1600000", "edptrainn4b4l0b1800000",
              "edptrainn6b2l4b1400000", "edptrainn6b2l4b1600000", "edptrainn6b2l4b1800000",
              "edptrainn8b4l4b1400000", "edptrainn8b4l4b1600000", "edptrainn8b4l4b1800000",
              "edptrainn8b4l4b1200000", "edptrainn8b4l4b2000000",] 


for conf in configfolder:
    filename = conf +"1.txt"
    with open(filename) as f:
        contents = f.readlines()
        l=list()
        e=list()
        myHardware = None
        varPos = 0
        for content in contents:
            words = content.split()
            if len(words) == 6:
                if words[0][:2] == "c[":
                    l.append(float(words[1]))
                    e.append(float(words[2]))
                elif words[0][:4] == "mean":
                    l.append(float(words[1]))
                    e.append(float(words[2]))
                elif words[0][:2] == "sd":
                    l.append(float(words[1]))
            elif len(words) == 7:
                if words[0] == "y" and words[1] == "scale":
                    l.append(float(words[2]))
    f = open(outfilename,"a+")
    arrayStr =  "{"
    found = None
    for i in range(len(l)):
        if found == None:
            arrayStr = arrayStr + str(l[i])
            found = True
        else:
            arrayStr = arrayStr + ", " + str(l[i])
    arrayStr = arrayStr + "}";
    doubleName = coeffName + conf 
    f.write("\t\t static final double[]" + doubleName + " =  " + arrayStr +";\n")
