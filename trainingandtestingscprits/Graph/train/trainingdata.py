import sys
#import getmaxaandavg as maxavg
#import gettimeandverify as timev
import csv
import os.path
import numpy as np


#exelist = ["communities", "conduct", "pageRank", "sssp", "triangleCounting"]


# we overwrote element csv with multiple csv
#stupid
#exelist = ["element" ,"array", "element", "locker"]
#******************************* VERY IMPORTANT Read carefully ******************************
# We parse the energy array in reverse order
# if any program-graph  pair was not big enough
# for recording the power consumption,
# we dervive the average power from previous set.


# VERY IMPORTANT We still process everthing else in
# the acending order.
# Since the content of  exelist is only used while parsing the
# power array we are able to use this.
# If we plan to use this array for the rest of the cases,
# the program will fail
#exelist=["128", "256", "512", "1024", "2048", "4096", "8192"]
exelist=["train2", "train3", "train4", "train5"]
exenames=["multiple", "parsemult", "updateatomic", "trainaed"]

exelen=len(exelist)


#exeNames = ["com", "cond", "pr", "sssp", "tric"]

repeat= 7

def gettime(filename , nooffiles):
    print filename
    timevalues = [0 for col in range(nooffiles)]
    rowVal = 0
    with open(filename) as fp:
            for line in fp:
                if "Algorithm Running Time" in line:
                    timeVal = float(line[line.find("=")+1:-3])
                    timevalues[rowVal] = timeVal
                    rowVal += 1
                    if rowVal == nooffiles:
                        return timevalues
    return timevalues

def getenergy(folder, timevalues , nooffiles, exename):
    avgEnergy = [0 for col in range(nooffiles)]
    for n in range(nooffiles):
        inputfile = folder + exename +"_" +str(n) + ".csv"
        if os.path.isfile(inputfile) == False:
            print "File not found "+ inputfile
            return [[]]
        lineNos = 0
        tot = 0
        entry = 0
        print inputfile
        with open(inputfile) as fp:
            for line in fp:
                lineNos += 1
                fc = line.split(",")
                if(len(fc)>3 and lineNos > 1):
                    totPow= float(fc[len(fc) -1])
                    if totPow != 0:
                        tot += totPow
                        entry += 1
        if entry != 0:
                avgPow  = tot/ entry
        else:
            assert 0 == 1
        avgEnergy[n] = avgPow * timevalues[n]
    return avgEnergy

def extractdata(argv):
    allgraphs=["academia2Anonymized", "amazon", "amazonpre", "com-dblp", "com-youtube",
           "r1e6u99e5", "r7e5u56e5", "r7eu599e5", "randcom-dblp", "randcom-youtube",
           "randrmat125e4u325e4", "randrmat1e5u7e5", "randrmat225e4u725e4",
           "randrmat3e5u33e5", "randrmat3e5u7e6", "randrmat45e4u7e6", "randrmat65e4u165e5",
           "randTheMarkerAnonymized", "rmat125e4u325e4", "rmat225e4u725e4",
           "rmat3e5u33e5", "rmat3e5u7e6", "rmat45e4u7e6", "rmat65e4u165e5", "roadNet-PA",
           "roadNet-TX", "rrmatv7e5e5e6", "web-Google", "web-NotreDame", "web-Stanford", "WikiTalk",
           "wiki-topcats", "wiki-topcatspre"]
    
    print len(allgraphs)
    
    metrics=[ "Avg Time", "Avg Total Energy", "EDP"]
    edp = [[0 for column in range(len(exelist))] for row in range(len(allgraphs))]
    bestchunk=[0 for row in range(len(allgraphs))]
    for graph in allgraphs:
        print graph
        bracket = ""
        repeat = 7
        graphid = allgraphs.index(graph)        
        for exe in exelist:
            exeindex = exelist.index(exe)
            exename = exenames[exeindex]
            folder = argv[0] + "/" + exe + "." + graph + "/"
            timefile = folder + exe + "." + graph + ".out.txt"
            timelist = gettime(timefile, repeat)
            energyfolder = folder 
            energylist = getenergy(energyfolder, timelist, repeat, exename)
            edpavg = 0
            for i in range(repeat):
                edpavg += timelist[i] * energylist[i]
            edp[graphid][exeindex] = edpavg/(1000000 * repeat)
    print edp
    with open('edp.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(edp)
    with open('chunks.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(bestchunk)

    


if __name__ == '__main__':
    extractdata(sys.argv[1:])
