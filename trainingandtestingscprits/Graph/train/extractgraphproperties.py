import sys
#import getmaxaandavg as maxavg
#import gettimeandverify as timev
import csv
import os.path
import numpy as np


# allgraphs=["amazon", "rm1e5u89e5", "roadNet-PA",  "web-Google", "wiki-topcats",
#            "amazonpre", "rm1e5u9e6", "roadNet-TX", "soc-sign-bitcoinalpha", "web-NotreDame",
#            "wiki-topcatspre", "Email-Enron", "rm3e5u7e6",  "soc-sign-bitcoinalphapre",
#            "web-Stanford",  "rmatv1e5e1e7", "rrmatv1e5e1e7",  "soc-sign-bitcoinotc", "WikiTalk",
#            "academia2Anonymized", "randcom-dblp",  "randrmat1e5u1e7",  "randTheMarkerAnonymized",
#            "rm5e4u56e5",
#            "com-dblp", "randcom-youtube",  "randrmat1e5u7e5",  "randweb-Google",
#            "rmat1e5u1e7",
#            "com-youtube", "randEmail-Enron",  "randrmat2e4u7e5",  "randweb-Stanford",
#            "rmat1e5u7e5",
#            "links", "randEmail-EuAll", "randroadNet-PA",   "rm15e4u87e5",
#            "rmat2e4u7e5",
#            "randacademia2Anonymized",  "randlinks",
#            "randroadNet-TX",   "rm3e4u16e5",
#            "TheMarkerAnonymized",
#            "r1e6u99e5",  "randrmat125e4u325e4",  "randrmat3e5u7e6", "rmat125e4u325e4",
#           "rmat3e5u7e6", "r7e5u56e5",  "randrmat225e4u725e4",  "randrmat45e4u7e6",
#           "rmat225e4u725e4",  "rmat45e4u7e6", "r7eu599e5",  "randrmat3e5u33e5",
#            "randrmat65e4u165e5",  "rmat3e5u33e5",     "rmat65e4u165e5"]


# allgraphs=["amazon", "rm1e5u89e5", "roadNet-PA", "rrmatv7e5e5e6", "web-Google", "wiki-topcats",
#            "amazonpre", "rm1e5u9e6", "roadNet-TX", "web-NotreDame", "wiki-topcatspre",
#            "rm3e5u7e6", "rpatent", "web-Stanford", "pokecr",

#            "academia2Anonymized", "randcom-dblp", "randrmat1e5u1e7",  "randTheMarkerAnonymized",
#            "rm5e4u56e5", "com-dblp", "randcom-youtube",
#            "randrmat1e5u7e5",  "randweb-Google", "rmat1e5u1e7",
           
#            "r1e6u99e5",  "randrmat125e4u325e4",  "randrmat3e5u7e6", "rmat125e4u325e4",
#            "rmat3e5u7e6", "r7e5u56e5",  "randrmat225e4u725e4",
#            "randrmat45e4u7e6", "rmat225e4u725e4",  "rmat45e4u7e6",
#            "r7eu599e5",  "randrmat3e5u33e5", "randrmat65e4u165e5",
#            "rmat3e5u33e5", "rmat65e4u165e5"]


allgraphs=["academia2Anonymized", "amazon", "amazonpre", "com-dblp", "com-youtube",
           "r1e6u99e5", "r7e5u56e5", "r7eu599e5", "randcom-dblp", "randcom-youtube",
           "randrmat125e4u325e4", "randrmat1e5u7e5", "randrmat225e4u725e4",
           "randrmat3e5u33e5", "randrmat3e5u7e6", "randrmat45e4u7e6", "randrmat65e4u165e5",
           "randTheMarkerAnonymized", "rmat125e4u325e4", "rmat225e4u725e4",
           "rmat3e5u33e5", "rmat3e5u7e6", "rmat45e4u7e6", "rmat65e4u165e5", "roadNet-PA",
           "roadNet-TX", "rrmatv7e5e5e6", "web-Google", "web-NotreDame", "web-Stanford", "WikiTalk",
           "wiki-topcats", "wiki-topcatspre"]


#allgraphs=[ "amazon", "Email-Enron", "pokecr", "rm1e5u89e5", "rmatv1e5e1e7", "roadNet-PA", "roadNet-TX", "rpatent", "rrmatv1e5e1e7", "rrmatv7e5e5e6", "soc-sign-bitcoinalpha", "soc-sign-bitcoinotc", "web-BerkStan", "WikiTalk", "wiki-topcats"]


prop=["adjecency", "ClusterCoeff", "AED", "sparsity"]

def extractdata(argv):
    print len(allgraphs)
    properties = [[0 for col in range(len(prop))] for row in range(len(allgraphs))]
    graphpos = -1
    found = []
    with open(argv[0]) as fp:
        for line in fp:
                if ".edge" in line:
                    # find the position of graph
                    #remove .edge
                    graphname = line[: -6]
                    try:
                        graphpos = allgraphs.index(graphname)
                    except ValueError:
                        graphpos = -1
                    if graphpos in found:
                        print "found multiple definitions"
                        assert 1 == 0
                    if graphpos != -1:
                        found.append(graphpos)                    
                if "Adjecency" in line:
                    if graphpos != -1:
                        properties[graphpos][0] = float(line[line.find("=")+1: ])
                if "ClusterCoeff" in line:
                    if graphpos != -1:
                        properties[graphpos][1] = float(line[line.find("=")+1: ])
                if "Distance" in line:
                    if graphpos != -1:
                        properties[graphpos][2] = float(line[line.find("=")+1: ])
                if "Sparsity" in line:
                    if graphpos != -1:
                        properties[graphpos][3] = float(line[line.find("=")+1: ])
    print len(found)
    with open('graphproperties.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(properties)
                
                    
if __name__ == '__main__':
    extractdata(sys.argv[1:])



