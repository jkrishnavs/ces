#reducing configurations
freqs=(train12K train14K train16K train18K)
cp="-cp ./:./flanagan.jar"

configfolder=(trainn4b2l2b1400000 trainn4b2l2b1600000 trainn4b2l2b1800000 trainn4b4l0b1400000 trainn4b4l0b1600000 trainn4b4l0b1800000 trainn6b2l4b1400000 trainn6b2l4b1600000 trainn6b2l4b1800000 trainn8b4l4b1200000 trainn8b4l4b1400000 trainn8b4l4b1600000 trainn8b4l4b1800000 trainn8b4l4b2000000 )

conindex=0
python extractgraphproperties.py trainproperties.txt
for config in "${configfolder[@]}"
do
    index=0
    cf="${configs[$conindex]}"
    echo $config
    python trainingdata.py $config
    mv edp.csv ${config}.csv
    #mv chunks.csv ${config}.csv
done


python createtrainsets.py
javac $cp codegen.java
java -ea $cp codegen &>out
test -s out
if [ $? -eq 0 ]
then
    cat out
    #    exit
fi

index=0
CLASSFIT="MultiregressionEDP"
LFILE=$CLASSFIT".java"
rm $LFILE || true
echo "package imop.ces.data.classifier.Input;" >> $LFILE
echo "import imop.ces.data.classifier.*;" >> $LFILE
echo "import imop.Main;" >> $LFILE
echo "import imop.ces.enums.*;" >> $LFILE
echo "public class "$CLASSFIT" {" >> $LFILE
echo "@Override" >> $LFILE
echo "public void update(Classifier cf) {" >> $LFILE
echo "}" >> $LFILE
COEFFNAME="coeff"
echo "@Override" >> $LFILE
echo "public void "$FNNAME"(Classifier c, CostFunction cf) {" >>$LFILE
python getEigenValues.py $LFILE 
echo "   }" >> $LFILE
echo "}" >> $LFILE
echo "}" >> $LFILE
index=$(( $index + 1 ))

rm edp*.txt
