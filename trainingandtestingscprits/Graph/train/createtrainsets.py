
import sys
#import getmaxaandavg as maxavg
#import gettimeandverify as timev
import csv
import os.path
import numpy as np

#import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score




configfolder=["trainn4b2l2b1400000", "trainn4b2l2b1600000",
              "trainn4b2l2b1800000", "trainn4b4l0b1400000",
              "trainn4b4l0b1600000", "trainn4b4l0b1800000",
              "trainn6b2l4b1400000", "trainn6b2l4b1600000",
              "trainn6b2l4b1800000", "trainn8b4l4b1200000",
              "trainn8b4l4b1400000", "trainn8b4l4b1600000",
              "trainn8b4l4b1800000", "trainn8b4l4b2000000"]



allgraphs=["academia2Anonymized", "amazon", "amazonpre", "com-dblp", "com-youtube",
           "r1e6u99e5", "r7e5u56e5", "r7eu599e5", "randcom-dblp", "randcom-youtube",
           "randrmat125e4u325e4", "randrmat1e5u7e5", "randrmat225e4u725e4",
           "randrmat3e5u33e5", "randrmat3e5u7e6", "randrmat45e4u7e6", "randrmat65e4u165e5",
           "randTheMarkerAnonymized", "rmat125e4u325e4", "rmat225e4u725e4",
           "rmat3e5u33e5", "rmat3e5u7e6", "rmat45e4u7e6", "rmat65e4u165e5", "roadNet-PA",
           "roadNet-TX", "rrmatv7e5e5e6", "web-Google", "web-NotreDame", "web-Stanford", "WikiTalk",
           "wiki-topcats", "wiki-topcatspre"]

multiples=[25,25,4,25]
nooftest=4
trainset=[[[4.0, 1.0, 24.0], [0.0, 2.0, 0.0], [0.0, 0.0, 0.0], [18.0, 8.0, 6.0]],
          [[0.0, 2.0, 16.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0], [8.0, 5.0, 7.0]],
          [[0.5, 0.5, 33.0], [0.0, 0.25, 0.0], [0.0, 0.0, 0.5], [2.75, 2.25, 5.5]],
          [[0.0, 1.0, 11.0], [0.0, 3.0, 0.0], [0.0, 0.0, 0.0], [0.0, 6.0, 7.0]]]



# these should be identical to the runall.sh configs and
# frequencies
configs=["4b4l0", "8b4l4", "4b2l2",  "6b2l4"]
#littleconfig="4b0l4"
#configs=["4b4l0","8b4l4", "4b2l2", "6b2l4"]
#freq=["12K", "14K",  "16K", "18K", "2K"]
frequencies=["18K",  "16K", "14K",  "12K"]


def createtable(argv):
    # to do extract gr
    graphproperties = np.genfromtxt('graphproperties.csv', delimiter=',')
    
    for i in range(len(allgraphs)):
        graphproperties[i][3] = graphproperties[i][3] * 1000    
    print graphproperties
    #print len(graphproperties)
    #print len(graphproperties[0])

    xvec=[]
    scalevec=[]
    for i in range(len(allgraphs)):
        for j in range(nooftest):
            st1=trainset[j][0][0] * (graphproperties[i][0] * graphproperties[i][0]) + trainset[j][0][1] * (graphproperties[i][0]) + trainset[j][0][2]
            rt1=trainset[j][1][0] * (graphproperties[i][0] * graphproperties[i][0]) + trainset[j][1][1] * (graphproperties[i][0]) + trainset[j][1][2]
            lt1=trainset[j][2][0] * (graphproperties[i][0] * graphproperties[i][0]) + trainset[j][2][1] * (graphproperties[i][0]) + trainset[j][2][2]
            at1=trainset[j][3][0] * (graphproperties[i][0] * graphproperties[i][0]) + trainset[j][3][1] * (graphproperties[i][0]) + trainset[j][3][2]
            #print "mydatas", st1,  rt1, lt1, at1
            st1 = st1/at1
            rt1 = rt1/at1
            lt1 = lt1/at1
            scale = (trainset[j][3][0] + trainset[j][2][0] + trainset[j][1][0] + trainset[j][0][0]) * (graphproperties[i][0] * graphproperties[i][0]) + (trainset[j][3][1] + trainset[j][2][1] + trainset[j][1][1] + trainset[j][0][1]) * graphproperties[i][0] +  (trainset[j][3][2] + trainset[j][2][2] + trainset[j][1][2] + trainset[j][0][2])
            scale = scale * multiples[j]
            vecr1 = [graphproperties[i][0], graphproperties[i][1], graphproperties[i][2], graphproperties[i][3], st1, rt1, lt1, 1]
            xvec.append(vecr1)
            scalevec.append(scale)

    allxvector = np.array(xvec);
    
    #print "ALL  xvector", allxvector
    #print "X vector size", len(allxvector)
    #print scalevec

    
    f = open("codegen.java", "w+")
    f.write("import flanagan.analysis.Regression;\n")
    f.write("public class codegen {\n")
    f.write("  public static void main (String[] args) { \n\n")
    xVal = "x_val"
    f.write("\n\n\n double " + xVal +"[][] = {")
    rowflag = 0
    for i in range(len(allxvector[0])):
        if rowflag != 0:
            f.write(", ")
        f.write("{")
        rowflag = 1
        flag = 0
        for j in range(len(allxvector)):
            if flag != 0:
                f.write(", ")
            flag = 1
            f.write(str(allxvector[j][i]))
        f.write("}")
    f.write("};")

    
#=============== FROM HERE========================================
    for conf in configfolder:
        edpvec=[]
        edpfile = conf +".csv"
        edp = np.genfromtxt(edpfile, delimiter=',')
        sid = 0
        #print len(edp)
        #print len(edp[0])
        #print len(scalevec)
        for i in range(len(allgraphs)):
            for dj in range(nooftest):
                edpvec.append((edp[i][dj]/scalevec[sid]))
                sid += 1
        #print(sid)
        #print "This edpvec", len(edpvec)
        edpvector= np.array(edpvec)
        regVal = "multreg" + conf 
        f.write("\n Regression " + regVal + ";\n")
        yval = "edp" + conf ;
        f.write("\n\n\n double " + yval +"[] = {")
        flag = 0
        for j in range(len(edpvector)):
            if flag != 0:
                f.write(", ")
            flag = 1
            f.write(str(edpvector[j]))
        f.write("}; \n\n")
        f.write(regVal + " = new Regression(" + xVal +", "+  yval + ");\n")
        f.write(regVal + ".linearGeneral();\n")
        f.write(regVal + ".print(\"" + "edp" + conf +".txt\");\n")
        #print len(allxvector), len(edpvector)
        regr = linear_model.LinearRegression()
            
    #regr.fit(allxvector, edpvector)
    #print regr.score(allxvector, edpvector)

    # coeffName="coeff"
    #print('Coefficients: \n', regr.coef_)
    #confarray = np.array(regr.coef_)
    #doubleName = coeffName + "8b4l42Kedp"
    # strval = "static final double[] " + doubleName + " = { "
    # flag = 0
    # for i in range(len(confarray)):
    #     if flag == 1:
    #         strval += ", " + str(confarray[i])
    #     else:
    #         strval += str(confarray[i])
    #         flag = 1
    # strval += "};"
    
    #print strval
    #coeffName="coeff"
    #print('Coefficients: \n', regr.coef_)
    
    #t = regr.predict(allxvector)
    f.write("}}")


if __name__ == '__main__':
    createtable(sys.argv[1:])
