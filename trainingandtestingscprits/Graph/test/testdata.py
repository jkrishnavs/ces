import sys
#import getmaxaandavg as maxavg
#import gettimeandverify as timev
import csv
import os.path
import shutil
import re
import numpy as np
from copy import copy, deepcopy

exelist = ["communities", "conduct", "pageRank", "sssp", "triangleCounting"]
sssppos = 3
exelisttex = ["\\texttt{com}", "\\texttt{con}", "\\texttt{pR}", "\\texttt{sssp}", "\\texttt{tc}"]
exelistplot = ["com", "con", "pR", "sssp", "tc"]
inputtype=["base", "preprocessed"]
#dylist = ["dynamic"]

configs=["n8b4l4b2000000", "n8b4l4b1800000", "n8b4l4b1600000", "n8b4l4b1400000", "n8b4l4b1200000",
         "n6b2l4b1800000", "n6b2l4b1600000", "n6b2l4b1400000",
         "n4b4l0b1800000", "n4b4l0b1600000", "n4b4l0b1400000",    
         "n4b2l2b1800000", "n4b2l2b1600000", "n4b2l2b1400000"]

basecase = "base";

baseindex=0
hwlist=["4L4b(2)", "4L4b(1.8)",  "4L4b(1.6)",  "4L4b(1.4)", "4L4b(1.2)",
        "4L2b(1.8)",  "4L2b(1.6)",  "4L2b(1.4)", 
        "0L4b(1.8)",  "0L4b(1.6)",  "0L4b(1.4)", 
        "2L2b(1.8)",  "2L2b(1.6)",  "2L2b(1.8)"]
allgraphs=["higgs",  "patent",  "pokec",  "roadNet-CA" , "web-BerkStan" , "rm1e6u8e6" , "rm1e6u35e6",     "rmatv7e5e5e6"]
allgraphstex=["\\texttt{hig}",  "\\texttt{pat}",  "\\texttt{pok}",  "\\texttt{rca}",  "\\texttt{web}",  "\\texttt{rg1}",  "\\texttt{rg2}" , "\\texttt{rma}"]
allgraphsplot=["hig",  "pat",  "pok",  "rca",  "web",  "rg1",  "rg2" , "rma"]

nooffiles= 7


def gettime(filename, times):
    timevalues = [0 for row in range(times)]
    rowVal =0
    with open(filename) as fp:
        for line in fp:
            if "Algorithm Running Time" in line:
                timeVal = float(line[line.find("=")+1:-3])
                timevalues[rowVal] = timeVal
                rowVal += 1
    return timevalues



def getenergy(folder, timevalues,exename, times):
    avgEnergy = [0 for col in range(times)] 
    colVal = 0
    for n in range(times):
        inputfile = folder + "/"+ exename  + "." +str(n) + ".csv"
        print inputfile
        if os.path.isfile(inputfile) == False:
            print "ERROR File not found "+ inputfile
            return [[]]
        lineNos = 0
        tot = 0
        entry = 0
        with open(inputfile) as fp:
            for line in fp:
                lineNos += 1
                fc = line.split(",")
                if(len(fc)>3 and lineNos > 1):
                    powVal = float(fc[len(fc) -1])
                    if powVal > 0: 
                        entry += 1
                        tot += powVal 
        if entry != 0:
            avgPow  = tot/ entry
        else:
            assert 1 == 0
        avgEnergy[colVal] = timevalues[colVal] * avgPow
        colVal += 1
    assert colVal == times
    return avgEnergy


overalledp = np.full((len(exelist),len(allgraphs), len(configs), len(inputtype)), 0.0)
overalltime = np.full((len(exelist),len(allgraphs), len(configs), len(inputtype)), 0.0)
overallEnergy = np.full((len(exelist),len(allgraphs), len(configs), len(inputtype)), 0.0)
def extractdata(argv):    
    metrics=[ "Avg Time", "Avg Total Energy", "EDP"]
    totEnergyPos = 7
    edpbest=[[0 for col in range(len(exelist)*2)] for row in range (len(allgraphs))]
    for conf in configs:
        cid = configs.index(conf)
        for exe in exelist:
            eid = exelist.index(exe)
            for graph in allgraphs:
                gid = allgraphs.index(graph)
                for gtype in inputtype:
                    tid = inputtype.index(gtype)
                    folder = conf + "/" + exe +"." + graph
                    # if conf == "n8b4l4b2000000":
                    #     folder = folder + ".base"
                    #     if gtype == "preprocessed":
                    #         folder  = folder + "/" + "opt"
                    #         timefile = folder + "/" + exe + "." + graph + ".base.preprocessed.out.txt"
                    #     else:
                    #         folder  = folder + "/" + "base"
                    #         timefile = folder + "/" + exe + "." + graph + ".base.out.txt"
                    # elif gtype == "preprocessed":
                    #     folder = folder + ".preprocessed"
                    #     if gtype  == "preprocessed":
                    #         timefile = folder + "/" + exe + "." + graph + ".preprocessed.out.txt"
                    #     else:
                    #         timefile = folder + "/" + exe + "." + graph + ".out.txt"
                    if gtype == "preprocessed":
                        folder = folder + ".preprocessed"
                        timefile = folder + "/" + exe + "." + graph + ".preprocessed.out.txt"
                    else:
                        timefile = folder + "/" + exe + "." + graph + ".out.txt"
                    print timefile
                    timevalues = gettime(timefile, nooffiles)
                    energyvalues = getenergy(folder, timevalues, exe, nooffiles)
                    sumVal = 0
                    sumEnergy = 0
                    sumTime = 0
                    for itr in range(nooffiles):
                        sumTime = sumTime + timevalues[itr]
                        sumEnergy = sumEnergy + energyvalues[itr]
                        sumVal = sumVal + timevalues[itr] * energyvalues[itr] 
                    sumVal = sumVal/ 1000000
                    sumEnergy = sumEnergy / 1000
                    sumTime = sumTime  / 1000
                    overalledp[eid][gid][cid][tid] = sumVal 
                    overalltime[eid][gid][cid][tid] = sumTime
                    overallEnergy[eid][gid][cid][tid] = sumEnergy

                    
    
def comparedata(args):
    print overalledp
    print overalltime
    print overallEnergy
    avgexes = [[0 for col in range(3)] for row in range(len(exelist))]
    avggraphs = [[0 for col in range(3)] for row in range(len(allgraphs))]
    avgchunks = [[0 for col in range(2)]  for row in range(len(allgraphs))]  

    for i in range(len(exelist)):
        for j in range(len(allgraphs)):
            with open(str( exelist[i]+allgraphs[j] + '.edp.data.csv'), 'wb') as f:
                writer = csv.writer(f)
                writer.writerows(overalledp[i][j])
            with open(str( exelist[i]+allgraphs[j] + '.time.data.csv'), 'wb') as f:
                writer = csv.writer(f)
                writer.writerows(overalltime[i][j])
            with open(str( exelist[i]+allgraphs[j] + '.energy.data.csv'), 'wb') as f:
                writer = csv.writer(f)
                writer.writerows(overallEnergy[i][j])


    for i in range (len(exelist)):
        for j in range (len(allgraphs)):
            for k in range (len(configs)):
                #print i,j,k
                assert overalledp[i][j][k][0] != 0
                assert overalledp[i][j][k][1] != 0


    sf = open("selections.tex", "w") 
    selectedfile = args[0]
    selections=[[0 for col in range(len(exelist)*2)] for row in range (len(allgraphs))]
    with open(selectedfile) as fp:
        for line in fp:
            if "optimalConfig" in line:
                fc = line.split()              
                config = fc[len(fc)-1]
                id = configs.index(config)
                assert id != -1
                hwconfig = hwlist[id]
                # find graph id and exelid
                exeid = -1
                graphid = -1
                for graph in allgraphs:
                    if graph in line:
                        assert graphid == -1
                        graphid = allgraphs.index(graph)
                for exe in exelist:
                    if exe in line:
                        assert exeid == -1
                        exeid = exelist.index(exe)
                # 0 for base 1 for preprocessed
                if "preprocessed" in line:
                    baseid = 1
                else:
                    baseid = 0
                assert graphid != -1
                assert exeid != -1
                assert selections[graphid][exeid*2+baseid] == 0
                selections[graphid][exeid*2+baseid] = hwconfig


    
    with open(str('fileselections.csv'), 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(selections)


    basebestedp = [[0 for col in range(len(exelist)*2)] for row in range(len(allgraphs))]
    prebestedp = [[0 for col in range(len(exelist)*2)] for row in range(len(allgraphs))]
    for graph in allgraphs:
        gid = allgraphs.index(graph)
        for exe in exelist:
            eid = exelist.index(exe)
            bestedp= overalledp[eid][gid][0][0]
            selection = configs[0]
            for conf in configs:
                cid = configs.index(conf)
                if bestedp < overalledp[eid][gid][cid][0]:
                    bestedp = overalledp[eid][gid][cid][0]
                    selection = conf
            basebestedp[gid][2* eid] = hwlist[configs.index(selection)]
            basebestedp[gid][2* eid+1] = bestedp
            bestedp= overalledp[eid][gid][0][1]
            selection = configs[0]
            for conf in configs:
                cid = configs.index(conf)
                if bestedp < overalledp[eid][gid][cid][1]:
                    bestedp = overalledp[eid][gid][cid][1]
                    selection = conf
            prebestedp[gid][2* eid] = hwlist[configs.index(selection)]
            prebestedp[gid][2* eid+1] = bestedp
            
    
    avgGraphcomparetobestcase = np.full((len(allgraphs),2), 0.0)
    avgAlgocomparetobestcase = np.full((len(exelist),2), 0.0)
    avgBaseEnergyGraphCase = np.full((len(allgraphs),3), 0.0)
    avgBaseEnergyAlgoCase = np.full((len(exelist),3), 0.0)
    avgBaseTimeGraphCase = np.full((len(allgraphs),3), 0.0)
    avgBaseTimeAlgoCase = np.full((len(exelist),3), 0.0)


    #find brute fore best case  and bestcase configuration  
    for exe in exelist:
        bestselect = [[0 for col in range(4)] for row in range(len(allgraphs))]
        base=0
        prebase=1
        selectionbase=2
        preselection=3
        basebest=4
        prebest=5
        imppre=6
        impselection=7
        imppreselection=8
        dipbase=9
        dippre=10
        chunkbase=11
        chunkpre=12
        advantage = np.zeros((len(allgraphs), 13))
        exeid = exelist.index(exe)
        avgbestEntry = np.full((len(allgraphs),2), 0.0)
        avgEnergyEntry = np.full((len(allgraphs),3), 0.0)
        avgtimeEntry = np.full((len(allgraphs),3), 0.0)
        
        for graph in allgraphs:
            gid = allgraphs.index(graph)
            #print exeid, gid
            bestselect[gid][1]= basebestedp[gid][2*exeid]
            bestselect[gid][3]= prebestedp[gid][2*exeid]
            bestselect[gid][0]= selections[gid][2*exeid]
            bestselect[gid][2]= selections[gid][2*exeid+1]

            advantage[gid][base] = overalledp[exeid][gid][baseindex][0]
            energybase =  overallEnergy[exeid][gid][baseindex][0]
            timebase =  overalltime[exeid][gid][baseindex][0]

            advantage[gid][prebase] = overalledp[exeid][gid][baseindex][1]
            energypre = overallEnergy[exeid][gid][baseindex][1]
            timepre = overalltime[exeid][gid][baseindex][1]

            advantage[gid][selectionbase] = overalledp[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
            energyselection = overallEnergy[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
            timeselection = overalltime[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
            
            advantage[gid][preselection] = overalledp[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]
            energypreselection = overallEnergy[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]
            timepreselection = overalltime[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]


            avgEnergyEntry[gid][0] = (energybase - energypre)/energybase * 100
            avgEnergyEntry[gid][1] = (energybase - energyselection)/energybase * 100
            avgEnergyEntry[gid][2] = (energybase - energypreselection)/energybase * 100
           
            avgtimeEntry[gid][0] = (timebase - timepre)/timebase * 100
            avgtimeEntry[gid][1] = (timebase - timeselection)/timebase * 100
            avgtimeEntry[gid][2] = (timebase - timepreselection)/timebase * 100
            
            
            
            advantage[gid][basebest] = basebestedp[gid][2*exeid+1]
            advantage[gid][prebest] = prebestedp[gid][2*exeid+1]
            
            advantage[gid][imppre] = (advantage[gid][base] - advantage[gid][prebase])/advantage[gid][base] * 100

            advantage[gid][impselection] =  (advantage[gid][base] - advantage[gid][selectionbase])/advantage[gid][base] * 100
            advantage[gid][imppreselection] = (advantage[gid][base] - advantage[gid][preselection])/advantage[gid][base] * 100

            advantage[gid][dipbase] = (advantage[gid][selectionbase] - advantage[gid][basebest] )/advantage[gid][selectionbase]* 100
            #if exe == "sssp":
            #    print "sssp", advantage[gid][selectionbase] , advantage[gid][basebest]
            avgbestEntry[gid][0] = advantage[gid][dipbase]
            advantage[gid][dippre] = (advantage[gid][preselection] - advantage[gid][prebest] )/advantage[gid][preselection] * 100
            avgbestEntry[gid][1] = advantage[gid][dippre]

        assert avgAlgocomparetobestcase[exeid][0] == 0.0
        assert avgAlgocomparetobestcase[exeid][1] == 0.0
        assert avgBaseTimeAlgoCase[exeid][0] == 0.0
        assert avgBaseTimeAlgoCase[exeid][1] == 0.0
        assert avgBaseTimeAlgoCase[exeid][2] == 0.0
        assert avgBaseEnergyAlgoCase[exeid][0] == 0.0
        assert avgBaseEnergyAlgoCase[exeid][1] == 0.0
        assert avgBaseEnergyAlgoCase[exeid][2] == 0.0
        
        for gid in range(len(allgraphs)):
            avgAlgocomparetobestcase[exeid][0] += avgbestEntry[gid][0]
            avgAlgocomparetobestcase[exeid][1] += avgbestEntry[gid][1]
            avgBaseTimeAlgoCase[exeid][0] +=  avgtimeEntry[gid][0]
            avgBaseTimeAlgoCase[exeid][1] += avgtimeEntry[gid][1]
            avgBaseTimeAlgoCase[exeid][2] += avgtimeEntry[gid][2]
            avgBaseEnergyAlgoCase[exeid][0] += avgEnergyEntry[gid][0]
            avgBaseEnergyAlgoCase[exeid][1] += avgEnergyEntry[gid][1]
            avgBaseEnergyAlgoCase[exeid][2] += avgEnergyEntry[gid][2]


        avgAlgocomparetobestcase[exeid][0] /= len(allgraphs) 
        avgAlgocomparetobestcase[exeid][1] /= len(allgraphs) 
        avgBaseTimeAlgoCase[exeid][0] /= len(allgraphs) 
        avgBaseTimeAlgoCase[exeid][1] /= len(allgraphs) 
        avgBaseTimeAlgoCase[exeid][2] /= len(allgraphs) 
        avgBaseEnergyAlgoCase[exeid][0] /= len(allgraphs) 
        avgBaseEnergyAlgoCase[exeid][1] /= len(allgraphs) 
        avgBaseEnergyAlgoCase[exeid][2] /= len(allgraphs) 
        

            
            
        with open(str(exe + 'advantages.csv'), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(advantage)

        with open(str(exe + 'selectionswithbest.csv'), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(bestselect)

        #selections with best in latex.
        sf.write("\n\n\\begin{table}\n\\caption{The predicted and best configuration for Algorithm "
                 + exelisttex[exeid] +". }\n\\label{tab:"+ exe
                 +"}\n \\begin{tabular}{|p{1cm}|p{1.2cm}|p{1.2cm}|p{1.2cm}|p{1.2cm}|} \n")        
        sf.write("\\hline \n  & \multicolumn{2}{c|}{Base case} & \multicolumn{2}{c|}{Preprocessed}  \\\\ \n \\hline \n")
        sf.write("\n Graph & Pred & Best & Pred & Best \\\\ \n \\hline \n")
        for ti in range(len(allgraphs)):
            sf.write(allgraphstex[ti] + " & " + bestselect[ti][0]   + " & " + bestselect[ti][1]
                    + " & " + bestselect[ti][2]  + " & " + bestselect[ti][3] + " \\\\ \n")
        sf.write("\\hline \n \\end{tabular} \n \\end{table} \n" )
        
        f = open(str(exe+"gains.data"), "w")
        f.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
        avglist = np.full(3,0.0)
        for ei in range(len(allgraphs)):
            f.write(allgraphsplot[ei] +"\t" +str(round(advantage[ei][imppre],2)) + "\t" + str(round(advantage[ei][impselection],2)) + "\t"
                    + str(round(advantage[ei][imppreselection],2)) + "\n")
            avglist[0] += advantage[ei][imppre]
            avglist[1] += advantage[ei][impselection]
            avglist[2] += advantage[ei][imppreselection]

        avglist =  [i/len(allgraphs) for i in avglist]
        avgexes[exeid][0] = avglist[0]
        avgexes[exeid][1] = avglist[1]
        avgexes[exeid][2] = avglist[2]
        
        
        f.write("Avg\t"+str(round(avglist[0],2))+"\t"+str(round(avglist[1],2))+"\t"+str(round(avglist[2],0))+"\n")
        
        f.close()

        #add average
        


        f = open(str(exe+"gainswithbest.data"), "w")
        avglist = np.full(2,0.0)
        f.write("\tpre\tPred\tPred+pre\tPredVBest\tPredPreVBest\n")
        for ei in range(len(allgraphs)):
            f.write(allgraphsplot[ei] +"\t" + str(round(advantage[ei][imppre],2)) + "\t" + str(round(advantage[ei][impselection],2))
                    + "\t" + str(round(advantage[ei][imppreselection],2)) + "\t" +
                    str(round(advantage[ei][dipbase],2)) + "\t" + str(round(advantage[ei][dippre],2)) + "\n")
        f.close()
        
        
    af = open("algobest.data", "w")
    avg= [0.0,0.0]
    af.write("\t P_{bl}v/sO_{bl}\t P_{bl}v/sO_{bl}(GC)\n")
    for ei in range((len(exelist))):
        af.write(exelistplot[ei] + "\t" + str(round(avgAlgocomparetobestcase[ei][0],2))
                 + "\t" + str(round(avgAlgocomparetobestcase[ei][1],2)) + "\n")
        avg[0] +=avgAlgocomparetobestcase[ei][0]
        avg[1] += avgAlgocomparetobestcase[ei][1]
    af.write("Avg\t" + str(round(avg[0]/len(exelist),2))
             + "\t" + str(round(avg[1]/len(exelist),2)) + "\n")
    af.close()

    af = open("algotime.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(exelist))):
        af.write(exelistplot[ei] + "\t" + str(round(avgBaseTimeAlgoCase[ei][0],2)) + "\t"
                 + str(round(avgBaseTimeAlgoCase[ei][1],2)) + "\t"
                 + str(round(avgBaseTimeAlgoCase[ei][2],2)) + "\n"  )
        avg[0] += avgBaseTimeAlgoCase[ei][0]
        avg[1] += avgBaseTimeAlgoCase[ei][1]
        avg[2] += avgBaseTimeAlgoCase[ei][2]
    af.write("Avg\t" + str(round(avg[0]/len(exelist),2))
                 + "\t" + str(round(avg[1]/len(exelist),2))
             + "\t" + str(round(avg[2]/len(exelist),2))+ "\n")

    
    af.close()


    af = open("algoEnergy.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(exelist))):
        af.write(exelistplot[ei] + "\t" + str(round(avgBaseEnergyAlgoCase[ei][0],2)) + "\t"
                 + str(round(avgBaseEnergyAlgoCase[ei][1],2)) + "\t"
                 + str(round(avgBaseEnergyAlgoCase[ei][2],2)) + "\n"  )
        avg[0] += avgBaseEnergyAlgoCase[ei][0]
        avg[1] += avgBaseEnergyAlgoCase[ei][1]
        avg[2] += avgBaseEnergyAlgoCase[ei][2]
        
    af.write("Avg\t" + str(round(avg[0]/len(exelist),2))
             + "\t" + str(round(avg[1]/len(exelist),2))
             + "\t" + str(round(avg[2]/len(exelist),2))+ "\n")
    af.close()

    

    #graph over exe
    for graph in allgraphs:
        #Selections with 4 columns Base selected & best
        #pre processed selected and best
        bestselect = [[0 for col in range(4)] for row in range(len(exelist))]
        # adv columns: base, pre+base, selection+base, pre+selection
        # base-best, pre+best , % pre+base ,% selection+ base
        # % pre+selection, % from base best, % from pre+ base
        base=0
        prebase=1
        selectionbase=2
        preselection=3
        basebest=4
        prebest=5
        imppre=6
        impselection=7
        imppreselection=8
        dipbase=9
        dippre=10
        chunkbase=11
        chunkpre=12
        advantage = np.zeros((len(exelist), 13))
        gid = allgraphs.index(graph)
        
        avgbestEntry = np.full((len(exelist),2), 0.0)
        avgEnergyEntry = np.full((len(exelist),3), 0.0)
        avgTimeEntry = np.full((len(exelist),3), 0.0)
        
        for exe in exelist:
            exeid = exelist.index(exe)
            bestselect[exeid][1]= basebestedp[gid][2*exeid]
            bestselect[exeid][3]= prebestedp[gid][2*exeid]
            bestselect[exeid][0]= selections[gid][2*exeid]
            bestselect[exeid][2]= selections[gid][2*exeid+1]
            
            advantage[exeid][base] = overalledp[exeid][gid][baseindex][0]
            energybase =  overallEnergy[exeid][gid][baseindex][0]
            timebase =  overalltime[exeid][gid][baseindex][0]
            advantage[exeid][prebase] = overalledp[exeid][gid][baseindex][1]
            energypre = overallEnergy[exeid][gid][baseindex][1]
            timepre = overalltime[exeid][gid][baseindex][1]
            
            
            advantage[exeid][selectionbase] = overalledp[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
            energyselection = overallEnergy[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
            timeselection = overalltime[exeid][gid][hwlist.index(selections[gid][2*exeid])][0]
    
            
            advantage[exeid][preselection] = overalledp[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]
            energypreselection = overallEnergy[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]
            timepreselection = overalltime[exeid][gid][hwlist.index(selections[gid][2*exeid+1])][1]

            
            avgEnergyEntry[exeid][0] = (energybase - energypre)/energybase * 100
            avgEnergyEntry[exeid][1] = (energybase - energyselection)/energybase * 100
            avgEnergyEntry[exeid][2] = (energybase - energypreselection)/energybase * 100

            avgtimeEntry[exeid][0] = (timebase - timepre)/timebase * 100
            avgtimeEntry[exeid][1] = (timebase - timeselection)/timebase * 100
            avgtimeEntry[exeid][2] = (timebase - timepreselection)/timebase * 100


       
            advantage[exeid][basebest] = basebestedp[gid][2*exeid+1]
            advantage[exeid][prebest] = prebestedp[gid][2*exeid + 1]

            advantage[exeid][imppre] = (advantage[exeid][base] - advantage[exeid][prebase])/advantage[exeid][base] * 100
            advantage[exeid][impselection] =  (advantage[exeid][base] - advantage[exeid][selectionbase])/advantage[exeid][base] * 100
            advantage[exeid][imppreselection] = (advantage[exeid][base] - advantage[exeid][preselection])/advantage[exeid][base] * 100
            #chunk optimization

            advantage[exeid][dipbase] = (advantage[exeid][selectionbase] - advantage[exeid][basebest])/advantage[exeid][selectionbase] * 100
            
            advantage[exeid][dippre] = ( advantage[exeid][preselection] - advantage[exeid][prebest])/ advantage[exeid][preselection]* 100

            avgbestEntry[exeid][0] = advantage[exeid][dipbase]
            avgbestEntry[exeid][1] = advantage[exeid][dippre]


        
        assert avgGraphcomparetobestcase[gid][0] == 0.0
        assert avgGraphcomparetobestcase[gid][1] == 0.0
        assert avgBaseTimeGraphCase[gid][0] == 0.0
        assert avgBaseTimeGraphCase[gid][1] == 0.0
        assert avgBaseTimeGraphCase[gid][2] == 0.0
        assert avgBaseEnergyGraphCase[gid][0] == 0.0
        assert avgBaseEnergyGraphCase[gid][1] == 0.0
        assert avgBaseEnergyGraphCase[gid][2] == 0.0
        


        for exeid in range(len(exelist)):
            avgGraphcomparetobestcase[gid][0] += avgbestEntry[exeid][0]
            avgGraphcomparetobestcase[gid][1] += avgbestEntry[exeid][1]
            avgBaseTimeGraphCase[gid][0] +=  avgtimeEntry[exeid][0]
            avgBaseTimeGraphCase[gid][1] += avgtimeEntry[exeid][1]
            avgBaseTimeGraphCase[gid][2] += avgtimeEntry[exeid][2]
            avgBaseEnergyGraphCase[gid][0] += avgEnergyEntry[exeid][0]
            avgBaseEnergyGraphCase[gid][1] += avgEnergyEntry[exeid][1]
            avgBaseEnergyGraphCase[gid][2] += avgEnergyEntry[exeid][2]




        avgGraphcomparetobestcase[gid][0] /= len(exelist) 
        avgGraphcomparetobestcase[gid][1] /= len(exelist) 
        avgBaseTimeGraphCase[gid][0] /= len(exelist) 
        avgBaseTimeGraphCase[gid][1] /= len(exelist) 
        avgBaseTimeGraphCase[gid][2] /= len(exelist) 
        avgBaseEnergyGraphCase[gid][0] /= len(exelist) 
        avgBaseEnergyGraphCase[gid][1] /= len(exelist) 
        avgBaseEnergyGraphCase[gid][2] /= len(exelist) 
        


        with open(str(graph + 'advantages.csv'), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(advantage)

        with open(str(graph + 'selectionswithbest.csv'), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(bestselect)

        #selections with best in latex.
        sf.write("\n\n\\begin{table}\n\\caption{The predicted and best configuration for graph "
                 + allgraphstex[gid] +". }\n\\label{tab:"+ graph
                 +"}\n \\begin{tabular}{|p{1cm}|p{1.2cm}|p{1.2cm}|p{1.2cm}|p{1.2cm}|} \n")        
        sf.write("\\hline \n  & \multicolumn{2}{c|}{Base case} & \multicolumn{2}{c|}{Preprocessed}  \\\\ \n \\hline \n")
        sf.write("\n Algo & Pred & Best & Pred & Best \\\\ \n \\hline \n")
        for ti in range(len(exelist)):
            sf.write(exelisttex[ti] + " & " + bestselect[ti][0]   + " & " + bestselect[ti][1]
                    + " & " + bestselect[ti][2]  + " & " + bestselect[ti][3] + " \\\\ \n")
        sf.write("\\hline \n \\end{tabular} \n \\end{table} \n" )


        f = open(str(graph+"gains.data"), "w")
        f.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
        avglist = np.full(3,0.0)
        for ei in range(len(exelist)):
            f.write(exelistplot[ei] + "\t"+ str(round(advantage[ei][imppre],2)) + "\t" + str(round(advantage[ei][impselection],2)) + "\t"
                    + str(round(advantage[ei][imppreselection],2)) + "\n")
            avglist[0] += advantage[ei][imppre]
            avglist[1] += advantage[ei][impselection]
            avglist[2] += advantage[ei][imppreselection]
        avglist =  [i/len(exelist) for i in avglist]
        f.write("Avg\t"+str(round(avglist[0],2))+"\t"+str(round(avglist[1],2))+"\t"+str(round(avglist[2],0))+"\n")

        avggraphs[gid][0] = avglist[0]
        avggraphs[gid][1] = avglist[1]
        avggraphs[gid][2] = avglist[2]
        f.close()

        
        f = open(str(graph+"chunkgains.data"), "w")
        f.write("\tP_{bl}\t GC+P_{bl}\n")
        avglist = np.full(2,0.0)
        for ei in range(len(exelist)):
            f.write(exelistplot[ei] + "\t"+  str(round(advantage[ei][chunkbase],2)) + "\t"
                    + str(round(advantage[ei][chunkpre],2)) + "\n")
            avglist[0] += advantage[ei][chunkbase]
            avglist[1] += advantage[ei][chunkpre]
        avglist =  [i/len(exelist) for i in avglist]
        f.write("Avg\t"+str(round(avglist[0],2))+"\t"+str(round(avglist[1],2))+"\n")
        avgchunks[gid][0] = avglist[0]
        avgchunks[gid][1] = avglist[1]
        f.close()

        
        f = open(str(graph+"gainswithbest.data"), "w")
        f.write("\tpre\tPred\tPred+pre\n+PredVBest+PredPreVBest\n")
        for ei in range(len(exelist)):
            f.write(exelistplot[ei] + "\t"+str(round(advantage[ei][imppre],2)) + "\t" + str(round(advantage[ei][impselection],2)) + "\t"
                    + str(round(advantage[ei][imppreselection],2))  + "\t"  +
                    str(round(advantage[ei][dipbase],2)) + "\t"
                    + str(round(advantage[ei][dippre],2)) + "\n")
        f.close()

    af = open("graphbest.data", "w")
    avg= [0.0,0.0]
    af.write("\tP_{bl}v/sO_{bl} \t P_{bl}v/sO_{bl}(GC)\n")
    for ei in range((len(allgraphs))):
        af.write(allgraphsplot[ei] +"\t"+str(round(avgGraphcomparetobestcase[ei][0],2))
                 + "\t" + str(round(avgGraphcomparetobestcase[ei][1],2)) + "\n")
        avg[0] += avgGraphcomparetobestcase[ei][0]
        avg[1] += avgGraphcomparetobestcase[ei][1]
    af.write("Avg\t" + str(round(avg[0]/len(allgraphs),2))
                 + "\t" + str(round(avg[1]/len(allgraphs),2)) + "\n")
    af.close()

    
    af = open("graphtime.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(allgraphs))):
        af.write(allgraphsplot[ei] + "\t" + str(round(avgBaseTimeGraphCase[ei][0],2)) + "\t"
                 + str(round(avgBaseTimeGraphCase[ei][1],2)) + "\t"
                + str(round(avgBaseTimeGraphCase[ei][2],2)) + "\n"  )
        avg[0] += avgBaseTimeGraphCase[ei][0]
        avg[1] += avgBaseTimeGraphCase[ei][1]
        avg[2] += avgBaseTimeGraphCase[ei][2]
    af.write("Avg\t" + str(round(avg[0]/len(allgraphs),2))
                 + "\t" + str(round(avg[1]/len(allgraphs),2))
             + "\t" + str(round(avg[2]/len(allgraphs),2))+ "\n")
        
    af.close()


    af = open("graphEnergy.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(allgraphs))):
        af.write(allgraphsplot[ei] + "\t" + str(round(avgBaseEnergyGraphCase[ei][0],2)) + "\t"
                 + str(round(avgBaseEnergyGraphCase[ei][1],2)) + "\t"
                 + str(round(avgBaseEnergyGraphCase[ei][2],2)) + "\n"  )
        avg[0] += avgBaseEnergyGraphCase[ei][0]
        avg[1] += avgBaseEnergyGraphCase[ei][1]
        avg[2] += avgBaseEnergyGraphCase[ei][2]
    af.write("Avg\t" + str(round(avg[0]/len(allgraphs),2))
                 + "\t" + str(round(avg[1]/len(allgraphs),2))
             + "\t" + str(round(avg[2]/len(allgraphs),2))+ "\n")
    af.close()


    sf.close()

    #overall gains
    
    #overall chunks grains
    af = open("overallgainschunks.data", "w")
    avg=[0.0,0.0]
    af.write("\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(allgraphs))):
        af.write(allgraphsplot[ei] + "\t" + str(round(avgchunks[ei][0],2)) + "\t"
                 + str(round(avgchunks[ei][1],2)) + "\n"  )
        avg[0] += avgchunks[ei][0]
        avg[1] += avgchunks[ei][1]
    af.write("Avg\t" + str(round(avg[0]/len(allgraphs),2))
                 + "\t" + str(round(avg[1]/len(allgraphs),2)) + "\n")
    af.close()


    #overallgains

    af = open("overallgainsgraph.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(allgraphs))):
        af.write(allgraphsplot[ei] + "\t" + str(round(avggraphs[ei][0],2)) + "\t"
                 + str(round(avggraphs[ei][1],2)) + "\t"
                 + str(round(avggraphs[ei][2],2)) + "\n"  )
        avg[0] += avggraphs[ei][0]
        avg[1] += avggraphs[ei][1]
        avg[2] += avggraphs[ei][2]
    af.write("Avg\t" + str(round(avg[0]/len(allgraphs),2))
                 + "\t" + str(round(avg[1]/len(allgraphs),2))
             + "\t" + str(round(avg[2]/len(allgraphs),2))+ "\n")
    af.close()

    # overall gains
    af = open("overallgainsalgo.data", "w")
    avg=[0.0,0.0,0.0]
    af.write("\tGC\tP_{bl}\t GC+P_{bl}\n")
    for ei in range((len(exelist))):
        af.write(exelistplot[ei] + "\t" + str(round(avgexes[ei][0],2)) + "\t"
                 + str(round(avgexes[ei][1],2)) + "\t"
                 + str(round(avgexes[ei][2],2)) + "\n"  )
        avg[0] += avgexes[ei][0]
        avg[1] += avgexes[ei][1]
        avg[2] += avgexes[ei][2]
    af.write("Avg\t" + str(round(avg[0]/len(exelist),2))
                 + "\t" + str(round(avg[1]/len(exelist),2))
             + "\t" + str(round(avg[2]/len(exelist),2))+ "\n")
    af.close()


def writeselectedselections(argv):
    sf = open("selections.tex", "w")
    sf.write("\n\n\\begin{table*}\n\\caption{The predicted and best configuration"+ 
             "for various algorithm-graph pairs. }\n\\label{tab:selections}\n "+
             "\\begin{tabular}{|p{0.75cm}|p{1cm}|p{1cm}|p{1cm}|p{1cm}| "+
             "p{1cm}|p{1cm}|p{1cm}|p{1cm}| p{1cm}|p{1cm}|p{1cm}|p{1cm}|} \n")
    sf.write("\\hline \n  & \multicolumn{4}{c|}{"+exelisttex[0]
             +"} & \multicolumn{4}{c|}{"+exelisttex[1]+"}"
             + "& \multicolumn{4}{c|}{"+exelisttex[2]+"} \\\\ \n \\hline \n " )
    sf.write("  & \multicolumn{2}{c|}{Base case} & \multicolumn{2}{c|}{Preprocessed}"+
             " & \multicolumn{2}{c|}{Base case} & \multicolumn{2}{c|}{Preprocessed} & "+
             "\multicolumn{2}{c|}{Base case} & \multicolumn{2}{c|}{Preprocessed}  \\\\ \n \\hline \n")
    sf.write("\n Graph & Pred & Best & Pred & Best & Pred & Best & Pred & Best  & Pred & Best & Pred & Best \\\\ \n \\hline \n")
    # add data
    with open('communitiesselectionswithbest.csv', 'r') as f:
            comdata = list(csv.reader(f, delimiter=','))
    with open('conductselectionswithbest.csv', 'r') as f:
            condata = list(csv.reader(f, delimiter=','))
    with open('pageRankselectionswithbest.csv', 'r') as f:
            pagedata = list(csv.reader(f, delimiter=','))

    # print condata
    # print pagedata
    # print comdata
    for i in range(len(allgraphs)):
        sf.write("\n "+ allgraphstex[i]+ " & " + comdata[i][0] + " & " +
                 comdata[i][1] + " & "  + comdata[i][2] + " & "  +
                 comdata[i][3] + " & "  + condata[i][0] + " & "  +
                 condata[i][1] + " & "  + condata[i][2] + " & "  +
                 condata[i][3] + " & "  + pagedata[i][0] + " & "  +
                 pagedata[i][1] + " & "  + pagedata[i][2] + " & "  +
                 pagedata[i][3] + "\\\\ \n")
    
    
    sf.write("\n \hline \n \\end{tabular} \n \\end{table*} \n")

        

            
if __name__ == '__main__':
    extractdata(sys.argv[1:])
    comparedata(sys.argv[1:])
    writeselectedselections(sys.argv[1:])
