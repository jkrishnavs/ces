## A) ABBREVIATIONS   
IMOP: IIT Madras OpenMP Framework, A source to source OpenMP compiler. See http://www.cse.iitm.ac.in/~amannoug/imop/ for more details on the project.  
CES: Compiler Enhanced Scheduling.  
$HOME: Refers to the home folder of this repository. (The current directory)  
$CES: refers to the CES project home folder. ($IMOP/ces)  
$STREXP: refers to the String Builder Expression Project ($HOME/StringExpression)  


## I) INTRODUCTION    
This project is part of the Compiler Enhanced Scheduling (CES) for OpenMP parallel programs
for Asymmetric Multicore Processors.
The compiler is written in Java with Eclipse IDE.  

NOTE: Some of the eclipse project files are not synced though you can create a new project in eclipse
and import the source code.  


The CES project consists of several mini projects:
i) Compiler Enhanced Scheduling: This project is a source to source translated which transforms the input OpenMP program 
optimized for execution time in Asymmetric Multicore Processors. Please refer to (i) in Publication sections for more details.  

ii) Classifier (CHOAMP): This project is used to find the optimal AMP hardware configuration (like number of big/ LITTLE cores
the frequencies of each core) based on a user selected cost function (execution time/ energy/edp) for a given input program.
Please refer to (ii) in Publication section for more details. The classifier project is compatible with the CES project and CES
project can be integrated into classifier.  


iii) GRAPH ALGORITHMS: This project is used to find the Optimal AMP hardware configuration t optimize the EDP consumption of the
input program. Please refer to (iii) in Publication section for more details.  

We can switch between these projects using command line arguments or by changing the system config file.
See $CES/SystemConfigFile.java for more details.  
These programs are tested in Odroid-XU3 board. The full discription of the test system is given below.  
------------------ TEST SYSTEM -----------------------  
Board: Odroid-XU3  
CPU: Samsung Exynos 5422  
OS: LUBUNTU 14.04/ LUBUNTU 16.04  
big cores: coreids 4-7 @ 2.0 GHz  
LITTLE cores: coreids 0-4 @ 1.4GHz  
gcc compiler: 4.9 (used for preprocessing also)  

NOTE: The IMOP compiler accepts ANSIC compiler. The ARM  preprocessed gcc is not compatible
with ANSIC c. We need to have a manual pass to remove such differences before feeding to the 
IMOP project.  

## II) BACKGROUND  
i) IMOP
IMOP (IIT Madras OpenMP Framework) is a source to source compiler for OpenMP. For more details
see http://www.cse.iitm.ac.in/~amannoug/imop/. We have remmoved IMOP source and we are using a jar of an 
older IMOP version. For the latest source code of IMOP: https://github.com/amannougrahiya/imop-compiler

**NEWS from IMOP developers:  ** pre-release version of IMOP is NOW AVAILABLE HERE: https://github.com/amannougrahiya/imop-compiler


**ii) Other Referred and Dependant Projects:**  

a) The Java Tree Builder: http://compilers.cs.ucla.edu/jtb/  
Used to create the parser for String builder and OpenMP parser in IMOP. 
We are packing the jtb jar along wth this project.
You can view the jtb license here:http://compilers.cs.ucla.edu/jtb/license.html

b) Java Scientific Library: https://www.ee.ucl.ac.uk/~mflanaga/java/  
We use JSL to learn the coefficients for different kernels for the classifier in CES project. 
We are packing the JSL jar along with this project.

c) The ODROID-XU3 energy monitor library: https://github.com/jkrishnavs/Energymonitorlibrary  
A C/C++ library to collect energy information in the ODROID-XU3 board.  

d) OpenMP Eigen Bench: https://bitbucket.org/jkrishnavs/openmp-eigenbench/src/master/   
The project generates OpenMP source codes to train for different intensities of
Program features to study the behaviour of big.LITTLE architecture for varying intensities
of parallel program features.
To see the base results and working of the OpenMP-Eigenbench refer the CHOAMP paper
(https://ieeexplore.ieee.org/document/8254393/).  

e) OpenMP Graph Algorithms: https://github.com/jkrishnavs/OpenMPGraphAlgorithms  
The project contains some common graph Algorithms written in OpenMP parallel programs.
It also contains the training programs and preprocessing input graphs.
for more details see Optimizing Graph Algorithms in Asymmetric Multicore Processors.   



## III) HOW TO    
The project takes the following commandline arguments.
Please refer to printhelpMenu and setparameters in $SRC/Main.java.  
Options:  
i) project Option: ces|classifier|graph  
ii) cost function option: time|energy|edp|pdp  
iii) optimizationOptions: hardware|dynamic|custom (Custom stands for CES)  (Specific to classifier project)  
iv) mainFunction: The project assumes full C file with "main" function. But we can still process sinppets of program
by providing new starting function name using this option. (Specific to Graph project)  
v) graphPropFile: TO input the input graph property file. (Specific to Graph project).  


  
**PYTHON SCRIPTS:**  
We have added the python scripts we have used to extract data, train and test datas.
These scripts are mostly for reference as we have used quite a few




## IV) TODOS/ LIMITATIONS / FUTURE WORKS  
This section we list out some of the TODOs and proposed future works of the project.

**TODOs:**  
CES Project:  

Classifier:  
i) Identify the while and dowhile iterator in    

Graph Project:  

**FUTURE WORKS:**  
CES Project:  

Classifier Project:  
i) Generate a best fit regression model to improve the efficiency of the predictor in the Classifier 


Graph Project:  


## V) LICENSE  
There are three projects in this repository  


ii) The CES Project is currently registered under MIT License. Please see
the license file $CES/License.md for more details.  

iii) The StringExpression project is not currently having any license.  


## VI) PUBLICATIONS  
Below are the publications based on this CES project. You can refer these to understand these
projects better.  
i) Compiler Enhanced Scheduling For Heterogeneous Multicore Processors, Jyothi
Krishna V S and Shankar Balachandran, at 2nd Energy Efficiency with Heterogeneous Computing
(EECHO) Workshop, Jan '16.  

ii) CHOAMP:Cost Based Hardware Optimization for Asymmetric Multicore Processors, Jyothi
Krishna V S, Shankar Balachandran, Rupesh Nasre.in IEEE Transactions on
Multi-Scale Computing Systems (TMSCS).  


iii) Optimizing Graph Algorithms in Asymmetric Multicore Processors, Jyothi Krishna V S
Rupesh Nasre in ACM SIGBED International Conference on Embedded Software (EMSOFT) 18.  


##VII) CONTACT  
For queries on CES and String Expression project you can use the following contacts.  

i) Jyothi Krishna V S  
mailid: jkrishna@cse.iitm.ac.in, jkrishnavs@gmail.com  

ii) Rupesh Nasre  
PACE LAB,
BSB 331A,
IIT Madras
TN-600036, India
mailid: rupesh@cse.iitm.ac.in  
