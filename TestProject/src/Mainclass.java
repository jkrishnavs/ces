import java.util.ArrayList;

public class Mainclass {
	
	public static void printlist(ArrayList<Integer> list){
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i));
		}
		System.out.println();
	}
	public static void mainstr(String []args) {
	        System.out.println("Hello World");
	        ArrayList<String> data = new ArrayList<String>();
	        data.add("hello ");
	        String str="Work "; 
	        data.add(str);
	        data.add("End");
	        data.add(data.indexOf(str),"World ");
	        
	        String finalstr = "";
	        
	        for(String dat: data) {
	            finalstr += dat;
	        }
	        
	        System.out.println(finalstr);
	 }

	
	public static void main(String [] args){
		ArrayList<Integer> data = new ArrayList<Integer>();
		
		Integer part = 3;
		data.add(0);
		data.add(1);
		data.add(2);
		data.add(part);
		data.add(4);
		data.add(5);
		data.add(6);
		data.add(7);
		data.add(8);
		data.add(9);
		
		data.add(data.indexOf(part),100101);
		
		printlist(data);
		
		ArrayList<Integer> sublist = new ArrayList<Integer>(data.subList(1, 4));
		printlist(sublist);
	    ArrayList<String> color_list = new ArrayList<String>();  
	    
	    // use add() method to add values in the list  
	    color_list.add("White");  
	    color_list.add("Black");  
	    color_list.add("Red");  
	     
	   // create an empty array sample with an initial capacity   
	    ArrayList<String> sample = new ArrayList<String>();  
	      
	   // use add() method to add values in the list   
	    sample.add("Green");   
	    sample.add("Red");   
	    sample.add("White");  
	    sample.add("Red1");
	      

	    System.out.println("Second List :"+ sample);  
	   // remove all elements from second list if it exists in first list  
	 //   sample.removeRange(1,4);  
	      
	    System.out.println("First List :"+ color_list);  
	    System.out.println("Second List :"+ sample);  
	  
		
		
		System.out.println("End");
		mainstr(args);
	}
}
